% Economics 232 Winter 2013
% Computational project 1 - Sample solution
% Computing the Cass-Koopmans model
% Mohammad Moravvej

clc
clear
close all

%% Initializing
% Setting model parameters
A       = 1     ;	% Total factor productivity (TFP)
alpha   = 0.28  ;   % Share of capital
rho     = 0.04  ;   % Discount factor
sigma   = 1     ;   % Coefficient of relative risk aversion (RRA)
delta   = 0.1   ;   % Rate of depreciation

% Defining the production function and its derivatives
F       = @(K) A .* K .^ alpha    ;
F_prime = @(K) alpha .* A .* K .^ (alpha - 1)   ;
F_doublePrime = @(K) alpha .* (alpha - 1) .* A .* K .^ (alpha - 2)   ;


%% Solving for the steady state
K_ss    = ( A .* alpha ./ (rho + delta) ) .^ ( 1 / (1-alpha) )  ;

% % If we could not solve K_ss explicitly we used the following instead:
% k0 = 1;
% Options = optimset('Display','iter');
% K_ss    =  fsolve(@(K) F_prime(K) - (rho + delta) , k0, Options)

C_ss    = F(K_ss) - delta .* K_ss   ;

%% Calculating the eigenvalues and eigenvectors

% The characteristic matrix
M       = [ rho     -1  ;   F_doublePrime(K_ss) .* C_ss ./ sigma    0 ] ;

% Soling for eigenvectors and eigenvalues
[V,R]   = eig(M) ;

% The negative characteristic root and its associated eigenvector
R1      = R(2,2)    ;
V1      = V(:,2)    ;

% The column vector for time
TV = linspace(0,50,51)'  ;

% initial values
K_0H = 1.5 * K_ss   ;
K_0L = 0.1 * K_ss   ;

% TCs can hold only if a2 = 0

% The initial condition for capital
a1_H    = (K_0H - K_ss) ./ V1(1)  ;
a1_L    = (K_0L - K_ss) ./ V1(1)  ;

% Solutions of the linearized system

% The transition path for capital
% z1 = K - K_ss
kV_H    = K_ss + a1_H .* V1(1) .* exp(R1 * TV)    ;
kV_L    = K_ss + a1_L .* V1(1) .* exp(R1 * TV)    ;

% The transition path for consumption
% z2 = C - C_ss
cV_H    = C_ss + a1_H .* V1(2) .* exp(R1 * TV)    ;
cV_L    = C_ss + a1_L .* V1(2) .* exp(R1 * TV)    ;

%% Plot the transition paths
plot(TV, kV_H, TV, cV_H, TV, kV_L, TV, cV_L)
title('The transition paths for consumption and capital') ;

xlabel('Year')  ;
ylabel('c(t) ,  k(t)')  ;

legend('k_H(t)','c_H(t)','k_L(t)','c_L(t)')   ;




%% Plot the phase diagram

figure
% The grid for capital
kX      =   linspace(K_0L,K_0H,100)'    ;

% The locus where kdot = 0cV_L(1)
kdot0X  =   F(kX) - delta .* kX         ;

% The locus where cdot = 0
cdot0X  =   linspace(cV_L(1),cV_H(1),100)'    ;

% plot locus for kdot = 0 and cdot=0
plot(kX, kdot0X,K_ss*ones(length(cdot0X),1),cdot0X)
title('The phase diagram') ;

xlabel('k(t)')  ;
ylabel('c(t)')  ;

legend('kdot=0','cdot=0','Location','NorthWest');

hold on

% plot the linearized saddle path at steady-state
plot(kV_H, cV_H,'k', kV_L, cV_L,'k')

%% The saddle path
c0_H = 1.316635841;
c0_L = 0.3258807984;
N =  11;
c11 = 0.3 ;
c12 = 0.5 ;
neib = 10^-4  ;
for j =1:5
for i=1:N
    c0_guess = linspace(c11, c12, N) ;
    c0_L1(i) = real(fsolve(@(c0) saddle( K_0L, c0 , K_ss) , c0_guess(i)));
    err(i,:) = saddle( K_0L, c0_L1(i) , K_ss);
 end
[errSizeL(j), indx] = min(abs(err)) ;
neib = neib / 10 ;
c11 = c0_L1(indx) - neib ;
c12 = c0_L1(indx) + neib ;
cl_a(j) = abs(c0_L1(i) - c0_L) ; 
% abs(c0_guess - c0_L1)>10^-3
end

c21 = 1.6 ;
c22 = 1 ;
neib = 10^-4  ;
for j =1:5
for i=1:N
    c0_guess = linspace(c21, c22, N) ;
    c0_H1(i) = real(fsolve(@(c0) saddle( K_0H, c0 , K_ss) , c0_guess(i)));
    err(i,:) = saddle( K_0H, c0_H1(i) , K_ss);
 end
[errSize(j), indx] = min(abs(err)) ;
neib = neib / 10 ;
c21 = c0_H1(indx) - neib ;
c22 = c0_H1(indx) + neib ;
ch_a(j) = abs(c0_H1(i) - c0_H) ; 
end



[T,Y_H] = ode23(@my_system,[0 50],[kV_H(1) c0_H]);
[T,Y_L] = ode23(@my_system,[0 50],[kV_L(1) c0_L]);



plot(Y_L(:,1),Y_L(:,2),'r',Y_H(:,1),Y_H(:,2),'r')

hold on

% plot the linearized saddle path at steady-state
plot(kV_H, cV_H,'k', kV_L, cV_L,'k')