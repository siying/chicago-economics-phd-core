function dy = my_system(t,y)
% This function defines the pair of ODE in Cass-Koopmans growth model
        dy = zeros(2,1);    % a column vector (k,c)
        A       = 1     ;	% Total factor productivity (TFP)
        alpha   = 0.28  ;   % Share of capital
        rho     = 0.04  ;   % Discount factor
        sigma   = 1     ;   % Coefficient of relative risk aversion (RRA)
        delta   = 0.1   ;   % Rate of depreciation
        F       = @(K) A .* K .^ alpha    ;
        F_prime = @(K) alpha .* A .* K .^ (alpha - 1)   ;
        dy(1) = F(y(1)) - delta*y(1) - y(2) ;
        dy(2) = y(2) / sigma * ( F_prime(y(1)) - delta - rho )   ;
end
