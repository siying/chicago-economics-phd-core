function e = saddle( k0, c0 , K_ss)

[T,Y] = ode23(@my_system,[0 50],[k0 c0]);
e  = Y(end,1) - K_ss;
end

