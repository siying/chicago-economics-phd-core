clear
disp('The program has begun.')
%Define Variables
sigma=4;
rho=.075/12;
A=1/12;
alpha=.3;
delta=.075/12;
pk=1;
N=500;
V0=zeros(N,1); V=zeros(N,1); %column vector
conv=10^-6;
omega=zeros(N);
dist=1;
iterations=0;
T=500; ktime=zeros(T,1); ctime=zeros(T,1); itime=zeros(T,1);


%Calculate Other Variables
beta=1/(1+rho);
kstar=(A*alpha/(pk*(rho+delta)))^(1/(1-alpha));
kmin=.99*kstar;
kmax=1.01*kstar;
kgrid=linspace(kmin,kmax,N);

%populate theta matrix
theta=zeros(N);
for i=1:N,
for j=1:N
theta(i,j)=(1/(1-sigma))*(A*kgrid(i)^alpha+pk*(1-delta)*kgrid(i)-pk*kgrid(j))^(1-sigma);
end
end

%Specify starting conditions
for i=1:N
V0(i)=((A*kgrid(i)^alpha-pk*delta*kgrid(i))^(1-sigma))/((1-beta)*(1-sigma));
end
Vp=V0;
k0=kmin;


%Find V
disp('Variables are set up, ready to find V.')
while dist > conv
    V=Vp;
    %Generate omega matrix   
    for i=1:N
    for j=1:N
        omega(i,j)=theta(i,j)+beta*V(j);
    end
    end
    [Vp,J]=max(omega,[],2);  
    dist=max(abs(Vp-V));
    iterations=1+iterations;
    if mod(iterations,10)==0
        iterations
		dist
    end
end
disp('I have found V!')
Vstar=Vp;
iterations

disp('Finding policy functions.')
%Finding Policy Functions
kpol=zeros(N,1); ipol=zeros(N,1); cpol=zeros(N,1);
for i=1:N
    kpol(i)=kgrid(J(i));
    ipol(i)=kpol(i)-(1-delta)*kgrid(i);
    cpol(i)=A*kgrid(i)^alpha+pk*(1-delta)*kgrid(i)-pk*kpol(i);
end
disp('Discrete policy functions found.')
disp('Finding path of consumption, capital, and investment over time.')
   
ktime(1)=interp1(kgrid,kpol,k0);
itime(1)=interp1(kgrid,ipol,k0);
ctime(1)=interp1(kgrid,cpol,k0);

for i=2:T
    ktime(i)=interp1(kgrid,kpol,ktime(i-1));
    itime(i)=interp1(kgrid,ipol,ktime(i-1));
    ctime(i)=interp1(kgrid,cpol,ktime(i-1));
end
disp('Paths found.')

%Problem Specific Code (iv)
% disp('Problem Specific Calculations (iv):')
% It=(interp1(kgrid,ipol,kmax)-interp1(kgrid,ipol,kmin))/(kmax-kmin);
% xi=It/delta;
% disp('I_tilde(K*)=')
% disp(It)
% disp('xi(K*)=')
% disp(xi)

%Make a Picture!
kk=kmin:.0001:kmax;
plot(kk,interp1(kgrid,ipol,kk));
xlabel('Capital Stock')
ylabel('Optimal Investment')
xlim([kmin kmax])
exportfig(gcf,'pic.eps', 'width',4, 'fontmode','fixed', 'fontsize',8);

%Problem Specific Code (iv)
disp('Problem Specific Calculations (iv):')
lambda=rho/2*(1-(1+4*interp1(kgrid,cpol,kstar)/(pk*kstar)*(1-alpha)/sigma*(rho+delta)/rho^2)^.5);
tbar=log(.5)/lambda; tbar=tbar/12
i=1;
while ktime(i)-kstar <=.5*(k0-kstar);
i=i+1;
end;
that=(i-1)/12
save test tbar
sendmail('jonathan.hall@uchicago.edu','It is finished.','Yeah! it is done and the email worked!');
disp('The program has finished.')