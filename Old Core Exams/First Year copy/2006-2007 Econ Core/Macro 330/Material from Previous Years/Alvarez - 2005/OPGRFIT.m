function [V0,k,V,K,I,C,Kt,It,Ct,Yt] = OPGRFIT(A,a,d,pk,r,s,epsi,p,N,T,freq)
% Solves an infinite-horizon discrete-time optimal growth model using value
% function iteration.
% INPUTS:
% A    = technology parameter.
% a    = capital's ahare in output.
% r    = discount rate.
% d    = depreciation rate.
% pk   = price of capital.
% epsi = convergence criterion.
% 1-p  = lower bound for grid (as a & of the SS stock of capital).
% N    = number of grid points.
% T    = number of time periods.
% freq = frequency (annual = 1; monthly = 12)
% OUTPUTS:
% V0    = initial guess for the policy function (zero net saving).
% k     = grid of capital stocks.
% V     = value function.
% K,I,C = next-period's capital, investment and consumption policy functions.
% Kt,It,Ct,Yt = capital, invesmtent, consumption and output time series.
% Written by Luis Fernando Mej�a, U of C, December 2004.
%                               Modified: November 2005.

t0 = clock;
A = A/freq;  d = d/freq;  r = r/freq;
b = 1/(1 + r); % Discount factor

% Steady state values
kstar = (A*a/(pk*(r + d)))^(1/(1 - a));
istar = d*kstar;
ystar = f(kstar);
cstar = ystar - pk*istar;

disp('==================================')
disp('Steady state quantities and ratios')
disp('----------------------------------')
disp(['Consumption             : ' , num2str(cstar,3)])
disp(['Output                  : ' , num2str(ystar,3)])
disp(['Investment              : ' , num2str(pk*istar,3)])
disp(['Capital                 : ' , num2str(pk*kstar,3)])
disp(['Consumption-output ratio: ' , num2str(cstar/ystar,3)])
disp(['Investment-output ratio : ' , num2str(pk*istar/ystar,3)])
disp(['Capital-output ratio    : ' , num2str(pk*kstar/ystar,3)])
disp('----------------------------------')

% Grid over capital stock
k = linspace(kstar*(1 - p),kstar*(1 + p),N);

% Initial value function (under zero net savings)
V0 = u(f(k) - pk*d*k,s)/(1 - b);

% Value function iteration algorithm
dist = 1;
while dist > epsi
    % Maximization step
    for i = 1:N
        v(:,i) = u(f(k) + pk*(1 - d)*k - pk*k(i),s) + b*V0(i);
    end
    [V,j] = max(v');
    dist  = norm(V - V0,inf);
    V0    = V;
end
V0 = u(f(k) - pk*d*k,s)/(1 - b); % Initial value function

% Policy functions
K  = k(j);          % Next period's capital policy function
I  = K - (1 - d)*k; % Investment policy function
C  = f(k) - pk*I;   % Consumption policy function

% Slopes of policy functions
Cpn = (C(end) - C(1))/(k(end) - k(1));
Cpa = r*pk/2*(1 + sqrt(1 + 4*(1 - a)/s*(cstar/(pk*kstar))*(r + d)/r^2));
Ipn = (I(end) - I(1))/(k(end) - k(1));

disp('======================================')
disp('      Slopes of policy functions')
disp('--------------------------------------')
disp(['Numerical slope of C(k*)     : ' , num2str(Cpn,3)])
disp(['Analytical slope of C(k*)    : ' , num2str(Cpa,3)])
disp(['Numerical slope of I(k*)     : ' , num2str(Ipn,3)])
disp(['Numerical elasticity of I(k*): ' , num2str(Ipn/d,3)])
disp('--------------------------------------')

% Time series
Kt(1) = k(1);
for t = 1:T
    Kt(t+1) = interp1(k,K,Kt(t));      % Time series for capital
    It(t)   = Kt(t+1) - (1 - d)*Kt(t); % Time series for investment
    Ct(t)   = f(Kt(t)) - pk*It(t);     % Time series for consumption
    Yt(t)   = f(Kt(t));                % Time series for output
end
Kt = Kt(1:end-1);

% Speed of convergence
Hln = find(Kt >= (Kt(1) + kstar)/2,1);
Hla = -log(2)/(r/2*(1 - sqrt(1 + 4*(1 - a)/s*(cstar/(pk*kstar))*(r + d)/r^2)));

disp('==========================')
disp('   Speed of convergence')
disp('--------------------------')
disp(['Numerical half life : ' , num2str(Hln/freq,3)])
disp(['Analytical half life: ' , num2str(Hla/freq,3)])
disp('--------------------------')

% Auxiliary functions
    % Utility function
    function out = u(c,s)
        c = (c>0).*c + (c<0)*10^-10;
        if s ~= 1
            out = (c.^(1- s))/(1 - s);
        else
            out = log(c);
        end
    end
    % Production function
    function out = f(k)
        out = A * k.^a;
    end
disp(['Total elapsed time was ' , num2str(etime(clock,t0),3), ' seconds.'])
end