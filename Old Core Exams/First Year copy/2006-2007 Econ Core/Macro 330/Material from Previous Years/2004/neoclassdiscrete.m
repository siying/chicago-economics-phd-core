% Solving the neoclassical growth model in discrete time using discretization (or brute force).
% This code is used to solve problem set 7
% Written by Constantino Hevia. November 2004.

clear all;
close all;
clc;
t0=clock;

% PARAMETERS:
% ==========

len = 12; % number of time period in a year, len=12, monthly model, len =1 yearly model

% Preferences:
sigma = 1;				%inverse of the elasticity of intertemporal substitution
rho = 0.075/len;			%discount rate
beta	= 1/(1+rho);	%discount factor

% Technology:
A		= 1/len;			%parameter in the production function
alpha	= 0.2;			%share of capital in the production function
delta	= 0.075/len; 	%depreciation rate
pk		= 2;				%relative price of capital in terms of consumption



% Size of the capital grid
N		= 2000;
% Remember that computational time increases exponentially as you increase N. 
% Very big N's (approx N > 3500 or 4000) CANNOT be handled in matlab 

% Number of periods o run the simulation
T		= 50*len;


disp(' ')
disp('	Parameters')
disp('	==========')
disp(' ')
disp([' 	Number of model periods in a year = ', num2str(len)])
disp(' ')
disp('	Preference parameters:')
disp(['	sigma                = ',num2str(sigma)])
disp(['	beta (model periods) = ',num2str(beta)])
disp(['	rho (model periods)  = ',num2str(rho)])
disp(['	rho (yearly)         = ',num2str(rho*len)])
disp(' ')
disp(' 	Technology parameters:')
disp(['	A (model periods)    = ',num2str(A)])
disp(['	A (yearly)           = ',num2str(A*len)])
disp(['	alpha	             = ',num2str(alpha)])
disp(['	delta (model periods)= ',num2str(delta)])
disp(['	delta (yearly)       = ',num2str(delta*len)])
disp(['	pk                   = ',num2str(pk)])
disp(' ')
disp(['	N (number of grid points)            = ',num2str(N)])
disp(' ')
disp(['	T (number of periods for simulation) = ',num2str(T)])
disp(' ')




% Steady state level of capital:
kstar	= [A*alpha/(pk*(rho+delta))]^(1/(1-alpha));

% Initial level of capital (it is better to put it as a proportion of the steady state level of capital)
perc	= .01;
k0	   = (1-perc)*kstar;  

%--------------------------------------------------------------------------------------

% GRID OF CAPITAL
% ===============
if k0==kstar				
   kmin = (1-perc)*kstar;
   kmax = (1+perc)*kstar;
elseif k0<kstar
   kmin = k0;
   kmax = (1+perc)*kstar;
else
   kmin = (1-perc)*kstar;
   kmax = k0;
end
kgrid = linspace(kmin,kmax,N)';
%--------------------------------------------------------------------------------------

% STEADY STATE VALUES
% ===================

istar = pk*delta*kstar*len;
ystar = A*kstar^alpha*len;
cstar = ystar - istar;

disp(' ')
disp(['	Grid of capital [kmin,kmax] 		= [',num2str(kmin),',',num2str(kmax),']'])
disp(' ')
disp(['	Initial stock of capital		= ',num2str(k0)]);
disp(' ')
disp(' ')
disp('	--------------------------------------------')
disp('	STEADY STATE VALUES AND QUANTITIES ')
disp(' ')
disp(['	Steady state stock of capital		= ',num2str(kstar)]);
disp(' ')
disp(['	Value of steady state stock of capital	= ',num2str(pk*kstar)]);
disp(' ')
disp(['	Steady state GDP (yearly)		= ',num2str(ystar)]);
disp(' ')
disp(['	Steady state consumption (yearly)	= ',num2str(cstar)]);
disp(' ')
disp(['	Value of capital over (yearly) GDP 	= ',num2str(pk*kstar/ystar,3)]);
disp(' ')
disp(['	Value of investment over GDP		= ',num2str(istar/ystar,3)]);
disp(' ')
disp('	--------------------------------------------')
disp(' ')




%--------------------------------------------------------------------------------------

% INITIAL GUESS
% ============
pf0 = kgrid;						%initial policy: maintain stock of capital unchanged
% Initial guess
if sigma==1
   V0=log(A*kgrid.^alpha+pk*(1-delta)*kgrid-pk*pf0)/(1-beta);
else
   V0 = [(1/(1-sigma))*(A*kgrid.^alpha+pk*(1-delta)*kgrid-pk*pf0).^(1-sigma)]/(1-beta);
end


%--------------------------------------------------------------------------------------

% PARAMETERS AND VARIABLES FOR VALUE FUNCTION ITERATION
% =====================================================

maxiter 	= 500;			% maximum number of iterations
conv		= 10^-8;			% convergence criterium
dist		= 1;				% initial distance
cons		= zeros(N,N);	
theta		= zeros(N,N);
omega		= zeros(N,N);	
V 			= V0;				% initialize value function

% Construct theta (doing this with one loop instead of two saves a lot of computational time)

for s=1:N
   cons(:,s) = A*kgrid.^alpha+pk*(1-delta)*kgrid - pk*kgrid(s);
end

cons = (cons>0).*cons + (cons<0)*10^-10;		%if c(i,j)<0, put c(i,j) almost zero
if sigma==1
   theta=log(cons);
else
   theta=cons.^(1-sigma)/(1-sigma);
end

clear cons	%clear useless variable

%--------------------------------------------------------------------------------------

% VALUE FUNCTION ITERATION
% ========================
disp('Iterating on the Value Function. Please wait...')
for iter=1:maxiter
   if iter==maxiter
      disp(['Value function iteration didn''t converge in ',num2str(maxiter),' iterations'])
      disp('Increase maxiter')
   end
      if dist<=conv		%check convergence
      break
   end
   for s=1:N
      omega(:,s) = theta(:,s) + beta*V(s);	 % update omega
   end


   
   [Vp,index]	=	max(omega,[],2);	% maximize and store index of policy function in "index"
   dist	= max(abs(Vp-V));				% compute the distance between the two value functions		
   V		= Vp;								% update the value function
end

%--------------------------------------------------------------------------------------

% POLICY FUNCTIONS
% ================
for i=1:N
   kpol(i) 	= kgrid(index(i));														% capital pol func
   cpol(i) 	= A*kgrid(i)^alpha+pk*(1-delta)*kgrid(i) - pk*kpol(i);		% consumption pol func
   ipol(i)	= kpol(i) - (1-delta)*kgrid(i);  									% investment pol func
end

% Slope of continuous time consumption policy function at SS

% cp solves c'(c'-rho)=(rho+delta)(cstar/pk*kstar)*(epsilon/sigma)
epsi = 1-alpha;
coverpkk = ( rho + delta*(1-alpha) )/alpha;

cp = (rho*pk*0.5)*(1 + sqrt(1 + 4 * (epsi/sigma)*(rho+delta)* coverpkk/(rho^2) ));

% Numerical slope of the consumption policy function

cpnum = (cpol(N)-cpol(1))/(kgrid(N)-kgrid(1));

disp(' ')
disp(['Continuous time slope of the saddle path dc(k*)/dk: ',num2str(cp,2)] )
disp(' ')
disp(['Numerical slope of C(K), discrete time model: ',num2str(cpnum,2)] )
disp(' ')

% Numerical slope of the discrete time investment policy function

ipnum = (ipol(N)-ipol(1))/(kgrid(N)-kgrid(1));
elainum = ipnum/delta;
disp(' ')
disp(['Numerical slope of I(K), discrete time model: ',num2str(ipnum,2)] )
disp(' ')
disp(['Numerical elasticity I(K), discrete time model: ',num2str(elainum,2)] )
disp(' ')



% FIGURES
% Plot value function and policy functions
figure(1)
plot(kgrid,V)
title('VALUE FUNCTION')
xlabel('k')
ylabel('V(k)')

figure(2)
subplot(2,2,1)
plot(kgrid,kpol)
title('CAPITAL POLICY FUNCTION')
xlabel('k')
ylabel('k''(k)')

subplot(2,2,2)
plot(kgrid,cpol)
title('CONSUMPTION POLICY FUNCTION')
xlabel('k')
ylabel('c(k)')

subplot(2,2,3)
plot(kgrid,ipol)
title('INVESTMENT POLICY FUNCTION')
xlabel('k')
ylabel('I(k)')

%--------------------------------------------------------------------------------------

% TIME SERIES 
% ===========
% We will construct the time series interpolating the policy functions, starting from k0

% Interpolated series:
k_t = zeros(1,T+1);
c_t = zeros(1,T);
i_t = zeros(1,T);
y_t = zeros(1,T);
k_t(1) = k0;

for i=1:T
   k_t(i+1) = interp1(kgrid,kpol,k_t(i),'linear');
   c_t(i)	= interp1(kgrid,cpol,k_t(i),'linear');
   i_t(i)	= interp1(kgrid,ipol,k_t(i),'linear');
   y_t(i)	= A*k_t(i)^alpha; % time path of gdp
end
k_t = k_t(1:end-1);

% Growth rates
gk_t = k_t(2:end)./k_t(1:end-1)-1;
gc_t = c_t(2:end)./c_t(1:end-1)-1;
gi_t = i_t(2:end)./i_t(1:end-1)-1;
gy_t = y_t(2:end)./y_t(1:end-1)-1;


%Plot time series
figure(3)
subplot(2,2,1)
plot([1:T],k_t)
title(['CAPITAL. K_0 = ',num2str(k0,3)])
xlabel('t')
ylabel('K_t')
subplot(2,2,2)
plot([1:T],c_t)
title(['CONSUMPTION. K_0 = ',num2str(k0,3)])
xlabel('t')
ylabel('c_t')
subplot(2,2,3)
plot([1:T],pk*i_t./y_t)
title(['VALUE OF GROSS INVESTMENT OVER GDP. K_0 = ',num2str(k0,3)])
xlabel('t')
ylabel('pk*i_t/y_t')
subplot(2,2,4)
plot([1:T],y_t)
title(['GDP. K_0 = ',num2str(k0,3)])
xlabel('t')
ylabel('y_t')


% -------------------------------------------
% Speed of Convergence and half lives

% Continuous time model: lambda solves lambda(lambda-rho)=(cstar/pk*kstar)*(epsilon/sigma)
lambda = (0.5*rho)*(1 - sqrt(1 + 4 * (epsi/sigma)* (rho+delta)*coverpkk/(rho^2) ));
% Continuous time half life tbar
tbar = (log(1/2)/lambda)/len;

% Numerical half-life:

ktarget = kstar + .5*(kgrid(1)-kstar);
if k_t(T) >= ktarget
	i=1;
	while i <= T
   	if k_t(i) >= ktarget
      	ttilde = i;
      	i=T;
      end
      i = i+1;
   end
   ttilde = ttilde/len; 
else
   disp('simulation length T too small to get to 1/2 of steady state')
end


disp(' ')
disp(['Half-life for Continuous time model: ',num2str(tbar,3)] )
disp(' ')
disp(['Numerical half-life discrete time model: ',num2str(ttilde,3)] )
disp(' ')

% -------------------------------------------

disp(['Total elapsed time was: ' , num2str(etime(clock,t0)), ' seconds'])
