%% Numerical Programming of the Neoclassical Growth Model
clear all; close all; clc;

% Parameters
A = 1; alp = 0.3; del = 0.075; pk = 2; % Technology
sig = 2; rho  = 0.075; % Preferences
eps = 1e-6; p = 0.01; N = 500; T = N/2; freq = 1; % Other

% Value function iteration algorighm
[v0,k,V,K,I,C,Kt,It,Ct,Yt] = OPGRFIT(A,alp,del,pk,rho,sig,eps,p,N,T,freq);

% Plots
figure
subplot(2,2,1)
plot(k, V, 'Linewidth', 1.3), xlabel('Capital stock, {\itk}'),
ylabel('{\itV(k)}'), title('Optimal value function'), axis tight,
subplot(2,2,2)
plot(k, C, 'Linewidth', 1.3), xlabel('Capital stock, {\itk}'),
ylabel('{\itC(k)}'), title('Optimal consumption policy function'), axis tight,
subplot(2,2,3)
plot(k, I, 'Linewidth', 1.3), xlabel('Capital stock, {\itk}'),
ylabel('{\itI(k)}'), title('Optimal investment policy function'), axis tight,
subplot(2,2,4)
plot(k, k, ':', k, K, 'Linewidth', 1.3), xlabel('Capital stock, {\itk}'),
ylabel('{\itK\prime(k)}'), title('Optimal capital policy function'), axis tight,
print -depsc2 pset5_01.eps

t = (1:1:T)/freq;
figure
subplot(2,2,1)
plot(t, Kt, 'Linewidth', 1.3), xlabel('Time (in years)'),
ylabel('{\itK_t}'), title('Capital stock'),
subplot(2,2,2)
plot(t, Ct, 'Linewidth', 1.3), xlabel('Time (in years)'),
ylabel('{\itC_t}'), title('Consumption'),
subplot(2,2,3)
plot(t, It, 'Linewidth', 1.3), xlabel('Time (in years)'),
ylabel('{\itI_t}'), title('Invesment'),
subplot(2,2,4)
plot(t, Yt, 'Linewidth', 1.3), xlabel('Time (in years)'),
ylabel('{\itY_t}'), title('Output'),    
print -depsc2 pset5_02.eps