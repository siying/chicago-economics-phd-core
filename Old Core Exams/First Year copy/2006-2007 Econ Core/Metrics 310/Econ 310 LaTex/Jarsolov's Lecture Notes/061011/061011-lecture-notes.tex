\begin{example}
(Two sided test of the mean of a normal distribution) Assume we have iid
random variables $X_{1},\ldots ,X_{n}$, with $X_{i}\sim N(\mu ,\sigma ^{2})$%
. Both $\mu $ and $\sigma ^{2}$ are unknown parameters.

We construct the sample mean $\overline{X}=\frac{1}{n}\sum_{i=1}^{n}X_{i}$.
Then $\overline{X}\sim N\left( \mu ,\sigma ^{2}/n\right) $. Further,
construct sample standard deviation $s^{2}=\frac{1}{n-1}\sum_{i=1}^{n}(X_{i}-%
\overline{X})^{2}$. We will see later that $\frac{(n-1)s^{2}}{\sigma ^{2}}%
\sim \chi _{n-1}^{2}$.

Our two sided hypothesis is%
\begin{equation*}
H_{0}:\mu =\mu _{0}\qquad \text{vs.}\qquad H_{1}:\mu \neq \mu _{0}
\end{equation*}%
A suitable test statistic is then%
\begin{equation*}
T=\frac{\sqrt{n}(\overline{X}-\mu _{0})}{s}
\end{equation*}%
since%
\begin{equation*}
T=\frac{\overset{\sim N(0,1)}{\overbrace{\sqrt{n}(\overline{X}-\mu
_{0})/\sigma }}}{\sqrt{\underset{\sim \chi _{n-1}^{2}}{\underbrace{\left[
(n-1)s^{2}/\sigma ^{2}\right] }}/(n-1)}}\sim t_{n-1}
\end{equation*}%
under $H_{0}$. We then reject the null hypothesis if $\left\vert
T\right\vert >t_{n-1,1-\alpha /2}$, where $t_{n-1,1-\alpha /2}$ is the $%
1-\alpha /2$ quantile of the $t_{n-1}$ distribution.
\end{example}

\begin{example}
(One sided test of the mean of a normal distribution) Assume the same setup
but now%
\begin{equation*}
H_{0}:\mu \leq \mu _{0}\qquad \text{vs.}\qquad H_{1}:\mu >\mu _{0}.
\end{equation*}%
The test procedure is the same, we only reject $H_{0}$ if $\left\vert
T\right\vert >t_{n-1,1-\alpha }$.
\end{example}

\section{(Section \#4) OLS specification, estimation, and small sample
results}\label{sec:4}

We will work with the model%
\begin{equation*}
Y_{i}=X_{i1}\beta _{1}+\ldots +\beta _{k}X_{ik}+\varepsilon _{i}
\end{equation*}%
or, in matrix form%
\begin{equation*}
\underset{n\times 1}{Y}=\underset{n\times k}{X}\cdot \underset{k\times 1}{%
\beta }+\underset{n\times 1}{\varepsilon }
\end{equation*}
on a sample of $n$ observations of $(X_{i1},\ldots ,X_{ik},Y_{i})$. The
variables $X_{i1},\ldots ,X_{ik}$ are called the independent variables, or
explanatory variable, or regressor. The variable $Y_{i}$ is called the
dependent variable, or explained variable, or regressand. $\beta _{1}$, $%
\beta _{2}$ are the regression coefficients, and $\varepsilon _{i}$ is the
error term, or residual.

\begin{figure}[!t]
\begin{center}
\includegraphics[width=10cm]{061011/images/S4-regression.eps}
\vspace{-2mm}
\caption{A simple regression model.}\label{fig:S4-regression}
\end{center}
\vspace{-3mm}
\end{figure}

\begin{example}
We can have a simple model of the form%
\begin{equation*}
Y_{i}=\beta _{1}+\beta _{2}X_{i}+\varepsilon _{i}
\end{equation*}%
where $X_{i}$ is the years of schooling, and $Y_{i}$ is the income. The
model is depicted in Figure \ref{fig:S4-regression}. Since we have only a
subsample of the data available, the sample regression function which we
attempt to estimated from the data will most likely differ from the (true)
population regression function. Our task is not only to estimate the
parameters in the sample, but also to try to infer how much this sample
coefficients may differ from the true population coefficients.
\end{example}

In order to be able to accomplish this task, we may will have to state
certain assumption on our model.

\subsection{Assumptions on the OLS\ model}

\begin{center}
\begin{tabular}{lll}
(A0) & $Y=X\beta +\varepsilon $ & linear functional form \\ 
(A1) & $rank(X)=k$ & identification assumption \\ 
(A2) & $E\left( \varepsilon \mid X\right) =0$ & orthogonality \\ 
(A3) & $E\left( \varepsilon \varepsilon ^{\prime }\mid X\varepsilon \right)
=\sigma ^{2}I$ & homoskedasticity, no serial correlation \\ 
(A2') & $\varepsilon \mid X\sim N(0,\sigma ^{2}I)$ & normality%
\end{tabular}
\end{center}

\subsubsection{Linear functional form}

Linear functional form means that the model is linear in its regression
coefficients. Imagine a set of data where $W_{i}$ is the variable to be
explained and $Z_{i}$ is an $L\times 1$ vector of explanatory variables.
Write the relationship in the form%
\begin{equation*}
\underset{Y_{i}}{\underbrace{g(W_{i})}}~=~\underset{X_{i}}{\underbrace{\left[
f_{1}(Z_{i}),\ldots ,f_{k}(Z_{i})\right] }}\cdot \left[ \beta _{1},\ldots
,\beta _{k}\right] ^{\prime }+\varepsilon _{i}.
\end{equation*}%
Then we still have a linear model. Examples of a linear model (linear in the
regression coefficients) is the quadratic model%
\begin{equation*}
W_{i}=\beta _{1}+\beta _{2}Z_{i}+\beta _{3}Z_{i}^{2}+\varepsilon _{i}
\end{equation*}%
or the loglinear model%
\begin{equation*}
W_{i}=\exp (\beta _{0})\cdot \prod_{j=1}^{k}Z_{ij}^{\beta _{j}}\cdot \exp
(\varepsilon _{i}).
\end{equation*}

\subsection{Identification}

If $rank(X)<k$, then there $\exists \gamma \neq 0:X\gamma =0$. But then we
can write the model as $Y=X(\beta +c\gamma )+\varepsilon $ for each $c\in 
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
$.\ Thus there exists an infinite number of models or parameter vectors
which are consistent with the data, and the true model cannot be identified.

\subsubsection{Orthogonality}

The orthogonality assumption is a big concern for economists, since its
violation can lead to seriously biased results. However, we will not discuss
it in this course.

\subsubsection{Homoskedasticity}

Homoskedasticity means $\forall i:E(\varepsilon _{i}^{2}\mid X)=\sigma ^{2}$%
. If it is not satisfied, we want to put more weight on observations with
less noise.

\subsubsection{No serial correlation}

No serial correlation means $\forall i\neq j:E(\varepsilon _{i}\varepsilon
_{j}\mid X)=0$. If it is not satisfied, we want to know whether there is
some specific form for the serial correlation of the error term, e.g. $%
\varepsilon _{t}=\rho \varepsilon _{t-1}+u_{t}$ with $\left\vert \rho
\right\vert <1$ and $u_{t}$ iid. Both homoskedasticity and serial
correlation are relatively easy to deal with as soon as we know their
characteristics.

\subsection{Derivation and interpretation of the OLS\ estimator}

We obtain the OLS estimator $\hat{\beta}_{OLS}$ by minimizing the sum of
squared residuals%
\begin{equation*}
S_{n}(\beta )=\sum \varepsilon _{i}^{2}=(Y-X\beta )^{\prime }(Y-X\beta
)=Y^{\prime }Y-2(X^{\prime }Y)^{\prime }\beta +\beta ^{\prime }(X^{\prime
}X)\beta 
\end{equation*}%
The corresponding first order condition is

\begin{equation*}
\frac{\partial s_{n}(\beta )}{\partial \beta }=-2(X^{\prime }Y)+2(X^{\prime
}X)\beta =0
\end{equation*}%
where we applied the well known formulas from the matrix algebra handout:%
\begin{equation*}
\frac{\partial z^{\prime }b}{\partial b}=z\qquad \text{and\qquad }\frac{%
\partial b^{\prime }Ab}{\partial b}=(A+A^{\prime })b=2Ab\text{ (for a
symmetric }A\text{)}
\end{equation*}%
Since we know that $(X^{\prime }X)$ is invertible thanks to $rank(X)=k$, we
obtain%
\begin{equation*}
\hat{\beta}_{OLS}=(X^{\prime }X)^{-1}X^{\prime }Y.
\end{equation*}%
Technically, we also need to check the second order conditions:%
\begin{equation*}
\frac{\partial ^{2}s_{n}(\beta )}{\partial \beta ^{\prime }\partial \beta }%
=2X^{\prime }X
\end{equation*}%
which is positive definite, as we know from MAHO. Thus $\hat{\beta}_{OLS}$
is the solution of the minimization problem.

\begin{remark}
We can also give the $\hat{\beta}_{OLS}$ estimator an intuitive
\textquotedblleft MoM\textquotedblright\ interpretation. Assume that the
data is iid and that the $k\times k$ matrix $E\left[ X_{i}X_{i}^{\prime }%
\right] $ is invertible. Next observe that%
\begin{equation*}
E\left[ X_{i}\varepsilon _{i}\right] =E\left[ E(X_{i}\varepsilon _{i}\mid
X_{i})\right] =E\left[ X_{i}\cdot E(\varepsilon _{i}\mid X_{i})\right] =0
\end{equation*}%
Thus%
\begin{eqnarray*}
E[X_{i}Y_{i}] &=&E\left[ X_{i}\left( X_{i}^{\prime }\beta +\varepsilon
_{i}\right) \right] =E\left[ X_{i}X_{i}^{\prime }\beta +X_{i}\varepsilon _{i}%
\right] =E\left[ X_{i}X_{i}^{\prime }\beta \right] +E[X_{i}\varepsilon
_{i}]=E\left[ X_{i}X_{i}^{\prime }\beta \right]  \\
\Longrightarrow \beta  &=&\left( E\left[ X_{i}X_{i}^{\prime }\right] \right)
^{-1}E\left[ X_{i}Y_{i}\right] 
\end{eqnarray*}%
But our OLS estimator just replaces these expected values by sample
averages, since%
\begin{equation*}
\hat{\beta}_{OLS}=(X^{\prime }X)^{-1}X^{\prime }Y=\left( \sum
X_{i}X_{i}^{\prime }\right) ^{-1}\left( \sum X_{i}Y_{i}\right) =\left( \frac{%
1}{n}\sum X_{i}X_{i}^{\prime }\right) ^{-1}\left( \frac{1}{n}\sum
X_{i}Y_{i}\right) .
\end{equation*}
\end{remark}

\begin{remark}
OLS\ decomposes the data $Y$ into two orthogonal components. Define $\hat{Y}%
=X\hat{\beta}$ as the linear prediction of $Y$ given $\hat{\beta}$, and the $%
\hat{\varepsilon}=Y-\hat{Y}$ as the predicted residuals. Observe that%
\begin{equation*}
\hat{Y}=X(X^{\prime }X)^{-1}X^{^{\prime }}Y=PY\qquad \text{and}\qquad \hat{%
\varepsilon}=(I-P)Y\equiv QY.
\end{equation*}%
We known from the matrix algebra handout that $P$ and $Q$ are idempotent and
symmetric projection matrices such that%
\begin{equation*}
PX=X\qquad QX=0\qquad PQ=0.
\end{equation*}%
Then%
\begin{equation*}
Y=\hat{Y}+\hat{\varepsilon}=X\hat{\beta}+\hat{\varepsilon}%
=PY+QY=PY+Q\varepsilon 
\end{equation*}%
where the last follows from the fact that $QY=Q\varepsilon $. Thus the two
orthogonal components are

\begin{itemize}
\item[-] a $k$-dimensional subspace of $%
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
^{n}$ spanned by the columns of $X$ ($\hat{Y}=X\hat{\beta}=PY$);

\item[-] an orthogonal component to this subspace ($\hat{\varepsilon}=QY$).
\end{itemize}

The OL\ S estimator thus uses $E(\varepsilon _{i}X_{i})=0$ and translates it
into the geometric orthogonality of $X$ and $\hat{\varepsilon}$ ($X^{\prime }%
\hat{\varepsilon}=0$).
\end{remark}

\subsection{Partitioned regression}

\begin{theorem}
\label{thm:S4T1}(T1) Take the linear model $Y=X\beta +\varepsilon $ and
partition the regressors $X=\left[ \underset{n\times k_{1}}{X_{1}}~\underset{%
n\times k_{2}}{X_{2}}\right] $, so that $Y=X_{1}\beta _{1}+X_{2}\beta
_{2}+\varepsilon $. Let $Q_{1}=I-X_{1}(X_{1}^{\prime
}X_{1})^{-1}X_{1}^{\prime }$. The all the following estimators of $\beta _{2}
$ are numerically equivalent:

\begin{itemize}
\item[1)] $\hat{\beta}_{2}$ from OLS\ of $Y$ on $X_{1},X_{2}$;

\item[2)] $\tilde{\beta}_{2}$ from OLS\ of $\tilde{Y}$ on $\tilde{X}_{2}$;

\item[3)] $\bar{\beta}_{2}$ from OLS\ of $Y$ on $\tilde{X}_{2}$.
\end{itemize}

where $\tilde{Y}$ is the residual from OLS\ of $Y$ on $X_1$, and $\tilde{X}_{2}
$ is the residual matrix from the regression of columns of $X_{2}$ on $X_1$.
\end{theorem}

\begin{proof}
From decoposing $Y$ we know that $\tilde{Y}=Q_{1}Y$ and $\tilde{X}%
_{2}=Q_{1}X_{2}$. The equivalence of the three esimators follows from (E4)
from the matrix algebra handout:

\begin{itemize}
\item[1)] $\hat{\beta}_{2}=(X_{2}^{\prime }Q_{1}X_{2})^{-1}(X_{2}^{\prime
}Q_{1}Y)$ (from (E4) of the matrix algebra handout.

\item[2)] $\tilde{\beta}_{2}=(\tilde{X}_{2}^{\prime }\tilde{X}_{2})^{-1}(%
\tilde{X}_{2}^{\prime }\tilde{Y})=(X_{2}^{\prime }Q_{1}^{\prime
}Q_{1}X_{2})^{-1}(X_{2}^{\prime }Q_{1}^{\prime }Q_{1}Y)=(X_{2}^{\prime
}Q_{1}X_{2})^{-1}(X_{2}^{\prime }Q_{1}Y)$

\item[3)] $\bar{\beta}_{2}=(X_{2}^{\prime }Q_{1}^{\prime
}Q_{1}X_{2})^{-1}(X_{2}^{\prime }Q_{1}^{\prime }Y)=(X_{2}^{\prime
}Q_{1}X_{2})^{-1}(X_{2}^{\prime }Q_{1}Y)$
\end{itemize}
\end{proof}

\begin{corollary}
The following three regressions produce the same coefficient estimates:

\begin{itemize}
\item[i)] Regressing $Y$ on a constant and a set $X_{2}$ of $k-1$ regressors.

\item[ii)] Deviations of $Y$ from its mean on deviations of $X_{2}$ from its
mean.

\item[iii)] $Y$ on deviations of $X_{2}$ from its mean.
\end{itemize}
\end{corollary}

\begin{theorem}
If $X_{1}^{\prime }X_{2}=0$, then all the estimators from Theorem \ref%
{thm:S4T1} (T1) are equivalent numerically to the estimator obtained by
doing OLS\ of $Y$ on $X_{2}$ (i.e. we can drop the other regressors).
\end{theorem}

\begin{proof}
\begin{eqnarray*}
X_{2}^{\prime }Q_{1} &=&X_{2}^{\prime }\left[ I-X_{1}(X_{1}^{\prime
}X_{1})^{-1}X_{1}^{\prime }\right] =X_{2}^{\prime }-X_{2}^{\prime
}X_{1}(X_{1}^{\prime }X_{1})^{-1}X_{1}^{\prime }=X_{2}^{\prime } \\
\Longrightarrow \tilde{\beta}_{2} &=&(X_{2}^{\prime
}Q_{1}X_{2})^{-1}(X_{2}^{\prime }Q_{1}Y)=(X_{2}^{\prime
}X_{2})^{-1}X_{2}^{\prime }Y.
\end{eqnarray*}
\end{proof}

\subsection{Statistical properties of the OLS\ estimator}

\begin{theorem}
\label{thm:S4T3}(T3) Under assumptions (A0)-(A3), we have

\begin{itemize}
\item[1)] $E\left( \hat{\beta}\mid X\right) =\beta $, $Var(\hat{\beta}\mid
X)=\sigma ^{2}(X^{\prime }X)^{-1}$.

\item[2)] $\hat{\beta}$ is the unique best linear unbiased estimator (BLUE)
of $\beta $ (Gauss-Markov Theorem).

\item[3)] $s^{2}=\frac{\hat{\varepsilon}^{\prime }\varepsilon }{n-k}$ is an
unbiased estimator of $\sigma ^{2}$.
\end{itemize}
\end{theorem}

\begin{proof}
\begin{itemize}
\item[1)]  
\begin{eqnarray*}
E(Y &\mid &X)=X\beta +E(\varepsilon \mid X)=X\beta \text{ \ from assumption
(A2)} \\
Var(Y &\mid &X)=E(\varepsilon \varepsilon ^{\prime }\mid X)=\sigma ^{2}I%
\text{ \ from assumption (A3)} \\
E(\hat{\beta} &\mid &X)=E((X^{\prime }X)^{-1}X^{\prime }Y\mid X)=(X^{\prime
}X)^{-1}X^{\prime }E(Y\mid X)=(X^{\prime }X)^{-1}X^{\prime }X\beta =\beta  \\
Var(\hat{\beta} &\mid &X)=Var((X^{\prime }X)^{-1}X^{\prime }Y\mid
X)=(X^{\prime }X)^{-1}X^{\prime }Var(Y\mid X)X(X^{\prime }X)^{-1}=\sigma
^{2}(X^{\prime }X)^{-1}
\end{eqnarray*}

\item[2)] Let $\tilde{\beta}=AY$ be another linear unbiased estimator of $%
\beta $. Then%
\begin{eqnarray*}
E(\tilde{\beta} &\mid &X)=AE(Y\mid X)=AX\beta =\beta \Longleftrightarrow AX=I
\\
Var(\tilde{\beta} &\mid &X)=AVar(Y\mid X)A^{\prime }=\sigma ^{2}AA^{\prime }
\end{eqnarray*}%
Let $A^{\ast }=(X^{\prime }X)^{-1}X^{\prime }$ be the linear projection
giving the OLS\ estimator. Without loss of generality, assume that $V(Y\mid
X)=V(\varepsilon \mid X)=I$. Then%
\begin{eqnarray*}
Var(\tilde{\beta}-\hat{\beta}\mid X) &=&Var(AY-A^{\ast }Y\mid X)=V\left[
(A-A^{\ast })Y\mid X\right] = \\
&=&(A-A^{\ast })Var(Y\mid X)(A-A^{\ast })^{\prime }=(A-A^{\ast })^{\prime
}(A-A^{\ast })= \\
&=&AA^{\prime }-AA^{\ast \prime }-A^{\ast }A^{\prime }+A^{\ast }A^{\ast
\prime }= \\
&=&AA^{\prime }-\underset{=I}{\underbrace{AX}}(X^{\prime }X)^{-1}-(X^{\prime
}X)^{-1}\underset{=I}{\underbrace{X^{\prime }A^{\prime }}}+\underset{=I}{%
\underbrace{(X^{\prime }X)^{-1}(X^{\prime }X)}}(X^{\prime }X)^{-1}= \\
&=&AA^{\prime }-(X^{\prime }X)^{-1}-(X^{\prime }X)^{-1}+(X^{\prime
}X)^{-1}= \\
&=&AA^{\prime }-(X^{\prime }X)^{-1}=Var(\tilde{\beta}\mid X)-Var(\hat{%
\beta}\mid X)
\end{eqnarray*}%
which has to be positive semidefinite, since it is equal to the variance of $%
(\tilde{\beta}-\hat{\beta})$ conditional on $X$. Thus $\hat{\beta}$ is
indeed BLUE\ (in the sense that it has the lowest variance of all linear
unbiased estimators).

The uniqueness of the OLS estimator as BLUE can be shown as follows.
Assume there $\exists \tilde{\beta}:Var(\tilde{\beta}\mid X)=Var(\hat{\beta}%
\mid X)$. Then $\forall c\neq 0,~c\in 
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
^{k}$,%
\begin{eqnarray*}
0 &=&c^{\prime }\left[ Var(\tilde{\beta}\mid X)-Var(\hat{\beta}\mid X)\right]
c=c^{\prime }Var(\tilde{\beta}-\hat{\beta}\mid X)c= \\
&=&Var(c^{\prime }\left( \tilde{\beta}-\hat{\beta}\right) \mid X)=0\text{.}
\end{eqnarray*}%
This means that $c^{\prime }\left( \tilde{\beta}-\hat{\beta}\right) $ is
constant given $X$ for any $c\neq 0$. So taking $c=1$, $\tilde{\beta}-\hat{%
\beta}=m\in 
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
$, and so we have%
\begin{equation*}
m=E(m\mid X)=E(\tilde{\beta}\mid X)-E(\hat{\beta}\mid X)=\beta -\beta =0%
\text{.}
\end{equation*}%
Thus $\tilde{\beta}=\hat{\beta}$.

\item[3)] Recall that $Q=I-X(X^{\prime }X)^{-1}X^{\prime }$. We have%
\begin{equation*}
\hat{\varepsilon}^{\prime }\hat{\varepsilon}=(QY)^{\prime
}(QY)=(Q\varepsilon )^{\prime }(Q\varepsilon )=\varepsilon ^{\prime
}Q\varepsilon \text{.}
\end{equation*}

Then%
\begin{eqnarray*}
E(\hat{\varepsilon}^{\prime }\hat{\varepsilon}) &=&E(\underset{1\times 1}{%
\underbrace{\varepsilon ^{\prime }Q\varepsilon }}\mid X)=E(tr(\varepsilon
^{\prime }Q\varepsilon )\mid X)=\qquad \text{(from MAHO since }%
tr(scalar)=scalar\text{)} \\
&=&E(tr(\varepsilon \varepsilon ^{\prime }Q)\mid X)=\qquad \text{(since }%
tr(AB)=tr(BA)\text{)} \\
&=&tr(E(\varepsilon \varepsilon ^{\prime }Q\mid X))=\qquad \text{(since }tr%
\text{ is a linear operator)} \\
&=&tr(E(\varepsilon \varepsilon ^{\prime }\mid X)Q)=tr(\sigma ^{2}Q)= \\
&=&\sigma ^{2}tr(Q)=\qquad \text{(since }tr(cA)=c\cdot tr(A)\quad \forall
c\in 
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
\text{)} \\
&=&\sigma ^{2}tr(I_{n}-X(X^{\prime }X)^{-1}X^{\prime })= \\
&=&\sigma ^{2}\left[ tr(I_{n})-tr(X(X^{\prime }X)^{-1}X^{\prime })\right]
=\qquad \text{(since }tr(A+B)=tr(A)+tr(B)\text{)} \\
&=&\sigma ^{2}[n-tr((X^{\prime }X)(X^{\prime }X)^{-1})]=\qquad \text{(since }%
tr(AB)=tr(BA)\text{)} \\
&=&\sigma ^{2}\left[ n-tr(I_{k})\right] =\sigma ^{2}(n-k)
\end{eqnarray*}

which also implies that $tr(Q)=n-k=rank(Q)$.
\end{itemize}
\end{proof}
