\subsection{Generalized Least Squares (GLS)}

Can we do better than OLS\ with robust standard errors? Yes, in some cases.

Consider assumptions (SS0)-(SS3) and assume that $\Omega $ is known. Since $%
\Omega $ is symmetric, we can write (see MAHO) $\Omega =C\Lambda C^{\prime }$%
, where $\Lambda =diag(\lambda _{i})$ and $C^{\prime }C=CC^{\prime }=I$.
Further, since it is positive definite, $\lambda _{i}>0$ ~$\forall i$ and we
can define $\Omega ^{r}=C\Lambda ^{r}C^{\prime }~\forall r\in 
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
$ where $\Lambda ^{r}=diag(\lambda _{i}^{r})$.

In particular, $\Omega ^{-1/2}=C\Lambda ^{-1/2}C^{\prime }$ is symmetric,
positive definite with $\Omega ^{-1/2}\Omega \Omega ^{-1/2}=I$ and $\Omega
^{-1/2}\Omega ^{-1/2}=\Omega ^{-1}$. Consider transforming the model%
\[
\Omega ^{-1/2}Y=\Omega ^{-1/2}X\beta +\Omega ^{-1/2}\varepsilon 
\]%
or%
\[
\tilde{Y}=\tilde{X}\beta +\tilde{\varepsilon}
\]%
Then the new model has the following properties:

\begin{itemize}
\item[(1)] \bigskip $rank(\tilde{X})=k$ (since $rank(AB)=rank(B)$ if $A$ has
full rank)

\item[(2)] $E(\tilde{\varepsilon}\mid \tilde{X})=\Omega ^{-1/2}E\left[
\varepsilon \mid \tilde{X}\right] =\Omega ^{-1/2}E\left[ \varepsilon \mid X%
\right] =0$

\item[(3)] $E\left[ \tilde{\varepsilon}^{\prime }\tilde{\varepsilon}\mid 
\tilde{X}\right] =\Omega ^{-1/2}\underset{=E\left[ \varepsilon ^{\prime
}\varepsilon \mid X\right] }{\underbrace{E\left[ \varepsilon ^{\prime
}\varepsilon \mid \tilde{X}\right] }}\Omega ^{-1/2}=\Omega ^{-1/2}\Omega
\Omega ^{-1/2}=I$
\end{itemize}

Since the transformed model satisfies the Gauss-Markov assumptions, so the
OLS\ estimator in this transformed model will be unbiased and BLUE for the
transformed model.

The\ OLS\ estimator on the transformed model is called the \emph{GLS\
estimator}.%
\[
\hat{\beta}_{GLS}=\left( \tilde{X}^{\prime }\tilde{X}\right) ^{-1}\tilde{X}%
^{\prime }\tilde{Y}=\left( X^{\prime }\Omega ^{-1}X\right) ^{-1}X^{\prime
}\Omega ^{-1}Y
\]

\begin{theorem}
\label{thm:S8T4}(T4) Under (SS0)-(SS3), we have

\begin{itemize}
\item[(1)] $E(\hat{\beta}_{GLS}\mid X)=\beta $ and $Var(\hat{\beta}%
_{GLS}\mid X)=(X^{\prime }\Omega ^{-1}X)^{-1}$.

\item[(2)] $\hat{\beta}_{GLS}$ is BLUE for the untransformed model.
\end{itemize}
\end{theorem}

\begin{proof}
\begin{itemize}
\item[(1)] $E\left[ \hat{\beta}_{GLS}\mid X\right] =(X^{\prime }\Omega
^{-1}X)^{-1}X^{\prime }\Omega ^{-1}E\left[ Y\mid X\right] =(X^{\prime
}\Omega ^{-1}X)^{-1}X^{\prime }\Omega ^{-1}X\beta =\beta $

\item $Var(\hat{\beta}_{GLS}\mid X)=\left( X^{\prime }\Omega ^{-1}X\right)
^{-1}X^{\prime }\Omega ^{-1}Var(Y\mid X)\Omega ^{-1}X(X^{\prime }\Omega
^{-1}X)^{-1}=\left( X^{\prime }\Omega ^{-1}X\right) ^{-1}$ since $Var(Y\mid
X)=\Omega $.

\item[(2)] We know from Gauss-Markov Theorem that $\hat{\beta}_{GLS}$ is
best among all unbiased estimators that are linear in $\tilde{Y}$. To see
that it is best among all unbiased estimators that are linear in $Y$, let $%
\tilde{\beta}=AY$ be such an estimator. We know that $\tilde{\beta}$
unbiased $\iff $ $AX=I$. But then%
\[
\tilde{\beta}=A\Omega ^{1/2}\Omega ^{-1/2}Y=\tilde{A}\tilde{Y}\text{ \ \
with \ }\tilde{A}\tilde{X}=A\Omega ^{1/2}\Omega ^{-1/2}X=AX=I\text{.}
\]%
Thus $\tilde{\beta}$ is linear and unbiased in the transformed and
untransformed models $\Rightarrow $ $\hat{\beta}_{GLS}$ is BLUE\ even for
the untransformed model.
\end{itemize}
\end{proof}

\begin{remark}
Note that we rely on small sample properties here, no large sample
assumptions.\bigskip 
\end{remark}

In general, all results that are derived for OLS\ under homoskedasticity and
no serial correlation carry over to $\hat{\beta}_{GLS}$ and the transformed
model.

\begin{theorem}
\label{thm:S8T5}(T5) Under (SS0)-(SS2'), we have

\begin{itemize}
\item[(1)] $\hat{\beta}_{GLS}\sim N(\beta ,(X^{\prime }\Omega ^{-1}X)^{-1})$

\item[(2)] $\hat{\beta}_{GLS}=\hat{\beta}_{MLE}$ (more on this later)
\end{itemize}
\end{theorem}

\begin{remark}
\begin{itemize}
\item[-] We can apply our earlier results for tests of linear restrictions
on $\beta $ to the transformed model using (SS'2') even in small samples.
Notice, however, that since $\Omega $ and hence $(X^{\prime }\Omega ^{-1}X)$
are known, we use $z$-tests based on $N(0,1)$ and $\chi ^{2}$ tests rather
than $t$-tests and $F$-tests.

\item[-] $R^{2}$ has no precise counterpart in the GLS\ model.

\item[-] OLS\ is just a special case of GLS\ with $\Omega =\sigma ^{2}I\,$.
To see this, just substitute $(1/\sigma ^{2})I$ for $\Omega ^{-1}$ in the
formula for $\hat{\beta}_{GLS}$.
\end{itemize}
\end{remark}

\begin{theorem}
\label{thm:S8T6}(T6) Under (LS0)-(LS3):

\begin{itemize}
\item[(1)] $\hat{\beta}_{GLS}\overset{P}{\longrightarrow }\beta $

\item[(2)] $\sqrt{n}\left( \hat{\beta}_{GLS}-\beta \right) \overset{d}{%
\longrightarrow }N(0,W^{-1})$ where $W=p\lim (X^{\prime }\Omega ^{-1}X/n)$
\end{itemize}
\end{theorem}

\begin{proof}
\begin{itemize}
\item[(2)] $\sqrt{n}\left( \hat{\beta}_{GLS}-\beta \right) =\left( \frac{%
X^{\prime }\Omega ^{-1}X}{n}\right) ^{-1}\left( \frac{X^{\prime }\Omega
^{-1}\varepsilon }{\sqrt{n}}\right) \overset{d}{\longrightarrow }%
W^{-1}N(0,W)=N(0,W^{-1})$.

\item (2) $\Rightarrow $ (1) since $\frac{1}{\sqrt{n}}\sqrt{n}\left( \hat{%
\beta}_{GLS}-\beta \right) \overset{d}{\longrightarrow }p\lim_{n\rightarrow
\infty }\underset{\rightarrow 0}{\frac{1}{\sqrt{n}}}Z$ which implies $\hat{%
\beta}_{GLS}-\beta \overset{d}{\longrightarrow }0$.
\end{itemize}
\end{proof}

\subsection{Feasible GLS\ (FGLS)}

The problem with GLS\ is that it requires that we know $\Omega $, which is
unlikely to be the case. We can overcome this asymptotically by substituting
an estimate $\hat{\Omega}$ for $\Omega $. Then we get the feasible GLS\
estimator%
\[
\hat{\beta}_{FGLS}=\left( X^{\prime }\hat{\Omega}^{-1}X\right)
^{-1}X^{\prime }\hat{\Omega}^{-1}Y
\]

\begin{theorem}
\label{thm:S8T7}\bigskip (T7) If

\begin{itemize}
\item[(1)] $X^{\prime }\hat{\Omega}^{-1}X/n\overset{P}{\longrightarrow }W$
and

\item[(2)] $\left[ X^{\prime }\hat{\Omega}^{-1}\varepsilon -X^{\prime
}\Omega ^{-1}\varepsilon \right] /\sqrt{n}\overset{P}{\longrightarrow }0$
\end{itemize}

then $\sqrt{n}\left( \hat{\beta}_{FGLS}-\hat{\beta}_{GLS}\right) \overset{P}{%
\longrightarrow }0$, which means that the 2 estimators are the same
according to 1$^{st}$ order asymptotes. Note that all what we do here is
first order asymptotics --- we do Taylor expansions and neglect terms that
are higher than first order. Higher order terms may be important in some
applications, see topics courses in econometrics.
\end{theorem}

\begin{proof}
$\sqrt{n}\left( \hat{\beta}_{FGLS}-\beta \right) =\underset{\overset{d}{%
\longrightarrow }N(0,W^{-1})}{\underbrace{\sqrt{n}\left( \hat{\beta}%
_{GLS}-\beta \right) }}+\underset{\overset{P}{\longrightarrow }0}{%
\underbrace{\sqrt{n}\left( \hat{\beta}_{FGLS}-\hat{\beta}_{GLS}\right) }}%
\overset{d}{\longrightarrow }N(0,W^{-1})$ by Corollary \ref{thm:S3C5b} (C5b)
(a) in Section \ref{sec:3} (\#3).
\end{proof}

\begin{remark}
In practice, we write $\Omega $ as a function of an unknown finitedimensional
parameter vector $\alpha $, i.e. $\Omega =\Omega (\alpha )$. Then we do
FGLS\ as a 2-step procedure:

\begin{itemize}
\item[(1)] First do OLS\ of $Y$ on $X$. This gives us $\hat{\beta}_{OLS}$
and hence $\hat{\varepsilon}_{OLS}$.

\item[(2)] Use $\hat{\varepsilon}_{OLS}$ to obtain estimates of $\alpha $,
then form $\Omega (\hat{\alpha})$ and estimate $\hat{\beta}_{FGLS}$ as above.
\end{itemize}

If the $\Omega (\hat{\alpha})$ obtained in step 2 satisfies the conditions
of Theorem \ref{thm:S8T7} (T7), then it follows that

\begin{itemize}
\item[(1)] $\hat{\beta}_{FGLS}\overset{P}{\longrightarrow }\beta $ and

\item[(2)] $\sqrt{n}\left( \hat{\beta}_{FGLS}-\beta \right) \overset{d}{%
\longrightarrow }N(0,W^{-1})$ with $W=p\lim \left( \frac{X^{\prime }\Omega
^{-1}X}{n}\right) $.
\end{itemize}

Iterating (i.e. using $\hat{\beta}_{FGLS}$ to get a new $\hat{\varepsilon}$, estimate $\alpha$ again, do a new FGLS, obtain new residual estimates 
etc.) yields no gains according to first order asymptotics relative to the
2-step procedure.
\end{remark}

\begin{remark}
The small sample properties of the FGLS\ estimator are unknown. It is likely
to be unbiased.
\end{remark}

\section{(Section \#9) Heteroskedasticity}

Assumptions:

\begin{itemize}
\item[(A0)] \bigskip $Y=X\beta +\varepsilon $

\item[(A1)] $X^{\prime }X/n\overset{P}{\longrightarrow }M$ where $M$ is a
positive definite matrix

\item[(A2)] $X^{\prime }\varepsilon /n\overset{P}{\longrightarrow }0$

\item[(A3)] $X^{\prime }\varepsilon /\sqrt{n}\overset{d}{\longrightarrow }%
N(0,V)$ with $V=p\lim (X^{\prime }\Omega X/n)$

\item $X^{\prime }\Omega ^{-1}\varepsilon /\sqrt{n}\overset{d}{%
\longrightarrow }N(0,W)$ where $W=p\lim (X^{\prime }\Omega ^{-1}X/n)$
\end{itemize}

We assume now that $\Omega =diag(E\left[ \varepsilon _{i}^{2}\mid X_{i}%
\right] )=diag(\sigma _{i}^{2})$, i.e. the case of heteroskedasticity
without serial correlation.

We then have:

\begin{itemize}
\item[(1)] \bigskip OLS: $\sqrt{n}\left( \hat{\beta}_{OLS}-\beta \right) 
\overset{d}{\longrightarrow }N(0,M^{-1}VM^{-1})$.

\item To use $\hat{\beta}_{OLS}$ for inference, we need a consistent
estimate of $V$. Use%
\[
\frac{1}{n}X^{\prime }\Omega X=\frac{1}{n}\sum_{i=1}^{n}\sigma
_{i}^{2}X_{i}X_{i}^{\prime }\Longrightarrow \hat{V}_{n}=\frac{1}{n}%
\sum_{i=1}^{n}\hat{\varepsilon}_{i}^{2}X_{i}X_{i}^{\prime }
\]%
i.e. we use the White estimator of $V$.

\item[(2)] FGLS: Assume that $\sigma _{i}^{2}=Var(\alpha ,X_{i})$ with $%
Var(0,X_{i})=\sigma ^{2}$. Then obtain the FGLS\ estimator in the following
way

\begin{itemize}
\item[(1)] Run OLS of $Y_{i}$ on $X_{i}$ and obtain $\hat{\varepsilon}_{i}$.

\item[(2)] Use $\hat{\varepsilon}_{i}^{2}$ and $V(\alpha ,X_{i})$ to
estimate $\alpha ~$consistently. Obtain $\hat{\alpha}$.
\end{itemize}

\item Then use $\hat{\Omega}=\Omega (\hat{\alpha})$ and form the FGLS\
estimator 
\[
\hat{\beta}_{FGLS}=\left( X^{\prime }\hat{\Omega}^{-1}X\right)
^{-1}X^{\prime }\hat{\Omega}^{-1}Y=\left[ \sum_{i=1}^{n}\frac{1}{V(\hat{%
\alpha},X_{i})}X_{i}X_{i}^{\prime }\right] ^{-1}\left[ \sum_{i=1}^{n}\frac{1%
}{V(\hat{\alpha},X_{i})}X_{i}Y_{i}\right] \text{.}
\]
\end{itemize}

\begin{remark}
FGLS is just a weighted least squares where $X_{i}$, $Y_{i}$ are weighted by 
$\sqrt{1/V(\hat{\alpha},X_{i})}$.

Also, we have $\sqrt{n}\left( \hat{\beta}_{FGLS}-\beta \right) \overset{d}{%
\longrightarrow }N(0,W^{-1})$ and thus $\hat{\beta}_{FGLS}\overset{P}{%
\longrightarrow }\beta $.
\end{remark}

\begin{example}
\bigskip \label{thm:S9E1}(E1) (FGLS)

Assume $Y_{i}=\beta X_{i}+\varepsilon _{i}$ where $X_{i}$ is a scalar. Also
we conjecture a functional form for the variance $\sigma _{i}^{2}=\alpha
_{0}+\alpha _{1}f(X_{i})$ (for example $f(X_{i})=X_{i}^{2}$)

\begin{itemize}
\item[(1)] Run\ OLS of $Y_{i}$ on $X_{i}$, obtain $\hat{\varepsilon}_{i}^{2}$%
.

\item[(2)] Run OLS\ $\hat{\varepsilon}_{i}^{2}$ on $1,f(X_{i})$ to obtain $%
\hat{\alpha}_{0}$ and $\hat{\alpha}_{1}$. Form $\hat{\sigma}_{i}^{2}=\hat{%
\alpha}_{0}+\hat{\alpha}_{1}f(X_{i})$. Calculate 
\[
\hat{\beta}_{FGLS}=\left[ \sum_{i=1}^{n}(X_{i}^{2}/\hat{\sigma}_{i}^{2})%
\right] ^{-1}\left[ \sum_{i=1}^{n}(X_{i}Y/\hat{\sigma}_{i}^{2})\right] 
\]
\end{itemize}

Asymptotically, the FGLS\ estimator converges to the GLS\ estimator.
\end{example}

\begin{remark}
For homoskedastic error term, $\alpha _{1}=0$.
\end{remark}

\begin{remark}
In practice, people rather use OLS\ with White-adjusted standard errors.
This is more robust than doing FGLS. If we did FGLS\ with an incorrectly
specified functional form for the variance, we would get consistent
estimates of $\beta $, but with incorrect standard errors.
\end{remark}

\begin{remark}
Test for heteroskedasticity:\ see TA\ session.
\end{remark}

\section{(Section \#10) Serial correlation}

Assumptions:

\begin{itemize}
\item[(A0)] \bigskip $Y=X\beta +\varepsilon $

\item[(A1)] $X^{\prime }X/n\overset{P}{\longrightarrow }M$ where $M$ is a
positive definite matrix

\item[(A2)] $X^{\prime }\varepsilon /n\overset{P}{\longrightarrow }0$

\item[(A3)] $X^{\prime }\varepsilon /\sqrt{n}\overset{d}{\longrightarrow }%
N(0,V)$ with $V=p\lim (X^{\prime }\Omega X/n)$

\item $X^{\prime }\Omega ^{-1}\varepsilon /\sqrt{n}\overset{d}{%
\longrightarrow }N(0,W)$ where $W=p\lim (X^{\prime }\Omega ^{-1}X/n)$
\end{itemize}

where we no longer assume that $\Omega $ is diagonal. We then have:

\begin{itemize}
\item[(1)] \bigskip OLS with robust standard errors: $\sqrt{n}\left( \hat{%
\beta}_{OLS}-\beta \right) \overset{d}{\longrightarrow }N(0,M^{-1}VM^{-1})$.
See Section \ref{sec:8} (\#8) for $\hat{V}_{R}$ and $\hat{V}_{NW}$.

\item[(2)] The AR(1) model

\item $Y_{t}=X_{t}^{\prime }\beta +\varepsilon _{t}$ with $\varepsilon
_{t}=\rho \varepsilon _{t-1}+u_{t}$, where $\left\vert \rho \right\vert <1$
to have stationarity. $u_{t}$ is iid and uncorrelated with $X_{t\pm s}$ $%
\forall s\geq 0$. Further $E(u_{t})=0$, $E(u_{t}^{2})=\sigma _{u}^{2}$ $%
~\forall t$.

\item Solving for $\Omega $: By repeated substitution into the expression
for $\varepsilon _{t}$, we get%
\begin{eqnarray*}
\varepsilon _{t} &=&\rho \varepsilon _{t-1}+u_{t}=\rho \left( \rho
\varepsilon _{t-2}+u_{t-1}\right) +u_{t}=\ldots = \\
&=&\rho ^{\tau }\varepsilon _{t-\tau }+\sum_{j=0}^{\tau -1}\rho ^{j}\upsilon
_{t-j}\overset{\tau \rightarrow \infty }{\longrightarrow }\sum_{j=0}^{\infty
}\rho ^{j}u_{t-j}
\end{eqnarray*}%
where the limiting step was possible due to $\left\vert \rho \right\vert <1$%
, which implies $\rho ^{\tau }\varepsilon _{t-\tau }\rightarrow 0$.

\item Further we have%
\begin{eqnarray*}
E\left[ \varepsilon _{t}\varepsilon _{t-s}\right]  &=&E\left[
\sum_{j=0}^{\infty }\rho ^{j}u_{t-j}\sum_{k=0}^{\infty }\rho ^{k}u_{t-s-k}%
\right] =\sum_{k=0}^{\infty }\sum_{j=0}^{\infty }\rho ^{j}\rho ^{k}E\left[
u_{t-j}u_{t-s-k}\right] = \\
&=&\sum_{k=0}^{\infty }\rho ^{s+k}\rho ^{k}E\left[ u_{t-s-k}^{2}\right] 
\end{eqnarray*}%
since $E\left[ u_{t-j}u_{t-s-k}\right] =0$ if $j\neq s+k$ since $u_{i}$ is
iid.\ Thus we get%
\[
E\left[ \varepsilon _{t}\varepsilon _{t-s}\right] =\rho ^{s}\left[
\sum_{k=0}^{\infty }\rho ^{2k}\right] \sigma _{u}^{2}=\frac{\rho ^{s}}{%
1-\rho ^{2}}\sigma _{u}^{2}
\]%
since $E\left( u_{t}^{2}\right) =\sigma _{u}^{2}$. This holds for any $s$.
Thus for $s=0$, we get%
\[
E\varepsilon _{t}^{2}=\sigma _{\varepsilon }^{2}=\frac{\sigma _{u}^{2}}{%
1-\rho ^{2}}\text{.}
\]%
Notice that the correlation coefficient for disturbances $s$ periods apart
is $\rho ^{s}$, and thus fades toward zero as distance between observarions
increases. Now we can solve for $\Omega :$%
\[
\underset{T\times T}{\Omega }=\frac{\sigma _{u}^{2}}{1-\rho ^{2}}\left[ 
\begin{array}{cccc}
1 & \rho  & \cdots  & \rho ^{T-1} \\ 
\rho  & 1 &  & \vdots  \\ 
\vdots  &  & \ddots  & \rho  \\ 
\rho ^{T-1} &  & \rho  & 1%
\end{array}%
\right] \text{ and }\underset{T\times T}{\Omega ^{-1}}=\frac{1}{\sigma
_{u}^{2}}\left[ 
\begin{array}{ccccc}
1 & -\rho  & 0 & \cdots  & 0 \\ 
-\rho  & 1+\rho ^{2} & -\rho  &  & \vdots  \\ 
0 & -\rho  & \ddots  & -\rho  & 0 \\ 
\vdots  &  & -\rho  & 1+\rho ^{2} & -\rho  \\ 
0 & \cdots  & 0 & -\rho  & 1%
\end{array}%
\right] 
\]%
(check that $\Omega ^{-1}\Omega =I$). \ We also have%
\[
P=\frac{1}{\sigma _{u}^{2}}\left[ 
\begin{array}{ccccc}
\sqrt{1-\rho ^{2}} & 0 & 0 & \cdots  & 0 \\ 
-\rho  & 1 & 0 &  & \vdots  \\ 
0 & -\rho  & \ddots  & 0 & 0 \\ 
\vdots  &  & -\rho  & 1 & 0 \\ 
0 & \cdots  & 0 & -\rho  & 1%
\end{array}%
\right] \text{ where }P^{\prime }P=\Omega ^{-1}
\]%
Then $\hat{\beta}_{GLS}=(X^{\prime }\Omega ^{-1}X)^{-1}X^{\prime }\Omega
^{-1}Y$ can be obtained by doing OLS\ on the transformed model $\tilde{Y}=%
\tilde{X}\beta +\tilde{\varepsilon}\,$\ where $\tilde{Y}=PY$, $\tilde{X}=PX$%
, and $\tilde{\varepsilon}=P\varepsilon $. Then%
\[
\tilde{Y}=\left[ 
\begin{array}{c}
\sqrt{1-\rho ^{2}}Y_{1} \\ 
Y_{2}-\rho Y_{1} \\ 
\vdots  \\ 
Y_{T}-\rho Y_{T-1}%
\end{array}%
\right] \text{ and }\tilde{X}=\left[ 
\begin{array}{c}
\sqrt{1-\rho ^{2}}X_{1}^{\prime } \\ 
\left( X_{2}-\rho X_{1}\right) ^{\prime } \\ 
\vdots  \\ 
\left( X_{T}-\rho X_{T-1}\right) ^{\prime }%
\end{array}%
\right] 
\]%
If we disregarded the first observation, we can write the transformed model
as%
\[
Y_{t}-\rho Y_{t-1}=\left( X_{t}-\rho X_{t-1}\right) ^{\prime }\beta +%
\underset{u_{t}}{\underbrace{\varepsilon _{t}-\rho \varepsilon _{t-1}}}
\]%
So the transformation replaces the serially correlated distribance $%
\varepsilon _{t}$ by a spherical one, $u_{t}$. The slightly different
transformation in the first observation accounts for the effect of the
observations that precede $\left( Y\,_{1},X_{1}\right) $\ and that are not
included in the sample (when constructing the model, we assumed that the
process originates at $t=-\infty $). In large samples, the effect of the
first observation is minimal, and we might want to drop it.

\item \emph{FGLS\ estimation of the }$\beta $\emph{\ in an AR(1) model}

\begin{itemize}
\item[-] Prais-Winsten (PW):

\begin{itemize}
\item[(1)] Do OLS\ $Y_{t}$ on $X_{t}$ and obtain $\hat{\beta}_{OLS}$ and $%
\hat{\varepsilon}_{t}$.

\item[(2)] Do OLS\ $\hat{\varepsilon}_{t}$ on $\hat{\varepsilon}_{t-1}$ and
obtain the estimate $\hat{\rho}$ and $\hat{\sigma}_{u}^{2}$.

\item[(3)] Form $\tilde{Y}_{1}=\sqrt{1-\hat{\rho}^{2}}Y_{1}$, $\tilde{X}_{1}=%
\sqrt{1-\hat{\rho}^{2}}X_{1}$, and $\tilde{Y}_{t}=Y_{t}-\hat{\rho}Y_{t-1}$, $%
\tilde{X}_{t}=X_{t}-\hat{\rho}X_{t-1}$ for $t=2,\ldots ,T$.

\item Regress $\tilde{Y}_{t}$ on $\tilde{X}_{t}$ for $t=1,\ldots ,T$, which
yields $\hat{\beta}_{PW}=\hat{\beta}_{FGLS}$.
\end{itemize}

\item[-] Cochrane-Orcutt (CO): Same as PW\ procedure, except that now we
omit the first observation. In large sampels, omitting one observation from
the dataset does not matter, and the two procedures are asymptotically
equivalent.
\end{itemize}

\item \emph{Consistency of FGLS (i.e. PW) and CO} (estimated parameters $\hat{\rho%
}$, $\hat{\sigma}_{u}^{2}$, $\hat{\beta}_{PW}$ or $\hat{\beta}_{CO}$)

\item Consistency of $\hat{\rho}$: 
\[
\hat{\rho}=\frac{\sum_{t=2}^{T}\hat{\varepsilon}_{t}\hat{\varepsilon}_{t-1}}{%
\sum_{t=2}^{T}\hat{\varepsilon}_{t-1}^{2}}\text{ where }\hat{\varepsilon}%
_{t}=Y_{t}-X_{t}\hat{\beta}_{OLS}
\]%
The reasoning for consistency is similar as for White standard errors:
Express in terms of $\varepsilon $ and $\hat{\beta}_{OLS}$, then argue that $%
\hat{\beta}_{OLS}\overset{P}{\longrightarrow }\beta $, and use the law of
large numbers.

\item We have%
\[
\hat{\rho}=\frac{\frac{1}{T-1}\sum_{t=2}^{T}\hat{\varepsilon}_{t}\hat{%
\varepsilon}_{t-1}}{\frac{1}{T-1}\sum_{t=2}^{T}\hat{\varepsilon}_{t-1}^{2}}%
\overset{P}{\longrightarrow }\frac{E(\varepsilon _{t}\varepsilon _{t-1})}{%
E(\varepsilon _{t}^{2})}=\rho 
\]

\item We can also show consistency of $\hat{\sigma}_{u}^{2}$:%
\begin{eqnarray*}
\hat{\sigma}_{u}^{2} &=&\frac{1}{T-1}\sum_{t=2}^{T}\hat{u}_{t}^{2}=\frac{1}{%
T-1}\sum_{t=2}^{T}\left( \hat{\varepsilon}_{t}-\hat{\rho}\hat{\varepsilon}%
_{t-1}\right) ^{2}= \\
&=&\underset{\overset{P}{\longrightarrow }E(\varepsilon _{t}^{2})}{%
\underbrace{\frac{1}{T-1}\sum_{t=2}^{T}\hat{\varepsilon}_{t}^{2}}}-\underset{%
\overset{P}{\longrightarrow }2\rho }{\underbrace{2\hat{\rho}}}\underset{%
\overset{P}{\longrightarrow }E\left( \varepsilon _{t}\varepsilon
_{t-1}\right) }{\underbrace{\frac{1}{T-1}\sum_{t=2}^{T}\hat{\varepsilon}_{t}%
\hat{\varepsilon}_{t-1}}}+\underset{\overset{P}{\longrightarrow }\rho ^{2}}{%
\underbrace{\hat{\rho}^{2}}}\underset{\overset{P}{\longrightarrow }E\left(
\varepsilon _{t}^{2}\right) }{\underbrace{\frac{1}{T-1}\sum_{t=2}^{T}\hat{%
\varepsilon}_{t-1}^{2}}}
\end{eqnarray*}%
and thus%
\[
\hat{\sigma}_{u}^{2}\overset{P}{\longrightarrow }\left[ \left( 1+\rho
^{2}\right) E\varepsilon _{t}^{2}-2\rho E\left( \varepsilon _{t}\varepsilon
_{t-1}\right) \right] =\frac{\left( 1+\rho ^{2}\right) \sigma _{u}^{2}}{%
1-\rho ^{2}}-2\frac{\rho ^{2}\sigma _{u}^{2}}{1-\rho ^{2}}=\sigma _{u}^{2}%
\text{.}
\]

\item Consistency of $\hat{\beta}_{PW}$ and $\hat{\beta}_{CO}$: Since
dropping a single observation does not matter, focus on consistency of $\hat{%
\beta}_{CO}$. Observe that $\forall t:E\left( X_{t}\varepsilon _{t+s}\right)
-E\left( X_{t}\sum_{v=0}^{\infty }\rho ^{v}u_{t+s-v}\right)
=\sum_{v=0}^{\infty }\rho ^{v}\underset{=0}{\underbrace{E\left(
X_{t}u_{t+s-v}\right) }}=0$. So $X$ and $u$ being uncorrelated at all leads
and lags implies that $X$ and $\varepsilon $ are uncorrelated at all leads
and lags. Recall that $\hat{\beta}_{CO}$ is the OLS\ estimator from
regression of $Y_{t}-\hat{\rho}Y_{t-1}$ on $X_{t}-\hat{\rho}X_{t-1}$:%
\[
Y_{t}-\hat{\rho}Y_{t-1}=\left( X_{t}-\hat{\rho}X_{t-1}\right) ^{\prime
}\beta +\varepsilon _{t}-\hat{\rho}\varepsilon _{t-1}
\]%
All we need to show for consistency is that%
\[
\frac{1}{T-1}\sum_{t=2}^{T}\left( X_{t}-\hat{\rho}X_{t-1}\right) \left(
\varepsilon _{t}-\hat{\rho}\varepsilon _{t-1}\right) \overset{P}{%
\longrightarrow }0
\]%
But this is straightforward since%
\begin{eqnarray*}
&&\frac{1}{T-1}\sum_{t=2}^{T}\left( X_{t}-\hat{\rho}X_{t-1}\right) \left(
\varepsilon _{t}-\hat{\rho}\varepsilon _{t-1}\right) = \\
&=&\underset{\overset{P}{\longrightarrow }E(X_{t}\varepsilon _{t})=0}{%
\underbrace{\frac{1}{T-1}\sum_{t=2}^{T}X_{t}\varepsilon _{t}}}-\underset{%
\overset{P}{\longrightarrow }\rho }{\hat{\rho}}\underset{\overset{P}{%
\longrightarrow }E(X_{t}\varepsilon _{t-1})=0}{\underbrace{\frac{1}{T-1}%
\sum_{t=2}^{T}X_{t}\varepsilon _{t-1}}}-\underset{\overset{P}{%
\longrightarrow }\rho }{\hat{\rho}}\underset{\overset{P}{\longrightarrow }%
E(X_{t-1}\varepsilon _{t})=0}{\underbrace{\frac{1}{T-1}\sum_{t=2}^{T}X_{t-1}%
\varepsilon _{t}}}+\underset{\overset{P}{\longrightarrow }\rho ^{2}}{\hat{%
\rho}^{2}}\underset{\overset{P}{\longrightarrow }E(X_{t-1}\varepsilon
_{t-1})=0}{\underbrace{\frac{1}{T-1}\sum_{t=2}^{T}X_{t-1}\varepsilon _{t-1}}}%
\overset{P}{\longrightarrow }0
\end{eqnarray*}
\end{itemize}

% ==============================================================================

\begin{example} (Durbin-Watson test, TA session 11/03/06)
We have a linear regression model with serial correlation in the residuals%
\[
\varepsilon =\rho \varepsilon _{t-1}+u_{t}
\]%
where $u_{t}$ is white noise. The Durbin-Watson statistic is%
\[
DW=\frac{\sum_{t=2}^{T}\left( e_{t}-e_{t-1}\right) ^{2}}{%
\sum_{t=2}^{T}e_{t}^{2}}\simeq 2\left( 1-\hat{\rho}\right) 
\]%
where $\hat{\rho}=\frac{\sum_{t=2}^{T}e_{t}e_{t-1}}{\sum_{t=2}^{T}e_{t}^{2}}$%
, and $e_{t}$ are the residuals from OLS. The values of the Durbin-Watson
statistic are in $0\leq DW\leq 4$. There is no known distribution, it is
only known that the the critical value $d_{c}$ for the significance level $c$
is restricted by $d_{l}\leq d_{c}\leq d_{u}$. Values close to 2 mean that we
cannot reject the null of no autocorrelation. $DW>2$ indicates negative
autocorrelation, $DW<2\,$\ indicates positive autocorrelation.

So we may have three outcomes: reject \thinspace $H_{0}$, not reject $H_{0}$%
, and inconclusive.

Take for example the one-sided test:%
\[
H_{0}:\rho \leq 0\qquad \text{vs.\qquad }H_{1}:\rho >0
\]%
Then reject $H_{0}$ if $DW\leq D_{U}^{>0}$, do not reject $H_{0}$ if $DW\geq
D_{L}^{>0}$, and conclude that the test is inconclusive if $D_{U}^{>0}\leq
DW\leq D_{L}^{>0}$.

The opposite one-sided test is%
\[
H_{0}:\rho \geq 0\qquad \text{vs.\qquad }H_{1}:\rho <0
\]%
Then reject $H_{0}$ if $DW\geq D_{U}^{<0}$, do not reject $H_{0}$ if $DW\leq
D_{L}^{<0}$, and conclude that the test is inconclusive if $D_{U}^{<0}\geq
DW\geq D_{L}^{<0}$.

A both sided test is then a combination of the above.
\end{example}

% ==============================================================================

\begin{example} (A test for heteroskedasticity, TA session 11/03/06)

What if $E(\varepsilon _{i}^{2})\neq \sigma ^{2}.$

Assume a linear model $Y=X\beta +\varepsilon $. Get residuals $e_{t}$, and
run a regression%
\begin{equation}
e_{t}^{2}=\alpha +\sum_{i=1}^{k}\sum_{j=1}^{i}\delta _{ij}x_{it}x_{jt}+u_{t}
\label{eq:061103-whitetest}
\end{equation}%
If the errors are homoskedastic, then $E\left( e_{t}^{2}\right) =\frac{n-k}{n%
}\sigma ^{2}=\alpha $. Then our test is%
\[
H_{0}:\delta _{ij}=0\qquad \text{vs.}\qquad H_{1}:\exists i,j:\delta
_{ij}\neq 0
\]%
The White test statistic $W=nR^{2}$, where $R^{2}$ comes from regression in
equation (\ref{eq:061103-whitetest}), converges in distribution to $\chi
_{(H-1)}^{2}$, where $H=\frac{k+1}{2}k$ is the number of regressors in (\ref%
{eq:061103-whitetest}).

The rationale behind the test is the following:%
\[
EstVar\left( \hat{\beta}\right) =\left( X^{\prime }X\right) ^{-1}\left(
X^{\prime }\hat{\Omega}X)(X^{\prime }X\right) ^{-1}=\left( X^{\prime
}X\right) ^{-1}\left( \sum_{i=1}^{n}e_{i}^{2}x_{i}x_{i}^{\prime }\right)
(X^{\prime }X)^{-1}
\]%
so that under $H_{0}$ we have%
\begin{eqnarray*}
E(EstVar(\hat{\beta})) &=&\left( X^{\prime }X\right) ^{-1}E\left(
\sum_{i=1}^{n}e_{i}^{2}x_{i}x_{i}^{\prime }\right) (X^{\prime }X)^{-1}= \\
&=&\left( X^{\prime }X\right) ^{-1}\sigma ^{2}\left(
\sum_{i=1}^{n}x_{i}x_{i}^{\prime }\right) (X^{\prime }X)^{-1}=\left(
X^{\prime }X\right) ^{-1}\sigma ^{2}
\end{eqnarray*}

Check this, it looks a bit imprecise. ;-(
\end{example}