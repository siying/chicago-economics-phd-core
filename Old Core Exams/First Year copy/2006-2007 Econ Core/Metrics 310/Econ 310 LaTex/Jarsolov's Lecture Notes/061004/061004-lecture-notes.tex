\begin{remark}
Recall from last time that:
\begin{align*}
X_{n}\overset{r-\text{mean}}{\longrightarrow}X  & \Longleftrightarrow E\left[
\left\Vert X_{n}-X\right\Vert ^{r}\right]  \longrightarrow0,\quad r>0\\
X_{n}\overset{P}{\longrightarrow}X  & \Longleftrightarrow\forall
\varepsilon>0:\ P\left(  \left\Vert X_{n}-X\right\Vert >\varepsilon\right)
\longrightarrow0\\
X_{n}\overset{d}{\longrightarrow}X  & \Longleftrightarrow P\left(  X_{n}\leq
X_{0}\right)  \longrightarrow P\left(  X\leq X_{0}\right)  \ \forall
X_{0}\text{ s.t. }P\left(  X=X_{0}\right)  =0
\end{align*}
\end{remark}

\begin{theorem} (T1)\label{thm:S3T1}
Some relations between convergence concepts:
\begin{align*}
(1)\qquad & X_{n}\overset{\text{a.s.}}{\longrightarrow}X\Longrightarrow X_{n}\overset
{P}{\longrightarrow}X\\
(2)\qquad & X_{n}\overset{r-\text{mean}}{\longrightarrow}X\text{ \ for some
}r>0\Longrightarrow X_{n}\overset{P}{\longrightarrow}X\\
(3)\qquad & X_{n}\overset{P}{\longrightarrow}X\Longrightarrow X_{n}\overset
{d}{\longrightarrow}X\\
(4)\qquad & X_{n}\overset{P}{\longrightarrow}X_{0}\Longleftrightarrow X_{n}\overset
{d}{\longrightarrow}X_{0}%
\end{align*}
\end{theorem}

\begin{example}
From last week --- convergence in mean square $\Longrightarrow$ convergence in probability $\Longrightarrow$ convergence in distribution.
\end{example}

\begin{example}
Let $\{X_n\}$ be a sequence of random variables with p.d.f.:
\[
f_{n}(x)=\left\{
\begin{array}
[c]{cc}%
\frac{n-1}{2} & -\frac{1}{n}<x<\frac{1}{n}\\
\frac{1}{n} & n<x<n+1\\
0 & \text{elsewhere}%
\end{array}
\right.
\]
The p.d.f. is illustrated in Figure \ref{fig:S3pdf_convergence}. It can be shown (see last lecture) that $X_n$ converges in probability to 0, but does not convergence in mean square, since its variance is diverging.
\end{example}

\begin{example}
Let $X$ be a  Bernoulli random variable with $p=1/2$. Let $X_1, X_2, \ldots$ be identical random variables given by $X_n = X, \forall n$. By construction $X_n \overset{d}\to X$. Now define $Y=1-X$. Then $Y$ and $X$ have the same distribution, since $P(\text{success}) = P(\text{failure}) = 1/2$. So $X_n \overset{d}\to Y$. But $|X_n-Y|=1, \forall n$, so $X_n$ \emph{does not} converge in probability to $Y$. This shows that
\begin{align*}
X_{n}\overset{d}{\longrightarrow}Y  & \nRightarrow X_{n}\overset
{P}{\longrightarrow}Y\\
X_{n}\overset{d}{\longrightarrow}Y  & \nRightarrow X_{n}-Y\overset
{d}{\longrightarrow}0
\end{align*}
\end{example}

\begin{theorem} (T2)\label{thm:convergence_vectors}\label{thm:S3T2}
\[
X_{n}\overset{P}{\longrightarrow}X\text{ and }Y_{n}\overset{P}{\longrightarrow
}Y\Longrightarrow\left(  X_{n},Y_{n}\right)  \overset{P}{\longrightarrow
}\left(  X,Y\right)
\]
\begin{proof}
Since
\[
\left\Vert \left(
\begin{array}
[c]{c}%
X\\
Y
\end{array}
\right)  \right\Vert =\left\Vert \left(
\begin{array}
[c]{cc}%
X^{\prime} & Y^{\prime}%
\end{array}
\right)  ^{\prime}\right\Vert =\left(  \sum x_{i}^{2}+\sum y_{i}^{2}\right)
^{1/2}=\left(  \left\Vert X\right\Vert ^{2}+\left\Vert Y\right\Vert
^{2}\right)  ^{1/2}%
\]
we have
\[
P\left\{  \left\Vert \left(
\begin{array}
[c]{c}%
X_{n}\\
Y_{n}%
\end{array}
\right)  -\left(
\begin{array}
[c]{c}%
X\\
Y
\end{array}
\right)  \right\Vert >\varepsilon\right\}  =P\left\{  \left(  \left\Vert
X_{n}-X\right\Vert ^{2}+\left\Vert Y_{n}-Y\right\Vert ^{2}\right)
^{1/2}>\varepsilon\right\}
\]
and since
\[
P\left\{  \left\Vert X_{n}-X\right\Vert ^{2}>\varepsilon\right\}
\longrightarrow0\quad\text{and}\quad P\left\{  \left\Vert Y_{n}-Y\right\Vert
^{2}>\varepsilon\right\}  \longrightarrow0
\]
we also get
\[
P\left\{  \left\Vert X_{n}-X\right\Vert ^{2}>\varepsilon\right\}
\longrightarrow0\quad\text{and}\quad P\left\{  \left\Vert Y_{n}-Y\right\Vert
^{2}>\varepsilon\right\}  \longrightarrow0
\]
\end{proof}
\end{theorem}

\begin{theorem} (T3) [Continuous Mapping Theorem (convergence in probability)]\label{thm:CMT}\label{thm:S3T3}
For any continuous function $g$, we have
\[
X_{n}\overset{P}{\longrightarrow}X\text{ }\Longrightarrow g\left(
X_{n}\right)  \overset{P}{\longrightarrow}g\left(  X\right)  \text{ }%
\]
\
\end{theorem}

Combining theorems \ref{thm:convergence_vectors} and \ref{thm:CMT} yields the following:
\begin{dusl} (C3a)\label{thm:S3C3a}
Let $X_{n}\overset{P}{\longrightarrow}X\text{ and }Y_{n}\overset{P}{\longrightarrow}Y$. Then
\begin{align*}
(1) && X_{n}+Y_{n}\overset{P}{\longrightarrow}  & X+Y\\
(2) && X_{n}Y_{n}\overset{P}{\longrightarrow}  & XY\\
(3) && X_{n}/Y_{n}\overset{P}{\longrightarrow}  & X/Y\quad\text{if }y\neq0\text{ with
probability 1}%
\end{align*}
\end{dusl}

\begin{dusl} (C3b) \label{thm:S3C3b}
Let $A_{n}\overset{P}{\longrightarrow}A\text{ and }B_{n}\overset{P}{\longrightarrow}B$. Then
\begin{align*}
(1) && A_{n}+B_{n}\overset{P}{\longrightarrow}  & A+B\\
(2) && A_{n}B_{n}\overset{P}{\longrightarrow}  & AB\\
(3) && A_{n}^{-1}\overset{P}{\longrightarrow}  & A^{-1}\quad\text{if }A^{-1}\text{ exists with probability 1}%
\end{align*}
\end{dusl}

These corollaries required joint convergence (Theorem \ref{thm:convergence_vectors}). However note that the same result as stated in Theorem \ref{thm:convergence_vectors} \emph{does not} hold for convergence in distribution:
\[
X_{n}\overset{d}{\longrightarrow}X\text{ and }Y_{n}\overset{d}{\longrightarrow
}Y\nRightarrow\left(  X_{n},Y_{n}\right)  \overset{d}{\longrightarrow}\left(
X,Y\right)
\]
To see this, consider the following exercise:

\begin{example}
Assume two independent dice. Let $X_n = X = Y_n$ be the number rolled with the first die, $Y$ be the number thrown with the second die. Then $X_n$, $X$, $Y_n$, $Y$ all have the same marginal distribution, but while $X_n,Y_n$ are perfectly correlated, $X,Y$ are independent, co the joint distribution of $(X_n,Y_n)$ cannot converge to the joint distribution of $(X,Y)$.  
\end{example}
However, the following theorem holds:

\begin{theorem} (T4) \label{thm:S3T4}
\[
X_{n}\overset{d}{\longrightarrow}X\text{ and }Y_{n}\overset{P}{\longrightarrow
}Y_{0}\Longrightarrow\left(  X_{n},Y_{n}\right)  \overset{d}{\longrightarrow
}\left(  X,Y_{0}\right)
\]
\end{theorem}

\begin{remark}
Recall that $Y_{n}\overset{d}{\longrightarrow}Y_{0}\Longleftrightarrow Y_{n}\overset {P}{\longrightarrow}Y_{0}$
\end{remark}

\begin{theorem} (T5) [Continuous Mapping Theorem (convergence in distribution)]\label{thm:CMT2}\label{thm:S3T5}
For any continuous function $g$, we have
\[
X_{n}\overset{d}{\longrightarrow}X\text{ }\Longrightarrow g\left(
X_{n}\right)  \overset{d}{\longrightarrow}g\left(  X\right)  \text{ }%
\]
\
\end{theorem}

\begin{dusl} (C5a) [Slutsky]\label{thm:S3C5a}
Let $X_{n}\overset{d}{\longrightarrow}X\text{ and }Y_{n}\overset{d}{\longrightarrow}Y_0$. Then
\begin{align*}
(1) && X_{n}+Y_{n}\overset{d}{\longrightarrow}  & X+Y_0\\
(2) && X_{n}Y_{n}\overset{d}{\longrightarrow}  & XY_0\\
(3) && X_{n}/Y_{n}\overset{d}{\longrightarrow}  & X/Y_0\quad\text{if }y\neq0
\end{align*}
\end{dusl}

\begin{dusl} (C5b)\label{thm:S3C5b}
Let $X_{n}\overset{d}{\longrightarrow}X\text{ and }Y_{n}\overset{P}{\longrightarrow}Y_0$, and $A_n\overset{P}\longrightarrow A_0$. Then
\begin{align*}
(1) && X_{n}+Y_{n}\overset{d}{\longrightarrow}  & X+Y_0\\
(2) && A_{n}X_{n}\overset{d}{\longrightarrow}  & A_0 X\\
\end{align*}
\end{dusl}

\begin{theorem} (T6)\label{thm:S3T6}
\[
X_{n}\overset{d}{\longrightarrow}X\text{ and }X_{n}-Y_{n}\overset
{P}{\longrightarrow}0\Longrightarrow Y_{n}\overset{d}{\longrightarrow}X
\]
\begin{proof}
This is an application of the immediately preceding corollary, namely $Y_n = X_n + Y_n-X_n$ where $Z_n = Y_n - X_n \overset{d}\longrightarrow 0$.
\end{proof}
\end{theorem}

\begin{theorem} (T10) [Weak Law of Large Numbers]\label{thm:S3T10}
Let $\{X_i\}_{i=1}^\infty$ be a sequence of iid random vectors with $E(\Vert X_i\Vert^2)<\infty$. Then $\overline{X_n} \overset{P}\longrightarrow EX_i$. 
\begin{proof} (Idea)
We prove convergence in mean square, which is easy, since there is no bias and $\Var(\overline{X_n})=\sigma^2/n \to 0$. Mean square convergence then implies convergence in probability.
\end{proof}
\end{theorem}

\begin{theorem} (T11) [Khintchine Law of Large Numbers]\label{thm:S3T11}
Let $\{X_i\}_{i=1}^\infty$ be a sequence of iid random vectors with $E(\Vert X_i\Vert)<\infty$. Then $\overline{X_n} \overset{P}\longrightarrow EX_i$. 
\end{theorem}

\begin{theorem} (T12) [Markov's Law of Large Numbers]\label{thm:S3T12}
Let $\{X_i\}_{i=1}^\infty$ be a sequence of independent random vectors, and there exists $\delta > 0$ s.t. $\forall i,\ E(\Vert X_i\Vert^{1+\delta})<n<\infty$. Then $\overline{X_n} \overset{P}-E\overline{X_n}\longrightarrow 0$. 
\end{theorem}

\begin{define}
A process is called \emph{strictly stationary} if $\forall i$ and $\forall j_1,\ldots, j_m$, the joint distribution of $(X_i, X_{i+j_1}, \ldots, X_{i+j_m})$ depends only on $j_1,\ldots, j_m$, and not on $i$.

A process is said to be \emph{weakly stationary} if $\forall i,j,\ EX_i = \mu$ and $\mathrm{Cov}(X_i, X_{i+j})=\sigma_j$.
\end{define}

\begin{theorem} (T13) [Law of Large Numbers for stationary data]\label{thm:S3T13}
Let $\{X_i\}_{i=1}^\infty$ be a sequence of weakly stationary random vectors, with $E(\Vert X_i\Vert^{2})<\infty$ and $\sum_{j=1}^\infty \Vert \mathrm{Cov}(X_i,X_{i+j}) < \infty$. Then $\overline{X_n} \overset{P}\longrightarrow EX_i$. 
\end{theorem}

\begin{example}
Some applications of the previous theorems.
\begin{enumerate}
\item
$\chi^2_n / n \overset{P}\longrightarrow 1$. Apply law of large numbers together with the fact that for $X\sim \chi^2_1$, $EX=1$.
\item
$t_n \overset{d}\longrightarrow N(0,1)$. Recall that $t_n =\frac{z}{\sqrt{\chi^2_n/n}}$, and $\sqrt{\chi^2_n/n} \overset{P}\longrightarrow 1$ by continuous mapping theorem. Thus $t\overset{d}\longrightarrow N(0,1)$ by Slutsky.
\item
$n_1 F(n_1,n_2)\overset{d}\longrightarrow \chi^2_{n_1}$ for $n_2\to \infty$. This requires same reasoning as before, recall that $F=(\chi^2_{n_1}/n_1)/(\chi^2_{n_2}/n_2)$.
\end{enumerate}
\end{example}

\begin{theorem} (T14) [Lindberg-Levy Central Limit Theorem]\label{thm:S3T14}
Let $X_i$ be iid random vectors with $EX_i = \mu$ and $V(X_i) = \Sigma$. Then
$$\sqrt{n}(\overline{X}_n-\mu)\overset{d}\longrightarrow N(0,\Sigma) \qquad\text{or}\qquad \sqrt{n} \Sigma^{-1/2}(\overline{X}_n-\mu)\overset{d}\longrightarrow N(0,I).$$
\begin{proof}(Sketch)
Let $X_i$ be iid random variables with expectation $\mu$ and variance $\sigma^2$. Assume that the moment generating function $M_{X_i}(t)=E(\exp(tY))$ exists in the neighborhood of 0 (not necessary!).

Define $Y_i = \frac{X_i-\mu}{\sigma}$. Then
\begin{eqnarray*}
M_{Y_i}'(0) &=& EY = 0 \\
M_{Y_i}''(0) &=& V(Y) + (E(Y))^2 = 1
\end{eqnarray*}

Define further
\[
Z_{n}=\frac{1}{\sqrt{n}}\left(  \sum_{i=1}^{n}\frac{X_{i}-\mu}{\sigma}\right)
=\frac{1}{\sqrt{n}}\sum_{i=1}^{n}Y_{i}%
\]
Then the corresponding moment generating function is
\begin{align*}
M_{Z_{n}}\left(  t\right)    & =E\left[  \exp\left(  t\sum_{i=1}^{n}%
Y_{i}/\sqrt{n}\right)  \right]  =E\left[  \prod\limits_{i=1}^{n}\exp\left(
tY_{i}/\sqrt{n}\right)  \right]  =\left\{  \text{by independence}\right\}
=\\
& =\prod\limits_{i=1}^{n}E\left[  \exp\left(  tY_{i}/\sqrt{n}\right)  \right]
=\prod\limits_{i=1}^{n}M_{Y}\left(  t/\sqrt{n}\right)  =\left[  M_{Y}\left(
t/\sqrt{n}\right)  \right]  ^{n}%
\end{align*}
Using Taylor series expansion for $M_{Y}\left(  t/\sqrt{n}\right)$ around zero, we get
\[
M_{Y}\left(  t/\sqrt{n}\right)  =\underset{1}{\underbrace{M_{Y}\left(
0\right)  }}+\underset{0}{\underbrace{M_{Y}^{\prime}\left(  0\right)
t/\sqrt{n}}}+M_{Y}^{\prime\prime}\left(  \tilde{t}\right)  t^{2}/2n
\]
for some $\tilde{t} \in [0,t/\sqrt{n}]$. Then
\[
\left[  M_{Y}\left(  t/\sqrt{n}\right)  \right]  ^{n}=\left[  1+M_{Y}%
^{\prime\prime}\left(  \tilde{t}\right)  t^{2}/2n\right]  ^{n}.%
\]
Taking the limit as $n\to\infty$, we have $\tilde{t}\to 0$ (since $\tilde{t} \in [0,t/\sqrt{n}]$). Thus, since $M''_Y$ is continuous at $t=0$,
\[
M_{Y}^{\prime\prime}\left(  \tilde{t}\right)  \longrightarrow M_{Y}%
^{\prime\prime}\left(  0\right)  =1.
\]
Therefore
\[
\lim_{n\rightarrow\infty}\,\left[  M_{Y}\left(  t/\sqrt{n}\right)  \right]
^{n}=\lim_{n\rightarrow\infty}\,\left[  1+M_{Y}^{\prime\prime}\left(
\tilde{t}\right)  t^{2}/2n\right]  ^{n}=\exp\left[  t^{2}/2\right]
\]
since $\lim_{n\to\infty}(1+a/n)^n = \exp(a)$. But $\exp(t^2/2)$ is the MGF of a standard normal distribution, which completes the proof.
\end{proof}
\end{theorem}

\begin{theorem} (T15) [Lindberg-Feller Central Limit Theorem]\label{thm:S3T15}
Let $X_i$ be independent random vectors s.t.
\begin{enumerate}
\item
$\E X_i = \mu_i$ finite $\forall i$. Define $\bar\mu_n = \frac1n \sum_{i=1}^n\mu_i$.
\item
$\V(X_i) = Q_i$ finite $\forall i$. Define $\bar Q_n = \frac1n \sum_{i=1}^nQ_i$.
\item
All mixed $3^\text{th}$ moments of $X_i$ are finite $\forall i$.
\item
$\bar Q_n \to Q$, where $Q$ is a finite PD matrix.
\item
$\left( n\bar Q_n \right)^{-1}Q_i \to 0\ \forall i$ (to make sure that no one observation dominates the series).
\end{enumerate}
Then
$$\sqrt{n}(\bar{X}_n-\bar\mu_n)\overset{d}\longrightarrow N(0,Q) \qquad\text{or}\qquad \sqrt{n} \bar Q_n^{-1/2}(\bar{X}_n-\bar\mu_n)\overset{d}\longrightarrow N(0,I).$$
\end{theorem}

\begin{example}
Toss a fair coin 900 times. We are interested in the probability of obtaining more than 495 heads.

\emph{Solution:} Let $X_i = 1$ if head on $i$-th toss, and $X_i = 0$ otherwise. Then $X_i \sim Ber(1/2)$, and we know $\E(X_i)=1/2$, $\V(X_i)=1/4$. The number of heads in 900 attempts is $H=\sum_{i=1}^{900}X_i$. We also know that for $\bar X = H/n$, $\E(\bar X)=1/2$ and $\V(\bar X)=1/(4\cdot 900)$.

We know that $\sqrt{n} \Sigma^{-1/2}(\overline{X}_n-\mu)\overset{d}\longrightarrow N(0,I)$, so that in our case
$$\sqrt{900} \cdot 2 \cdot(\overline{X}_n-\mu) = \frac{2}{\sqrt{900}}(H-450) = \frac{1}{15}(H-450)$$
will have approximately standard distribution. Therefore
$$P(H>495) = P\left(\frac{H-450}{15} >\frac{495-450}{15}\right) \approx P(Z>3) = 1-\Phi(3) \approx 0.0013.$$
\end{example}

\begin{theorem} (T16) [Delta method]\label{thm:S3T16}
If $Z_n$ is a sequence of $k\times1$ vectors s.t. $\sqrt{n}(Z_n-Z_0)\overset{d}\longrightarrow N(0,\Sigma)$ for some $k\times1$ vector $Z_0$, and $g:\R^k\to\R^J$, ($J\le K$) is continuously differentiable at $Z_0$, then
$$ \sqrt{n}(g(Z_n)-g(Z_0)) \overset{d}\longrightarrow N(0,G\Sigma G')$$
where $G=\frac{\partial g(Z_0}{\partial Z'}$ is the $J\times k$ matrix of partial derivatives at 0.
\begin{proof}(Sketch)
By Taylor expansion,
$$\sqrt{n}(g(Z_n)-g(Z_0))=\frac{\partial g(\bar Z)}{\partial Z'}\sqrt{n}(Z_n-Z_0)$$ for some $\bar Z$ between $Z_n$ and $Z_0$. Further,
$$\sqrt{n}(Z_n-Z_0)\tod N(0,\Sigma) \Longrightarrow Z_n-Z_0 = \frac{1}{\sqrt n} \sqrt n (Z_n-Z_0) \tod 0\cdot N(0,\Sigma) = 0$$
(by Slutsky). Then $Z_n-Z_0\tod 0 \Longrightarrow Z_n-Z_0\toP 0$ (convergence to a constant is equivalent in distribution and in probability). Therefore $Z_n\toP Z_0 \Longrightarrow \bar Z \toP Z_0$, since $\bar Z$ is the mean. Then, by continuous mapping theorem and the fact that $G$ is continuously differentiable,
$$\frac{\partial g(\bar Z)}{\partial Z'} \toP G$$
and then by Slutsky
$$\underset{\toP G}{\underbrace{\frac{\partial g(\bar Z)}{\partial Z'}}} \underset{\tod N(0,\Sigma)}{\underbrace{\sqrt{n}(Z_n-Z_0) \tod G\cdot N(0,\Sigma)}} = N(0,G\Sigma G'),$$
which gives our result.
\end{proof}
\end{theorem}

\begin{example}(Delta method)
Suppose $\hat\theta$ is a random variable for which we know that
$$\sqrt n (\hat \theta - \theta)\tod N(0,\sigma^2).$$
We are interested in the distribution of $g(\hat\theta)=\exp(\hat\theta)$.

\emph{Solution:} $g'(\theta) = \exp(\theta)$, and then
$$\sqrt n (g(\hat\theta)-g(\theta)) \tod \exp(\theta)\cdot N(0, \sigma^2) = N(0, \exp(2\theta)\sigma^2).$$  
\end{example}


% ==============================================================================
% FROM TA SESSION 061110
\subsection{$o_p$, $O_p$ concepts}

\begin{definition}
(Little oh p) $\left\{ X_{n}\right\} $ is $o_{p}(n^{\delta })$ for $\delta
\in \mathbb{R} $ iff $\frac{X_{n}}{n^{\delta }}\overset{P}{\longrightarrow }0$ (also write $%
X_{n}=o_{p}(n^{d})$)

When $\delta =0$, $\left\{ X_{n}\right\} $ is $o_{p}(1)$ iff $X_{n}\overset{P%
}{\longrightarrow }0$. Thus little oh p one means that the sequence
converges in probability to 0.

$X_{n}\overset{P}{\longrightarrow }X$, write $X_{n}=X+o_{p}(1)$.
\end{definition}

\begin{definition}
(Big oh p) $\left\{ X_{n}\right\} $ is $O_{p}(n^{\delta })$ iff $\forall
\varepsilon >0$, $\exists M_{\varepsilon }<\infty $ and $n_{\varepsilon }$
s.t. $P\left[ \frac{\left\vert X_{n}\right\vert }{n^{\delta }}%
>M_{\varepsilon }\right] <\varepsilon $ for all $n\geq n_{\varepsilon }$.

When $\delta =0$, $\left\{ X_{n}\right\} $ is $O_{p}(1)$ iff $\forall
\varepsilon >0$, $\exists M_{\varepsilon }<\infty $ and $n_{\varepsilon }$
s.t. $P\left[ \left\vert X_{n}\right\vert >M_{\varepsilon }\right]
<\varepsilon $ for all $n\geq n_{\varepsilon }$. Thus big oh p one means
that the sequence is stochastically bounded, or bounded in probability.
\end{definition}

\begin{example}
Let $c$ be a constant, define $X_{n}=\sqrt{n}c$. Then $X_{n}=O_{p}(n^{1/2})$
and $X_{n}=o_{p}(n^{\sigma })~\forall \delta >1/2$.
\end{example}

\begin{remark}
(Fact 1) If $X_{n}=o_{p}(1)$ then $X_{n}=O_{p}(1)$.

Why? $X_{n}=o_{p}(1)\Longleftrightarrow \forall \varepsilon >0:\lim P\left[
\left\vert X_{n}\right\vert >\varepsilon \right] =0$. Take $M_{\varepsilon
}=\varepsilon $ and $n_{\varepsilon }$ large enough so that for $%
n>n_{\varepsilon }$%
\begin{equation*}
P(\left\vert X_{n}\right\vert >M_{\varepsilon })=P(\left\vert
X_{n}\right\vert >\varepsilon )<\varepsilon .
\end{equation*}
\end{remark}

\begin{remark}
(Fact 2) \bigskip If $X_{n}\overset{d}{\longrightarrow }X$, then $%
X_{n}=O_{p}(1)$.

Why? Assume $F_{X_{n}}$ is the cdf of $X_{n}$. Then 
\begin{eqnarray*}
P\left[ \left\vert X_{n}\right\vert >M_{\varepsilon }\right]  &=&P\left[
X_{n}>M_{\varepsilon }\text{ or }X_{n}<-M_{\varepsilon }\right] = \\
&=&1-F_{X_{n}}(M_{\varepsilon })+F_{X_{n}}(-M_{\varepsilon })\overset{d}{%
\longrightarrow }1-F_{X}\left( M_{\varepsilon }\right)
+F_{X}(-M_{\varepsilon })\longrightarrow 0
\end{eqnarray*}

If we choose $M_{\varepsilon }$ large enough so that $F_{X}(M_{\varepsilon
})\longrightarrow 1$ and $F_{X}(-M_{\varepsilon })\longrightarrow 0$. So we
can choose $M_{\varepsilon }$ so that $P(\left\vert X_{n}\right\vert
>n_{\varepsilon })<\varepsilon $.
\end{remark}

\begin{example}
\bigskip $\left\{ U_{i}\right\} _{i=1}^{\infty }$ of iid RVs. $E(U_{i})=0$, $%
Var(U_{i})=\sigma ^{2}$.

Then $\frac{1}{N}\sum U_{i}\overset{P}{\longrightarrow }0$, so $\frac{1}{N}%
\sum U_{i}=o_{p}(1)$.

Also $\frac{1}{\sqrt{N}}\frac{\sqrt{N}}{N}\sum U_{i}\overset{d}{%
\longrightarrow }N(0,\sigma ^{2})$, so that $\sum U_{i}=O_{p}(N^{1/2})$ or $%
\frac{\sum U_{i}}{N^{1/2}}=O_{p}(1)$.
\end{example}

\begin{remark}
(Fact 3) If $X_{n}=O_{p}(1)$ and $Y_{n}=O_{p}(1)$, then $X_{n}Y_{n}=O_{p}(1)$
and $X_{n}+Y_{n}=O_{p}(1)$.
\end{remark}

\begin{remark}
(Fact 4) If $X_{n}=o_{p}(1)$ and $Y_{n}=o_{p}(1)$, then $X_{n}Y_{n}=o_{p}(1)$
and $X_{n}+Y_{n}=o_{p}(1)$.
\end{remark}

\begin{remark}
(Fact 5) If $X_{n}=O_{p}(1)$ and $Y_{n}=o_{p}(1)$, then $X_{n}Y_{n}=o_{p}(1)$
and $X_{n}+Y_{n}=O_{p}(1)$.
\end{remark}

\begin{example}
Proof of first part:%
\begin{eqnarray*}
P\left( \left\vert X_{n}Y_{n}\right\vert >\varepsilon \right) 
&=&P(\left\vert X_{n}Y_{n}\right\vert >\varepsilon \text{ }\wedge \text{ }%
\left\vert X_{n}\right\vert >M_{\varepsilon })+P(\left\vert
X_{n}Y_{n}\right\vert >\varepsilon \text{ }\wedge \text{ }\left\vert
X_{n}\right\vert \leq M_{\varepsilon })\leq  \\
&\leq &P(\left\vert X_{n}\right\vert >M_{\varepsilon })+P(M_{\varepsilon
}\left\vert Y_{n}\right\vert >\varepsilon )<\delta 
\end{eqnarray*}%
when $M_{e}$ is large and $n\rightarrow \infty $.
\end{example}

\begin{example}
\bigskip Consider $\left( X_{n}\right) _{j}=o_{p}(1)$ for $j=1,\ldots ,J$.
Let $Y_{n}=\sum_{j=1}^{J}X_{n}$.

\begin{enumerate}
\item Show that $Y_{n}=o_{p}(1)$ for finite $J$.

Straightforward, just apply Fact\ 4.

\item Show that as $J\rightarrow \infty $, $Y_{n}$ may not be $o_{p}(1)$ or
even $O_{p}(1)$.

Counterexample: $\left( X_{n}\right) _{j}=Z_{n}\left( \frac{j}{n}\right) $
where $Z_{n}\sim Unif\left[ 0,1\right] $. Surely $\left( X_{n}\right)
_{j}=o_{p}(1)$ for all $j<\infty $.

But $Y_{n}=\sum_{j=1}^{J}\left( X_{n}\right) _{j}=\sum_{j=1}^{J}Z_{n}\left( 
\frac{j}{n}\right) =\frac{Z_{n}}{n}\frac{J(J+1)}{2}\longrightarrow \infty $
as $J\longrightarrow \infty $ and $n\longrightarrow \infty $.
\end{enumerate}
\end{example}


% ==============================================================================
\subsection{Review of hypothesis testing}
Whenever we want to test a $H_0$ against $H_1$ (e.g. $H_0: \E(X_i)=0, H_1:\E(X_i)\ne 0$), we use a \emph{test statistic} based on data. A test statistic is thus a random variable.

The test procedure is based on deciding whether or not we reject $H_0$ by comparing the realization of the test statistic to some present value (called \emph{critical value}) of the test statistic. If the realization of the test statistic falls inside some \emph{critical region}, we reject $H_0$. Otherwise, we cannot reject $H_0$.

\emph{Type I error:} we reject $H_0$, although it is true.

\emph{Type II error:} we do not reject $H_0$, although it is false.
 
\emph{Size} of the test $\equiv \alpha =$ maximum probability of Type I error. This is also called the \emph{significance level} of the test.

\emph{Power} of the test $\equiv \tilde\beta = 1-P(\text{Type II error}) = 1-\beta$. The power of the test will generally vary over $\H_1$, the alternative parameter space.

\begin{figure}[!t]
\begin{picture}(120,70)
\put(0,5){\line(1,0){120}}
\multiput(40,4)(30,0){2}{\line(0,1){2}}

\qbezier(20,35)(40,80)(60,35)
\qbezier(20,35)(10,12.5)(0,10)
\qbezier(60,35)(70,12.5)(80,10)
\qbezier(120,6)(90,7.5)(80,10)

\qbezier(50,35)(70,80)(90,35)
\qbezier(50,35)(40,12.5)(30,10)
\qbezier(90,35)(100,12.5)(120,8.5)
\qbezier(0,7)(20,7.5)(30,10)

\put(39.5,1){$0$}
\put(69.5,1){$\mu$}
\put(18.5,1){$t_{\alpha/2}$}
\put(57.5,1){$t_{1-\alpha/2}$}
\put(115.5,1){$\theta$}

\put(10,45){null ($F_0$)}
\put(91,35){true ($F_1$)}

\multiput(20,4)(40,0){2}{\line(0,1){31}}

\end{picture}
\caption{Hypothesis testing scheme.}\label{fig:testexample}
\end{figure}

\begin{example}
Let assume $H_0: \E(X_i)=0$ vs. $H_1: \E(X_i)\ne 0$. The situation is captured in Figure \ref{fig:testexample}. The critical region is $(-\infty,t_{\alpha/2}) \cup (t_{1-\alpha/2},\infty)$.
\begin{eqnarray*}
\text{Size of the test}&=& P(\text{type I error}) =\int_{\text{crit.region}}\d F_0(\theta) = \alpha \\
\text{Power of the test}&=& 1-P(\text{type II error})=1-\beta = 1-\int_{(\text{crit.region})^C}\d F_1(\theta) = \int_{\text{crit.region}}\d F_1(\theta)
\end{eqnarray*} 
\end{example}

A test that has greater power (smaller $\beta$) than all other tests of the same size (same $\alpha$) is called \emph{most powerful} (for a given value of the parameter to be tested).

A test which is most powerful for all possible values of the underlying parameter is called the \emph{uniformly most powerful test}.