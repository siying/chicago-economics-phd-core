
\documentclass{article}
\usepackage{amsfonts}
\usepackage{amsmath}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TCIDATA{OutputFilter=LATEX.DLL}
%TCIDATA{Version=5.50.0.2953}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{Created=Wednesday, November 08, 2006 10:38:17}
%TCIDATA{LastRevised=Wednesday, November 08, 2006 14:52:05}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{<META NAME="DocumentShell" CONTENT="Standard LaTeX\Blank - Standard LaTeX Article">}
%TCIDATA{CSTFile=40 LaTeX article.cst}

\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\newenvironment{proof}[1][Proof]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\input{tcilatex}

\begin{document}


\section{(Section \#11) Introduction to Maximum Likelihood Estimation}

\emph{Intuition}: If $\theta $ is the right vaue of the parameter, then the
likelihood of observing the sample $Z_{i}$ according to our model $%
f(Z_{i};\theta )$ should be large. This suggests the following estimator:%
\[
\hat{\theta}=\arg \max_{\theta }\prod_{i=1}^{n}f(Z_{i};\theta )=\arg
\max_{\theta }L(Z_{i},\theta ),
\]%
where $f(Z_{i};\theta )$ is the density of $Z_{i}$ according to our model
for a given value of $\theta $.

We assume iid observations to factor out the likelihood as a product of the
likelihoods of each observation.

For convenience, the MLE\ estimator is usuallly written in terms of the
log-likelihood:%
\[
\hat{\theta}=\arg \max_{\theta }\frac{1}{n}\sum_{i=1}^{n}\ln f(Z_{i};\theta
)=\arg \max_{\theta }\tciLaplace (Z_{i},\theta ),
\]

Since $\ln $ is a strictly increasing transformation, the $\arg \max $ will
stay the same.

$\frac{1}{n}$ also does not change the location of the maximum, so that the
objective function becomes a sample average.

In the limit as $n\rightarrow \infty $, we obtain the population version for
the true parameter vector $\theta _{0}$:%
\[
\theta _{0}=\arg \max_{\theta }E\left[ \ln f(Z_{i};\theta )\right] 
\]

The first order conditions for the maximization problem are%
\[
\underset{k\times 1}{\left. \frac{\partial E\left[ \ln f(Z_{i};\theta )%
\right] }{\partial \theta }\right\vert _{\theta =\theta _{0}}}=0
\]

Under regularity conditions, the first order conditions can be written as%
\[
E\left[ s(Z_{i};\theta )\right] =0\qquad \text{where}\qquad s(Z_{i};\theta )=%
\frac{\partial \ln f(Z_{i};\theta )}{\partial \theta }
\]%
is called the \emph{score}.

Economic models often take the form of a conditional likelihood $f(Z_{i}\mid
X_{i};\theta )$ (as in OLS). It is easy to see that if $\theta $ does not
affect the distribution of $X_{i}$, (so that $f(X_{i};\theta )=f(X_{i})$),
then maximizing the likelihood $f((Z_{i},X_{i});\theta )$ or the conditional
likelihood $f(Z_{i}\mid X_{i};\theta )$ gives the same estimator:%
\begin{eqnarray*}
\hat{\theta} &=&\arg \max_{\theta }\frac{1}{n}\sum_{i=1}^{n}\ln f(\left(
Z_{i},X_{i}\right) ;\theta )=\hat{\theta}= \\
&=&\arg \max_{\theta }\left( \frac{1}{n}\sum_{i=1}^{n}\ln f(Z_{i}\mid
X_{i};\theta )+\frac{1}{n}\sum_{i=1}^{n}\ln f(X_{i})\right) = \\
&=&\arg \max_{\theta }\frac{1}{n}\sum_{i=1}^{n}\ln f(Z_{i}\mid X_{i};\theta )
\end{eqnarray*}

\subsection{Cramer-Rao Lower Bound}

Let $Z$ denote the sample $(Z_{1},\ldots ,Z_{n})$ and consider an estimator $%
t(Z)$ of $\theta $ (a scalar for simplicity). Then $E\left[ t(Z)\right]
=\int t(Z)f(Z;\theta )dZ$, where $f(Z;\theta )=\prod_{i=1}^{n}f(Z_{i};\theta
)$ and the integral goes over all observations, i.e. $dZ=dZ_{1}\cdot \ldots
\cdot dZ_{n}$.

Under regularity conditions (i.e. if we can interchange the partial
derivative and the integral):%
\begin{eqnarray}
\frac{\partial E\left[ t(Z)\right] }{\partial \theta } &=&\int t(Z)\frac{%
\partial f(Z;\theta )}{\partial \theta }dZ=\int t(Z)\frac{\partial \ln
f(Z;\theta )}{\partial \theta }f(Z;\theta )dZ=  \nonumber \\
&=&Cov\left[ t(Z),\frac{\partial \ln f(Z;\theta )}{\partial \theta }\right] 
\label{eq:s11EtZ}
\end{eqnarray}%
because%
\[
E\left[ \frac{\partial \ln f(Z;\theta )}{\partial \theta }\right]
=\sum_{i=1}^{n}E\left[ \frac{\partial \ln f(Z;\theta )}{\partial \theta }%
\right] =0
\]%
by the first order conditions of the MLE.

By the Cauchy-Schwartz inequality we have%
\begin{eqnarray}
\left( Cov\left[ t(Z),\frac{\partial \ln f(Z;\theta )}{\partial \theta }%
\right] \right) ^{2} &\leq &Var(t(Z))Var\left( \frac{\partial \ln f(Z;\theta
)}{\partial \theta }\right)   \nonumber \\
&=&nVar(t(Z))\cdot \underset{=J}{\underbrace{\frac{1}{n}Var\left( \frac{%
\partial \ln f(Z;\theta )}{\partial \theta }\right) }}  \label{eq:s11J}
\end{eqnarray}%
where%
\begin{eqnarray*}
J &=&\frac{1}{n}Var\left( \frac{\partial \ln \prod_{i=1}^{n}f(Z_{i};\theta )%
}{\partial \theta }\right) =\frac{1}{n}\sum_{i=1}^{n}Var\left( \frac{%
\partial \ln f(Z_{i};\theta )}{\partial \theta }\right) = \\
&=&Var\left( \frac{\partial \ln f(Z_{i};\theta )}{\partial \theta }\right) 
\end{eqnarray*}%
Now equation (\ref{eq:s11J}) can be written as%
\begin{eqnarray*}
nVar(t(Z)) &\geq &J^{-1}\left( Cov\left[ t(Z),\frac{\partial \ln f(Z;\theta )%
}{\partial \theta }\right] \right) ^{2}= \\
&=&J^{-1}\left( \frac{\partial E\left[ t(Z)\right] }{\partial \theta }%
\right) ^{2}
\end{eqnarray*}%
from equation (\ref{eq:s11EtZ}).

If $t(Z)$ is unbiased, then $E\left[ t(Z)\right] =\theta $, so that $\frac{%
\partial E\left[ t(Z)\right] }{\partial \theta }=1$, so that we get%
\[
nVar(t(Z))\geq J^{-1}
\]%
This is the \emph{Cramer-Rao lower bound}.

There is also a vector version of for $\theta $ being a $k\times 1$ vector:%
\[
nVar(t(Z))-J^{-1}\text{ \ is positive semidefinite,}
\]%
where%
\[
\underset{k\times k}{J}=Var\left[ \frac{\partial \ln f(Z;\theta )}{\partial
\theta }\right] =E\left[ \frac{\partial \ln f(Z;\theta )}{\partial \theta }%
\frac{\partial \ln f(Z;\theta )}{\partial \theta }\right] 
\]%
since $E\left[ \frac{\partial \ln f(Z;\theta )}{\partial \theta }\right] =0$.

\subsection{Information Matrix Equality}

The population version of the first order conditions for the MLE are $E\left[
s(Z_{i};\theta )\right] =0$:%
\begin{equation}
\int \frac{\partial \ln f(Z_{i};\theta )}{\partial \theta }f(Z_{i};\theta
)dZ_{i}=0  \label{eq:s11IME}
\end{equation}%
Note that $f(Z_{i};\theta )$ is the same for both $f(Z_{i};\theta )$ in the
expression above if the model is correctly specified. The derivative of
equation (\ref{eq:s11IME}) with respect to $\theta $ is therefore also $0$:%
\begin{equation}
\int \underset{k\times k}{\frac{\partial ^{2}\ln f(Z_{i};\theta )}{\partial
\theta \partial \theta ^{\prime }}}f(Z_{i};\theta )dZ_{i}+\int \underset{%
k\times 1}{\frac{\partial \ln f(Z_{i};\theta )}{\partial \theta }}\underset{%
1\times k}{\frac{\partial f(Z_{i};\theta )}{\partial \theta ^{\prime }}}%
dZ_{i}=0\text{,}  \label{eq:s11IME2}
\end{equation}%
where $\frac{\partial ^{2}\ln f(Z_{i};\theta )}{\partial \theta \partial
\theta ^{\prime }}$ is the matrix of second derivatives of $\ln
f(Z_{i};\theta )$.

Further we note that%
\[
\frac{\partial \ln f(Z_{i};\theta )}{\partial \theta ^{\prime }}=\frac{1}{%
f(Z_{i};\theta )}\frac{\partial f(Z_{i};\theta )}{\partial \theta ^{\prime }}
\]%
so that equation (\ref{eq:s11IME2}) becomes%
\[
\int \frac{\partial ^{2}\ln f(Z_{i};\theta )}{\partial \theta \partial
\theta ^{\prime }}f(Z_{i};\theta )dZ_{i}+\int \frac{\partial \ln
f(Z_{i};\theta )}{\partial \theta }\frac{\partial \ln f(Z_{i};\theta )}{%
\partial \theta ^{\prime }}f(Z_{i};\theta )dZ_{i}=0
\]%
or%
\[
E\left[ \frac{\partial ^{2}\ln f(Z_{i};\theta )}{\partial \theta \partial
\theta ^{\prime }}\right] =-E\left[ \frac{\partial \ln f(Z_{i};\theta )}{%
\partial \theta }\frac{\partial \ln f(Z_{i};\theta )}{\partial \theta
^{\prime }}\right] 
\]%
which is the \emph{information matrix equality}. Thus, under regularity
conditions, we conclude

\begin{description}
\item[-] $-E\left[ \frac{\partial ^{2}\ln f(Z_{i};\theta )}{\partial \theta
\partial \theta ^{\prime }}\right] $ is also the CRLB

\item[-] This expression will lead to simplifications in the asymptotic
variance for MLE.
\end{description}

\subsection{OLS as a MLE}

Assumptions: (A0)-(A2') (including $\varepsilon \sim N(0,\sigma ^{2}I)\mid X$%
) of Section \ref{sec:4} (\#4).

Then we can write the likelihood of the OLS\ model as%
\[
L(Y\mid X;\beta ,\sigma ^{2})=L(\beta ,\sigma ^{2})=\left( 2\pi \sigma
^{2}\right) ^{-n/2}\exp \left[ -(Y-X\beta )^{\prime }(Y-X\beta )/2\sigma ^{2}%
\right] 
\]%
Thus in terms of the log-likelihood we obtain%
\[
\tciLaplace (\beta ,\sigma ^{2})=\frac{1}{n}\ln L=K-\frac{1}{2}\ln \sigma
^{2}-\frac{1}{2n\sigma ^{2}}\underset{s_{n}(\beta )}{\underbrace{(Y-X\beta
)^{\prime }(Y-X\beta )}}
\]%
The maximum likelihood estimator is%
\[
\left( \hat{\beta},\hat{\sigma}^{2}\right) _{MLE}=\arg \max_{\left( \beta
,\sigma ^{2}\right) }\tciLaplace (\beta ,\sigma ^{2})
\]%
The first order conditions are%
\begin{eqnarray*}
\frac{\partial \tciLaplace }{\partial \beta } &=&-\frac{1}{2n\sigma ^{2}}%
\frac{\partial s_{n}(\beta )}{\partial \beta }=\frac{-2(X^{\prime
}Y-X^{\prime }X\beta )}{-2n\sigma ^{2}}=0 \\
&\Longrightarrow &\hat{\beta}_{MLE}=(X^{\prime }X)^{-1}X^{\prime }Y=\hat{%
\beta}_{OLS}
\end{eqnarray*}%
However, the MLE\ estimator for $\sigma ^{2}$ will be different:%
\[
\frac{\partial \tciLaplace }{\partial \sigma ^{2}}=-\frac{1}{2\sigma ^{2}}+%
\frac{(Y-X\beta )^{\prime }(Y-X\beta )}{2n\sigma ^{4}}=0,
\]%
thus yielding%
\[
\hat{\sigma}_{MLE}^{2}=\frac{(Y-X\hat{\beta}_{MLE})^{\prime }(Y-X\hat{\beta}%
_{MLE})}{n}=\frac{\hat{\varepsilon}^{\prime }\hat{\varepsilon}}{n}
\]%
Thus $\hat{\sigma}_{MLE}^{2}$ is a biased estimator for $\sigma ^{2}$, since%
\[
\hat{\sigma}_{MLE}^{2}=\frac{\hat{\varepsilon}^{\prime }\hat{\varepsilon}}{n}%
=\frac{n-k}{n}s^{2}\neq s^{2}
\]%
where $s^{2}$ was shown to be an unbiased estimator of $\sigma ^{2}$.

\begin{remark}
\bigskip The fact the the MLE\ estimator is biased is nothing unusual. This
will often be the case in small samples.
\end{remark}

Solving the CRLB using the Information Matrix Equality leads to%
\begin{eqnarray*}
\frac{\partial ^{2}\tciLaplace }{\partial \beta \partial \beta ^{\prime }}
&=&-\frac{(X^{\prime }X)}{n\sigma ^{2}}\Longrightarrow E\left[ \frac{%
\partial ^{2}\tciLaplace }{\partial \beta \partial \beta ^{\prime }}\mid X%
\right] =-\frac{(X^{\prime }X)}{n\sigma ^{2}} \\
\frac{\partial ^{2}\tciLaplace }{\partial \left( \sigma ^{2}\right) ^{2}} &=&%
\frac{1}{2\sigma ^{4}}-\frac{\varepsilon ^{\prime }\varepsilon }{n\sigma ^{6}%
}\Longrightarrow E\left[ \frac{\partial ^{2}\tciLaplace }{\partial \left(
\sigma ^{2}\right) ^{2}}\mid X\right] =\frac{1}{2\sigma ^{4}}-\frac{\sigma
^{2}}{\sigma ^{6}}=-\frac{1}{2\sigma ^{4}}
\end{eqnarray*}

\[
\frac{\partial ^{2}\tciLaplace }{\partial \sigma ^{2}\partial \beta }=-\frac{%
(X^{\prime }Y-X^{\prime }X\beta )}{n\sigma ^{4}}\Longrightarrow E\left[ 
\frac{\partial ^{2}\tciLaplace }{\partial \sigma ^{2}\partial \beta }\mid X%
\right] =-\frac{X^{\prime }(E(Y\mid X)-X\beta )}{n\sigma ^{4}}=0
\]%
So that the CRLB is%
\begin{eqnarray*}
CRLB\left( 
\begin{array}{c}
\beta  \\ 
\sigma ^{2}%
\end{array}%
\right)  &=&\left[ J\left( 
\begin{array}{c}
\beta  \\ 
\sigma ^{2}%
\end{array}%
\right) \right] ^{-1}=-\left[ 
\begin{array}{cc}
\underset{k\times k}{\underbrace{E\left[ \frac{\partial ^{2}\tciLaplace }{%
\partial \beta \partial \beta ^{\prime }}\mid X\right] }} & \underset{%
k\times 1}{\underbrace{E\left[ \frac{\partial ^{2}\tciLaplace }{\partial
\sigma ^{2}\partial \beta }\mid X\right] }} \\ 
\underset{1\times k}{\underbrace{E\left[ \frac{\partial ^{2}\tciLaplace }{%
\partial \sigma ^{2}\partial \beta ^{\prime }}\mid X\right] }} & \underset{%
1\times 1}{\underbrace{E\left[ \frac{\partial ^{2}\tciLaplace }{\partial
\left( \sigma ^{2}\right) ^{2}}\mid X\right] }}%
\end{array}%
\right] ^{-1}= \\
&=&-\left[ 
\begin{array}{cc}
-\frac{(X^{\prime }X)}{n\sigma ^{2}} & 0 \\ 
0 & -\frac{1}{2\sigma ^{4}}%
\end{array}%
\right] ^{-1}=\left[ 
\begin{array}{cc}
\sigma ^{2}\left( \frac{X^{\prime }X}{n}\right) ^{-1} & 0 \\ 
0 & \frac{1}{2\sigma ^{4}}%
\end{array}%
\right] 
\end{eqnarray*}

Thus the variance of the OLS estimator in case of homoskedasticity and no
serial correlation coincides with CRLB $\Longrightarrow $ The OLS\ estimator
for $\beta $ is the best unbiased estimator (BUE)

Similarly, if $\varepsilon \sim N(0,\Omega )\mid X$, we again obtain $\hat{%
\beta}_{GLS}=\hat{\beta}_{MLE}$ so that this is again BUE for the general
specification of the variance matrix.

\section{(Section \#12) Examples of Nonlinear Models}

\subsection{Binary Choice (Probit and Logit)}

\begin{example}
An iIndividual is deciding whether to enter the labor force or stay
inactive. Utility level under both alternatives are latent variables which
depend on both observed and unobserved determinants.%
\begin{eqnarray*}
u_{1} &=&W^{\prime }\theta _{1}+\nu _{1}\text{ if in labor force }\left(
D=1\right)  \\
u_{0} &=&W^{\prime }\theta _{0}+\nu _{0}\text{ if inactive }(D=0)
\end{eqnarray*}
\end{example}

Individual decides whether to enter labor force if $u_{1}-u_{0}\geq 0$ or if 
$\nu \leq W^{\prime }\theta $ where $\nu =\nu _{0}-\nu _{1}$ and $\theta
=\theta _{1}-\theta _{0}$. If $\nu _{1}$ and $\nu _{0}$ are independent of $W
$, then%
\[
P(D=1\mid W)=P(\nu \leq W^{\prime }\theta \mid W)=P(\nu \leq W^{\prime
}\theta )=F_{\nu }(W^{\prime }\theta )\text{,}
\]%
where the second equality holds since $\nu $ is independent of $W$.

If $\nu _{1}$ and $\nu _{0}$ have a joint normal distribution, then $\nu
\sim N(0,\sigma ^{2})$ and $F_{\nu }(W^{\prime }\theta )=\Phi (W^{\prime
}\theta /\sigma )$ (since $P(\xi \leq a)=F_{\xi }(a)$, $P_{\sigma \xi
}(b)=P(\sigma \xi \leq b)=P(\xi \leq \frac{b}{\sigma })=F_{\xi }\left( \frac{%
b}{\sigma }\right) $). So%
\[
P(D=1\mid W)=\Phi (W^{\prime }\gamma )\text{ where }\gamma =\frac{\theta }{%
\sigma }
\]%
We thus cannot separately identify $\theta $ and $\sigma $. Further%
\[
\Phi (r)=\int_{-\infty }^{r}\phi (t)dt\text{ \ where }\phi (t)=\frac{1}{%
\sqrt{2\pi }}e^{-t^{2}/2}
\]%
i.e. $\Phi (\cdot )$ is the cdf of the normal distribution. This is the
so-called \emph{probit model}.

If $\nu $ has a logistic distribution, then%
\[
P(D=1\mid W)=\Lambda (W^{\prime }\gamma )=\frac{\exp (W^{\prime }\gamma )}{%
1+\exp (W^{\prime }\gamma )}
\]%
where $\Lambda (\cdot )$ is the cdf of a logistic distribution. This is the
so-called \emph{logit model}.

Consider a sample of $N$ iid observations $\left\{ D_{i},W_{i}\right\}
_{i=1}^{N}$. The likelihood is given by%
\[
\prod_{i=1}^{N}\left[ P(D=1\mid W_{i}\right] ^{D_{i}}\left[ P(D=0\mid W_{i}%
\right] ^{1-D_{i}}
\]%
The log-likelihood then is%
\[
\sum_{i=1}^{N}\left[ D_{i}\ln P(D=1\mid W_{i})+(1-D_{i})\ln P(D=0\mid W_{i})%
\right] 
\]%
Probit model maximizes%
\[
\frac{1}{N}\ln L(\gamma )=\frac{1}{N}\sum_{i=1}^{N}\left[ D_{i}\ln \Phi
\left( W_{i}^{\prime }\gamma \right) +(1-D_{i})\ln (1-\Phi \left(
W_{i}^{\prime }\gamma \right) )\right] \text{ over }\gamma 
\]%
Logit model maximizes%
\[
\frac{1}{N}\ln L(\gamma )=\frac{1}{N}\sum_{i=1}^{N}\left[ D_{i}\ln \Lambda
\left( W_{i}^{\prime }\gamma \right) +(1-D_{i})\ln (1-\Lambda \left(
W_{i}^{\prime }\gamma \right) )\right] \text{ over }\gamma 
\]

\subsection{Censored Regression (Tobit)}

\bigskip We observe a random variable $Y$, defined as

\[
Y=%
\begin{array}{cc}
0 & \text{if }Y^{\ast }\leq 0 \\ 
Y^{\ast } & \text{if }Y^{\ast }>0%
\end{array}%
\]%
where $Y^{\ast }$ is some unobserved variable (for instance, when we
maximize utility, we cannot consume negative amounts of a good)

Assume that $Y^{\ast }=X^{\prime }\beta +\varepsilon $ where $\varepsilon
\mid X\sim N(0,\sigma ^{2})$. Then%
\[
P(Y=0\mid X)=P(Y^{\ast }\leq 0\mid X)=P\left( \frac{\varepsilon }{\sigma }%
\leq -\frac{X^{\prime }\beta }{\sigma }\right) =\Phi \left( -\frac{X^{\prime
}\beta }{\sigma }\right) \text{.}
\]%
The likelihood of a sample of iid observations $\left\{ Y_{i},X_{i}\right\}
_{i=1}^{N}$ is%
\[
L\left( \beta ,\sigma ^{2}\right) =\prod_{i=1}^{N}\Phi \left( -\frac{%
X_{i}^{\prime }\beta }{\sigma }\right) ^{1\left\{ Y_{i}=0\right\} }\left( 
\frac{1}{\sigma }\cdot \phi \left( \frac{Y_{i}-X_{i}^{\prime }\beta }{\sigma 
}\right) \right) ^{1\left\{ Y_{i}>0\right\} }
\]%
where $1\left\{ \cdot \right\} $ is the indicator function. We can thus do
MLE.

Le us first see that OLS\ would not be consistent here:

\begin{eqnarray*}
E\left[ Y\mid X\right]  &=&\underset{=0}{\underbrace{E\left[ Y\mid X,Y=0%
\right] }}P(Y=0\mid X)+E\left[ Y\mid X,Y>0\right] P(Y>0\mid X)= \\
&=&E\left[ Y\mid X,Y>0\right] P(Y>0\mid X)
\end{eqnarray*}%
We have%
\begin{eqnarray*}
E\left[ Y\mid X,Y>0\right]  &=&E\left[ Y^{\ast }\mid X,Y^{\ast }>0\right] =E%
\left[ X^{\prime }\beta +\varepsilon \mid X,X^{\prime }\beta +\varepsilon >0%
\right] = \\
&=&X^{\prime }\beta +E\left[ \varepsilon \mid \varepsilon >-X^{\prime }\beta %
\right] =X^{\prime }\beta +\sigma E\left[ \frac{\varepsilon }{\sigma }\mid 
\frac{\varepsilon }{\sigma }>-\frac{X^{\prime }\beta }{\sigma }\right] 
\end{eqnarray*}%
Since $\frac{\varepsilon }{\sigma }\sim N(0,1)$, then%
\[
E\left[ \frac{\varepsilon }{\sigma }\mid \frac{\varepsilon }{\sigma }>-\frac{%
X^{\prime }\beta }{\sigma }\right] =\frac{1}{P\left( \frac{\varepsilon }{%
\sigma }>-\frac{X^{\prime }\beta }{\sigma }\right) }\int_{-\frac{X^{\prime
}\beta }{\sigma }}^{\infty }t\cdot \phi (t)dt
\]%
(note that $\frac{\phi }{P(\cdot )}$ is the proper distribution that
itnegrates to $1$ on $\int_{-\frac{X^{\prime }\beta }{\sigma }}^{\infty }$).

Note that $P\left( \frac{\varepsilon }{\sigma }>-\frac{X^{\prime }\beta }{%
\sigma }\right) =1-\Phi \left( -\frac{X^{\prime }\beta }{\sigma }\right)
=\Phi \left( \frac{X^{\prime }\beta }{\sigma }\right) $.\ Also, since%
\[
\frac{d\phi }{dt}(t)=-t\phi (t)\text{ for }\phi (t)=\frac{1}{\sqrt{2\pi }}%
e^{-t^{2}/2}
\]%
we obtain%
\[
\int_{-\frac{X^{\prime }\beta }{\sigma }}^{\infty }t\cdot \phi (t)dt=\int_{-%
\frac{X^{\prime }\beta }{\sigma }}^{\infty }-\frac{d\phi }{dt}(t)dt=-\left(
0-\phi \left( -\frac{X^{\prime }\beta }{\sigma }\right) \right) =\phi \left( 
\frac{X^{\prime }\beta }{\sigma }\right) \text{.}
\]%
Further,%
\[
P\left( Y>0\mid X\right) =1-\Phi \left( -\frac{X^{\prime }\beta }{\sigma }%
\right) =\Phi \left( \frac{X^{\prime }\beta }{\sigma }\right) 
\]%
Thus our result is%
\[
E\left[ Y\mid X,Y>0\right] =X^{\prime }\beta +\sigma \frac{\phi \left(
X^{\prime }\beta /\sigma \right) }{\Phi \left( X^{\prime }\beta /\sigma
\right) }
\]%
and%
\[
E\left[ Y\mid X\right] =\Phi \left( X^{\prime }\beta /\sigma \right)
X^{\prime }\beta +\sigma \phi \left( X^{\prime }\beta /\sigma \right) 
\]%
So we see that OLS would be misspecified in this case.

But we can estimate the parameters $\beta $ and $\sigma $ by "\emph{%
Nonlinear Least Squares}". We minimize the following sum of squares with
respect to $\beta ,\sigma ^{2}$:%
\[
\frac{1}{N}\sum_{i=1}^{N}\left( Y_{i}-\Phi \left( X_{i}^{\prime }\beta
/\sigma \right) X_{i}^{\prime }\beta -\sigma \phi \left( X_{i}^{\prime
}\beta /\sigma \right) \right) ^{2}
\]

\subsection{Estimation Methods - Extremum Estimators}

All the estimators that we will study can be viewed as so-called "extremum
estimators", i.e. estimators that optimize a function of the data with
respect to the vector of parameters:%
\[
\hat{\theta}=\arg \max_{\theta \in \Theta }\hat{Q}_{N}(\theta )
\]

\subsubsection{1) Maximum Likelihood (MLE)}

Suppose that the data $\left\{ Z_{1},\ldots ,Z_{N}\right\} $ are iid with
pdf $f(Z;\theta _{0})$. The parameter vector $\theta _{0}$ identifies the
density of $Z$ within the parametric family of densities $\left\{ f\left(
\cdot ;\theta \right) :\theta \in \Theta \subset 
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
^{k}\right\} $.

By Jensen's inequality (since $\ln $ function is concave), for any $\theta $
s.t. $\theta _{0}\neq \theta \in \Theta $, if $f(Z;\theta )\neq f\left(
Z;\theta _{0}\right) $ in a set of positive probability, then%
\[
\ln E\left[ \frac{f(Z;\theta )}{f(Z;\theta _{0})}\right] >E\left[ \ln \left( 
\frac{f(Z;\theta )}{f(Z;\theta _{0})}\right) \right] =E\left[ \ln f(Z;\theta
)\right] -E[\ln f(Z;\theta _{0})].
\]%
Note that%
\[
\ln E\left[ \frac{f(Z;\theta )}{f(Z;\theta _{0})}\right] =\ln \int \frac{%
f(Z;\theta )}{f(Z;\theta _{0})}f(Z;\theta _{0})dZ=\ln \int f(Z;\theta )dZ=0
\]%
Therefore%
\[
E\left[ \ln f(Z;\theta _{0})\right] >E\left[ \ln f(Z;\theta )\right] \text{
for }\theta _{0}\neq \theta \in \Theta \text{.}
\]%
So $\theta _{0}$ solves the following problem in the population:%
\[
\theta _{0}=\arg \max_{\theta \in \Theta }E[\ln f(Z;\theta )].
\]%
Maximum likelihood estimators solve the same problem in the sample:%
\[
\hat{\theta}=\arg \max_{\theta \in \Theta }\frac{1}{N}\sum_{i=1}^{N}\ln
f(Z_{i};\theta )
\]%
(This is an application of the analogy principle --- also see OLS).

\begin{remark}
\bigskip Probit model is one example of MLE:%
\[
\hat{\gamma}=\arg \max_{\gamma \in \Gamma }\frac{1}{N}\sum_{i=1}^{N}\left[
D_{i}\ln \Phi \left( W_{i}^{\prime }\gamma \right) +(1-D_{i})\ln (1-\Phi
\left( W_{i}^{\prime }\gamma \right) )\right] 
\]
\end{remark}

\subsubsection{2)\ Nonlinear Least Squares}

For $Z=(Y,X)$, let $E\left[ Y\mid X\right] =h(X;\theta _{0})$. The parameter 
$\theta _{0}$ identifies the conditiona; mean of $Y$ given $X$ within the
parametric family $\left\{ h(\cdot ;\theta ):\theta \in \Theta \subset 
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
^{k}\right\} $. Note that%
\begin{eqnarray*}
E\left[ (Y-h(X;\theta ))^{2}\right]  &=&E\left[ \left( \left( Y-h\left(
X;\theta _{0}\right) \right) +\left( h\left( X;\theta _{0}\right) -h\left(
X;\theta \right) \right) \right) ^{2}\right] = \\
&=&E\left[ (Y-h(X;\theta _{0}))^{2}\right] +E\left[ (h\left( X;\theta
\right) -h(X;\theta _{0}))^{2}\right] + \\
&&+2\cdot \underset{=0\text{ by Iterated Expectations and fact that }E\left[
Y\mid X\right] =h(X;\theta _{0})}{\underbrace{E\left[ \left( Y-h\left(
X;\theta _{0}\right) \right) \left( h\left( X;\theta _{0}\right) -h\left(
X;\theta \right) \right) \right] }}
\end{eqnarray*}%
Therefore, if $h(Z;\theta )\neq h(Z;\theta _{0})$ in a set of positive
probability for every $\theta $ s.t. $\theta _{0}\neq \theta \in \Theta $,
then%
\[
\theta _{0}=\arg \min_{\theta \in \Theta }E\left[ \left( Y-h(X;\theta
)\right) ^{2}\right] 
\]%
since $E\left[ \left( h(X;\theta \right) -h(X;\theta _{0}))^{2}\right] =0$
iff $\theta =\theta _{0}$, and is $\geq 0$ otherwise.

Thus, again by analogy principle, the Nonlinear Least Squares solve the same
problem in the sample%
\[
\hat{\theta}=\arg \min_{\theta \in \Theta }\frac{1}{N}\sum_{i=1}^{N}\left(
Y_{i}-h(X_{i}^{\prime };\theta )\right) ^{2}
\]

The parameters of the Probit model can also be estimated with Nonlinear
Least Squares:%
\[
\hat{\gamma}=\arg \min_{\gamma \in \Gamma }\frac{1}{N}\sum_{i=1}^{N}\left(
Y_{i}-\Phi \left( W_{i}^{\prime }\gamma \right) \right) ^{2}\text{ since }%
P\left( Y_{i}=1\mid W\right) =E\left[ Y_{i}\mid W\right] \text{.}
\]

\subsubsection{3)\ Generalized Methods of Moments\ (GMM)}

Suppose there is an $r\times 1$ vector of functions $g(Z;\theta )$ such
that, for certain $\theta _{0}\in \Theta $, we have that $E\left[ g(Z;\theta
)\right] =0$.

\end{document}
