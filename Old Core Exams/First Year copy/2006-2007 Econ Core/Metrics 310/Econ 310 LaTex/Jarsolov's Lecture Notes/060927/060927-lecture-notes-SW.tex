
\documentclass{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amsfonts}
\usepackage{amsmath}

\setcounter{MaxMatrixCols}{10}
%TCIDATA{OutputFilter=LATEX.DLL}
%TCIDATA{Version=5.50.0.2953}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{Created=Friday, October 20, 2006 16:04:44}
%TCIDATA{LastRevised=Saturday, October 21, 2006 02:43:17}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{<META NAME="DocumentShell" CONTENT="Standard LaTeX\Blank - Standard LaTeX Article">}
%TCIDATA{CSTFile=40 LaTeX article.cst}

\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\newenvironment{proof}[1][Proof]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\input{tcilatex}
\begin{document}


\section{(Section \#1) Review of probability and statistics results}

\subsection{Moment generating function of a random variable}

\begin{definition}
Let $X$ be a random variable. If there exists a real constant $h>0$ such
that expectation $E(e^{tX})$ exists $\forall t,~\left\vert t\right\vert <h$,
then $M_{X}(t)=E\left( \exp (tX)\right) $ is called the \emph{moment
generating function} for X.
\end{definition}

\begin{remark}
The moment generating function may not exist. Contrary to this, the \emph{%
characteristic function} $\psi _{X}(t)=E\left( \exp (itX)\right) $ always
exists.
\end{remark}

\begin{remark}
Properties of the moment generating function:

\begin{enumerate}
\item[a)] It uniquely defines the distribution of a random variable. If two
random variables have the same moment generating function for all $t$ in an
interval around 0, then they have the same cummulative distribution function
and probability density function (a.s.).

\item[b)] The $k$-th raw moment $E(X^{k})$ is equal to $E(X^{k})=\mu _{k}=%
\frac{\partial ^{k}(M_{X})}{\partial t^{k}}(0)$.
\end{enumerate}
\end{remark}

Denote $M_{X}^{k}(t)=\frac{\partial ^{k}M_{X}}{\partial t^{k}}(t)$. Then we
have%
\begin{eqnarray*}
M_{X}(0) &=&1 \\
E(X) &=&\mu =M_{X}^{1}(0) \\
Var(X) &=&\sigma ^{2}=E(X^{2})-(EX)^{2}=M_{X}^{2}(0)-\left[ M_{X}^{1}(0)%
\right] ^{2}
\end{eqnarray*}

\begin{example}
Let $X$ be a random variable with exponential distribution%
\begin{equation*}
f(x)=\left\{ 
\begin{array}{cl}
\lambda \exp (-\lambda x) & \text{ \ \ for }x>0 \\ 
0 & \text{ \ \ elsewhere}%
\end{array}%
\right. 
\end{equation*}%
Then%
\begin{eqnarray*}
M_{X}(t) &=&\int_{0}^{\infty }\lambda \exp (-\lambda x)\exp
(tx)dx=\int_{0}^{\infty }\lambda \exp ((t-\lambda )x)dx\underset{\text{for }%
t<\lambda }{=} \\
&=&-\frac{\lambda }{\lambda -t}\left. \exp (-x(\lambda -t))\right\vert
_{0}^{\infty }=\frac{\lambda }{\lambda -t}. \\
M_{X}^{k}(t) &=&k!\frac{\lambda }{(\lambda -t)^{k+1}}\quad \Longrightarrow
\quad E(X)=\frac{1}{\lambda },~~Var(X)=\frac{1}{\lambda ^{2}}.
\end{eqnarray*}
\end{example}

\subsection{Chebyshev's Inequality}

\begin{theorem}
Let $Y$ be a random variable with mean $\mu $ and variance $\sigma ^{2}$.
Then for any $k>0$,%
\begin{equation*}
P(\left\vert Y-\mu \right\vert \geq k\sigma )\leq \frac{1}{k^{2}}. 
\end{equation*}%
\newline
\end{theorem}

\begin{proof}
Start by proving a simpler result, the so-called \emph{Markov's inequality}:
Let $X$ be a nonnegative random variable. Then for any positive $c$, $%
P(X\geq c)\leq \frac{E(X)}{c}.$

For continuous case, we have%
\begin{eqnarray*}
EX &=&\int_{0}^{\infty }xf_{X}(x)dx=\int_{0}^{c}xf_{X}(x)dx+\int_{c}^{\infty
}xf_{X}(x)dx\geq \\
&\geq &\int_{c}^{\infty }xf_{X}(x)dx\geq c\int_{c}^{\infty
}f_{X}(x)dx=c\cdot P(X\geq c)\qquad \Longrightarrow \\
P(X\geq c) &\leq &\frac{E(X)}{c}
\end{eqnarray*}

Now apply this result to prove the Chebyshev's inequality:%
\begin{equation*}
P(\left\vert Y-\mu \right\vert \geq k\sigma )=P(\left\vert Y-\mu \right\vert
^{2}\geq k^{2}\sigma ^{2})\leq \frac{E\left\vert Y-\mu \right\vert ^{2}}{%
k^{2}\sigma ^{2}}=\frac{\sigma ^{2}}{k^{2}\sigma ^{2}}=\frac{1}{k^{2}}. 
\end{equation*}
\end{proof}

\subsection{Jensen's Inequality}

\begin{theorem}
Let $X$ be a random variable with finite mean $E(X)$. Then for a convex
function $g$, we have $E\left[ g(X)\right] \geq g(EX)$, and for a concave
function $h$, we have $E\left[ h(X)\right] \leq h(EX)$.
\end{theorem}

\subsection{Cauchy-Schwarz inequality}

\begin{theorem}
For any two random variables $X$ and $Y$ with finite second moments,%
\begin{equation*}
(E(XY))^{2}\leq E\left( X^{2}\right) E\left( Y^{2}\right) . 
\end{equation*}
\end{theorem}

\begin{proof}
For any $\alpha ,\beta \in 
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
$, we have $E(\alpha X+\beta Y)^{2}\geq 0$, or $\alpha ^{2}E(X^{2})+2\alpha
\beta E(XY)+\beta ^{2}E(Y^{2})\geq 0$. Thus the latter is a positive
semidefinite quadratic form in $\alpha ,\beta $:%
\begin{equation*}
\left( 
\begin{array}{cc}
\alpha & \beta%
\end{array}%
\right) \left( 
\begin{array}{cc}
E(X^{2}) & E(XY) \\ 
E(XY) & E(Y^{2})%
\end{array}%
\right) \left( 
\begin{array}{c}
\alpha \\ 
\beta%
\end{array}%
\right) \geq 0. 
\end{equation*}%
Therefore%
\begin{equation*}
\det \left( 
\begin{array}{cc}
E(X^{2}) & E(XY) \\ 
E(XY) & E(Y^{2})%
\end{array}%
\right) =E(X^{2})E(Y^{2})-(E(XY))^{2}\geq 0. 
\end{equation*}
\end{proof}

\begin{remark}
Recall other versions of the Cauchy-Schwarz inequality:%
\begin{eqnarray*}
\left( \sum_{i=1}^{n}|a_{i}b_{i}|\right) ^{2} &\leq
&\sum_{i=1}^{n}a_{i}^{2}\sum_{i=1}^{n}b_{i}^{2}. \\
\left( \int_{a}^{b}\left\vert f(x)g(x)\right\vert dx\right) ^{2} &\leq
&\int_{a}^{b}(f(x))^{2}dx\cdot \int_{a}^{b}(g(x))^{2}dx
\end{eqnarray*}
\end{remark}

\section{(Section \#2)}

\subsection{Expectations and variance of random variables}

Let $X$ be a $k\times 1$ random vector, $Y$ a $l\times 1$ random vector. Then%
\begin{eqnarray*}
E(X) &=&(E(X_{1}),\ldots ,E(X_{k}))^{\prime }\qquad (k\times 1) \\
Var(X) &=&E\left[ (X-EX)(X-EX)^{\prime }\right] \qquad (k\times k) \\
Cov(X,Y) &=&E\left[ (X-EX)(Y-EY)^{\prime }\right] \qquad ~(k\times l) \\
Cov(X_{\,i},Y_{j}) &=&\left[ Cov(X,Y)\right] _{i,j}
\end{eqnarray*}

Note that $Cov(X,Y)$ is in general not symmetric, even if $k=l$.

\begin{theorem}
\label{thm:S2T2}(T2) Properties of $E(\cdot )$, $Var(\cdot )$, $Cov(\cdot
,\cdot )$:%
\begin{eqnarray*}
&&a)\quad Ea=a\qquad \forall a\in 
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
\\
&&b)\quad EX^{\prime }=(EX)^{\prime } \\
&&c)\quad E(AX)=AEX\qquad \forall \text{ constant matrix }A \\
&&d)\quad E(X^{\prime }A^{\prime })=(EX^{\prime })A^{\prime } \\
&&e)\quad E(X+Y)=EX+EY \\
&&f)\quad Cov(AX+a,BX+b)=A\cdot Cov(X,Y)\cdot B^{\prime } \\
&&g)\quad Var(AX+b)=A\cdot Var(X)\cdot A^{\prime } \\
&&h)\quad Cov(Y,X)=\left[ Cov(X,Y)\right] ^{\prime } \\
&&i)\quad E(g(X)\cdot f(Y)\mid X)=g(X)\cdot E(f(Y)\mid X) \\
&&j)\quad EY=E\left[ E(Y\mid X)\right] \qquad \text{law of iterated
expectations}
\end{eqnarray*}
\end{theorem}

\begin{proof}
f)%
\begin{eqnarray*}
Cov(AX+a,BY+b) &=&E\left[ (AX+a-E(AX+a))(BY+b-E(BY+b))^{\prime }\right] = \\
&=&E\left[ A(X-EX)(Y-EY)^{\prime }B^{\prime }\right] =AE\left[
(X-EX)(Y-EY)^{\prime }\right] B^{\prime }= \\
&=&ACov(X,Y)B^{\prime }
\end{eqnarray*}

j)%
\begin{eqnarray*}
E\left[ E(Y\mid X)\right] &=&\int \left( \int yf_{Y\mid X}(y\mid x)dy\right)
f_{X}(x)dx=\int \int yf_{Y\mid X}(y\mid x)\cdot f_{X}(x)dxdy= \\
&=&\int \int yf_{X,Y}(x,y)dxdy=\int y\left( \int f_{X,Y}(x,y)dx\right) dy= \\
&=&\int yf_{Y}(y)dy=EY.
\end{eqnarray*}
\end{proof}

\subsection{Normal distributions}

\begin{definition}
A univariate random variable $X$ has standard normal distribution (denote $%
X\sim N(0,1)$) if it's probability density function takes the form%
\begin{equation*}
\phi (x)=\frac{1}{\sqrt{2\pi }}\exp \left( -\frac{1}{2}x^{2}\right) \qquad 
\text{for }x\in 
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
. 
\end{equation*}
\end{definition}

\begin{remark}
The corresponding cummulative density function is denote $\Phi (x)$, and has
no analytical formula.
\end{remark}

\begin{remark}
All moments $E(X^{k})=\int x^{k}\phi (x)dx$ of a random variable $X$ with
standard normal distrubution are finite. The distribution is symmetric, thus
all odd moments are zero.
\end{remark}

\begin{remark}
The moment generating function exists $\forall t\in 
%TCIMACRO{\U{211d} }%
%BeginExpansion
\mathbb{R}
%EndExpansion
$:%
\begin{eqnarray*}
E(\exp (tX)) &=&\int_{-\infty }^{+\infty }\exp (tx)f_{X}(x)dx=\int_{-\infty
}^{+\infty }\frac{1}{\sqrt{2\pi }}\exp (tx)\exp \left( -\frac{1}{2}%
x^{2}\right) dx= \\
&=&\frac{1}{\sqrt{2\pi }}\int_{-\infty }^{+\infty }\exp \left(
-x^{2}/2+tx-t^{2}/2\right) \exp (t^{2}/2)dx= \\
&=&\frac{1}{\sqrt{2\pi }}\int_{-\infty }^{+\infty }\exp \left( -\frac{1}{2}%
(x-t)^{2}\right) \exp (t^{2}/2)dx= \\
&=&\exp \frac{t^{2}}{2}\underset{=1}{\underbrace{\int_{-\infty }^{+\infty }%
\frac{1}{\sqrt{2\pi }}\exp (-u^{2}/2)du}}=\exp \frac{t^{2}}{2}.
\end{eqnarray*}
\end{remark}

\begin{example}
Assume $Z\sim N(0,1)$, and $X=\mu +\sigma Z$. Then we use the formula for
transformation of densities%
\begin{equation*}
f_{X}(x)=f_{Z}(g^{-1}(x))\left\vert \frac{\partial g^{-1}(x)}{\partial x}%
\right\vert \qquad \text{where }g^{-1}(x)=\frac{x-\mu }{\sigma } 
\end{equation*}%
to obtain%
\begin{equation*}
f_{X}(x)=\frac{1}{\sqrt{2\pi }\sigma }\exp \left\{ -\frac{1}{2}\left( \frac{%
x-\mu }{\sigma }\right) ^{2}\right\} . 
\end{equation*}%
The transformation formula can be obtained as follows:%
\begin{equation*}
\int_{-\infty }^{x_{0}}f_{X}(x)dx=P(X\leq x_{0})=P(g(Z)\leq x_{0})=P(Z\leq
g^{-1}(x_{0}))=\int_{-\infty }^{g^{-1}(x_{0})}f_{Z}(z)dz. 
\end{equation*}%
Now differentiate with respect to $x_{0}$ to get%
\begin{equation*}
f_{X}(x_{0})=f_{Z}(g^{-1}(x_{0}))\left\vert \frac{\partial g^{-1}(x_{0})}{%
\partial x_{0}}\right\vert 
\end{equation*}

Thus $X$ has then univariate normal distribution with mean $\mu $ and
variance $\sigma ^{2}$, and we write $X\sim N(\mu ,\sigma ^{2})$.
\end{example}

\begin{definition}
The random vector $X$ has \emph{multivariate normal distribution} if%
\begin{equation*}
f_{X}(x)=\frac{1}{(2\pi )^{n/2}(\det \Sigma )^{1/2}}\exp \left\{ -\frac{1}{2}%
(x-\mu )^{\prime }\Sigma ^{-1}(x-\mu )\right\} . 
\end{equation*}
\end{definition}

\begin{example}
\begin{remark}
If $X$ is multivariate normal and the elements of $X$ are mutually
uncorrelated, then $\Sigma =diag(\sigma _{j}^{2})$, and%
\begin{eqnarray*}
f_{X}(x) &=&\frac{1}{(2\pi )^{n/2}\sigma _{1}\cdot \ldots \cdot \sigma _{n}}%
\exp \left\{ -\frac{1}{2}\sum_{j=1}^{n}\left( \frac{x_{j}-\mu _{j}}{\sigma
_{j}}\right) ^{2}\right\} = \\
&=&\prod_{j=1}^{n}\frac{1}{\sqrt{2\pi }\sigma _{j}}\exp \left\{ -\frac{1}{2}%
\left( \frac{x_{j}-\mu _{j}}{\sigma _{j}}\right) ^{2}\right\} ,
\end{eqnarray*}%
so we see that the components are \emph{mutually independent }as well.
\end{remark}

\begin{remark}
Note that the fact that two random variables are uncorrelated does not in
general imply that they are independent. Take for example $X$ and $Y$
uniformly distributed in a circle, so that $(x,y)$ denotes the coordinates
within the circle. Then $E(XY)=E(XE(Y\mid X))=0$, and since also $EX=EY=0$,
then $X$ and $Y$ are uncorrelated. However, they are not independent, since
in general $P(Y\mid x_{1})\neq P(Y\mid x_{2})$.
\end{remark}

\begin{theorem}
\label{thm:S2T3}(T3) (Univariate results related to normal distribution)

\begin{description}
\item[a)] $X\sim N(\mu ,\sigma ^{2})~\Longrightarrow ~\frac{X-\mu }{\sigma }%
\sim N(0,1)$

\item[b)] $Z\sim N(0,1)~\Longrightarrow ~Z^{2}\sim \chi _{1}^{2}$

\item[c)] $X_{1},\ldots ,X_{n}$ independent $\chi _{r_{1}}^{2},\ldots ,\chi
_{r_{n}}^{2}$ $\Longrightarrow ~\sum_{i=1}^{n}X_{i}\sim \chi _{\Sigma
r_{i}}^{2}.$

\item[d)] $Z_{1},\ldots ,Z_{n}$ iid $N(0,1)~\Longrightarrow
~\sum_{i=1}^{n}Z_{i}^{2}\sim \chi _{n}^{2}$

\item[e)] $X_{1}\sim \chi _{n_{1}}^{2}$, $X_{2}\sim \chi _{n_{2}}^{2}$
independent $\Longrightarrow ~F=\frac{X_{1}/n_{1}}{X_{2}/n_{2}}\sim
F(n_{1},n_{2})$

\item[f)] $Z\sim N(0,1)$, $X\sim \chi _{n}^{2}$ independent $\Longrightarrow
~t=\frac{Z}{\sqrt{X/n}}\sim t_{n}$

\item[g)] $t\sim t_{n}~\Longrightarrow ~t^{2}\sim F(1,n)$
\end{description}
\end{theorem}
\end{example}

Let us state also some results for the multivariate normal distributions.

\begin{theorem}
\label{thm:S2T4}(T4) If $X\sim N(\mu ,\Sigma )$, then $Y=AX+a\sim N(A\mu
+a,A\Sigma A^{\prime })$.
\end{theorem}

\begin{theorem}
\label{thm:S2T5}(T5) If\ $Z\sim N(0,I)$ and $A$ is an idempotent symmetric
matrix with $rank(A)=J$, then $Z^{\prime }AZ\sim \chi _{J}^{2}$ (this is a
projection of $n$-dimenstional $Z$ onto a smaller-dimensional space ($J\leq
n $).
\end{theorem}

\begin{theorem}
\label{thm:S2T6}(T6) If $X\sim N(\mu ,\Sigma )$, then $Z=\Sigma
^{-1/2}(X-\mu )\sim N(0,I)$. The \textquotedblleft square
root\textquotedblright\ matrix can be obtained using procedures described in
the matrix algebra handout.
\end{theorem}

\begin{theorem}
\label{thm:S2T7}(T7) If $X\sim N(\mu ,\Sigma )$, then $(X-\mu )^{\prime
}\Sigma ^{-1}(X-\mu )\sim \chi _{n}^{2}$ (since $Z^{\prime }Z\sim \chi
_{n}^{2}$ where $Z=\Sigma ^{-1/2}(X-\mu )$).
\end{theorem}

\section{(Section \#3) Convergence of random variables}

Consider independent random variables $X_{1},\ldots ,X_{n}$ with mean $\mu $
and variance $\sigma ^{2}$. We know%
\begin{eqnarray*}
E\left( \sum_{i=1}^{n}X_{i}\right) =n\mu \text{\qquad } &&\text{and\qquad }%
Var\left( \sum_{i=1}^{n}X_{i}\right) =n\sigma ^{2} \\
E\left( \frac{1}{n}\sum_{i=1}^{n}X_{i}\right) =\mu \qquad &&\text{and\qquad }%
Var\left( \frac{1}{n}\sum_{i=1}^{n}X_{i}\right) =\frac{\sigma ^{2}}{n}.
\end{eqnarray*}

Consider the behavior of $\overline{X}_{n}=\frac{1}{n}\sum_{i=1}^{n}X_{i}$
as $n$ gets large. Using Chebyshev's inequality, we know%
\begin{equation*}
P(\left\vert Y-\mu \right\vert \geq k\sigma _{Y})\leq \frac{1}{k^{2}} 
\end{equation*}%
so that applying it on $\overline{X}_{n}$ with $k=\varepsilon \sqrt{n}%
/\sigma $ yields%
\begin{equation*}
P\left( \left\vert \overline{X}_{n}-\mu \right\vert \geq \varepsilon \right)
\leq \frac{\sigma ^{2}}{n\varepsilon ^{2}}, 
\end{equation*}%
so that $\overline{X}_{n}$ \textquotedblleft seems to
converge\textquotedblright\ to $\mu $.

\begin{example}
Let $\{X_{n}\}$ be a sequence of random variables with probability
distribution function: 
\begin{equation*}
f_{n}(x)=\left\{ 
\begin{array}{cc}
\frac{n-1}{2} & -\frac{1}{n}<x<\frac{1}{n} \\ 
\frac{1}{n} & n<x<n+1 \\ 
0 & \text{elsewhere}%
\end{array}%
\right. 
\end{equation*}%
(the distribution function is depicted in Figure \ref{fig:S3pdf_convergence}%
).\ We then have%
\begin{eqnarray*}
E(X_{n}) &=&\int_{-1/n}^{1/n}\frac{n-1}{2}xdx+\int_{n}^{n+1}\frac{1}{n}xdx=1+%
\frac{1}{2n} \\
Var(X_{n}) &=&EX^{2}-(EX)^{2}
\end{eqnarray*}

and it can be shown that $Var(X_{n})$ is an increasing function of $n$. Now
what does $\left\{ X_{n}\right\} $ converge to? The mass of the distribution
concentrates at 0, but the mean converges to 1. Moreover, the variance is
increasing with $n$. We need to introduce a more precise definition of
convergence.
\begin{figure}[th]
\begin{picture}(120,50)
\put(0,5){\line(1,0){120}}
\multiput(10,5)(30,0){2}{\line(0,1){10}}
\put(10,15){\line(1,0){30}}
\put(25,5){\line(0,1){40}}
\multiput(20,5)(0,2.5){12}{\line(0,1){1.5}}
\multiput(30,5)(0,2.5){12}{\line(0,1){1.5}}
\multiput(20,35)(2.5,0){4}{\line(1,0){1.5}}

\multiput(60,5)(15,0){2}{\line(0,1){10}}
\put(60,15){\line(1,0){15}}

\multiput(95,5)(0,2.5){2}{\line(0,1){1.5}}
\multiput(110,5)(0,2.5){2}{\line(0,1){1.5}}
\multiput(95,10)(2.5,0){6}{\line(1,0){1.5}}

\put(77,10){\vector(1,0){15}}
\put(40,17){\vector(-1,2){6}}
\put(3,17){$\frac{n-1}{2}$}
\put(77,17){$\frac{1}{n}$}
\put(115,2){$x$}
\put(26,42){$f(x)$}
\end{picture}
\caption{Probability density function of a variable that converges to $0$ in
probability, but does not converge in quadratic mean.}
\label{fig:S3pdf_convergence}
\end{figure}
\end{example}

\subsection{Modes of convergence}

\begin{remark}
(Notation) We denote 
\begin{equation*}
\begin{array}{cccc}
\text{univariate random variables} & x_{n}\text{, }y_{n}\text{, }z_{n}\text{%
, }x\text{, }y\text{, }z & \text{constant scalars} & x_{0}\text{, }y_{0}%
\text{, }z_{0} \\ 
\text{random vectors} & X_{n}\text{, }Y_{n}\text{, }Z_{n}\text{, }X\text{, }Y%
\text{, }Z & \text{constant vectors} & X_{0}\text{, }Y_{0}\text{, }Z_{0} \\ 
\text{random matrices} & A_{n}\text{, }B_{n}\text{, }C_{n}\text{, }A\text{, }%
B\text{, }C & \text{constant matrices} & A_{0}\text{, }B_{0}\text{, }C_{0}%
\end{array}%
\end{equation*}

We use the Euclidean norm%
\begin{equation*}
\begin{array}{cc}
\text{scalars} & \left\Vert x\right\Vert =\left\vert x\right\vert  \\ 
\text{vectors} & \left\Vert X\right\Vert =\left( X^{\prime }X\right) ^{1/2}
\\ 
\text{matrices} & \left\Vert A\right\Vert =\left( tr(A^{\prime }A\right)
)^{1/2}=\left( \sum_{i,j}A_{ij}^{2}\right) ^{1/2}%
\end{array}%
\end{equation*}
\end{remark}

\begin{definition}
(Convergence in $r$-mean)\ A sequence $\left\{ X_{n}\right\} $ of random
variables converges to $X$ in $r$-mean for some $r>0$ iff $E\left(
\left\Vert X_{n}-X\right\Vert ^{r}\right) \rightarrow 0$.

For $r=2$, we say that $\left\{ X_{n}\right\} $ converges to $X$ in mean
square error (MSE).
\end{definition}

\begin{example}
Let $\left\{ x_{n}\right\} $ be a sequence of univariate random variables.%
\begin{eqnarray*}
E\left( (x_{n}-x)^{2}\right)  &=&E((x_{n}-x-E(x_{n}-x)+E(x_{n}-x))^{2})= \\
&=&\underset{=Var(x_{n}-x)}{\underbrace{E((x_{n}-x-E(x_{n}-x))^{2})}}+2E%
\left[ (x_{n}-x-E(x_{n}-x))\cdot E(x_{n}-x)\right] +\left( E(x_{n}-x)\right)
^{2}= \\
&=&Var(x_{n}-x)+2\underset{=0}{\underbrace{\left(
E(x_{n}-x)-E(x_{n}-x)\right) }}\cdot E(x_{n}-x)+\left( E(x_{n}-x)\right)
^{2}= \\
&=&Var(x_{n}-x)+\left( E(x_{n}-x)\right) ^{2}
\end{eqnarray*}%
So we observe that both the mean and the variance have to go to zero for
convergence in mean square error.
\end{example}

\begin{definition}
A sequence $\left\{ X_{n}\right\} $ of random variables converges to $X$ in
probability iff $\forall \varepsilon >0,~P(\left\Vert X_{n}-X\right\Vert
>\varepsilon )\rightarrow 0$.
\end{definition}

\begin{definition}
A sequence $\left\{ X_{n}\right\} $ of random variables converges to $X$ in
distribution iff $P(X_{n}\leq X_{0})\rightarrow P(X\leq X_{0})$ for $\forall
X_{0}$ s.t. $P(X=X_{0})=0$, i.e. only for points qhere $F_{X}(\cdot )$ is
continuous.
\end{definition}

\begin{example}
Let $X_{n}$ be uniform on $\left[ 0,1/n\right] $ and $X\equiv 0$. Then $%
P(X_{n}\leq 0)=0$ and $\lim_{n\rightarrow \infty }P(X_{n}\leq 0)=0$. But $%
P(X\leq 0)=1$. Thus we need to exclude points where $F_{X}(\cdot )$ is
discontinuous in order for the definition above to be reasonable.
\end{example}

\begin{definition}
A sequence $\left\{ X_{n}\right\} $ of random variables converges to $X$
almost surely iff $P(X_{n}\rightarrow X)=1$.
\end{definition}

\begin{remark}
Summary of modes of convergence

\begin{tabular}{ll}
in $r$-mean & $X_{n}\overset{r\text{-mean}}{\longrightarrow }X\iff
E(\left\Vert X_{n}-X\right\Vert ^{r})\longrightarrow 0$ \\ 
in probability & $X_{n}\overset{P}{\longrightarrow }X\equiv
p\lim_{n\rightarrow \infty }X_{n}=X\iff \forall \varepsilon >0:P(\left\Vert
X_{n}-X\right\Vert >\varepsilon )\rightarrow 0$ \\ 
in distribution & $X_{n}\overset{D}{\longrightarrow }X\iff (X_{n}\leq
X_{0})\rightarrow P(X\leq X_{0})$ for $\forall X_{0}$ s.t. $P(X=X_{0})=0$ \\ 
almost surely & $X_{n}\overset{\text{a.s.}}{\longrightarrow }X\iff
P(X_{n}\rightarrow X)=1$%
\end{tabular}
\end{remark}

\end{document}
