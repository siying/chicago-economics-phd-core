\begin{theorem}
\label{thm:S4T4}(T4) Under assumptions (A0) to (A2'), we can add to Theorem %
\ref{thm:S4T3} (T3) the following:

(1) $\hat{\beta}\sim N(\beta ,\sigma ^{2}(X^{\prime }X)^{-1})$

(2) $(n-k)s^{2}/\sigma ^{2}\sim \chi _{n-k}^{2}$

(3) $\hat{\beta}$, $s^{2}$ are independent

(4) $(\hat{\beta}_{j}-\beta _{j})/SE_{j}\sim t_{n-k}\quad \forall j=1,\ldots
,k\quad $where $SE_{j}=\sqrt{s^{2}\left[ (X^{\prime }X)^{-1}\right] _{jj}}$.

(5)~$\hat{\beta}_{OLS}=\hat{\beta}_{MLE}$ (more on this in the second part
of this course).

\begin{proof}
(Sketch)

(1) Follows from Theorem \ref{thm:S2T4} (T4) from Section \ref{sec:2} (\#2)
and result (1) of Theorem \ref{thm:S4T3} (T3) in this section since $\hat{%
\beta}$ is a linear transformation of $Y$ and hence also of $\varepsilon $.

(2) Recall from Theorem \ref{thm:S2T5}\ (T5) in Section \ref{sec:2} (\#2)
that if $z\sim N(0,1)$ and $rank(A)=J$, then $z^{\prime }Az\sim \chi
_{J}^{2} $. Then we have 
\begin{equation*}
(n-k)s^{2}/\sigma ^{2}=\hat{\varepsilon}^{\prime }\hat{\varepsilon}/\sigma
^{2}=\varepsilon ^{\prime }Q\varepsilon /\sigma ^{2}=(\varepsilon /\sigma
)^{\prime }Q(\varepsilon /\sigma )\sim \chi _{n-k}^{2}\text{.}
\end{equation*}

(3) $(\hat{\beta},\hat{\varepsilon})^{\prime }$ are jointly normal since
they are both linear functions of the normally distributed $\varepsilon $.
Since multivariate normal random variables with uncorrelated elements are
independent, we can show that $\hat{\beta}$, $\hat{\varepsilon}$ are
independent by showing that $Cov(\hat{\beta},\hat{\varepsilon}\mid X)=0$.

\begin{proof}
Further, since $s^{2}$ is a function of $\hat{\varepsilon}$ ($s^{2}=\hat{%
\varepsilon}^{\prime }\hat{\varepsilon}/(n-k)$), it would also follow that $%
\hat{\beta}$ and $s^{2}$ are independent (since $C$, $D$ independent $%
\Longrightarrow $ $f(C)$, $f(D)$ independent $\forall f(\cdot ),g(\cdot )$).
\end{proof}
\end{proof}
\end{theorem}

\begin{proof}
\begin{equation*}
E(\hat{\varepsilon}|X)=E(Y-X\hat{\beta}|X)=E(Y|X)-XE(\hat{\beta}|X)=X\beta
-X\beta =0
\end{equation*}

and

\begin{equation*}
\hat{\beta}-\beta =(X^{\prime }X)^{-1}X^{\prime }Y-\beta =(X^{\prime
}X)^{-1}(X^{\prime }\beta +\varepsilon )-\beta =(X^{\prime
}X)^{-1}\varepsilon
\end{equation*}

Also

\begin{eqnarray*}
Cov(\hat{\beta},\hat{\varepsilon}|X) &=&E((\hat{\beta}-\beta )\hat{%
\varepsilon}|X)=E\left[ (X^{\prime }X)^{-1}X^{\prime }\varepsilon
(Q\varepsilon )^{\prime }|X\right] = \\
&=&(X^{\prime }X)^{-1}X^{\prime }E(\varepsilon \varepsilon ^{\prime
}|X)Q=\sigma ^{2}(X^{\prime }X)^{-1}\underset{=0}{\underbrace{X^{\prime }Q}}%
=0
\end{eqnarray*}

So if $\hat{\beta}$ and $\hat{\varepsilon}$ are independent, then also $\hat{%
\beta}$ and $s^{2}$ are independent.

(4) Observe that since $\hat{\beta}\sim N(\beta ,\sigma ^{2}(X^{\prime
}X)^{-1})$, we have%
\begin{equation*}
z_{j}=\frac{\hat{\beta}_{j}-\beta _{j}}{\sqrt{\sigma ^{2}[(X^{\prime
}X)^{-1}]_{jj}}}\sim N(0,1)\qquad \forall j=1,\ldots ,k
\end{equation*}

and from (2) we have

\begin{equation*}
W=\frac{(n-k)s^{2}}{\sigma ^{2}}\sim \chi _{n-k}^{2}
\end{equation*}

Then it follows

\begin{equation*}
t=\frac{\hat{\beta}_{j}-\beta _{j}}{\sqrt{s^{2}[(X^{\prime }X)^{-1}]_{jj}}}%
=z_{j}=\frac{\hat{\beta}_{j}-\beta _{j}}{\sqrt{\sigma ^{2}[(X^{\prime
}X)^{-1}]_{jj}}}/\sqrt{\frac{s^{2}}{\sigma ^{2}}}=\frac{z_{j}}{\sqrt{W/(n-k)}%
}\sim t_{n-k}
\end{equation*}

where the last is implied by Theorem \ref{thm:S2T1} (T1) from Section \ref%
{sec:2} \#2, since $z_{j}$ and $W$ are independent by (3).
\end{proof}

\subsection{The $R^{2}$ ("How good is the fit?")}

Let's assume that $X$ contains a column of ones.

One measure is the residual sum of squares:

\begin{equation*}
RSS=\sum (Y_{i}-\hat{Y}_{i})^{2}=\sum (Y_{i}-X_{i}^{\prime }\hat{\beta}%
)^{2}=\sum \hat{\varepsilon}_{i}^{2}=\hat{\varepsilon}^{\prime }\hat{%
\varepsilon}
\end{equation*}

Problem: It depends on units: if $(Y_{i},X_{i})$ are all multiplied by the
same $K$, then RSS\ is multiplied by $K^{2}$.

Instead, use a normalized measure:

Let $\bar{Y}=n^{-1}\sum Y_{i}$. and $TSS=\sum (Y_{i}-\bar{Y}_{i})^{2}$ where 
$TSS$ is the Total Sum of Squares.

Note: $Y_{i}-\bar{Y}$ is the residual when you regress only on the constant
(i.e. the model contains only the intercept and no other regressors).

In that case, the matrix of regressors is $i$ (which is a $n\times 1$ matrix
of ones). Then the corresponding "$P$" matrix is $P_{0}=i(i^{\prime
}i)^{-1}i $. Then

\begin{equation*}
P_{0}Y=i(i^{\prime }i)^{-1}(i^{\prime }Y)=i\left( \sum_{i=1}^{n}1\right)
^{-1}\left( \sum_{i=1}^{n}Y_{i}\right) =in^{-1}\sum_{i=1}^{n}Y_{i}=i\bar{Y}.
\end{equation*}

where $(Y_{i}-\bar{Y})$ is one element of $(Y-i\bar{Y})$. Now let $%
Q_{0}=I-P_{0}$. Then

\begin{eqnarray*}
TSS &=&(Y-i\bar{Y})^{\prime }(Y-i\bar{Y})=(Y-P_{0}Y)^{\prime
}(Y-P_{0}Y)=(Q_{o}Y)^{\prime }(Q_{o}Y)= \\
&=&Y^{\prime }Q_{0}^{\prime }Q_{o}Y=Y^{\prime }Q_{0}Y.
\end{eqnarray*}

Then it follows that

\begin{equation*}
R^{2}=1-\frac{RSS}{TSS}=1-\frac{\hat{\varepsilon}^{\prime }\hat{\varepsilon}%
}{Y^{\prime }Q_{0}Y}.
\end{equation*}

Why is $R^{2}$ reasonable?

\begin{eqnarray*}
TSS &=&(Y-i\bar{Y})^{\prime }(Y-i\bar{Y})=(Y-\hat{Y}+\hat{Y}-i\bar{Y}%
)^{\prime }(Y-\hat{Y}+\hat{Y}-i\bar{Y})= \\
&=&(Y-\hat{Y})^{\prime }(Y-\hat{Y})+(\hat{Y}-i\bar{Y})^{\prime }(\hat{Y}-i%
\bar{Y})+2\underset{1\times n}{\underbrace{(\hat{Y}-i\bar{Y})^{\prime }}}%
\underset{n\times 1}{\underbrace{(Y-\hat{Y})}}
\end{eqnarray*}

(the cross terms can be grouped together since the result is a scalar).

But

\begin{equation*}
(\hat{Y}-i\bar{Y})^{\prime }(Y-\hat{Y})=(X\hat{\beta}-i\bar{Y})^{\prime }(Y-X%
\hat{\beta})=\hat{\beta}^{\prime }\underset{=0}{\underbrace{X^{\prime }(Y-X%
\hat{\beta})}}-\bar{Y}\cdot i(Y-X\hat{\beta})
\end{equation*}

\bigskip Note that $X^{\prime }(Y-X\hat{\beta})=X^{\prime }Y-X^{\prime }X%
\hat{\beta}=0$ by $n\times 1$ normal equations.\ This implies $i^{\prime
}(Y-X\hat{\beta})=0$ since $X$ contains a column of ones.

Then it follows that

\begin{equation*}
TSS=(Y-\hat{Y})^{\prime }(Y-\hat{Y})+(\hat{Y}-i\bar{Y})^{\prime }(\hat{Y}-i%
\bar{Y})=\sum (Y_{i}-\hat{Y}_{i})^{2}+\sum (\hat{Y}_{i}-\bar{Y}%
_{i})^{2}=RSS+ESS
\end{equation*}

where $ESS$ is the so-called Explained Sum of Squares, i.e. the variability
explained by the regression.

So

\begin{equation*}
R^{2}=1-\frac{RSS}{TSS}=\frac{TSS-RSS}{TSS}=\frac{ESS}{TSS}.
\end{equation*}

Thus $R^{2}$ expresses the fraction of the sample variability "explained" by
the linear model. Also $0\leq R^{2}\leq 1$, since

\begin{equation*}
R^{2}=\frac{ESS}{TSS}=\frac{ESS}{RSS+ESS}
\end{equation*}

and both $ESS$ and $RSS$ are nonnegative quantities.

\begin{remark}
$R^{2}=1-\frac{RSS}{TSS}$ generally increases as the number of regressors $k$
increases. Why is that?\ $TSS$ does not change, but $RSS=\sum \left( Y_{i}-%
\hat{Y}\right) ^{2}$ will not increase and will likely decrease. This is due
to the fact that OLS\ is defined by $\min RSS$, thus if more parameters are
available to adjust, one can always do at least as well as with fewer
parameters.
\end{remark}

A\ better measure in a multiple regression model is the \emph{adjusted} $%
R^{2}$:

\begin{equation*}
\bar{R}^{2}=1-\frac{n-1}{n-k}\frac{RSS}{TSS}=1-\frac{(n-1)s^{2}}{TSS}%
=1-(1-R^{2})\frac{n-1}{n-k}
\end{equation*}

which is in general not monotone in $k$. The term $(n-1)/(n-k)$ is supposed
to control for the number of degress of freedom in the regression. However,
neither $R^{2}$ nor $\bar{R}^{2}$ are in general recommendable to use in
assessing the quality of a model. Do not choose a model with high $R^{2}$ or 
$\bar{R}^{2}$ that does not have economic justification.

\begin{remark}
For $k>1$ , $\bar{R}^{2}<R^{2}$ and $\bar{R}^{2}$ can also be negative
(since $n-k<n-1$, thus $\frac{n-1}{n-k}>1$).
\end{remark}

\section{(Section \#5) OLS:\ Hypothesis Testing\label{sec:5}}

Assumptions:

(A0): $Y=X\beta +\varepsilon $

(A1): $rank(X)=k$

(A2'): $\varepsilon \sim N(0,\sigma ^{2}I)\mid X$

\subsection{Testing Linear Restrictions}

We want to test linear restrictions on $\beta $ of the form

\begin{equation*}
H_{0}:\quad R\beta =c\qquad \text{vs.\qquad }H_{1}:\quad R\beta \neq
\varepsilon
\end{equation*}

where $R$ is a $J\times k$ matrix with $J\leq k$ and $rank(R)=J$. Each row
of $R$ represents a linear restriction on $\beta $.

\begin{example}
First row in $H_{0}$ is%
\begin{equation*}
\underset{(1\times k)}{\underbrace{r_{1}^{\prime }}}\beta =r_{11}\beta
_{1}+r_{12}\beta _{2}+\ldots +r_{1k}\beta _{k}=c_{1}.
\end{equation*}
\end{example}

\begin{example}
$r_{1}^{\prime }=(1,-1,0,\ldots ,0),~c_{1}=0\Longrightarrow \beta _{1}-\beta
_{2}=0$ or $\beta _{1}=\beta _{2}$

$r_{1}^{\prime }=(1,1,0,\ldots ,0),~c_{1}=1\Longrightarrow \beta _{1}+\beta
_{2}=1$
\end{example}

We will use \bigskip $\hat{\beta}$ and its derived distribution for testing
these restrictions. Recall from Theorem \ref{thm:S4T4} (T4) of Section \ref%
{sec:4} (\#4) that under Assumptions (A0) -- (A2'), we have

\begin{equation*}
\hat{\beta}\sim N(\beta ,\sigma ^{2}(X^{\prime }X)^{-1})\Longrightarrow R%
\hat{\beta}\sim N(R\beta ,\sigma ^{2}R(X^{\prime }X)^{-1}R^{\prime })
\end{equation*}

and

\begin{equation*}
(R\hat{\beta}-R\beta )^{\prime }\left[ \sigma ^{2}R(X^{\prime
}X)^{-1}R^{\prime }\right] ^{-1}(R\hat{\beta}-R\beta )\sim \chi
_{J}^{2}\qquad \text{by Theorem \ref{thm:S2T7} (T7) of Section \ref{sec:2}
(\#2)}
\end{equation*}

\begin{remark}
Variance matrix of $R\hat{\beta}$ is only invertible if $R$ has full row
rank $J$ (i.e. no redundant restrictions).
\end{remark}

Let us now present 3 numericaly equivalent test statistics that can be used
to do an F-test of $H_{0}$ vs $H_{1}$.

\begin{theorem}
\label{thm:S5T1} (T1) (Wald statistic) The statistic%
\begin{equation*}
\frac{W}{J}=\frac{(R\hat{\beta}-c)^{\prime }\left[ s^{2}R(X^{\prime
}X)^{-1}R^{\prime }\right] ^{-1}(R\hat{\beta}-c)}{J}\sim F(J,n-k)\quad \text{%
under }H_{0}.
\end{equation*}%
So we reject $H_{0}$ if $W/J>F_{J,n-k,\alpha }$, where $F_{J,n-k,\alpha }$
is the critical value for $\alpha $ level of significance:%
\begin{equation*}
P\left( W/J>F_{J,n-k,\alpha }\right) =\alpha
\end{equation*}
\end{theorem}

\begin{proof}
We have%
\begin{equation*}
\frac{W}{J}=\frac{(R\hat{\beta}-c)^{\prime }\left[ \sigma ^{2}R(X^{\prime
}X)^{-1}R^{\prime }\right] ^{-1}(R\hat{\beta}-c)/J}{\frac{(n-k)s^{2}}{%
(n-k)\sigma ^{2}}}\underset{\text{under~}H_{0}}{=}\frac{\chi _{J}^{2}/J}{%
\chi _{n-k}^{2}/(n-k)}\sim F(J,n-k)
\end{equation*}

Since by Theorem \ref{thm:S4T4} (T4) in Section \ref{sec:4} (\#4) the
denominator is a $\chi _{n-k}^{2}/(n-k)$, which by independence of $\hat{%
\beta}$, $s^{2}$ (Theorem \ref{thm:S4T4} (T4) in Section \ref{sec:4} (\#4))
is independent of numerator (which is $\chi _{J}^{2}/J$ under $H_{0}$ since $%
R\beta =c$).
\end{proof}

Recall $H_{0}:R\beta =c$ vs. $H_{1}:R\beta \neq c$. In\ Wald test, we
essentially take "a square" so that $W/J\geq 0$. So we are always looking at
2-sided alternatives (i.e. $\neq $ in $H_{1}$).

\begin{theorem}
\label{thm:S5T2}(T2) (F-statistic)\bigskip Let $\hat{\varepsilon}_{U}=Y-X%
\hat{\beta}$ be the \emph{unrestricted} vector of residuals from the OLS\
regression of $Y$ on $X$ and let $\hat{\varepsilon}_{R}=Y-X\hat{\beta}_{R}$
be the \emph{restricted} vector of residuals from the restricted regression
of $Y$ on $X$:%
\begin{eqnarray*}
&&\min_{\beta }(Y-X\beta )^{\prime }(Y-X\beta ) \\
&&\text{s.t. }R\beta =c
\end{eqnarray*}%
($\hat{\beta}_{R}$ is the solution to the restricted minimization problem,
i.e. the restricted OLS\ estimator), then the statistic%
\begin{equation*}
F=\frac{(\hat{\varepsilon}_{R}^{\prime }\hat{\varepsilon}_{R}-\hat{%
\varepsilon}_{U}^{\prime }\hat{\varepsilon}_{U})/J}{\hat{\varepsilon}%
_{U}^{\prime }\hat{\varepsilon}_{U}/(n-k)}=\frac{W}{J}\text{.}
\end{equation*}
\end{theorem}

\begin{proof}
Since the denominator is $\hat{\varepsilon}_{U}^{\prime }\hat{\varepsilon}%
_{U}/(n-k)=s^{2}$, we only need to show that%
\begin{equation*}
\hat{\varepsilon}_{R}^{\prime }\hat{\varepsilon}_{R}-\hat{\varepsilon}%
_{U}^{\prime }\hat{\varepsilon}_{U}=(R\hat{\beta}-c)^{\prime }\left[
R(X^{\prime }X)^{-1}R^{\prime }\right] ^{-1}(R\hat{\beta}-c).
\end{equation*}

Observe that%
\begin{equation*}
\hat{\varepsilon}_{R}=Y-X\hat{\beta}_{R}=Y-X\hat{\beta}+X(\hat{\beta}-\hat{%
\beta}_{R})=\hat{\varepsilon}_{U}+X(\hat{\beta}-\hat{\beta}_{R}).
\end{equation*}

Recall $\hat{\varepsilon}_{U}^{\prime }X=0$ (from decomposing $Y$ in Section %
\ref{sec:4} (\#4)), so the two parts in the sum are \bigskip orthogonal, so
that%
\begin{equation*}
\hat{\varepsilon}_{R}^{\prime }\hat{\varepsilon}_{R}=\hat{\varepsilon}%
_{U}^{\prime }\hat{\varepsilon}_{U}+(\hat{\beta}-\hat{\beta}_{R})^{\prime
}(X^{\prime }X)(\hat{\beta}-\hat{\beta}_{R})
\end{equation*}

Now suppose that the restricted OLS\ estimator takes the form%
\begin{equation}
\hat{\beta}_{R}=\hat{\beta}-(X^{\prime }X)^{-1}R^{\prime }\left[ R(X^{\prime
}X)^{-1}R^{\prime }\right] ^{-1}(R\hat{\beta}-c).  \label{eq:S5beta}
\end{equation}

So%
\begin{eqnarray*}
\hat{\varepsilon}_{R}^{\prime }\hat{\varepsilon}_{R}-\hat{\varepsilon}%
_{U}^{\prime }\hat{\varepsilon}_{U} &=&(\hat{\beta}-\hat{\beta}_{R})^{\prime
}(X^{\prime }X)(\hat{\beta}-\hat{\beta}_{R})= \\
&=&(R\hat{\beta}-c)^{\prime }\left[ R(X^{\prime }X)^{-1}R^{\prime }\right]
^{-1}R(X^{\prime }X)^{-1}\underset{=I}{\underbrace{(X^{\prime }X)(X^{\prime
}X)^{-1}}}R^{\prime }\left[ R^{\prime }(X^{\prime }X)^{-1}R^{\prime }\right]
^{-1}(R\hat{\beta}-c)= \\
&=&(R\hat{\beta}-c)^{\prime }\left[ R(X^{\prime }X)^{-1}R^{\prime }\right]
^{-1}\underset{=I}{\underbrace{R(X^{\prime }X)^{-1}R^{\prime }\left[
R^{\prime }(X^{\prime }X)^{-1}R^{\prime }\right] ^{-1}}}(R\hat{\beta}-c)= \\
&=&(R\hat{\beta}-c)^{\prime }\left[ R(X^{\prime }X)^{-1}R^{\prime }\right]
^{-1}(R\hat{\beta}-c)
\end{eqnarray*}

Finally, we have to show that the assumption made in equation (\ref%
{eq:S5beta}) indeed holds, i.e. that the solution to the restricted problem
indeed is $\hat{\beta}_{R}$ from (\ref{eq:S5beta}). We form the Lagrangean%
\begin{equation*}
L(\beta )=(Y-X\beta )^{\prime }(Y-X\beta )+2\lambda ^{\prime }(R\beta -c).
\end{equation*}%
Taking the first order conditions with respect to $\beta $ and $\lambda $,
we get%
\begin{eqnarray*}
-2\left[ X^{\prime }(Y-X\beta )-R^{\prime }\lambda \right]  &=&0\qquad \text{%
since }\frac{\partial (a^{\prime }b)}{\partial b}=a\text{ from MAHO.} \\
2(R\beta -c) &=&0
\end{eqnarray*}

Substituting the first equation into the second gives%
\begin{eqnarray*}
c &=&R\hat{\beta}_{R}=R(\hat{\beta}-(X^{\prime }X)^{-1}R^{\prime }\lambda )
\\
\Longrightarrow \lambda  &=&\left[ R(X^{\prime }X)^{-1}R^{\prime }\right]
^{-1}(R\hat{\beta}-c) \\
\Longrightarrow \hat{\beta}_{R} &=&\hat{\beta}-(X^{\prime }X)^{-1}R^{\prime }%
\left[ R(X^{\prime }X)^{-1}R^{\prime }\right] ^{-1}(R\hat{\beta}-c)
\end{eqnarray*}
\end{proof}

\begin{corollary}
\label{thm:S5C2}(C2)\bigskip ($R^{2}$ version of F-statistic) The statistic $%
F$ from Theorem \ref{thm:S5T2} (T2) can be written as (dividing numerator
and denominator by $Y^{\prime }Q_{o}Y$, where $Q_{0}=I-i(i^{\prime
}i)^{-1}i^{\prime }$):%
\begin{equation*}
F=\frac{\left( R_{U}^{2}-R_{R}^{2}\right) /J}{(1-R_{U}^{2})/(n-k)}
\end{equation*}%
where $R_{U}^{2}=1-\hat{\varepsilon}_{U}^{\prime }\hat{\varepsilon}%
_{U}/(Y^{\prime }Q_{o}Y)$ and $R_{R}^{2}=1-\hat{\varepsilon}_{R}^{\prime }%
\hat{\varepsilon}_{R}/(Y^{\prime }Q_{0}Y)$ are the $R^{2}$ from the
unrestricted and restricted regressions.
\end{corollary}

\begin{example}
\label{thm:S5E2}(E2) Testing a \emph{single} linear restriction:%
\begin{equation*}
H_{0}:r^{\prime }\beta =c\text{\quad vs\quad }H_{1}:r^{\prime }\beta \neq c.
\end{equation*}

Apart from the above F-tests, a t-test is possible in this case. Observe that%
\begin{equation*}
T=\frac{r^{\prime }\hat{\beta}-c}{\sqrt{s^{2}r^{\prime }(X^{\prime }X)^{-1}r}%
}\sim t_{n-k}\quad \text{under }H_{0}.
\end{equation*}

Note that%
\begin{equation*}
T^{2}=W=T=\frac{\left( r^{\prime }\hat{\beta}-c\right) ^{2}}{s^{2}r^{\prime
}(X^{\prime }X)^{-1}r}\sim F(1,n-k)\quad \text{from Theorem \ref{thm:S2T1}
(T1) of Section \ref{sec:2} (\#2).}
\end{equation*}

Compare this to the previously given definition of the Wald statistic.

The t-test also allows you to do a one-sided test.
\end{example}

\begin{example}
\label{thm:S5E3}(E3) (t-test on a single parameter)

If $r$ has a $1$ on the $k^{th}$ spot and $0$s everywhere else, and $c=\hat{%
\beta}_{k}$, then our test reduces to a t-test of the null $H_{0}:\beta _{k}=%
\hat{\beta}_{k}$ and the test statistic is%
\begin{equation*}
T=\frac{\hat{\beta}_{k}-\beta _{k}}{SE(\beta _{k})}
\end{equation*}%
since $\sqrt{s^{2}r^{\prime }(X^{\prime }X)^{-1}r}=SE(\hat{\beta}_{k})$ for $%
r^{\prime }=(0,\ldots ,0,1)$. Then%
\begin{equation*}
T^{2}=W=F\sim F(1,n-k).
\end{equation*}%
Again, $T^{2}$ can only be used for 2-sided tests.
\end{example}

\begin{remark}
For multiple linear restrictions (i.e. when $R$ is a $J\times k$ matrix with 
$J>1$), use the Wald test statistic or its equivalents. For instance, it
would be possible for $F=W/J$ to be quite large and to reject the \emph{joint%
}  $H_{0}$ that all the slope coefficients are zero, while at the same time
accepting $H_{0}:\delta _{k}=0$ when testing against. $H_{1}:\delta _{k}\neq
0$ in \emph{separate} tests of each element of $\delta $ (e.g. using
separate F-tests). The differences can appear for instance in case of
multicolinearity etc. (see TA session).

Thus we should always try to take advantage of the joint hypothesis und
prefer it to the individual t-tests when reasonable.
\end{remark}

\begin{example}
(Testing a set of linear restrictions, TA\ session) We have $H_{0}:R\beta =q$%
, where we have $J$ restrictions, $J\leq K$.

Test statistic is%
\[
\frac{(Rb-q)^{\prime }[R(X^{\prime }X)^{-1}R^{\prime }]^{-1}(Rb-q)/J}{\hat{%
\sigma}^{2}}\sim F(J,N-K)
\]%
We know%
\[
b\sim N(\beta ,\sigma ^{2}(X^{\prime }X)^{-1})
\]%
so that%
\[
Rb-q\sim N(R\beta -q,\sigma ^{2}R(X^{\prime }X)^{-1}R^{\prime }).
\]%
Thus%
\[
M(Rb-q)\sim N(0,I)
\]%
where $M=[\sigma ^{2}R(X^{\prime }X)^{-1}R^{\prime }]^{-1/2}$.

Derive the variance of $M(Rb-q)$. This is straightforward, since we know the
variance of $b$.

It follows immediately that $[M(Rb-q)]^{\prime }[M(Rb-q)]\sim \chi ^{2}(J)$
and we also know that $\frac{\hat{\sigma}^{2}(N-k)}{\sigma ^{2}}\sim \chi
^{2}(N-K)$. Combining these we have that%
\[
\frac{\lbrack M(Rb-q)]^{\prime }[M(Rb-q)]/J}{\frac{\hat{\sigma}^{2}(N-k)}{%
\sigma ^{2}}/(N-K)}\sim F(J,N-K).
\]
\end{example}

\begin{example}
(Confidence interval for the joint hypothesis, TA\ session) Define $\sigma
_{ij}=[(X^{\prime }X)^{-1}]_{ij}$, and assume that we test two restrictions $%
b_{1,2}=\beta _{1,2}$. Assume that $\sigma _{12}=\sigma _{21}=0$.\ The
confidence interval for the joint hypothesis is the inside of an ellipse,
given by%
\[
\frac{(b_{1}-\beta _{1})^{2}}{2\sigma _{11}\hat{\sigma}^{2}}+\frac{%
(b_{2}-\beta _{2})^{2}}{2\sigma _{22}\hat{\sigma}^{2}}\leq F_{\alpha
}(2,N-K).
\]

(check this, looks weird)

This is different than testing the two restrictions separately, since in the
case of two separate tests, the confidence interval (acceptance region) is a
rectangle.

The sizes of the elipse will be given by the reliability of the estimators
of $b_{1}$ and $b_{2}$ (i.e. by their variance).

If $\sigma _{12}\neq 0\,$, then the ellipse rotates. If $\sigma _{12}>0$,
then it will rotate toward the $x=y$ line.
\end{example}