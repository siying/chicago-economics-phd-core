%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% RBC model               %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;

format compact;

%cd '/Users/paymon/Documents/phd/Macro II TA/RBCsession'

%%% "calibrated" parameters of the model (for quarterly data)

epsilon = 3;
beta = 0.99;
alpha = 0.33;
delta = 0.01;
rho = 0.95;
gamma = 1;
A = 1;
B = 10;

[Ack,Acz,Alk,Alz,Akk,Akz,css,lss,kss] = RBCsolve(gamma,epsilon,beta,alpha,delta,A,B,rho);

%%% results

%%% main functions of the model
v = @(l) B.*(epsilon/(1+epsilon)).*l.^((1+epsilon)/epsilon);
vl = @(l) B.*l.^(1/epsilon);
vll = @(l) B*(1/epsilon)*l.^(1/epsilon - 1);
if gamma == 1
    U = @(c,l) log(c) - v(l);
else
    U = @(c,l) (c.*exp(-v(l))).^(1-gamma)/(1-gamma);
end
    % utility function
Uc = @(c,l) c.^(-gamma).*exp(-v(l)).^(1-gamma);
Ul = @(c,l) c.^(1-gamma).*exp(-v(l)).^(1-gamma).*(-vl(l));
Ucc = @(c,l) -gamma*c.^(-gamma-1).*exp(-v(l)).^(1-gamma);
Ucl = @(c,l) (1-gamma)*c.^(-gamma).*exp(-v(l)).^(1-gamma).*(-vl(l));
Ull = @(c,l) c.^(1-gamma).*exp(-v(l)).^(1-gamma).*((1-gamma).*vl(l).^2 - vll(l));
F = @(k,l) A*k.^(alpha).*l.^(1-alpha);
    % production function
Fk = @(k,l) A*alpha*(l./k).^(1-alpha);
Fl = @(k,l) A*(1-alpha)*(k./l).^(alpha);
Fkk = @(k,l) A*alpha*(alpha-1)*l.^(1-alpha).*k.^(alpha-2);
Fkl = @(k,l) A*alpha*(1-alpha)*l.^(-alpha).*k.^(alpha-1);
Fll = @(k,l) A*(1-alpha)*(-alpha)*k.^(alpha).*l.^(-alpha-1);

%%% impulse-response functions
% gives a small shock in the first period and no shocks thereafter, then
% finds the responses in current and future periods of capital,
% consumption, and labor. also computes the response to output, since it is
% determined by the others.
periods = 200;
epsilon0 = 0.01; %do a 1-percent shock
ztilde = zeros(periods,1);
for i = 1:periods
    ztilde(i) = rho^(i-1)*epsilon0;
end
ktilde = zeros(periods+1,1);
ctilde = zeros(periods,1);
ltilde = zeros(periods,1);
for i = 1:periods
    ktilde(i+1) = Akk*ktilde(i)+Akz*ztilde(i);
    ctilde(i) = Ack*ktilde(i)+Acz*ztilde(i);
    ltilde(i) = Alk*ktilde(i)+Alz*ztilde(i);
end
ktilde = ktilde(1:periods);
ytilde = ztilde + alpha*ktilde+(1-alpha)*ltilde; %output
t = (1:periods)';
figure;
subplot(4,1,1),plot(t,100*ltilde,'-g','linewidth',2),ylabel('hours')
subplot(4,1,2),plot(t,100*ktilde,'-r','linewidth',2),ylabel('capital')
subplot(4,1,3),plot(t,100*ytilde,'-b','linewidth',2),ylabel('output')
subplot(4,1,4),plot(t,100*ctilde,'-k','linewidth',2),ylabel('consumption')
%print -depsc2 impulse_responses.eps


%%% monte carlo
% here, we generate a path for the shocks (of length sims) and then find
% the responses of (c,l,k) to this path. from the (c,l,k) responses, get 
% variances and cross-correlations. then, repeat a bunch of times and
% average these statistics.
sims = 1000;
reps = 1000;
sigma_epsilon = sqrt(0.05^2*(1-rho^2)); %assumes a 5-percent unconditional shock volatility
SDs = zeros(3,1);
corrmat = zeros(3,3);
for j = 1:reps
    epsilon = normrnd(0,sigma_epsilon,sims,1);
    ztilde = zeros(sims,1);
    ztilde(1) = 0;
    for i = 2:sims
        ztilde(i) = rho*ztilde(i-1)+epsilon(i);
    end
    ktilde = zeros(sims+1,1);
    ctilde = zeros(sims,1);
    ltilde = zeros(sims,1);
    for i = 1:sims
        ktilde(i+1) = Akk*ktilde(i)+Akz*ztilde(i);
        ctilde(i) = Ack*ktilde(i)+Acz*ztilde(i);
        ltilde(i) = Alk*ktilde(i)+Alz*ztilde(i);
    end
    % recover the level series
    k1 = kss*exp(ktilde(2:end));
    c0 = css*exp(ctilde);
    l0 = lss*exp(ltilde);
    X = [c0,l0,k1];
    SDs = SDs + std(X,1)';
    corrmat = corrmat + corr(X);
end
SDs = 100*(1/reps)*SDs;
corrmat = (1/reps)*corrmat;
fprintf('\n')
disp('Standard deviations of (c,l,k) (%):');
disp(SDs);
fprintf('\n')
disp('Correlation matrix of (c,l,k):');
disp(corrmat);


%%% Euler equation check
% one check on how good the log-linear approximation is doing is to check
% how closely the Euler equation is to holding. to do this, simulate a path
% for the shocks (of length sims) and calculate the EE error in each period
% in that sample. then, average. then, repeat many times (reps) and average
% the errors.
avgEEdiff = zeros(reps,1);
absEEdiff = zeros(reps,1);
stdEEdiff = zeros(reps,1);
for j = 1:reps
    epsilon = normrnd(0,sigma_epsilon,sims,1);
    ztilde = zeros(sims+1,1);
    ztilde(1) = 0;
    for i = 1:sims
        ztilde(i+1) = rho*ztilde(i)+epsilon(i);
    end
    ktilde = zeros(sims+1,1);
    ctilde = zeros(sims,1);
    ltilde = zeros(sims,1);
    for i = 1:sims
        ktilde(i+1) = Akk*ktilde(i)+Akz*ztilde(i);
        ctilde(i) = Ack*ktilde(i)+Acz*ztilde(i);
        ltilde(i) = Alk*ktilde(i)+Alz*ztilde(i);
    end
    % recover the level series
    klevel = kss*exp(ktilde);
    clevel = [css;css*exp(ctilde)];
    llevel = [lss;lss*exp(ltilde)];
    % right-hand side of EE
    RHS = beta*(Uc(clevel(2:end),llevel(2:end)).* ...
        (exp(ztilde(2:end)).*Fk(klevel(2:end),llevel(2:end))+1-delta));
    % left-hand side of EE
    LHS = Uc(clevel(1:end-1),llevel(1:end-1));
    % calculate the average percentage difference, the average absolute
    % percentage difference, and the variance of the difference in sims 
    avgEEdiff(j) = (1/sims)*sum((LHS./RHS - 1));
    absEEdiff(j) = (1/sims)*sum(abs(LHS./RHS - 1));
    stdEEdiff(j) = std((LHS./RHS - 1),1);
end
avgEEdiff_stat = 100*(1/reps)*sum(avgEEdiff);
absEEdiff_stat = 100*(1/reps)*sum(absEEdiff);
stdEEdiff_stat = 100*(1/reps)*sum(stdEEdiff);
fprintf('\n')
disp('Average EE error (%):');
disp(avgEEdiff_stat);
fprintf('\n')
disp('Average absolute EE error (%):');
disp(absEEdiff_stat);
fprintf('\n')
disp('Average EE error standard deviation (%):');
disp(stdEEdiff_stat);


