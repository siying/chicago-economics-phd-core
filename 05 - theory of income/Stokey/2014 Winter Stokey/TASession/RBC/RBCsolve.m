%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function that performs log linearization
% for the basic real business cycle model
% 
% gamma = coefficient of relative risk aversion
% epsilon = frisch labor elasticity
% beta = subjective discount factor
% alpha = capital share in production
% delta = depreciation rate
% A = technology
% B = disutility of labor
% rho = persistence of shocks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Ack,Acz,Alk,Alz,Akk,Akz,css,lss,kss] = RBCsolve(gamma,epsilon,beta,alpha,delta,A,B,rho)

%%% main functions of the model
v = @(l) B.*(epsilon/(1+epsilon)).*l.^((1+epsilon)/epsilon);
vl = @(l) B.*l.^(1/epsilon);
vll = @(l) B*(1/epsilon)*l.^(1/epsilon - 1);
if gamma == 1
    U = @(c,l) log(c) - v(l);
else
    U = @(c,l) (c.*exp(-v(l))).^(1-gamma)/(1-gamma);
end
    % utility function
Uc = @(c,l) c.^(-gamma).*exp(-v(l)).^(1-gamma);
Ul = @(c,l) c.^(1-gamma).*exp(-v(l)).^(1-gamma).*(-vl(l));
Ucc = @(c,l) -gamma*c.^(-gamma-1).*exp(-v(l)).^(1-gamma);
Ucl = @(c,l) (1-gamma)*c.^(-gamma).*exp(-v(l)).^(1-gamma).*(-vl(l));
Ull = @(c,l) c.^(1-gamma).*exp(-v(l)).^(1-gamma).*((1-gamma).*vl(l).^2 - vll(l));
F = @(k,l) A*k.^(alpha).*l.^(1-alpha);
    % production function
Fk = @(k,l) A*alpha*(l./k).^(1-alpha);
Fl = @(k,l) A*(1-alpha)*(k./l).^(alpha);
Fkk = @(k,l) A*alpha*(alpha-1)*l.^(1-alpha).*k.^(alpha-2);
Fkl = @(k,l) A*alpha*(1-alpha)*l.^(-alpha).*k.^(alpha-1);
Fll = @(k,l) A*(1-alpha)*(-alpha)*k.^(alpha).*l.^(-alpha-1);


%%% equations for SS (same as deterministic steady state)
% first equation is labor FOC
% second equation is consumption EE
% third equation is the resource constraint
%options = optimset('Display','off');
fun = @(SS) [Uc(SS(1),SS(2)).*Fl(SS(3),SS(2)) + Ul(SS(1),SS(2));...
    Uc(SS(1),SS(2)) - beta*Uc(SS(1),SS(2)).*(Fk(SS(3),SS(2)) + (1-delta));...
    SS(1) - F(SS(3),SS(2)) - (1-delta)*SS(3) + SS(3)];
x0 = [0.5;0.5;5];
SS = fsolve(fun,x0);
css = SS(1);
lss = SS(2);
kss = SS(3);


%%% log-linearize
EElog = @(ct1,ct,lt1,lt,kt1,kt,zt1,zt) css*(-Ucc(css,lss))*ct + lss*(-Ucl(css,lss))*lt ...
    + css*(beta*Ucc(css,lss)*(Fk(kss,lss)+1-delta))*ct1 ...
    + lss*(beta*Ucl(css,lss)*(Fk(kss,lss)+1-delta) + beta*Uc(css,lss)*Fkl(kss,lss))*lt1 ...
    + kss*(beta*Uc(css,lss)*Fkk(kss,lss))*kt1 + (beta*Uc(css,lss)*Fk(kss,lss))*zt1;
LLlog = @(ct1,ct,lt1,lt,kt1,kt,zt1,zt) css*(Ucc(css,lss)*Fl(kss,lss)+Ucl(css,lss))*ct ...
    + lss*(Uc(css,lss)*Fll(kss,lss) + Ucl(css,lss)*Fl(kss,lss) + Ull(css,lss))*lt ...
    + kss*(Uc(css,lss)*Fkl(kss,lss))*kt + (Uc(css,lss)*Fl(kss,lss))*zt;
RClog = @(ct1,ct,lt1,lt,kt1,kt,zt1,zt) css*(-1)*ct + lss*(Fl(kss,lss))*lt ...
    + kss*(Fk(kss,lss)+1-delta)*kt + kss*(-1)*kt1 + (F(kss,lss))*zt;
% in this step, you substitute the guesses into the three log-linear
% equations and then set (a) kt = 1, zt = 0, (b) kt = 0, zt = 1 in order to
% determine the coefficients
loglinfun = @(C) [EElog(C(1)*C(5),C(1),C(3)*C(5),C(3),C(5),1,0,0);...
    EElog(C(1)*C(6)+C(2)*rho,C(2),C(3)*C(6)+C(4)*rho,C(4),C(6),0,rho,1);...
    LLlog(C(1)*C(5),C(1),C(3)*C(5),C(3),C(5),1,0,0);...
    LLlog(C(1)*C(6)+C(2)*rho,C(2),C(3)*C(6)+C(4)*rho,C(4),C(6),0,rho,1);...
    RClog(C(1)*C(5),C(1),C(3)*C(5),C(3),C(5),1,0,0);...
    RClog(C(1)*C(6)+C(2)*rho,C(2),C(3)*C(6)+C(4)*rho,C(4),C(6),0,rho,1)];
x0 = [1;1;1;1;1;1];
C = fsolve(loglinfun,x0);
Ack = C(1);
Acz = C(2);
Alk = C(3);
Alz = C(4);
Akk = C(5);
Akz = C(6);


end