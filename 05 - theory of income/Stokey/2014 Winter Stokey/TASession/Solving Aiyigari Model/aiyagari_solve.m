%%%%%%%%%%%%%%%%%%%%%%%%%
% Paymon Khorrami,
% modified from:
% 	Jung Sakong 		
% 	Applied Macro II	
% 	Problem Set 1		
%%%%%%%%%%%%%%%%%%%%%%%%%

cd '/Users/paymon/Documents/phd/Macro II TA/sessions/Aiyagari'


clear all;

%%% INPUTS %%%
% parameters
beta = 0.96;
mu = 3;
alpha = 0.36;
delta = 0.08;
sigma = 0.4;
phi = 0;
rho = 0.6;

% other things to set
nba = 2500;  % number of grid points for assets
Kdiff = 1;
adiff = 1;
Vdiff = 1;
tdiff = 1;
tolerance = 1e-6; % tolerance for policy fcn, value fcn, and stationary distn
toleranceK = 1e-3; % tolerance for capital stock
amax = 25;
xi = 0.8;


%%% FUNCTIONS & PROCESSES %%%
% utility function
util = @(consumption) consumption.^(1-mu)/(1-mu);

% Marginal products
f_k = @(capital,labor)   alpha .* (labor ./ capital) .^(1-alpha);
f_l = @(capital,labor)   (1-alpha) .* (capital ./ labor) .^ alpha;

% labor process - Markov chain
logl = [-sigma sigma];
P = [(1+rho)/2 (1-rho)/2; (1-rho)/2 (1+rho)/2];

%%% SET-UP %%%
abar = (alpha/(1/beta-1+delta))^(1/(1-alpha));   % bechmark: deterministic SS
agrid = linspace(phi+0.00000001,amax,nba)';
v = zeros(nba,2);
ev = zeros(nba,2);
dr = [(1:nba)' (1:nba)'];           % decision rule index
Q = zeros(2*nba,2*nba);             % a&l transition matrix
q = zeros(nba,2);                   % q(a,l)
K0 = 1.25*abar;
fprintf('K0, Kstar, Kdiff, beta*(1+r) \n');

%%% SOLVE %%%
while Kdiff > toleranceK;
    % guess prices implied by K0
    r = f_k(K0,(exp(sigma)+exp(-sigma))/2) - delta;
    w = f_l(K0,(exp(sigma)+exp(-sigma))/2);
    
    % Howard policy iteration
    % middle loop: policy
    while adiff > tolerance;
        % inner loop: value
        while Vdiff > tolerance;
            v0 = v;
            ev = [v([dr(:,1) dr(:,1)+nba])*(P(1,:))'  v([dr(:,2) dr(:,2)+nba])*(P(2,:))'];
            c = max(w*(ones(nba,1)*exp(logl))+(agrid*ones(1,2))*(1+r)-agrid(dr),1e-100);
            v = util(c) + beta * ev;
            Vdiff = max(max(abs(v0 - v)));
        end
        
        % given v, find best policy (do this a few times)
        v0 = v;
        dr0 = dr;
        for i = 1:size(v,1)
            for j = 1:size(v,2)
                anow = agrid(i);
                lnow = exp(logl(j));
                c = w*lnow + (1+r)*anow - agrid;
                c(c<=0) = 1e-100;
                ev = v*P(j,:)';
                [v(i,j),dr(i,j)] = max(util(c) + beta*ev);
            end
        end
        adiff = max(max(abs(dr-dr0)));  % iterate until policy rule is identical
        Vdiff = 1;          % to get back into the value loop
        
%         % 3-dimensional arrays
%         v0 = v;
%         dr0 = dr;
%         l3 = exp(repmat(logl,[nba 1 nba]));
%         a3 = repmat(agrid,[1 2 nba]);
%         ap3 = permute(a3,[3 2 1]);
%         c = max(w * l3 + a3 * (1+r) - ap3,1e-100);
%         ev = permute(repmat((v * P'),[1 1 nba]),[3 2 1]);
%         [v,dr] = max(util(c)+beta*ev,[],3);
%         adiff = max(max(abs(dr-dr0)));
%         Vdiff = 1;          % to get back into the value loop
    end
    
    % Stationary distribution
    Pl = kron(P,ones(nba));         % P(l'|a,l)
    Pa_half = zeros(nba*2,nba);          
    Pa_half(sub2ind(size(Pa_half),[1:2*nba],(reshape(dr,2*nba,1))')) = 1;
    Pa = [Pa_half Pa_half];         % P(a'|a,l)
    Q = Pl .* Pa;                   % P(a',l'|a,l)
    
    % invert matrices: fastest method
    q = (Q' - eye(size(Q)) + ones(size(Q)))^(-1)*ones(size(Q,1),1);
    q(q<0) = 0;
    q = q/sum(q);
    q = reshape(q,nba,2);
    
%     % unit eigenvalue: second fastest
%     [V,Lambda] = eig(Q');
%     Ldiag = diag(Lambda);
%     ind = find(Ldiag<=1.0001 & Ldiag>=0.9999,1,'first');
%     station = V(:,ind);  % eigenvector corresponding to unit eigenvalue
%     station = station/sum(station);
%     q = reshape(station,nba,2);
%     
%     % iterate: slowest method, and looks like it won't give true ergodic
%     PT0 = Q;
%     while tdiff > tolerance;
%        PT = PT0*Q;
%        tdiff = max(abs(PT(1,:)-PT(2,:))) + max(abs(PT(1,:)-PT0(1,:)));
%        PT0 = PT;
%     end
%     firstrow = (PT(1,:))';
%     q = reshape(firstrow,nba,2);
        
    % Implied aggregate capital
    Kstar = sum(sum([agrid agrid].*q));
    
    % update K0
    Kdiff = abs(Kstar - K0);
    adiff = 1;          % get back into the inner loops
    Vdiff = 1;
    tdiff = 1;
    fprintf('%5.3f %5.3f %5.3f %5.3f \n',K0,Kstar,Kdiff,beta*(1+r));
    K0 = xi*K0 + (1-xi) * Kstar;
end

% save the workspace
save('ta_paymon.mat');

%%% generate results %%%
clear all;
load('ta_paymon.mat');

% decision rule
aprime = agrid(dr);
drfig = figure();
plot(agrid,aprime(:,1),agrid,aprime(:,2));
legend('low l','high l','Location','Best');
xlabel('asset (t) ');
ylabel('asset (t+1) ');
title('Figure 1. Decision Rule for Saving');
saveas(drfig,'ps1_drfig.jpg');

% consumption
c = max(w*(ones(nba,1)*exp(logl))+(agrid*ones(1,2))*(1+r)-agrid(dr),1e-100);
cfig = figure();
plot(agrid,c(:,1),agrid,c(:,2));
legend('low l','high l','Location','Best');
xlabel('asset');
ylabel('consumption');
title('Figure 2. Decision Rule for Consumption');
saveas(cfig,'ps1_cfig.jpg');

% value
vfig = figure();
plot(agrid,v(:,1),agrid,v(:,2));
legend('low l','high l','Location','Best');
xlabel('asset');
ylabel('value');
title('Figure 3. Optimized Value');
saveas(vfig,'ps1_vfig.jpg');

% invariant distribution
sumst = sum(q');
idfig = figure();
plot(agrid,sumst,'.k');
hold on;
plot([Kstar,Kstar],[0,max(sumst)],'r');
xlabel('asset');
ylabel('density');
title('Figure 4. Invariant Asset Density');
hold off;
saveas(idfig,'ps1_idfig.jpg');
% separately by labor status - CDF
cdf = cumsum(q)*2;
idsepfig = figure();          
plot(agrid,cdf(:,1),agrid,cdf(:,2));
hold on;
plot([Kstar,Kstar],[0,max(max(cdf))],'r');
axis([agrid(1),agrid(nba),0,1]);
legend('low l','high l','Location','Best');
xlabel('asset');
ylabel('probability');
title('Figure 5. Invariant Asset Distribution (CDF) by Labor Type');
hold off;
saveas(idsepfig,'ps1_idsepfig.jpg');

% aggregate savings, labor, prices
fprintf('%-20s %5.2f \n','Aggregate capital:',Kstar);
fprintf('%-20s %5.2f \n','Aggregate labor:',(exp(sigma)+exp(-sigma))/2);
output = Kstar^alpha*((exp(sigma)+exp(-sigma))/2)^(1-alpha);
fprintf('%-20s %5.2f \n','Aggregate output:',output);
fprintf('%-20s %5.2f \n','Interest rate (%):',r*100);
fprintf('%-20s %5.2f \n','Wage:',w);
fprintf('%-20s %5.2f \n','Saving rate (%):',100*delta*Kstar/output);
fprintf('%-20s %5.2f \n','beta * (1+r):',beta*(1+r));




