%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function that performs log linearization
% for the basic real business cycle model
%
% Adapted by Yinan Su to use symbolic equations
%
% Add "_yns" to  "RBCexample" to call this function
% 
% gamma = coefficient of relative risk aversion
% epsilon = frisch labor elasticity
% beta = subjective discount factor
% alpha = capital share in production
% delta = depreciation rate
% A = technology
% B = disutility of labor
% rho = persistence of shocks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%clear all;
%close all;

%format compact;

%cd 'C:\Users\Yinan\Dropbox\Documents Dropbox\Income TA\TA session 20140131\RBCsession'
%'/Users/paymon/Documents/phd/Macro II TA/RBCsession'

%%% "calibrated" parameters of the model (for quarterly data)

%epsilon = 3;
%beta = 0.99;
%alpha = 0.33;
%delta = 0.01;
%rho = 0.95;
%gamma = 1;
%A = 1;
%B = 10;

function [Ack,Acz,Alk,Alz,Akk,Akz,css,lss,kss] = RBCsolve_yns(gamma,epsilon,beta,alpha,delta,A,B,rho)


%%% Set up the non-linear system in terms of the eight variables
syms k c l z kp cp lp zp_;                  % p stands for prime, next period value. There is a name duplicate with a function called zp in matlab. So I changed to zp_. Same for lt_ later. Please let me know anyone can solve this better.
%syms v U Uc Ucp F Fk Fkp EE LL RC;         % all should be interms of the eight variables: [k c l z kp cp lp zp_]
% p st


v = B * (epsilon/(1+epsilon)) * l ^ ((1+epsilon)/epsilon);
if gamma == 1
    U = log(c) - v;
else
    U = (c * exp(-v))^(1-gamma)/(1-gamma);
end
Uc = diff(U, c);
Ucp = subs (Uc, [c, l], [cp, lp]);
Ul = diff(U, l);

F = A * k^ (alpha) * l^ (1-alpha);
Fk = diff(F, k);
Fkp = subs(Fk, [k, l], [kp, lp]);
Fl = diff(F, l);

EE = beta * (Ucp * (exp(zp_) * Fkp + (1 - delta))) - Uc;  %% Omit expectation for now
LL = exp(z) * Uc * Fl + Ul;
RC = exp(z) * F + (1-delta) * k - kp - c;
system = [EE; LL; RC];


%%% Solve Steady state, fun is a double type function, not symbolic.
fun = @(SSx)  double ( subs(system, [k c l z kp cp lp zp_], [SSx, 0, SSx, 0]) );
x0 = [5, 0.5, 0.5];
SS = fsolve(fun, x0);


%%% Linearization See "TA session note 20140206.pdf" for more detailed explanation
%   Use percentage deviation for k c l, regular deviation for z. 

% Jacobians of the system should be 3 times 8. Separate to two parts for
% convenience. These are numeric matrices evaluated at SS.
Jac36 = double ( subs(jacobian(system, [k c l kp cp lp]), [k c l z kp cp lp zp_], [SS, 0, SS, 0]) );
Jacz32= double ( subs(jacobian(system, [z zp_]),          [k c l z kp cp lp zp_], [SS, 0, SS, 0]) );

syms kt ct lt_ kpt cpt lpt zh zph;       % t stands for telde for percentage deviation; h stands for hat for deviation. notice zh = z since zss = 0. I just add redundant zh, zph for clearification. 

kclt16 = [kt ct lt_ kpt cpt lpt];
zh12   = [zh zph];
linsys = ( Jac36 .* (ones(3, 1) * [SS, SS]) ) * kclt16' + Jacz32 * zh12';
%   linsys is the regular deviation of the three equations.



%%% Method of undetermined coeffcients
%   Set up AB32 as symbolic matrix. Get the new linear system (nlinsys)of kt, zh, and
%   epsilon only.

AB32 = sym('AB32', [3, 2]);

syms epsilon ;
nkclz = sym('nkclz', [1, 8]);
nkclz(1)   = kt;
nkclz(7)   = zh;
nkclz(8)   = rho * zh + epsilon;
nkclz(2:4) = ( AB32 * [kt; zh] )';
nkclz(5:6) = ( AB32([1, 2], :) * [nkclz(4); nkclz(8)] )';

nlinsys = subs(linsys, [kt ct lt_ kpt cpt lpt zh zph], nkclz);   %nlinsys only of (kt zh epsilon)

%   nlinsys should hold for any kt, zh. Checking (kt, zh) for (1, 0) and (0,
%   1) is enough since it's linear
%   Evaluate epsilon at 0 for expectation.
%   Get 6 equations, 6 unknow linear formular. Solve it as numeric function

eq1 = subs(nlinsys, [kt zh epsilon], [1, 0, 0]);
eq2 = subs(nlinsys, [kt zh epsilon], [0, 1, 0]);
eq  = [eq1; eq2]; 

fun2 = @(AB32x)  double ( subs(eq, AB32, AB32x) );
x0 = [1, 1; 1, 1; 1, 1];
AB32r = fsolve(fun2, x0);

%   I am done. The rest is to connect to Paymon's output interface

Ack = AB32r(1, 1);
Acz = AB32r(1, 2);
Alk = AB32r(2, 1);
Alz = AB32r(2, 2);
Akk = AB32r(3, 1);
Akz = AB32r(3, 2);

kss = SS(1);
css = SS(2);
lss = SS(3);

display('--RBCsolve_yns exits--');

end