%%%% A littel example to show how symbolic equations work.
clear all;

syms x y z;
y = x + 2;
z = y + 3;
% now z = 'x + 5', x = 'x', y = 'x + 2'
subs(z, x, 1)
% answer is '6' 

clear all;

syms x y z;
z = y + 3;
y = x + 2;
% now z = 'y + 3', x = 'x', y = 'x + 2'
subs(z, x, 1)
% subs finds no 'x' in z now, answer is still 'y + 3'

subs(z, y, 1)
% now y = 'x + 2', subs finds no 'x + 2' in z now, answer is still 'y + 3'

subs(z, 'y', 1)
% This works. Subs finds 'y' in z now, answer is '4'

y
y = y
%This doen not change the value of y, y still = 'y + 3'

y = 'y'
% This makes y = 'y'

display (subs(z, y, 1) );
% now this works since y is restored to 'y' answer is '4' compared to line
% 20

display (subs(z, y, sin(x)));
%can substitute in an equation too


