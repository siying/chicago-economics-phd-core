%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Neoclassical growth model %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;

format compact;

cd '/Users/paymon/Documents/phd/Macro II TA/VIsession'

%%% "calibrated" parameters of the model

epsilon = 1;
beta = 0.96;
alpha = 0.33;
delta = 0.04;
A = 1;
B = 5;
tol = 1e-10;
N = 100;

gamma = 0.5;
[v_lowg,cons_lowg,labor_lowg,kap_lowg,Ret_lowg,k0,SS_lowg] = ...
    VIsolve(gamma,epsilon,beta,alpha,delta,A,B,tol,N);

gamma = 1;
[v_medg,cons_medg,labor_medg,kap_medg,Ret_medg,k0_medg,SS_medg] = ...
    VIsolve(gamma,epsilon,beta,alpha,delta,A,B,tol,N);

gamma = 5;
[v_highg,cons_highg,labor_highg,kap_highg,Ret_highg,k0_highg,SS_highg] = ...
    VIsolve(gamma,epsilon,beta,alpha,delta,A,B,tol,N);

%%% plots

figure;
hold on;
box on;
plot(k0,v_lowg,'-b','linewidth',1.25);
plot(k0,v_medg,'-r','linewidth',1.25);
plot(k0,v_highg,'-k','linewidth',1.25);
legend('low gamma','medium gamma','high gamma','location','southeast');
xlabel('capital')
ylabel('value')
axis([0.1 6 -100 inf])
print -depsc2 value.eps
close

figure;
hold on;
box on;
plot(k0,kap_lowg,'-b','linewidth',1.25);
plot(k0,kap_medg,'-r','linewidth',1.25);
plot(k0,kap_highg,'-k','linewidth',1.25);
plot(k0,k0,'--k','linewidth',0.75);
legend('low gamma','medium gamma','high gamma','location','southeast');
xlabel('capital')
ylabel('next period capital')
axis([0 6 0 6])
print -depsc2 capital.eps
close

figure;
hold on;
box on;
plot(k0,cons_lowg,'-b','linewidth',1.25);
plot(k0,cons_medg,'-r','linewidth',1.25);
plot(k0,cons_highg,'-k','linewidth',1.25);
legend('low gamma','medium gamma','high gamma','location','southeast');
xlabel('capital')
ylabel('consumption')
axis([0 6 0 2])
print -depsc2 consumption.eps
close

figure;
hold on;
box on;
plot(k0,labor_lowg,'-b','linewidth',1.25);
plot(k0,labor_medg,'-r','linewidth',1.25);
plot(k0,labor_highg,'-k','linewidth',1.25);
legend('low gamma','medium gamma','high gamma','location','southeast');
xlabel('capital')
ylabel('labor')
axis([0 6 0 1.5])
print -depsc2 labor.eps
close



