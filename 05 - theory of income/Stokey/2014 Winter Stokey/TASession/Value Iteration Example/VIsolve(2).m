%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function that performs value function iteration
% for the neoclassical growth model
% 
% gamma = coefficient of relative risk aversion
% epsilon = frisch labor elasticity
% beta = subjective discount factor
% alpha = capital share in production
% delta = depreciation rate
% A = technology
% B = disutility of labor
% tol = tolerance for convergence in VI
% N = number of grid points on capital
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [v,c2,l2,k2,R,k0,SS] = VIsolve(gamma,epsilon,beta,alpha,delta,A,B,tol,N)

%%% main functions of the model
v = @(l) B.*(epsilon/(1+epsilon)).*l.^((1+epsilon)/epsilon);
vl = @(l) B.*l.^(1/epsilon);
if gamma == 1
    U = @(c,l) log(c) - v(l);
else
    U = @(c,l) (c.*exp(-v(l))).^(1-gamma)/(1-gamma);
end
    % utility function
Uc = @(c,l) c.^(-gamma).*exp(-v(l)).^(1-gamma);
Ul = @(c,l) c.^(1-gamma).*exp(-v(l)).^(1-gamma).*(-vl(l));
F = @(k,l) A*k.^(alpha).*l.^(1-alpha);
    % production function
Fk = @(k,l) A*alpha*(l./k).^(1-alpha);
Fl = @(k,l) A*(1-alpha)*(k./l).^(alpha);


%%% equations for SS
% first equation is labor FOC
% second equation is consumption EE
% third equation is the resource constraint
%options = optimset('Display','off');
fun = @(SS) [Uc(SS(1),SS(2)).*Fl(SS(3),SS(2)) + Ul(SS(1),SS(2));...
    Uc(SS(1),SS(2)) - beta*Uc(SS(1),SS(2)).*(Fk(SS(3),SS(2)) + (1-delta));...
    SS(1) - F(SS(3),SS(2)) - (1-delta)*SS(3) + SS(3)];
x0 = [0.5;0.5;5];
SS = fsolve(fun,x0);
css = SS(1);
lss = SS(2);
kss = SS(3);


%%% value function iteration
klowest = 0.01; % min value for k
khighest = (A/delta)^(1/(1-alpha)); % max value for k (if can only work max 1 unit)
k0 = linspace(klowest,khighest/10,N); % discretized state space
R = zeros(N,N); % return function
v = zeros(N,1); % value function
Tv = v; % initialize T operator
% policy functions
l1 = zeros(N,N);
c1 = zeros(N,N);
k1 = zeros(N,1);
% more functions I need
kprime = @(c,l,k) k.^(alpha).*l.^(1-alpha) + (1-delta)*k - c;
    % from resource constraint
cnowRC = @(l,k,knext) k.^(alpha).*l.^(1-alpha) + (1-delta)*k - knext;
    % from resource constraint
cnowLL = @(l,k) ((A/B)*l.^(-1/epsilon).*(1-alpha).*(k./l).^alpha).^(1/gamma);
    % from labor leisure

options = optimset('Display','off');
% (1) static problem: pick k and k' and then use solve for c and l
for i = 1:N % loop over the initial capital
    x = k0(i);
    % [klow,khigh] is the feasible set for next period's capital
    khigh = kprime(0,1,x);
    klow = 0;
    for j = 1:N % loop over next period's capital
        y = k0(j);
        R(i,j) = -Inf; % set the default return to negative infinity
        % check to see if next period's capital choice is feasible
        if y <= khigh && y >= klow
            % if so, find consumption and hours, and set the return fcn
            zl = linspace(0,1.5,5001);
            labor_leisure_eq = Uc(cnowRC(zl,x,y),zl).*Fl(x,zl) + Ul(cnowRC(zl,x,y),zl);
            min_dev = min(abs(labor_leisure_eq));
            %fprintf('labor FOC deviation from 0: %.3f \n',min_dev);
            index = find(abs(labor_leisure_eq)==min_dev,1,'first');
            l1(i,j) = zl(index);
            c1(i,j) = cnowRC(l1(i,j),x,y);
            if c1(i,j) <= 0
                R(i,j) = -Inf;
            else
                R(i,j) = U(c1(i,j),l1(i,j));
            end
        end
    end
end

diff = 1;
i = 1;
options = optimset('Display','on');
% (2) dynamic problem: find optimal capital evolution, given c and l
while diff > tol % keep going until convergence  
    for j = 1:N % loop over current capital
        Tv(j) = max(R(j,:) + beta*v'); % apply T operator
        % find the policy function k' index
        k1(j) = find(R(j,:) + beta*v'==Tv(j),1,'first');
    end
    diff = max(abs(Tv - v)); % calculate sup-norm
    %fprintf('Iteration %3d: %.6f\n',i,diff);
    if(diff <= tol)
        fprintf('Number of iterations: %3d \n',i);
        fprintf('Value function iteration has converged.\n');
        break;
    end
    v = Tv; % the next value function is the iterated result
    i = i+1;
end

%%% get policy functions as only functions of k
l2 = zeros(N,1);
c2 = zeros(N,1);
k2 = zeros(N,1);
for i = 1:N
    l2(i) = l1(i,k1(i));
    k2(i) = k0(k1(i));
    c2(i) = cnowRC(l2(i),k0(i),k2(i));
end

end