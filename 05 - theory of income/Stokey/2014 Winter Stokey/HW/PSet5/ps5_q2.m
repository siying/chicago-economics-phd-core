%%%%%%%%%%%%%%%%%%%%%%%%%
% Paymon Khorrami
% Theory of Income II	
% Problem Set 5, q2		
%%%%%%%%%%%%%%%%%%%%%%%%%

cd '/Users/paymon/Documents/phd/Macro II TA'

clear all;
close all;

%%% Parameters
beta = 0.9;
Zh = 1;
Zl = 0.5;
r = 0.05;
p = 0.6;
omega = 2;

%%% Functions we need
util = @(prefshock,consumption) prefshock*log(consumption);

%%% Grid
amax = 10; % arbitrary max value for assets
numpts = 2501; % number of grid points
agrid = linspace(0,amax,numpts)';
zgrid = [Zh,Zl];

%%% Initializations: (:,1) = high shock,  (:,2) = low shock
v0 = zeros(numpts,2); % start with value functions equal to 0, not important
v1 =  zeros(numpts,2);
Arule = [(1:numpts)' (1:numpts)']; % start with the decision rule of maintaining current assets
Vdiff = 99999;
Adiff = 99999;
tol = 1e-8;
iter = 1;

%%% Value function iteration
while Adiff > tol
    
    v0 = v1;
    Arule0 = Arule;
    Ev = p*v0(:,1) + (1-p)*v0(:,2); % expected continuation value
    
    for aa = 1:size(v1,1)
        
        for zz = 1:size(v1,2)
            
            anow = agrid(aa); % current assets
            znow = zgrid(zz); % current preference shock
            cons = (1+r)*anow + omega - agrid; % all possible consumptions
            cons(cons<=0) = 1e-200; % instead of negative consumption, make utility diverge
            [v1(aa,zz) Arule(aa,zz)] = max(util(zgrid(zz),cons)+beta*Ev); % Bellman equation
            
        end
        
    end
    
    Adiff = max(max(abs(Arule-Arule0))); % since decision rules are matrices of 0s and 1s, iterate until identical                    
    fprintf('Iteration #: %3.0f, d(a1,a0) = %3.0f \n',iter,Adiff);
    iter = iter + 1;

end

iter = 1;

% now, just for precision, iterate on the value function with the optimal
% policy (this is very fast)
while Vdiff > tol
    
    v0 = v1;
    Ev = p*v0(:,1) + (1-p)*v0(:,2); % expected continuation value
    
    for zz = 1:size(v1,2)
        
        aprime = agrid(Arule(:,zz)); % pick points from grid with decision rule
        cons = (1+r)*agrid + omega - aprime;
        cons(cons<=0) = 1e-200; % don't want negative consumption with log(c)
        v1(:,zz) = util(zgrid(zz),cons) + beta*Ev; % like a Bellman, but with no max
        
    end
    
    Vdiff = max(max(abs(v1-v0))); % sup-norm distance between v0 and v1
    fprintf('Iteration #: %3.0f, d(v1,v0) = %.3f \n',iter,Vdiff);
    iter = iter + 1;
    
end

assets = agrid(Arule);
cons = (1+r)*[agrid agrid] + omega - agrid(Arule);

%%% Stationary distribution
%% simulation...
% just simulate a path, and count times visiting states. this is the
% fastest. it works if there is a unique stationary distribution. it's
% required that we start somewhere in the ergodic set of assets, so I start
% with a = 0. then I also do the direct computation.
N = 100000000;
stationaryassetind=zeros(N+1,1);
stationaryassetind(1,1)=1;
randshocks = rand(N,1);

for i=1:N
    shock = 1*(randshocks(i)<p)+2*(randshocks(i)>=p);
    stationaryassetind(i+1,1) = Arule(stationaryassetind(i,1),shock);
end

stationaryassets = agrid(stationaryassetind);
stationarycons = (1+r).*stationaryassets(1:N)+omega-stationaryassets(2:N+1);

%% direct method...
% step (1): find the bivariate stationary distribution
% order the states: (a1,zh),(a2,zh),...,(aN,zh),(a1,zl),(a2,zl),...,(aN,zl)
Phalf = zeros(2*numpts,numpts);
Phalf(sub2ind(size(Phalf),(1:2*numpts),reshape(Arule,[2*numpts 1])')) = 1;
Pa = [Phalf Phalf];
    % given (a,Z), what is a'? this matrix places a 1 where a' will be.
    % since A(a,Z) is deterministic, the left half of Pa (corresponding to
    % zh) will be the same as the right half of Pa (corresponding to zl).
Pz = repmat([p 1-p],2,1);
Pz = kron(Pz,ones(numpts));
Q = Pz .* Pa;

% invert matrices: fastest direct method (without simulation)
q = (Q' - eye(size(Q)) + ones(size(Q)))^(-1)*ones(size(Q,1),1);
q(q<0) = 0;
q = q/sum(q);
q = reshape(q,numpts,2);

% step (2): sum over the z states
zpdf = [p 1-p];
apdf = sum(q,2);
acdf = cumsum(apdf);
consgrid = reshape(cons,[2*numpts 1]);
cpdf = reshape(q,[2*numpts 1]);
A = sortrows([consgrid cpdf],1);
consgrid = A(:,1);
cpdf = A(:,2);
ccdf = cumsum(cpdf);

plot(consgrid(ccdf<1-1e-4),ccdf(ccdf<1-1e-4));
plot(agrid(acdf<1-1e-4),acdf(acdf<1-1e-4));


%%% Plots

% for r = 0.05...
figure;
hold on;
plot(agrid,v1(:,1),agrid,v1(:,2))
legend('High Shock','Low Shock','location','northwest')
xlabel('a = Current Assets')
ylabel('v(a,Z) = Values')
print -depsc2 ps5_q2_value.eps
close

figure;
hold on;
plot(agrid,assets(:,1),agrid,assets(:,2))
plot(agrid,agrid,'--k')
legend('High Shock','Low Shock','location','northwest')
xlabel('a = Current Assets')
ylabel('A(a,Z) = Savings')
print -depsc2 ps5_q2_assets.eps
close

figure;
hold on;
plot(agrid,cons(:,1),agrid,cons(:,2))
legend('High Shock','Low Shock','location','northwest')
xlabel('a = Current Assets')
ylabel('C(a,Z) = Consumption')
print -depsc2 ps5_q2_cons.eps
close

% for r = 0.2...
figure;
hold on;
plot(agrid,v1(:,1),agrid,v1(:,2))
legend('High Shock','Low Shock','location','northwest')
xlabel('a = Current Assets')
ylabel('v(a,Z) = Values')
print -depsc2 ps5_q2_value_highr.eps
close

figure;
hold on;
plot(agrid,assets(:,1),agrid,assets(:,2))
plot(agrid,agrid,'--k')
legend('High Shock','Low Shock','location','northwest')
xlabel('a = Current Assets')
ylabel('A(a,Z) = Savings')
print -depsc2 ps5_q2_assets_highr.eps
close

figure;
hold on;
plot(agrid,cons(:,1),agrid,cons(:,2))
legend('High Shock','Low Shock','location','northwest')
xlabel('a = Current Assets')
ylabel('C(a,Z) = Consumption')
print -depsc2 ps5_q2_cons_highr.eps
close

% stationary distribution plots (with r = 0.05)
figure;
hold on;
plot(agrid(acdf<1-1e-4),acdf(acdf<1-1e-4));
xlabel('Stationary CDF: Assets')
print -depsc2 ps5_q2_cdf_assets.eps
close

figure;
hold on;
plot(consgrid(ccdf<1-1e-4),ccdf(ccdf<1-1e-4));
xlabel('Stationary CDF: Consumption')
print -depsc2 ps5_q2_cdf_cons.eps
close

figure;
hold on;
hist(stationaryassets,25)
xlabel('Stationary PDF: Assets')
print -depsc2 ps5_q2_pdf_assets.eps
close

figure;
hold on;
hist(stationarycons,25)
xlabel('Stationary PDF: Consumption')
print -depsc2 ps5_q2_pdf_cons.eps
close


