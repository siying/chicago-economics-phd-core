%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% RBC model               %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;

format compact;

cd 'C:\Users\Yinan\Dropbox\Documents Dropbox\Income TA'\ps5;
%'/Users/paymon/Documents/phd/Macro II TA/RBCsession'

%%% Primitives

alpha       = .4;
delta       = .012;
rho         = .95;
beta        = .987;
Zbar        = 1;
A           = 2.585;
sigma       = 0.007;
theta       = 1;                            %   log utilities


%%% Set up the non-linear system in terms of the eight variables
syms k c n z kp cp np zp_;                  %   p stands for prime, next period value. There is a name duplicate with a function called zp in matlab. So I changed to zp_. Please let me know anyone can solve this better.
%   syms EE CL RC system should all be in terms of the eight
%   variables: [k c n z kp cp np zp_]. Ignore expectation now.

Rp = alpha * Zbar * exp(zp_) * (kp / np) ^ (alpha - 1) + 1 - delta;

CL = c ^ (-theta) * (1 - alpha) * Zbar * exp(z) * (k / n) ^ alpha - A;
EE = beta * cp ^ (-theta) * (Rp)   -  c ^ (-theta);
RC = Zbar * exp(z) * k ^ alpha * n ^ (1 - alpha) + (1 - delta) * k - c - kp;

system = [EE; CL; RC];


%%% Solve Steady state, fun is a double type function, not symbolic.
fun = @(SSx)  double ( subs(system, [k c n z kp cp np zp_], [SSx, 0, SSx, 0]) );
x0 = [5, 0.5, 0.5];
SS = fsolve(fun, x0);
display(SS);


%%% Linearization See "TA session note 20140206.pdf" for more detailed explanation
%   Use percentage deviation for k c l (hat), regular deviation for z (tilde). 

% Jacobians of the system should be 3 times 8. Separate to two parts for
% convenience. These are numeric matrices evaluated at SS.
Jac36 = double ( subs(jacobian(system, [k c n kp cp np]), [k c n z kp cp np zp_], [SS, 0, SS, 0]) );
Jacz32= double ( subs(jacobian(system, [z zp_]),          [k c n z kp cp np zp_], [SS, 0, SS, 0]) );

syms kt ct nt kpt cpt npt zh zph;       % t stands for telde for percentage deviation; h stands for hat for deviation. notice zh = z since zss = 0. I just add redundant zh, zph for clearification. 

kcnt16 = [kt ct nt kpt cpt npt];
zh12   = [zh zph];
linsys = ( Jac36 .* (ones(3, 1) * [SS, SS]) ) * kcnt16' + Jacz32 * zh12';
%   linsys (linear system) is the regular deviation (hat) of the three equations.


%%% Method of undetermined coeffcients
%   Set up AB32 as symbolic matrix. Get the new linear system (nlinsys) of kt, zh, and
%   epsilon only.

AB32 = sym('AB32', [3, 2]);

syms epsilon ;
%   do many substitutions to get nlinsys
nkcnz = sym('nkcnz', [1, 8]);
nkcnz(1)   = kt;
nkcnz(7)   = zh;
nkcnz(8)   = rho * zh + epsilon;
nkcnz(2:4) = ( AB32 * [kt; zh] )';
nkcnz(5:6) = ( AB32([1, 2], :) * [nkcnz(4); nkcnz(8)] )';

nlinsys = subs(linsys, [kt ct nt kpt cpt npt zh zph], nkcnz);   %nlinsys only of (kt zh epsilon)

%   nlinsys should hold for any kt, zh. Checking (kt, zh) for (1, 0) and (0,
%   1) is enough since it's linear
%   Evaluate epsilon at 0 for expectation.
%   Get 6 equations, 6 unknow linear formular set. Solve it as numerical
%   function

eq1 = subs(nlinsys, [kt zh epsilon], [1, 0, 0]);
eq2 = subs(nlinsys, [kt zh epsilon], [0, 1, 0]);
eq  = [eq1; eq2];

fun2 = @(AB32x)  double ( subs(eq, AB32, AB32x) );
x0 = [1, 1; 1, 1; 1, 1];
AB32r = fsolve(fun2, x0);

display(AB32r);



%%% impulse-response functions and outputs

St          = 50;               % Let shock hit at t = St. 
T           = 150 + St + 1;
shocksize   = 0.01;
zt          = zeros(1, T);
epsilon     = zeros(1, T);
kcnt        = zeros(3, T);

epsilon(St) = shocksize;
for i = 2:T
    zt(i) = rho * zt(i - 1) + epsilon(i);
    kcnt(1, i)    = AB32r(3, :)     * [kcnt(1, (i - 1)); zt(i - 1)];
    kcnt((2:3), i)= AB32r((1:2), :) * [kcnt(1, i); zt(i)];
end

% restore the economic variables

kcn         = zeros(3, T);
invest      = zeros(1, T);
output      = zeros(1, T);

kcn = SS' * ones(1, T) .* (1 + kcnt);
invest(1: (T-1)) = kcn(1,(2: T)) - (1 - delta) * kcn(1,(1: (T - 1)));
output = kcn(2, :) + invest;

T = T - 1;
t = (1:T) - St;

figure;
subplot(5,1,1),plot(t(1:T), kcn(1, (1:T)),'.r' ),           ylabel('capital')
subplot(5,1,2),plot(t(1:T), kcn(2, (1:T)),'.g' ),           ylabel('consumption')
subplot(5,1,3),plot(t(1:T), kcn(3, (1:T)),'.b' ),           ylabel('labor')
subplot(5,1,4),plot(t(1:T), invest(1:T),'.k' ),             ylabel('investment')
subplot(5,1,5),plot(t(1:T), output(1:T),'.c' ),             ylabel('output')



%%% repeat for capital shock

T           = 150 + St + 1;

zt          = zeros(1, T);
epsilon     = zeros(1, T);
kcnt        = zeros(3, T);

for i = 2:T
    zt(i) = rho * zt(i - 1) + epsilon(i);
    kcnt(1, i)    = AB32r(3, :)     * [kcnt(1, (i - 1)); zt(i - 1)];
    if (i == St)
        kcnt(1, St) = -shocksize;
    end
    kcnt((2:3), i)= AB32r((1:2), :) * [kcnt(1, i); zt(i)];
end


kcn         = zeros(3, T);
invest      = zeros(1, T);
output      = zeros(1, T);

kcn = SS' * ones(1, T) .* (1 + kcnt);
invest(1: (T-1)) = kcn(1,(2: T)) - (1 - delta) * kcn(1,(1: (T - 1)));
invest(St - 1) = invest(St - 2);
output = kcn(2, :) + invest;

T = T - 1;
t = (1:T) - St;

figure;
subplot(5,1,1),plot(t(1:T), kcn(1, (1:T)),'.r' ),           ylabel('capital')
subplot(5,1,2),plot(t(1:T), kcn(2, (1:T)),'.g' ),           ylabel('consumption')
subplot(5,1,3),plot(t(1:T), kcn(3, (1:T)),'.b' ),           ylabel('labor')
subplot(5,1,4),plot(t(1:T), invest(1:T),'.k' ),             ylabel('investment')
subplot(5,1,5),plot(t(1:T), output(1:T),'.c' ),             ylabel('output')
