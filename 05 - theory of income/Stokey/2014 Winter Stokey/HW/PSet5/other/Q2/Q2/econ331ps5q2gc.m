%% ECON331 Winter 2014
% Computing the model of saving with preference shocks.
%
% Ging Cee Ng*
% *Referenced Michael Kirker's original code from 2012

%% Preamble
close all
clear all

%% Paramters
beta = 0.9;
zl = 0.5;
zh = 1;
r = 0.05;
p = 0.6;
omega = 2;

%% Part A
% Construct a grid 2[0:10] x 2[0:10] with 'nstep' granulations.

% :: amax = 10 since 1/(1-Beta) = 10 for Beta = 0.9.
% :: amax is maximal asset holdings if consume nothing forever.
amax = 10; 
nsteps = 500;
Agrid = linspace(0,amax,nsteps);  % (1 x 500)
Amat = repmat(Agrid,nsteps,1);  % (500 x 500)
Amatt = Amat';

%% Consumption and Utility
Cmat = (1+r)*Amat + omega - Amatt;
Cmat(Cmat<0)=NaN; % Remove negative values of Consumption
Uh = zh*log(Cmat);
Ul = zl*log(Cmat);

% Set any NaN values (where C is -ve) to -inf to ensure they will never be
% selected in the maximisaiton routine.
Uh(isnan(Uh))=-inf;
Ul(isnan(Ul))=-inf;

%% Part B: Value and Policy function iteration
% --------------------------------------------
v = zeros(nsteps,2);
A = zeros(nsteps,2);
tol = 10;

while tol > 1e-7

% Find maximum value of BE and corresponding optimal policy line (Aj, j = {h,l})
%   [k,i]=max(MAT), k = maximized value, i = index.
Ev = v*[p;1-p];
Evrep = repmat(Ev,1,nsteps);
[tvh,Ah] = max(Uh + beta*Evrep);
[tvl,Al] = max(Ul + beta*Evrep);

tA = [Ah' Al'];
tv = [tvh' tvl'];

% update deviations tolerance level and value and policy fcns
tol=max(max(abs(tv-v)./tv));
v=tv;
A=tA;

end
A = Agrid(A); %Optimal policy rule
C=(1+r)*repmat(Agrid',1,2)+omega-A;


               
%% Graphs
% -------

figure(1)

% Plot of value function v
% ------------------------------
subplot(3,1,1)
plot(Agrid,v)
title('Value Function')
xlim([0,10])
ylim([0,10])
legend('High','Low')
xlabel('Assets today')


% Plot of policy function A(a,Zj)
% -------------------------------
subplot(3,1,2)
plot(Agrid,A)
title('Policy Function')
xlim([0,10])
ylim([0,10])
legend('High','Low')
xlabel('Assets today')


% Plot of optimal consumption C(a,Zj)
% -------------------------------
subplot(3,1,3)
plot(Agrid,C)
title('Optimal Consumption')
xlim([0,10])
ylim([0,10])
legend('High','Low')
xlabel('Assets today')


% Print Figure
% ----------------------------------------------
print -depsc2 ps5q2figB.eps
unix('epstopdf ps5q2figB.eps');

%% Stationary distribution of assets
% Calculate distribution

% Construct selection matrix
gh=zeros(nsteps);
gl=gh;

for i=1:nsteps
    gh(i,Ah(i))=1; %selection matrix
    gl(i,Al(i))=1;
end

trans=[ p*gh (1-p)*gh; p*gl (1-p)*gl];
trans= trans';
probst = (1/(2*nsteps))*ones(2*nsteps,1); %initialize

test=10;

% Multiply the transition matrix by itself over and over again
while test > 10^(-8);
   probst1 = trans*probst;
   test=max(abs(probst1-probst));
   probst = probst1;
end;

kk=A(:);

lambda=zeros(nsteps,2);
lambda(:)=probst; %Create two columns with the value of the density for A for A_low and A_high


%%   Calculate stationary distribution of capital 

probk=sum(lambda,2); % Integrate the joint distribution of (k,a) to find the 
                     % marginal distribution of (A).
probk=probk';


% Plot graph
figure('Name','Stationary distribution of Assets')

subplot(1,2,1);
plot(Agrid,probk)
title('PDF')
axis square

subplot(1,2,2);
PAsum = cumsum(probk);
plot(Agrid,PAsum);
ylim([0 1])
title('CDF')
axis square

print -depsc2 ps5q2figE.eps
unix('epstopdf ps5q2figE.eps');

%% Stationary distribution of consumption

Cx = unique(C);
Px = zeros(size(Cx));

% For each unique consumption level, see if you can reach it under the
% stationary distribution of assets from either the high or low shock. If
% both, combine probabilities

for ii = 1:length(Cx)
    indxh = find(C(:,1)==Cx(ii));
    if ~isempty(indxh)
        Px(ii) = Px(ii)+p*probk(indxh);
    end
    indxl = find(C(:,2)==Cx(ii));
    if ~isempty(indxl)
        Px(ii) = Px(ii)+(1-p)*probk(indxl);
    end
end

%% plot graph
% -----------
figure('Name','Stationary distribution of Consumption')

subplot(1,2,1)
plot(Cx,Px);
title('PDF');
axis square

subplot(1,2,2)

Pxsum = cumsum(Px);
Cxhat = zeros(size(Cx,1)+2,size(Cx,2));
Cxhat(2)=Cx(1);
Cxhat(3:end)=Cx;
Pxhat = zeros(size(Pxsum,1)+2,size(Pxsum,2));
Pxhat(3:end)=Pxsum;

plot(Cxhat,Pxhat);
ylim([0 1])
title('CDF')
axis square

print -depsc2 ps5q2figF.eps
unix('epstopdf ps5q2figF.eps');
        
        

%% save old results
% -----------------
v_old=v;
A_old=A;
C_old=C;
Agrid_old=Agrid;


% ------
%% r=0.2
% ------
r = 0.2;

%% Consumption and Utility
Cmat = (1+r)*Amat + omega - Amatt;
Cmat(Cmat<0)=NaN; % Remove negative values of Consumption
Uh = zh*log(Cmat);
Ul = zl*log(Cmat);

% Set any NaN values (where C is -ve) to -inf to ensure they will never be
% selected in the maximisaiton routine.
Uh(isnan(Uh))=-inf;
Ul(isnan(Ul))=-inf;

%% Part B: Value and Policy function iteration
% --------------------------------------------
v = zeros(nsteps,2);
A = zeros(nsteps,2);
tol = 10;

while tol > 1e-7

% Find maximum value of BE and corresponding optimal policy line (Aj, j = {h,l})
%   [k,i]=max(MAT), k = maximized value, i = index.
Ev = v*[p;1-p];
Evrep = repmat(Ev,1,nsteps);
[tvh,Ah] = max(Uh + beta*Evrep);
[tvl,Al] = max(Ul + beta*Evrep);

tA = [Ah' Al'];
tv = [tvh' tvl'];

% update deviations tolerance level and value and policy fcns
tol=max(max(abs(tv-v)./tv));
v=tv;
A=tA;

end
A = Agrid(A); %Optimal policy rule
C=(1+r)*repmat(Agrid',1,2)+omega-A;


               
%% Graphs
% -------

figure(1)

% Plot of value function v
% ------------------------------
subplot(3,1,1)
plot(Agrid,v)
hold on
plot(Agrid,v_old,':','Linewidth',1.5)
hold off
title('Value Function')
xlim([0,10])
ylim([0,10])
legend('v(a,z^h), r=0.2','v(a,z^l), r=0.2','v(a,z^h), r=0.05','v(a,z^l), r=0.05')
xlabel('Assets today')

% Plot of policy function A(a,Zj)
% -------------------------------
subplot(3,1,2)
plot(Agrid,A)
hold on
  plot(Agrid,A_old,':','Linewidth',1.5)
hold off
title('Policy Function')
xlim([0,10])
ylim([0,10])
legend('A(a,z^h), r=0.2','A(a,z^l), r=0.2','A(a,z^h), r=0.05','A(a,z^l), r=0.05')
xlabel('Assets today')


% Plot of optimal consumption C(a,Zj)
% -------------------------------
subplot(3,1,3)
plot(Agrid,C)
hold on
  plot(Agrid,C_old,':','Linewidth',1.5)
hold off
title('Optimal Consumption')
xlim([0,10])
ylim([0,10])
legend('C(a,z^h), r=0.2','C(a,z^l), r=0.2','C(a,z^h), r=0.05','C(a,z^l), r=0.05')
xlabel('Assets today')


% Print Figure
% ----------------------------------------------
print -depsc2 ps5q2figD.eps
unix('epstopdf ps5q2figD.eps');

%% Save results
save econ331ps5q2
