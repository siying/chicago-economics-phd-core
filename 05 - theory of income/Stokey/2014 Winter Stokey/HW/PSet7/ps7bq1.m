clear all;
close all;

A = linspace(0,10,1000)';
ts = [0,5,20,1000];
inits = {[0,1],[3,8],[10,10]};
sims = 10000;
ph = 0.3;
fc = 1;
ref = @(a) a;

%% Case 1
gh = @(a) 2 * log(a+1)+4;
gl = @(a) 2 * log(a+1)+.5;
ghx = fsolve(@(x) gh(x)-ref(x),5);
glx = fsolve(@(x) gl(x)-ref(x),5); 
figure; plot(A,[gh(A) gl(A) ref(A)],'linewidth',2); xlim([0,10]); ylim([0,10]); 
legend({'g(a,l^H)','g(a,l^L)','45deg'},'location','best'); title('Case 1 Ergodic Sets');
for i = 1:length(ghx)
    line([ghx(i),ghx(i)],[0,10],'LineStyle','--','Color','black');
end
for i = 1:length(glx)
    line([glx(i),glx(i)],[0,10],'LineStyle','--','Color','black');
end
saveas(gcf,['ps7b_f' num2str(fc)],'png'); fc = fc + 1;

% Create Distro Functions
for i = 1:length(inits)
    figure;
    
    for j = 1:length(ts)
        subplot(2,2,j);
        [freq bins] = distroHelperFunction(gh,gl,ph,inits{i}(1),inits{i}(2),sims,ts(j));
        bar(bins,freq); title(['Case 1; U(' num2str(inits{i}) '); t=' num2str(ts(j))]);
        ym = max(freq)*1.2;
        ylim([0,ym])
        xlim([0,10]);
        for k = 1:length(glx)
            line([glx(k),glx(k)],[0,ym],'LineStyle','--','Color','black');
        end
        for k = 1:length(ghx)
            line([ghx(k),ghx(k)],[0,ym],'LineStyle','--','Color','black');
        end
    end
    saveas(gcf,['ps7b_f' num2str(fc)],'png'); fc = fc + 1;
end
    

%% Case 2
gh = @(a) 2 * log(a+1)+2;
gl = @(a) max(2 * log(a+1)-1.5,0);
ghx = fsolve(@(x) gh(x)-ref(x),5);
glx = fsolve(@(x) gl(x)-ref(x),5); 
figure; plot(A,[gh(A) gl(A) ref(A)],'linewidth',2); xlim([0,10]); ylim([0,10]); 
legend({'g(a,l^H)','g(a,l^L)','45deg'},'location','best'); title('Case 2 Ergodic Sets');
for i = 1:length(ghx)
    line([ghx(i),ghx(i)],[0,10],'LineStyle','--','Color','black');
end
for i = 1:length(glx)
    line([glx(i),glx(i)],[0,10],'LineStyle','--','Color','black');
end
saveas(gcf,['ps7b_f' num2str(fc)],'png'); fc = fc + 1;
% Create Distro Functions
for i = 1:length(inits)
    figure;
    
    for j = 1:length(ts)
        subplot(2,2,j);
        [freq bins] = distroHelperFunction(gh,gl,ph,inits{i}(1),inits{i}(2),sims,ts(j));
        bar(bins,freq); title(['Case 2; U(' num2str(inits{i}) '); t=' num2str(ts(j))]);
        ym = max(freq)*1.2;
        ylim([0,ym])
        xlim([0,10]);
        for k = 1:length(glx)
            line([glx(k),glx(k)],[0,ym],'LineStyle','--','Color','black');
        end
        for k = 1:length(ghx)
            line([ghx(k),ghx(k)],[0,ym],'LineStyle','--','Color','black');
        end
    end
    saveas(gcf,['ps7b_f' num2str(fc)],'png'); fc = fc + 1;
end
    

%% Case 3
c1 = .5;
c2 = 1.1;
gh = @(a) sin(a/c2+c1)+a;
gl = @(a) sin(a/c2+c1)+a-sin(c1);
% need to iterate through different starting points...
ghx = [];
glx = [];
for i = 1:.5:10
    ghxt = fsolve(@(x) gh(x)-ref(x),i);
    glxt = fsolve(@(x) gl(x)-ref(x),i); 
    % Only save if they cross from above!
    if gh(ghxt-1e-2)>ref(ghxt-1e-2)
        ghx = unique([ghx ghxt]);
    end 
    if gl(glxt-1e-2)>ref(glxt-1e-2)
        glx = unique([glx glxt]);
    end
end
ghx(ghx<0)=[];
glx(glx<0)=[];

figure; plot(A,[gh(A) gl(A) ref(A)],'linewidth',2); xlim([0,10]); ylim([0,10]); 
legend({'g(a,l^H)','g(a,l^L)','45deg'},'location','best'); title('Case 3 Ergodic Sets');
for i = 1:length(ghx)
    line([ghx(i),ghx(i)],[0,10],'LineStyle','--','Color','black');
end
for i = 1:length(glx)
    line([glx(i),glx(i)],[0,10],'LineStyle','--','Color','black');
end
saveas(gcf,['ps7b_f' num2str(fc)],'png'); fc = fc + 1;
% Create Distro Functions
for i = 1:length(inits)
    figure;
    
    for j = 1:length(ts)
        subplot(2,2,j);
        [freq bins] = distroHelperFunction(gh,gl,ph,inits{i}(1),inits{i}(2),sims,ts(j));
        bar(bins,freq); title(['Case 3; U(' num2str(inits{i}) '); t=' num2str(ts(j))]);
        ym = max(freq)*1.2;
        ylim([0,ym])
        xlim([0,10]);
        for k = 1:length(glx)
            line([glx(k),glx(k)],[0,ym],'LineStyle','--','Color','black');
        end
        for k = 1:length(ghx)
            line([ghx(k),ghx(k)],[0,ym],'LineStyle','--','Color','black');
        end
    end
    saveas(gcf,['ps7b_f' num2str(fc)],'png'); fc = fc + 1;
end
    

