%% Macro PS 7 Quesiton 2: Computing the consumption-savings problem
%
% Created by:
%       Michael Kirker
%       mkirker@uchicago.edu



%% Initial setup
% Set graph options, type in parameter values etc

close all
clear
home

% Settings for graph style
set(0,'DefaultAxesColorOrder',[0.25 0.25 0.25; 0.5 0.5 0.5],...
      'DefaultAxesLineStyleOrder','-|--|:|-.');
fontsize=18;
linewidth=2;

% if graphs folder doesnt exist, make it (graphs saved into subfolder)
if exist('graphs','dir')==0;
    mkdir('graphs');
end


%-------------------------------------------------------------------------%
% Parameter Values
%-------------------------------------------------------------------------%

beta    = 0.9;
zl      = 1;
zh      = 2;
r       = 0.05;
p       = 0.6;
omega   = 2;

amax=10;

% Grid for possible value A (and A') can take on
nsteps = 500; % number of steps in grid, larger # = finer grid
Agrid = linspace(0,amax,nsteps); % Vector of possible A values


Amat = repmat(Agrid,nsteps,1); % columns are constant
Amatt = Amat'; % rows are constant



%-------------------------------------------------------------------------%
% Construct Counsumption Matrix
%-------------------------------------------------------------------------%
% Cmat(ij) = Consumption given A(i) and A'(j)

Cmat = (1+r)*Amat+omega-Amatt;
Cmat(Cmat<0)=NaN; % Remove negative values of Consumption

%-------------------------------------------------------------------------%
% Construct Counsumption Matrix
%-------------------------------------------------------------------------%
% utilh = Utility when shock zh occurs
% utill = Utility when shock zl occurs
utilh = zh*log(Cmat);
utill = zl*log(Cmat);

% Set any NaN values (where C is -ve) to -inf to ensure they will never be
% selected in the maximisaiton routine.
utilh(isnan(utilh))=-inf;
utill(isnan(utilh))=-inf;

%-------------------------------------------------------------------------%
% Pre-allocate/initialise matrices
%-------------------------------------------------------------------------%
v = zeros(nsteps,2); % Intial value function for state (column j).
A = zeros(nsteps,2); % Will store optimal policy for each state

metric = 10; % indicator for convergence of value function


%% Main Loop - derive value and policy funciton.
% For the initial v, iterate on the Bellman equation a large number of
% times until the difference between the value function (tv) this period
% and the value function last period (v) is very small (1e-7).


while metric > 1e-7;

  [tvh,Ah]=max(utilh + beta*repmat(v*[p 1-p]',1,nsteps)); % Ah is the policy function. 
                                                          % For each A (a column), it 
                                                          % tells you which A' (a line) 
                                                          % is optimal.
  [tvl,Al]=max(utill + beta*repmat(v*[p 1-p]',1,nsteps));
  
  tA=[Ah' Al'];
  tv=[tvh' tvl'];
  
  metric=max(max(abs(tv-v)./tv));
  v=tv;
  A=tA;


end;

A = Agrid(A); % Optimal policy rule



%% Graphs for part b
% plot graphs of the value function, and optimal savings and consumption
% functions.

%=========================================================================%
% Graph of value function
%=========================================================================%
figure('Name','Value functions')

plot(Agrid,v,'linewidth',linewidth);
legend({'v(a,z^h)','v(a,z^l)'},'fontsize',fontsize,'location','NorthWest')

set(gca,'fontsize',fontsize-2);
        
        set(gcf,'paperType','usletter','paperunits','normalized',...
            'paperPosition',[0,0,1,1],'paperOrientation','landscape');
        
        xlabel('Assets today ($a$)','fontsize',fontsize,'interpreter','latex');
        ylabel('$v(a,z^j)$','fontsize',fontsize,'interpreter','latex');


        print('-dpdf','graphs/Q2a_value.eps');
        
%=========================================================================%
% Graph of optimal savings function
%=========================================================================%
figure('Name','Optimal Savings function')

ph = plot(Agrid,A,'linewidth',linewidth);
line([Agrid(1) Agrid(end)],[Agrid(1) Agrid(end)])

set(gca,'XTick',[0:amax])
set(gca,'YTick',[0:amax])

axis square

% Extra step - shade ergodic area
ergodic = find((A(:,2)'-Agrid)>=0 & (A(:,1)'-Agrid)<=0);

% Plot Ergodic set shading
ax=gca;
yrange=get(gca,'ylim');
    pt = patch([Agrid(ergodic(1)) Agrid(ergodic(1)) Agrid(ergodic(end)) Agrid(ergodic(end))],...
        [yrange(1) yrange(2) yrange(2) yrange(1)],0.9*[1 1 1],...
        'edgeColor','none');
    ch = get(ax,'children');
    ch(ch == pt) = [];
    ch(end+1) = pt;
    set(ax,'children',ch);

legend([ph' pt],{'A(a,z^h)','A(a,z^l)','Ergodic set'},'fontsize',fontsize,'location','NorthWest')
    
    
set(gca,'fontsize',fontsize-2);
        
        set(gcf,'paperType','usletter','paperunits','normalized',...
            'paperPosition',[0,0,1,1],'paperOrientation','landscape');
        

        xlabel('Assets today ($a$)','fontsize',fontsize,'interpreter','latex');
        ylabel('Assets tomorrow ($a''$)','fontsize',fontsize,'interpreter','latex');
        
        print('-dpdf','graphs/Q2a_savings.eps');
        
%=========================================================================%
% Graph of optimal consumption function
%=========================================================================%
figure('Name','Optimal consumption function')

C=(1+r)*repmat(Agrid',1,2)+omega-A;
plot(Agrid,C,'linewidth',linewidth);
legend({'C(a,z^h)','C(a,z^l)'},'fontsize',fontsize,'location','NorthWest')

set(gca,'fontsize',fontsize-2);
        
        set(gcf,'paperType','usletter','paperunits','normalized',...
            'paperPosition',[0,0,1,1],'paperOrientation','landscape');
        
        xlabel('Assets today ($a$)','fontsize',fontsize,'interpreter','latex');
        ylabel('$C(a,z^j)$','fontsize',fontsize,'interpreter','latex');
        
        print('-dpdf','graphs/Q2a_consumption.eps');

%% Stationary distribution of assets
% Calculate distribution

% Construct selction matrix
gh=zeros(nsteps);
gl=gh;

for i=1:nsteps
    gh(i,Ah(i))=1; %selection matrix
    gl(i,Al(i))=1;
end

trans=[ p*gh (1-p)*gh; p*gl (1-p)*gl];
trans= trans';
probst = (1/(2*nsteps))*ones(2*nsteps,1); %initialize


test=10;

while test > 10^(-8);
   probst1 = trans*probst;
   test=max(abs(probst1-probst));
   probst = probst1;
end;

kk=A(:);


lambda=zeros(nsteps,2);
lambda(:)=probst; %Create two columns with the value of the density for A for A_low and A_high


%%   Calculate stationary distribution of capital 

probk=sum(lambda,2); % Integrate the joint distribution of (k,a) to find the 
                     % marginal distribution of (A).
probk=probk';


% Plot graph
figure('Name','Stationary distribution of Assets')

subplot(1,2,1);
    plot(Agrid,probk,'linewidth',linewidth);
    title('PDF','fontsize',fontsize)
    set(gca,'fontsize',fontsize-2);
    axis square

subplot(1,2,2);
    PAsum = cumsum(probk);
    plot(Agrid,PAsum,'linewidth',linewidth);
    ylim([0 1])
    title('CDF','fontsize',fontsize)
    set(gca,'fontsize',fontsize-2);
    axis square
        
        set(gcf,'paperType','usletter','paperunits','normalized',...
            'paperPosition',[0,0,1,1],'paperOrientation','landscape');
        

        print('-dpdf','graphs/Q2d_assets.eps');

%% Stationary distribution of consumption

Cx = unique(C);
Px = zeros(size(Cx));

% For each unique consumption level, see if you can reach it under the
% stationary distribution of assets from either the high or low shock. If
% both, combine probabilities

for ii = 1:length(Cx)
   
    indxh = find(C(:,1)==Cx(ii));
    
    if ~isempty(indxh)
        Px(ii) = Px(ii)+p*probk(indxh);
    end
    
    indxl = find(C(:,2)==Cx(ii));
    
    if ~isempty(indxl)
        Px(ii) = Px(ii)+(1-p)*probk(indxl);
    end
end


% plot graph
figure('Name','Stationary distribution of Consumption')
subplot(1,2,1)
plot(Cx,Px,'linewidth',linewidth);
title('PDF','fontsize',fontsize)
axis square
set(gca,'fontsize',fontsize-2);

subplot(1,2,2)

Pxsum = cumsum(Px);

Cxhat = zeros(size(Cx,1)+2,size(Cx,2));

Cxhat(2)=Cx(1);
Cxhat(3:end)=Cx;

Pxhat = zeros(size(Pxsum,1)+2,size(Pxsum,2));
Pxhat(3:end)=Pxsum;

plot(Cxhat,Pxhat,'linewidth',linewidth);
ylim([0 1])
title('CDF','fontsize',fontsize)
axis square

        set(gca,'fontsize',fontsize-2);
        
        set(gcf,'paperType','usletter','paperunits','normalized',...
            'paperPosition',[0,0,1,1],'paperOrientation','landscape');
        

        print('-dpdf','graphs/Q2e_consumption.eps');
        
  
        
        

%% part g, repeat b with new higher interest rate.
% I have copied and pasted the code from above, the calculation is exactly
% the same.


% Save down previous lines so we can plot them on the graph to compare what
% happens when r changes.
v_old=v;
A_old=A;
C_old=C;
Agrid_old=Agrid;


% New value of r
r = 0.2;


% Recompute new consumption matrices
Cmat = (1+r)*Amat+omega-Amatt;
Cmat(Cmat<0)=NaN; % Remove negative values of Consumption

%-------------------------------------------------------------------------%
% Construct Counsumption Matrix
%-------------------------------------------------------------------------%
% utilh = Utility when shock zh occurs
% utill = Utility when shock zl occurs
utilh = zh*log(Cmat);
utill = zl*log(Cmat);

% Set any NaN values (where C is -ve) to -inf to ensure they will never be
% selected in the maximisaiton routine.
utilh(isnan(utilh))=-inf;
utill(isnan(utilh))=-inf;

%-------------------------------------------------------------------------%
% Pre-allocate/initialise matrices
%-------------------------------------------------------------------------%
v = zeros(nsteps,2); % Intial value function for state (column j).
A = zeros(nsteps,2); % Will store optimal policy for each state

metric = 10; % indicator for convergence of value function



while metric > 1e-7;

  [tvh,Ah]=max(utilh + beta*repmat(v*[p 1-p]',1,nsteps)); 
  [tvl,Al]=max(utill + beta*repmat(v*[p 1-p]',1,nsteps));
  
  tA=[Ah' Al'];
  tv=[tvh' tvl'];
  
  metric=max(max(abs(tv-v)./tv));
  v=tv;
  A=tA;


end;

A = Agrid(A); % Optimal policy rule


%=========================================================================%
% Graph of value function
%=========================================================================%
figure('Name','Value functions')

plot(Agrid,[v  v_old],'linewidth',linewidth);

legend({'v(a,z^h), r=0.2','v(a,z^l), r=0.2','v(a,z^h), r=0.05','v(a,z^l), r=0.05'},...
    'fontsize',fontsize,'location','NorthWest')
xlim([Agrid_old(1) Agrid_old(end)]);

set(gca,'fontsize',fontsize-2);
        
        set(gcf,'paperType','usletter','paperunits','normalized',...
            'paperPosition',[0,0,1,1],'paperOrientation','landscape');
        
        xlabel('Assets today ($a$)','fontsize',fontsize,'interpreter','latex');
        ylabel('$v(a,z^j)$','fontsize',fontsize,'interpreter','latex');


        print('-dpdf','graphs/Q2g_value.eps');
        
%=========================================================================%
% Graph of optimal savings function
%=========================================================================%
figure('Name','Optimal Savings function')

plot(Agrid,[A A_old],'linewidth',linewidth);


line([Agrid(1) Agrid(end)],[Agrid(1) Agrid(end)])
legend({'A(a,z^h), r=0.2','A(a,z^l), r=0.2','A(a,z^h), r=0.05','A(a,z^l), r=0.05'}...
    ,'fontsize',fontsize,'location','NorthWest')

set(gca,'XTick',[0:amax])
set(gca,'YTick',[0:amax])

xlim([Agrid_old(1) Agrid_old(end)]);
axis square
set(gca,'fontsize',fontsize-2);
        
        set(gcf,'paperType','usletter','paperunits','normalized',...
            'paperPosition',[0,0,1,1],'paperOrientation','landscape');
        

        xlabel('Assets today ($a$)','fontsize',fontsize,'interpreter','latex');
        ylabel('Assets tomorrow ($a''$)','fontsize',fontsize,'interpreter','latex');
        
        print('-dpdf','graphs/Q2g_savings.eps');
        
%=========================================================================%
% Graph of optimal consumption function
%=========================================================================%
figure('Name','Optimal consumption function')

C=(1+r)*repmat(Agrid',1,2)+omega-A;


plot(Agrid,[C C_old],'linewidth',linewidth);

legend({'C(a,z^h), r=0.2','C(a,z^l), r=0.2','C(a,z^h), r=0.05','C(a,z^l), r=0.05'}...
    ,'fontsize',fontsize,'location','NorthWest')

xlim([Agrid_old(1) Agrid_old(end)]);
set(gca,'fontsize',fontsize-2);
        
        set(gcf,'paperType','usletter','paperunits','normalized',...
            'paperPosition',[0,0,1,1],'paperOrientation','landscape');
        
        xlabel('Assets today ($a$)','fontsize',fontsize,'interpreter','latex');
        ylabel('$C(a,z^j)$','fontsize',fontsize,'interpreter','latex');
        
        print('-dpdf','graphs/Q2g_consumption.eps');