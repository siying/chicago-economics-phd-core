% Econ 331, TA Session
% Value Function Iterations

%% Preliminary Steps
clear all ;
close all ;
clc ;

% setting parameters
beta = 0.96;
A = 4;
alpha = 0.25;

% create a grid for capital
k_min = 0.01;                % minimum value of capital
k_max = 2;                  % max value of capital
n = 200;                    % number of grid points
k = linspace(k_min, k_max,n); % this is the grid for capital

% max number of iterations
%I = 400;

% initial guess for value function
V=[];
V(1,1:n)=0;

% output for values of capital on the grid
y = A*k.^alpha;

%% Value Function Iterations
eps = 0.0001;                % precision

i=1;
d = 10;
P=[];
tic
while (d>eps)
    i = i+1;
    for j=1:n
    V(i,j) = log(y(j)-k(1))+ beta * V(i-1,1);
    P(j)=1;
         for jj=2:n
             if y(j)> k(jj)
                vv = log(y(j)-k(jj)) + beta * V(i-1,jj);
                if vv > V(i,j)
                   V(i,j) = vv;
                   P(j)=jj;
                end
             end
        end
    end
    % d = max(abs(V(i,:)-V(i-1,:))./(1+abs(V(i-1,:))));
    d = max(abs(V(i,:)-V(i-1,:)));
end
toc
%% Plotting graphs
figure(1)
plot(k,V(1:5:i,:))
xlabel('Capital (k)');
ylabel('Value function - convergence');

fig2 = figure(2); 
set (fig2, 'Name', 'Value Function');
plot(k,V(i,:));
xlabel('Capital (k)');
ylabel('Value function - V(k)');
title('V(k)');

fig3 = figure(3); 
set (fig3, 'Name', 'Policy Function');
plot(k,k(P),'-');
hold on; % doesn't erase the previous graph drawn on this plot
plot(k,k,':');
legend('Policy Function','45 degree line','Location','NorthWest');
xlabel('Capital (k(t))');
ylabel('Policy function (k(t+1))');
title('Policy Function k_{t+1} = g(k_t)');
        
% steady state - numerical solution
grid = 1:n;
[temp ss] = max(grid == P);

% true steady state - analytical solution
k_ss_a = (alpha*beta*A)^(1/(1-alpha));

disp(sprintf('Steady state value of capital, numerical solution %1.5g',k(ss)))
disp(sprintf('Steady state value of capital, analytical solution %1.5g',k_ss_a))