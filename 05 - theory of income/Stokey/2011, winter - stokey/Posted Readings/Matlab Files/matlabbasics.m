% Econ 331, TA Session
% Matlab Basics

% First, notice that this is M-file. For those of you who know Stata, it's
% similar to a DO-file.

%% Part I. Data Entry
% row and column vectors
function matlabbasics()
a = [1 2 3 4]; % row vector
b = [1 2 3 4]'; % column vec; ' means transpose
c = [1;2;3;4]; % column vec

% Notice that if you have a semicolon(;) after a command, the output of
% that command will not show up on the Command Window.

a
b
c

% matrix
A = [1 1 1; 2 2 2; 3 3 3] % semicolon within a matrix or vector means go to the next row
B = [1 2 3
     4 5 6
     7 8 9]

% specialty matrices
C = zeros(5,4) %creates a matrix with five rows and four columns full of zeros
m = 4; n=3;
D = ones(m,n) % a m by n matrix where all the elements are 1
E = eye(4) % a 4 by 4 identity matrix
    
% random number from uniform and normal distributions
rand(m,n) % creates a m by n matrix where each element is a draw from a uniform distribution on a unit interval
randn('seed', 629) % sets the seed number
randn(m,n) % creates a m by n matrix where each element is a draw from N(0,1)
    

% to create a grid use ":" or linspace command
d = 0 : 0.5 : 3 % smallest number:distance:biggest number
e = 1:10 %integers
f = linspace(0, 10, 21)'  % linspace(X1, X2, N) where N defines how how many points should be between X1 and X2
    
% accessing parts 
B
B(2,1)
B(:,1)
B(2:3,:)
B(2:3,1:2)
    
output(1:3,:)=[A(1:3,1) B(1:3,1) C(1:3,1)] 
   

%control+enter => only runs the highlighted part separated by %%
%% Part II. the matrix algebra
    
d = a*b
e = a'*b'

V = A*B
inv(V) %inverse matrix of V

    % element by element operations
    U = A.*B
    W = A./B
    X = A.^B
    Y = A.^2
    
    % sum, max, ...
    RS = sum(B,1) %option 1 means sum across rows
    CS = sum(B,2) %option 2 means sum across columns

    T(:,:,1)=A(:,:);
    T(:,:,2)=B(:,:);
    T
    sum(T,3) %option 3 means sum across the stacks

    MR = max(B,[],1)
    MC = max(B,[],2)

    % some useful stuff
    A
    B
    AB = kron(A,B) %kronecker product

    % eigenvalues and eigenvectors
    [V D] = eig(A) %V is eigenvectors and D is eigenvalues
      
%% Part III. Loops

% for-loop
for i = 1:5
    q(i)=i^2
end
    
    % when matlab is really slow
    n=200;
    A=randn(n);
    B=randn(n);
    C=zeros(n);
    tic
    for i = 1:n
        for j = 1:n            
            for k=1:n
                C(i,j)=C(i,j)+A(i,k)*B(k,j);
            end
        end
    end
    toc
    tic
    D=A*B;
    toc

        
    % while loop
    d = 50;
    niter=0;
    while d > 1
        d = d/2
        niter=niter+1;
    end
    niter

    % conditions in matlab
    t = rand(1);
    if t > 0.5;
        s = 1;
    elseif t < 0.5;
        s = 0;
    else s = 0.5;
    end
    t
    s

%% Part IV. Other Commands

    % clearing the mess
    clear all %clears everything in workspace
    close all %closes all the graphs
    clc %clears texts on command window
    global alpha rho chi; % sets the values of these globally
    
    alpha = 0.3;
    rho = 0.5;
    chi = -0.25;
    
    % anonymous functions
    u = @(x,y,h) (alpha*x^rho+(1-alpha)*y^rho)^(1/rho)-h^(1-chi)/(1-chi);

    u(1,1,0.7)
    u(2,1,0.3)
    u(1,2,0.3)
    
    % functions in m-files

    x1=1.1;
    y1=2;
    h1=0.3;
    u1=utility(x1,y1,h1);

    % printing output
    u1
    sprintf('Utility for bundle (%g,%g) with hours worked %g is %g',x1,y1,h1,u1)
    disp(u1)
    disp(sprintf('Utility for bundle (%1.3g,%1.3g) with hours worked %1.2g is %5.5g',x1,y1,h1,u1))
    
    % help
    help randn
    help sprintf

      
%% Part V. Plots
clear all
close all
clc
 
    alpha = 0.3;
    beta = 0.98;
    gamma = 0.5;
    A = 1.1;
 
    grid = 200; %number of points I want in grid
    k = linspace(0.001, 0.5, grid);
    y = k.^alpha; % notice each element in vector k is affected
    u = alpha*beta*A*k.^alpha;
    V = 1/(1-beta)*log((1-alpha*beta)*A) ...
        +1/(1-beta)*(alpha*beta)/(1-alpha*beta)*log(alpha*beta*A) ...
        +alpha/(1-alpha*beta).*log(k);

    figure(1);
        plot(k,V(:),'-')
        xlabel('Capital (k)');
        ylabel('Value function');
 
    figure(2);
        plot(k,u,'-')
        hold;
        plot(k,k,':') % 45 degree line
        xlabel('Capital (k)');
        ylabel('Capital (k+1)');
        title('Policy function');

  
    figure (3);
    subplot(2,1,1); % this means create a plot that has subplots in the dimension of 2 by 1, and this subplot is the first one
        plot(k,V(:),'-');
        xlabel('Capital (k)');
        ylabel('Value function');
    subplot (2,1,2);
        plot(k,u,'-');
        hold;
        plot(k,k,':')
        xlabel('Capital (k)');
        ylabel('Capital (k+1)');
        title('Policy function');
        
function u = utility(x,y,h);
% u(x,y,h) is utility of consumption bundle (x,y) when hours worked is h

global alpha rho chi;

if or(or(x<0,y<0),h<0)
   error('only positive values allowed for consumption and hours worked')
end;

u = (alpha*x^rho+(1-alpha)*y^rho)^(1/rho)-h^(1-chi)/(1-chi);