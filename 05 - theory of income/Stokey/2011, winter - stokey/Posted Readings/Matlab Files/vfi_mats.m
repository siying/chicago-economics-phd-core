% setting parameters
close all
clear all
clc

beta = 0.96;
A = 4;
alpha = 0.25;

% create a capital
k_min = 0.1;
k_max = 2;
n = 200;

k = linspace(k_min, k_max,n);

% max number of iterations
I = 500;

V(1,1:n) = 0;
% V(1,1:n) = log(A*k.^alpha-k)/(1-beta);

y = A*k.^alpha;

UA =  ones(n,1)*y - k'*ones(1,n);
U  =  log(UA.*(UA > 0) + (UA<=0)) - 1000*(UA<=0);

% value function iterations
i=1;
d = 10;
eps = 0.001;

while (d > eps && i-1 < I)
    i=i+1;
    W = U + beta * (ones(n,1)*V(i-1,:))';
    [V(i,:), P] = max(W);
    % d = max(abs(V(i,:)-V(i-1,:))./(1+abs(V(i-1,:))));
    d = max(abs(V(i,:)-V(i-1,:)));
end

% plotting graphs
figure(4)
plot(k,V(1:5:i,:))
xlabel('Capital (k)');
ylabel('Value function - convergence');

figure(5)
plot(k,V(i,:));
xlabel('Capital (k)');
ylabel('Value function - last iteration');

figure(6)
plot(k,k(P),'-');
hold on;
plot(k,k,':');
xlabel('Capital (k(t))');
ylabel('Policy function (k(t+1))');
        
% steady state - numerical solution
grid = 1:n;
[temp ss] = max(grid == P);

% true steady state - analytical solution
k_ss_a = (alpha*beta*A)^(1/(1-alpha));

disp(sprintf('Steady state value of capital, numerical solution %1.5g',k(ss)))
disp(sprintf('Steady state value of capital, analytical solution %1.5g',k_ss_a))