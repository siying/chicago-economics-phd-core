% Q7.2. 1st case

clear all
clc
close all

N=1000;
iter=300;

a = 0:.05:10;
gH = 2*log(a+1) + 4;
gL = 2*log(a+1) + .5;

total=[a;gH;gL];

H = @(a) 2*log(a+1) + 4 - a;
L = @(a) 2*log(a+1) + .5 - a;

aH1= fzero(H,[0,10]);
aL1= fzero(L,[0,10]);
aH2= 2*log(aH1+1) + .5;
aL2= 2*log(aL1+1) + 4;
aH3= 2*log(aH2+1) + .5;
aL3= 2*log(aL2+1) + 4;
aH4= 2*log(aH2+1) + 4;
aL4= 2*log(aL2+1) + .5;
aL5 = 2*log(aL3+1) + .5;
aL6 = 2*log(aL4+1) + .5;
aL7 = 2*log(aL3+1) + 4;
aL8 = 2*log(aL4+1) + 4;
aL9 = 2*log(aH4+1) + .5;
aL10 = 2*log(aL2+1) + .5;
aL11 = 2*log(aH4+1) + 4;
aL12 = 2*log(aL2+1) + 4;
aL13 = 2*log(aH3+1) + .5;
aL14 = 2*log(aH3+1) + 4;


figure(1)
plot(a, gH, 'r')
hold on
plot(a, gL)
hold on
plot(a,a, 'k--')
legend('High state', 'Low state', '45d line', 'Location', 'NorthWest');
hold on
plot([aH1,aH1],[0,10], 'k:')
hold on
plot([aL1,aL1],[0,10], 'k:')
hold on
plot([aH2,aH2],[0,10], 'k:')
hold on
plot([aL2,aL2],[0,10], 'k:')
hold on
plot([aH3,aH3],[0,10], 'k:')
hold on
plot([aL3,aL3],[0,10], 'k:')
hold on
plot([aH4,aH4],[0,10], 'k:')
hold on
plot([aL4,aL4],[0,10], 'k:')
hold on
plot([aL5,aL5],[0,10], 'k:')
hold on
plot([aL6,aL6],[0,10], 'k:')
hold on
plot([aL7,aL7],[0,10], 'k:')
hold on
plot([aL8,aL8],[0,10], 'k:')
hold on
plot([aL9,aL9],[0,10], 'k:')
hold on
plot([aL10,aL10],[0,10], 'k:')
hold on
plot([aL11,aL11],[0,10], 'k:')
hold on
plot([aL12,aL12],[0,10], 'k:')
hold on
plot([aL13,aL13],[0,10], 'k:')
hold on
plot([aL14,aL14],[0,10], 'k:')
xlabel('Assets')
title('Policy functions - Ergotic and Transcient Sets')

rand('seed', 101);

shocks = binornd(1,.3,iter, N);
evo=zeros(iter,N);

start=rand(iter,1);
evo=[start evo];
    
for i = 2:N+1
    evo(:,i) = shocks(:,i-1).*(2*log(evo(:,i-1)+1) + 4) + (1-shocks(:,i-1)).*(2*log(evo(:,i-1)+1) + .5);
end


figure(2)
subplot(2,2,1)
hist(evo(:,1), a)
xlabel('Assets')
title('Distribution of initial assets: Uniform on [0,1]')

subplot(2,2,2)
hist(evo(:,5), a)
xlabel('Assets')
title('Distribution of asset after 5 periods')

subplot(2,2,3)
hist(evo(:,20), a)
xlabel('Assets')
title('Distribution of asset after 20 periods')

subplot(2,2,4)
hist(evo(:,N), a)
xlabel('Assets')
title('Distribution of asset after 1000 periods')




start= 5*rand(iter,1) + 3;
evo=[start evo];
    
for i = 2:N+1
    evo(:,i) = shocks(:,i-1).*(2*log(evo(:,i-1)+1) + 4) + (1-shocks(:,i-1)).*(2*log(evo(:,i-1)+1) + .5);
end


figure(3)
subplot(2,2,1)
hist(evo(:,1), a)
xlabel('Assets')
title('Distribution of initial assets: Uniform on [3,8]')

subplot(2,2,2)
hist(evo(:,5), a)
xlabel('Assets')
title('Distribution of asset after 5 periods')

subplot(2,2,3)
hist(evo(:,20), a)
xlabel('Assets')
title('Distribution of asset after 20 periods')

subplot(2,2,4)
hist(evo(:,N), a)
xlabel('Assets')
title('Distribution of asset after 1000 periods')

start= 10*ones(iter,1);
evo=[start evo];
    
for i = 2:N+1
    evo(:,i) = shocks(:,i-1).*(2*log(evo(:,i-1)+1) + 4) + (1-shocks(:,i-1)).*(2*log(evo(:,i-1)+1) + .5);
end


figure(4)
subplot(2,2,1)
hist(evo(:,1), a)
xlabel('Assets')
title('Distribution of initial assets: Mass point at 10')

subplot(2,2,2)
hist(evo(:,5), a)
xlabel('Assets')
title('Distribution of asset after 5 periods')

subplot(2,2,3)
hist(evo(:,20), a)
xlabel('Assets')
title('Distribution of asset after 20 periods')

subplot(2,2,4)
hist(evo(:,N), a)
xlabel('Assets')
title('Distribution of asset after 1000 periods')

%%%%%%%%%%
% Q7.2. 2nd case

clear all
clc
close all

N=1000;
iter=300;

a = 0:.05:10;
gH = 2*log(a+1) + 2;
gL = max(2*log(a+1) - 1.5, 0);

total=[a;gH;gL];

H = @(a) 2*log(a+1) + 2 - a; 
aH1= fzero(H,[0,10]);

figure(1)
plot(a, gH, 'r')
hold on
plot(a, gL)
hold on
plot(a,a, 'k--')
legend('High state', 'Low state', '45d line', 'Location', 'NorthWest');
hold on
plot([aH1,aH1],[0,10], 'k:')
xlabel('Assets')
title('Policy functions - Ergotic and Transcient Sets')


rand('seed', 101);

shocks = binornd(1,.3,iter, N);
evo=zeros(iter,N);

start=rand(iter,1);
evo=[start evo];
    
for i = 2:N+1
    evo(:,i) = shocks(:,i-1).*(2*log(evo(:,i-1)+1) + 2) + (1-shocks(:,i-1)).*max((2*log(evo(:,i-1)+1) - 1.5),0);
end


figure(2)
subplot(2,2,1)
hist(evo(:,1), a)
xlabel('Assets')
title('Distribution of initial assets: Uniform on [0,1]')

subplot(2,2,2)
hist(evo(:,5), a)
xlabel('Assets')
title('Distribution of asset after 5 periods')

subplot(2,2,3)
hist(evo(:,20), a)
xlabel('Assets')
title('Distribution of asset after 20 periods')

subplot(2,2,4)
hist(evo(:,N), a)
xlabel('Assets')
title('Distribution of asset after 1000 periods')


start= 5*rand(iter,1) + 3;
evo=[start evo];
    
for i = 2:N+1
    evo(:,i) = shocks(:,i-1).*(2*log(evo(:,i-1)+1) + 2) + (1-shocks(:,i-1)).*max((2*log(evo(:,i-1)+1) - 1.5),0);
end



figure(3)
subplot(2,2,1)
hist(evo(:,1), a)
xlabel('Assets')
title('Distribution of initial assets: Uniform on [3,8]')

subplot(2,2,2)
hist(evo(:,5), a)
xlabel('Assets')
title('Distribution of asset after 5 periods')

subplot(2,2,3)
hist(evo(:,20), a)
xlabel('Assets')
title('Distribution of asset after 20 periods')

subplot(2,2,4)
hist(evo(:,N), a)
xlabel('Assets')
title('Distribution of asset after 1000 periods')

start= 10*ones(iter,1);
evo=[start evo];
    
for i = 2:N+1
    evo(:,i) = shocks(:,i-1).*(2*log(evo(:,i-1)+1) + 2) + (1-shocks(:,i-1)).*max((2*log(evo(:,i-1)+1) - 1.5),0);
end



figure(4)
subplot(2,2,1)
hist(evo(:,1), a)
xlabel('Assets')
title('Distribution of initial assets: Mass point at 10')

subplot(2,2,2)
hist(evo(:,5), a)
xlabel('Assets')
title('Distribution of asset after 5 periods')

subplot(2,2,3)
hist(evo(:,20), a)
xlabel('Assets')
title('Distribution of asset after 20 periods')

subplot(2,2,4)
hist(evo(:,N), a)
xlabel('Assets')
title('Distribution of asset after 1000 periods')

%%%%%%%%%%%%%%%
% Q7.2. 3rd case

clear all
clc
close all

N=1000;
iter=300;
c1=.5;
c2=1.1;


a = 0:.05:10;
gH = sin(a/c2 + c1) + a;
gL = sin(a/c2 + c1) + a - sin(c1);

total=[a;gH;gL];

H = @(a) sin(a/c2 + c1);
aH1= fzero(H,[0,4]);
aH2= fzero(H,[8,10]);
L = @(a) sin(a/c2 + c1) - sin(c1);
aL1= fzero(L,[0.1,4]);
aL2= fzero(L,[8,10]);


figure(1)
plot(a, gH, 'r')
hold on
plot(a, gL)
hold on
plot(a,a, 'k--')
legend('High state', 'Low state', '45d line', 'Location', 'NorthWest');
hold on
plot([aH1,aH1],[0,10], 'k:')
hold on
plot([aH2,aH2],[0,10], 'k:')
hold on
plot([aL1,aL1],[0,10], 'k:')
hold on
plot([aL2,aL2],[0,10], 'k:')
xlabel('Assets')
title('Policy functions - Ergotic and Transcient Sets')





rand('seed', 101);

shocks = binornd(1,.3,iter, N);
evo=zeros(iter,N);

start=rand(iter,1);
evo=[start evo];
    
for i = 2:N+1
    evo(:,i) = shocks(:,i-1).*(sin(evo(:,i-1)/c2 + c1) + evo(:,i-1)) + (1-shocks(:,i-1)).*(sin(evo(:,i-1)/c2 + c1) + evo(:,i-1) - sin(c1));
end




gH = sin(a/c2 + c1) + a;
gL = sin(a/c2 + c1) + a - sin(c1);

figure(2)
subplot(2,2,1)
hist(evo(:,1), a)
xlabel('Assets')
title('Distribution of initial assets: Uniform on [0,1]')

subplot(2,2,2)
hist(evo(:,5), a)
xlabel('Assets')
title('Distribution of asset after 5 periods')

subplot(2,2,3)
hist(evo(:,20), a)
xlabel('Assets')
title('Distribution of asset after 20 periods')

subplot(2,2,4)
hist(evo(:,N), a)
xlabel('Assets')
title('Distribution of asset after 1000 periods')


start= 5*rand(iter,1) + 3;
evo=[start evo];
    
for i = 2:N+1
    evo(:,i) = shocks(:,i-1).*(sin(evo(:,i-1)/c2 + c1) + evo(:,i-1)) + (1-shocks(:,i-1)).*(sin(evo(:,i-1)/c2 + c1) + evo(:,i-1) - sin(c1));
end



figure(3)
subplot(2,2,1)
hist(evo(:,1), a)
xlabel('Assets')
title('Distribution of initial assets: Uniform on [3,8]')

subplot(2,2,2)
hist(evo(:,5), a)
xlabel('Assets')
title('Distribution of asset after 5 periods')

subplot(2,2,3)
hist(evo(:,20), a)
xlabel('Assets')
title('Distribution of asset after 20 periods')

subplot(2,2,4)
hist(evo(:,N), a)
xlabel('Assets')
title('Distribution of asset after 1000 periods')

start= 10*ones(iter,1);
evo=[start evo];
    
for i = 2:N+1
    evo(:,i) = shocks(:,i-1).*(sin(evo(:,i-1)/c2 + c1) + evo(:,i-1)) + (1-shocks(:,i-1)).*(sin(evo(:,i-1)/c2 + c1) + evo(:,i-1) - sin(c1));
end



figure(4)
subplot(2,2,1)
hist(evo(:,1), a)
xlabel('Assets')
title('Distribution of initial assets: Mass point at 10')

subplot(2,2,2)
hist(evo(:,5), a)
xlabel('Assets')
title('Distribution of asset after 5 periods')

subplot(2,2,3)
hist(evo(:,20), a)
xlabel('Assets')
title('Distribution of asset after 20 periods')

subplot(2,2,4)
hist(evo(:,N), a)
xlabel('Assets')
title('Distribution of asset after 1000 periods')













