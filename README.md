#Chicago Economics PhD Core


This repository is to provide a place to collaboratively practice qualifying exams in economics grad programs, 
focusing specifically on the Chicago PhD core classes and previous qualifying exams.


**Disclaimer**: This is designed only to include solutions to practice exams available freely online. Any indication 
of dishonest behavior or facilitation of cheating will not be tolerated. Copyrighted material is prohibited.