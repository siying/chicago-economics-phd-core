clear all;
close all;
clc;

cd 'C:\Users\ChattrinL\Documents\Economics\TA_Econometrics 2'

n = 10000;
a = linspace(0,1,n);
p = .25;
x = (1-p)/p;
lam = (x+x.^a + (x^2 + x.^(2*a) - 2*x*x.^a + 4*x*x.^(-a)).^.5)/(1+x)/2;
figure;
plot(a,-log(lam))
xlabel('\alpha')
ylabel('-log \eta(\alpha)');
title('Chernoff Energy');
print -djpeg ps4_hansen
[v p] = max(-log(lam));
a(p)
v