%%% Chattrin Laksanabunsong %%%

cd 'C:\Users\ChattrinL\Documents\ECONOMICS\TA_econ311'

close all;
clear all;
clc;

A = [1.34254  -0.65504
   1                  0];
B = [15.41; 0];

sel = [1 0];
for t=1:51
    impulse(t) = sel*A^(t-1)*B;
end

figure;
plot(0:50,impulse)
xlabel('Time');
title('Impulse Response');
print -djpeg ps2_hansen1


seed = 27;
rand('seed',seed);
randn('seed',seed);

time = 10000;
epsi = randn(time,100);
z = zeros(time,100);
z(1,:) = B(1)*epsi(1,:);
z(2,:) = A(1,1)*z(1,:) + B(1)*epsi(2,:);
for t=3:time
    z(t,:) = A(1,1)*z(t-1,:) + A(1,2)*z(t-2,:) + B(1)*epsi(t,:);
end

y = cumsum(z);
scal_y = y(end,:)/sqrt(length(y));
temp = randn(100,1)*([1 0]*(eye(length(A))-A)^(-1)*B);

figure;
subplot(1,2,1)
ksdensity(temp)
title('Theoretical Distribution')
subplot(1,2,2)
ksdensity(scal_y)
title('Sample Distribution')
axis([-200 200 0 .009])
print -djpeg ps2_hansen2

count = zeros(2,2);
for i=1:10000
    count = count + A^(i-1)*B*B'*A'^(i-1);
end



