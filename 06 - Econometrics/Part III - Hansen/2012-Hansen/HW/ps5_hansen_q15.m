%%% Chattrin Laksanabunsong %%%
clear all;
close all;
clc;

rho = [0 .75 .95];
sig = [.5 1 2];
f1 = [1 0];
f2 = [0 1];

for i=1:3
    for j=1:3
        A = rho(i)*eye(2);
        B = [(1-rho(i))*sig(j) 0; 0 sqrt(rho(i))*sig(j)];
        D = f1*A + f2*(A-eye(2)); 
        F = [1 1]*B;
        sigma = zeros(2,2);
%         for k=1:100
%             sigma = sigma + A^(k-1)*B*B'*A'^(k-1)';
%         end
        sigma
        for t=1:20
            temp = sigma;
            kal = (A*sigma*D'+B*F')*(D*sigma*D'+F*F')^(-1);
            sigma = A*sigma*A' + B*B' - kal*(D*sigma*D'+F*F')*kal';
        end
        sigma
        conv(i,j) = sum(sum(abs(sigma-temp)));
        vard(i,j)= D*sigma*D' + F*F'; % conditional variance
        varmar(i,j) = (f1*(eye(2)-A)^(-1)*B)*(f1*(eye(2)-A)^(-1)*B)'; % martingale
    end
end

            
