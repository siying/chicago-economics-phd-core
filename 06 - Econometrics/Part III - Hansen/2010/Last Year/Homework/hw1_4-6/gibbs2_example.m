% gibbs2
n=20; % make it an even number!
p=0.9;
m=0; % 100;
T=1000;
k=4;
currones1=zeros(T,k);
hist1=zeros(n+1,k);
for i=1:k,
    x=zeros(n,1);
    for t=1:T,
        for j=1:n,
            sumones=sum(x)-x(j);
            p1=p*(sum(x)-x(j)>(n-1)/2)+(1-p)*(sum(x)-x(j)<=(n-1)/2);
            x(j)=rand<p1;
        end;
        hist1(sum(x)+1,i)=hist1(sum(x)+1,i)+1;
        currones1(t,i)=sum(x)>n/2;
    end;
end;

p=0.8;
currones2=zeros(T,k);
hist2=zeros(n+1,k);
for i=1:k,
    x=zeros(n,1);
    for t=1:T,
        for j=1:n,
            sumones=sum(x)-x(j);
            p1=p*(sum(x)-x(j)>(n-1)/2)+(1-p)*(sum(x)-x(j)<=(n-1)/2);
            x(j)=rand<p1;
        end;
        hist2(sum(x)+1,i)=hist2(sum(x)+1,i)+1;
        currones2(t,i)=sum(x)>n/2;
    end;
end;

p=0.7
currones3=zeros(T,k);
hist3=zeros(n+1,k);
for i=1:k,
    x=zeros(n,1);
    for t=1:T,
        for j=1:n,
            sumones=sum(x)-x(j);
            p1=p*(sum(x)-x(j)>(n-1)/2)+(1-p)*(sum(x)-x(j)<=(n-1)/2);
            x(j)=rand<p1;
        end;
        hist3(sum(x)+1,i)=hist3(sum(x)+1,i)+1;
        currones3(t,i)=sum(x)>n/2;
    end;
end;

p=0.6;
currones4=zeros(T,k);
hist4=zeros(n+1,k);
for i=1:k,
    x=zeros(n,1);
    for t=1:T,
        for j=1:n,
            sumones=sum(x)-x(j);
            p1=p*(sum(x)-x(j)>(n-1)/2)+(1-p)*(sum(x)-x(j)<=(n-1)/2);
            x(j)=rand<p1;
        end;
        hist4(sum(x)+1,i)=hist4(sum(x)+1,i)+1;
        currones4(t,i)=sum(x)>n/2;
    end;
end;


%hndl=plot(1:T,currones);
%dispprint(hndl,'gibbs_evol.eps',1,1,1);
p=0.9;
% Solving for the true stationary distribution:
trans=zeros(n+1,n+1);  
% trans(i+1,j+1) is the transition probability for going from i to j in the
% sum of ones in x.
for s = 0 : n,
    p_xj_is_1 = s/n;
    p_xnew0_xold1 = p*(s-1<(n-1)/2)+(1-p)*(s-1>(n-1)/2);
    p_xnew1_xold0 = p*(s-0>(n-1)/2)+(1-p)*(s-0<(n-1)/2);
    pa = (1 - p_xj_is_1)*p_xnew1_xold0;
    pb =      p_xj_is_1 *p_xnew0_xold1;
    if s == 0,
        trans(1,1)=1-pa; trans(1,2)=pa;
    elseif s == n,
        trans(n+1,n+1)=1-pb; trans(n+1,n)=pb;
    else
        trans(s+1,s+2)=pa; trans(s+1,s)=pb; trans(s+1,s+1)=1-pa-pb;
    end;
end;

solvedist = eye(n+1) - trans';
for j = 1 : n+1,
    solvedist(1,j)=1;
end;
rhs=zeros(n+1,1);rhs(1)=1;
statdist=1000*(solvedist\rhs);
hist1=[hist1 statdist];

p=0.8;
% Solving for the true stationary distribution:
trans=zeros(n+1,n+1);  
% trans(i+1,j+1) is the transition probability for going from i to j in the
% sum of ones in x.
for s = 0 : n,
    p_xj_is_1 = s/n;
    p_xnew0_xold1 = p*(s-1<(n-1)/2)+(1-p)*(s-1>(n-1)/2);
    p_xnew1_xold0 = p*(s-0>(n-1)/2)+(1-p)*(s-0<(n-1)/2);
    pa = (1 - p_xj_is_1)*p_xnew1_xold0;
    pb =      p_xj_is_1 *p_xnew0_xold1;
    if s == 0,
        trans(1,1)=1-pa; trans(1,2)=pa;
    elseif s == n,
        trans(n+1,n+1)=1-pb; trans(n+1,n)=pb;
    else
        trans(s+1,s+2)=pa; trans(s+1,s)=pb; trans(s+1,s+1)=1-pa-pb;
    end;
end;

solvedist = eye(n+1) - trans';
for j = 1 : n+1,
    solvedist(1,j)=1;
end;
rhs=zeros(n+1,1);rhs(1)=1;
statdist=1000*(solvedist\rhs);
hist2=[hist2 statdist];

p=0.7;
% Solving for the true stationary distribution:
trans=zeros(n+1,n+1);  
% trans(i+1,j+1) is the transition probability for going from i to j in the
% sum of ones in x.
for s = 0 : n,
    p_xj_is_1 = s/n;
    p_xnew0_xold1 = p*(s-1<(n-1)/2)+(1-p)*(s-1>(n-1)/2);
    p_xnew1_xold0 = p*(s-0>(n-1)/2)+(1-p)*(s-0<(n-1)/2);
    pa = (1 - p_xj_is_1)*p_xnew1_xold0;
    pb =      p_xj_is_1 *p_xnew0_xold1;
    if s == 0,
        trans(1,1)=1-pa; trans(1,2)=pa;
    elseif s == n,
        trans(n+1,n+1)=1-pb; trans(n+1,n)=pb;
    else
        trans(s+1,s+2)=pa; trans(s+1,s)=pb; trans(s+1,s+1)=1-pa-pb;
    end;
end;

solvedist = eye(n+1) - trans';
for j = 1 : n+1,
    solvedist(1,j)=1;
end;
rhs=zeros(n+1,1);rhs(1)=1;
statdist=1000*(solvedist\rhs);
hist3=[hist3 statdist];

p=0.6;
% Solving for the true stationary distribution:
trans=zeros(n+1,n+1);  
% trans(i+1,j+1) is the transition probability for going from i to j in the
% sum of ones in x.
for s = 0 : n,
    p_xj_is_1 = s/n;
    p_xnew0_xold1 = p*(s-1<(n-1)/2)+(1-p)*(s-1>(n-1)/2);
    p_xnew1_xold0 = p*(s-0>(n-1)/2)+(1-p)*(s-0<(n-1)/2);
    pa = (1 - p_xj_is_1)*p_xnew1_xold0;
    pb =      p_xj_is_1 *p_xnew0_xold1;
    if s == 0,
        trans(1,1)=1-pa; trans(1,2)=pa;
    elseif s == n,
        trans(n+1,n+1)=1-pb; trans(n+1,n)=pb;
    else
        trans(s+1,s+2)=pa; trans(s+1,s)=pb; trans(s+1,s+1)=1-pa-pb;
    end;
end;

solvedist = eye(n+1) - trans';
for j = 1 : n+1,
    solvedist(1,j)=1;
end;
rhs=zeros(n+1,1);rhs(1)=1;
statdist=1000*(solvedist\rhs);
hist4=[hist4 statdist];

subplot(2,2,1), plot(0:n,hist4)
subplot(2,2,2), plot(0:n,hist3)
subplot(2,2,3), plot(0:n,hist2)
subplot(2,2,4), plot(0:n,hist1)

%hndl=plot(0:n,histprob,0:n,statdist,'k--*');
%xlabel('sum of yes voters');
%ylabel('Percent');
%title('Stationary distribution');
%FN=['gibbs_hist_',sprintf('%2.1f',p),'.eps'];
%dispprint(hndl,FN,1,1,1);