% problem5
s2=1;
x=10;

X=linspace(0.01,0.99,100);

% the flat prior
Prior_flat=1;

% Jeffrey's prior
Prior_jeff=X./(1-X.^2);

% posterior using flat prior
Posterior_flat=sqrt(1-X.^2).*exp(-.5*(x^2*(1-X.^2))/s2);

% posterior using Jeffrey's prior
Posterior_jeff=X./(sqrt(1-X.^2)).*exp(-.5*(x^2*(1-X.^2))/s2);

% normalizing everyone

Posterior_flat=Posterior_flat/sum(Posterior_flat);
Posterior_jeff=Posterior_jeff/sum(Posterior_jeff);

% plotting
plot(X,Prior_flat,X,Prior_jeff)

plot(X,Posterior_flat,X,Posterior_jeff)

% look at close to 1

X=linspace(0.9,0.999,1000);

% the flat prior
Prior_flat=1;

% Jeffrey's prior
Prior_jeff=X./(1-X.^2);

% posterior using flat prior
Posterior_flat=sqrt(1-X.^2).*exp(-.5*(x^2*(1-X.^2))/s2);

% posterior using Jeffrey's prior
Posterior_jeff=X./(sqrt(1-X.^2)).*exp(-.5*(x^2*(1-X.^2))/s2);

% normalizing everyone

Posterior_flat=Posterior_flat/sum(Posterior_flat);
Posterior_jeff=Posterior_jeff/sum(Posterior_jeff);

% plotting
plot(X,Prior_flat,X,Prior_jeff)

plot(X,Posterior_flat,X,Posterior_jeff)

