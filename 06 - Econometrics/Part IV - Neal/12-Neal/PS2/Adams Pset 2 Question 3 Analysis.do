clear all
set more off

cd "C:\Users\Jonathan\Dropbox\Teaching\Neal Hortacsu 2012\Stata pset"

use acs

keep statefip age sex incwage educ* bpl* perwt metro

*Dropping observations whose urban/rural status is not identifiable
drop if metro == 0

*Creating a dummy that identifies living in an urban (metro) area
gen urban = (metro > 1)

*Creating rural indicators by birth state
*Notice that we took care to weight the variable!
bysort statefip: egen statetot = total(perwt)
bysort statefip: egen urbshare = total(urban*perwt/statetot)
gen birthurbshare = 0
replace birthurbshare = urbshare if bpl == statefip
bysort bpl: egen bus = max(birthurbshare)
replace birthurbshare = bus
drop bus

*Generate indicators! (I use the 80% urban indicator, but tried these others for robustness)
forvalues i=5/9{
gen birthurb`i'0 = (birthurbshare >= `i'/10)
}

*We're going to pare down our sample
*First, drop if the person had zero wage income or were not in universe (less than 16 years old)
drop if incwage == 0
drop if incwage == 999999
*Ideally would use topcoding spreadsheet to correct by state.

*Second, we're going to drop people born outside of the 50 states
drop if bpl > 56

*Number of observations remaining:
count


*Our model uses log wages, so we need that variable:
gen lwage = log(incwage)


*coding a variable for years of schooling.
*I add a year to the high school grade for preschool education (e.g. grade 10 is 11 years)
*and assume everyone completed 13 years of schooling before going to college.
*5+ years of college is then broken down by degree type
*Dummies for each category are a good alternative if you do not believe the effect of education is linear
gen eduyrs = 9
forvalues i=3/11{
replace eduyrs = `i' + 7 if educ == `i'
}

replace eduyrs = 18 if educd == 114 | educd == 115
replace eduyrs = 20 if educd == 116

*Alternatively, if you think that the returns to years of schooling are a lot greater for post-secondary,
*we use years of post-secondary education:
gen eduyrs2 = 0
forvalues i=7/11{
replace eduyrs2 = `i' -6 if educ == `i'
}

replace eduyrs2 = 6 if educd == 114 | educd == 115
replace eduyrs2 = 8 if educd == 116

*Generating labor market experience as the the number of years since the person completed their education
*Will use both specifications
gen exp = age - eduyrs - 5
gen exp2 = age - eduyrs2 - 18

*Turning 'sex' into an indicator dummy for being male
replace sex = 0 if sex == 2




*Part A: Parametric!

*First stage: probit
probit urban sex eduyrs exp birthurb80 [pw=perwt]
predict u_prob1
*A second specification of education
probit urban sex eduyrs2 exp2 birthurb80 [pw=perwt]
predict u_prob
*NOTE The predicted value of the probit is the denominator of the inverse Mills
*If we wanted the linear predicted from the decision rule, instead of the CDF of it,
*we would have to specify the option 'xb' and the following calculation would change

*Creating the Inverse Mills ratio
gen mills1_u = normalden(invnormal(u_prob1))/u_prob1
gen mills_u = normalden(invnormal(u_prob))/u_prob
gen mills1_r = normalden(invnormal(1-u_prob1))/(1-u_prob1)
gen mills_r = normalden(invnormal(1-u_prob))/(1-u_prob)

*Second stage: OLS
reg lwage exp eduyrs sex mills1_u [pw=perwt] if urban == 1
reg lwage exp eduyrs sex mills1_r [pw=perwt] if urban == 0
*The second specification of education:
reg lwage exp2 eduyrs2 sex mills_u [pw=perwt] if urban == 1
reg lwage exp2 eduyrs2 sex mills_r [pw=perwt] if urban == 0


*Part B:

gen agebucket = 1
_pctile age [pw=perwt], percentiles(10(10)90)
forvalues i=1/9{
replace agebucket = `i'+1 if age > r(r`i')
}


bysort bpl eduyrs2 sex agebucket: egen size1 = count(urban)
bysort bpl eduyrs2 sex agebucket: egen totweight1 = total(perwt)
bysort bpl eduyrs2 sex agebucket: egen prop1 = total(urban*perwt/totweight1)
*Identify urban = .11 by BPL Vermont (50), eduyrs2 0, Male, age 16-21 (1)
*Identify urban = .99 by BPL New Jersey (34), eduyrs2 4, Female, age 27-30 (3)

gen prop2 = prop1^2
gen prop3 = prop1^3
gen prop4 = prop1^4


*First we have to consistently identify the beta's.
reg lwage exp2 eduyrs2 sex prop1 [pw=perwt] if urban == 1
reg lwage exp2 eduyrs2 sex prop1 prop2 [pw=perwt] if urban == 1
reg lwage exp2 eduyrs2 sex prop1 prop2 prop3 [pw=perwt] if urban == 1
reg lwage exp2 eduyrs2 sex prop1 prop2 prop3 prop4 [pw=perwt] if urban == 1

reg lwage exp2 eduyrs2 sex prop1 [pw=perwt] if urban == 0
reg lwage exp2 eduyrs2 sex prop1 prop2 [pw=perwt] if urban == 0
reg lwage exp2 eduyrs2 sex prop1 prop2 prop3 [pw=perwt] if urban == 0
reg lwage exp2 eduyrs2 sex prop1 prop2 prop3 prop4 [pw=perwt] if urban == 0
*The coefficients converged!

*Now let's get the constants
reg lwage exp2 if urban ==1 & sex == 0 & eduyrs2 == 4 & statefip == 34 & agebucket == 3
reg lwage exp2 if urban ==0 & sex == 1 & eduyrs2 == 0 & statefip == 50 & agebucket == 1
*In the first case, the education effect needs to be subtracted from the constant
*In the second case, the coefficient on sex needs to be subtracted.  Rural is poorly identified.


*Trying semi-parametric, using propensities from the probits.
gen u_prob2 = u_prob^2
gen u_prob3 = u_prob^3
gen u_prob4 = u_prob^4

reg lwage exp2 eduyrs2 sex u_prob [pw=perwt] if urban == 1
reg lwage exp2 eduyrs2 sex u_prob u_prob2 [pw=perwt] if urban == 1
reg lwage exp2 eduyrs2 sex u_prob u_prob2 u_prob3 [pw=perwt] if urban == 1
reg lwage exp2 eduyrs2 sex u_prob u_prob2 u_prob3 u_prob4 [pw=perwt] if urban == 1

reg lwage exp2 eduyrs2 sex u_prob [pw=perwt] if urban == 0
reg lwage exp2 eduyrs2 sex u_prob u_prob2 [pw=perwt] if urban == 0
reg lwage exp2 eduyrs2 sex u_prob u_prob2 u_prob3 [pw=perwt] if urban == 0
reg lwage exp2 eduyrs2 sex u_prob u_prob2 u_prob3 u_prob4 [pw=perwt] if urban == 0
*Again, the coefficients converge!

