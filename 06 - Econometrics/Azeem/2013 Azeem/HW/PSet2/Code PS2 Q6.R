# Solution - Metrics, PS2 Q6
# Note that R uses the unbiased estimate of variance, as opposed to S_{N}^{2}
rm(list=ls())

set.seed(101413)
sims <- 10^3

alpha <- 0.05
z_alpha <- qnorm(1-(alpha/2))
p_true <- c(10^-3,10^-1,0.25,0.5)
sample_size <- c(5,20,50,100,500,1000)

finite_sample_band <- (alpha*sample_size)^-0.5 # size of region is not a function of the realized data

count_asymptotic <- matrix(0,length(p_true),length(sample_size))
count_finite_sample <- matrix(0,length(p_true),length(sample_size))
results_asymptotic <- matrix(0,length(p_true),length(sample_size))
results_finite_sample <- matrix(0,length(p_true),length(sample_size))

for (i in 1:length(p_true)){
  for (j in 1:length(sample_size)){
    for (k in 1:sims){
      uniform_data <- runif(sample_size[j]) # distributed iid U([0,1])
      data <- as.numeric(uniform_data < (p_true[i]+0.1)/2) # distributed iid B((p_true[i]+0.1)/2)
      point_estimate <- 2*mean(data)-0.1
      
      asymptotic_band <- sd(data)*z_alpha*2*sample_size[j]^(-0.5)
      test_asymptotic <- as.numeric(abs(p_true[i] - point_estimate) < asymptotic_band)
      count_asymptotic[i,j] <- count_asymptotic[i,j] + test_asymptotic
      
      test_finite_sample <- as.numeric(abs(p_true[i] - point_estimate) < finite_sample_band[j])
      count_finite_sample[i,j] <- count_finite_sample[i,j] + test_finite_sample
    }
  }  
}

results_asymptotic <- count_asymptotic/sims
results_finite_sample <- count_finite_sample/sims
