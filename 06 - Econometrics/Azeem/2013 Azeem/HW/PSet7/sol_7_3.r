##Runing a single estimation w/ theta = .5, n = 2##

x1 <- runif(2,.5,1)


##Generating the two estimators##

theta_hat <- .5*max(x1)
theta_tilde <- 2*theta_hat


##parts three through five##

est3hat <- (theta_hat - .5)^2
est3tilde <- (theta_tilde - .5)^2

est4hat <- abs(theta_hat - .5)
est4tilde <- abs(theta_tilde - .5)

MLEcloser <- isTRUE(est4hat < est4tilde)

-------------
##Begin actual code for monte carlo##
##Code for theta=0.5, n=2 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(2,.5,1)
theta_hat <- .5*max(x)
theta_tilde <- (2.5/2)*theta_hat


est3hat <- (theta_hat - .5)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - .5)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - .5)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - .5)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)





##Code for theta=0.5, n=5 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(5,.5,1)
theta_hat <- .5*max(x)
theta_tilde <- (6/5)*theta_hat


est3hat <- (theta_hat - .5)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - .5)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - .5)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - .5)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)





##Code for theta=0.5, n=20 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(20,.5,1)
theta_hat <- .5*max(x)
theta_tilde <- (21/20)*theta_hat


est3hat <- (theta_hat - .5)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - .5)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - .5)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - .5)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)


##Code for theta=0.5, n=100 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(100,.5,1)
theta_hat <- .5*max(x)
theta_tilde <- (101/100)*theta_hat


est3hat <- (theta_hat - .5)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - .5)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - .5)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - .5)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)


##Code for theta=1, n=2 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(2,1,2)
theta_hat <- .5*max(x)
theta_tilde <- (3/2)*theta_hat


est3hat <- (theta_hat - 1)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - 1)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - 1)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - 1)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)



##Code for theta=1, n=5 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(5,1,2)
theta_hat <- .5*max(x)
theta_tilde <- (6/5)*theta_hat


est3hat <- (theta_hat - 1)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - 1)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - 1)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - 1)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)




##Code for theta=1, n=20 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(20,1,2)
theta_hat <- .5*max(x)
theta_tilde <- (21/20)*theta_hat


est3hat <- (theta_hat - 1)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - 1)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - 1)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - 1)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)


##Code for theta=1, n=100 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(100,1,2)
theta_hat <- .5*max(x)
theta_tilde <- (101/100)*theta_hat


est3hat <- (theta_hat - 1)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - 1)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - 1)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - 1)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)



##Code for theta=10, n=2 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(2,10,20)
theta_hat <- .5*max(x)
theta_tilde <- (3/2)*theta_hat


est3hat <- (theta_hat - 10)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - 10)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - 10)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - 10)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)


##Code for theta=10, n=5 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(5,10,20)
theta_hat <- .5*max(x)
theta_tilde <- (6/5)*theta_hat


est3hat <- (theta_hat - 10)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - 10)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - 10)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - 10)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)

##Code for theta=10, n=20 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(20,10,20)
theta_hat <- .5*max(x)
theta_tilde <- (21/20)*theta_hat


est3hat <- (theta_hat - 10)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - 10)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - 10)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - 10)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)

##Code for theta=10, n=100 ##

MLEcloserSUM=0
est3hatSUM=0
est3tildeSUM=0
est4hatSUM=0
est4tildeSUM=0

for (x in 1:10000) {
x=runif(100,10,20)
theta_hat <- .5*max(x)
theta_tilde <- (101/100)*theta_hat


est3hat <- (theta_hat - 10)^2
est3hatSUM = est3hatSUM + est3hat

est3tilde <- (theta_tilde - 10)^2
est3tildeSUM = est3tildeSUM + est3tilde

est4hat <- abs(theta_hat - 10)
est4hatSUM = est4hatSUM + est4hat

est4tilde <- abs(theta_tilde - 10)
est4tildeSUM = est4tildeSUM + est4tilde

MLEcloser <- isTRUE(est4hat < est4tilde)
MLEcloserSUM = MLEcloserSUM + MLEcloser

}

est3hatAVE = est3hatSUM/10000
print(est3hatAVE)

est3tildeAVE = est3tildeSUM/10000
print(est3tildeAVE)

est4hatAVE = est4hatSUM/10000
print(est4hatAVE)

est4tildeAVE = est4tildeSUM/10000
print(est4tildeAVE)

MLEave = MLEcloserSUM/10000
print(MLEave)


