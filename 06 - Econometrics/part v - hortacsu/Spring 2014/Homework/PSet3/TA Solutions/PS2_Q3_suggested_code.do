
* ECON 312 Spring 2014
* PS2 Q3
* Jack Mountjoy


* Housekeeping
clear all
set more off

* Set the directory
cd "C:\Users\Jack\Desktop\UCHICAGO\Teaching\EAIII TA Spring 2014\Problem Sets\PS2\data"

* Load data
use mlogit



****************
* Part (a.iii) *
****************

* Construct dependent variable
gen y = log(share)-log(s0)

* Naive OLS regression
reg y price x1 x2, robust

* Calculate own-price elasticities
gen eta_ols = _b[price]*price*(1-share)

* Display median elasticity for each good across its markets
by product, sort: summarize eta_ols, detail



**************
* Part (b.i) *
**************

* Construct BLP-type instruments
by mkt, sort: egen x1mean = mean(x1)
by mkt, sort: egen x2mean = mean(x2)
gen z_blp1 = (x1mean-(x1/5))*(5/4)
gen z_blp2 = (x2mean-(x2/5))*(5/4)

* Run BLP-IV regression
ivregress 2sls y x1 x2 (price=z_blp1 z_blp2), vce(robust)

* Calculate own-price elasticities
gen eta_blp = _b[price]*price*(1-share)

* Display median elasticity for each good across its markets
by product, sort: summarize eta_blp, detail



***************
* Part (b.ii) *
***************

* Construct Hausman-type instruments
by product, sort: egen pmean = mean(price)
gen z_hausman = (pmean-(price/25))*(25/24)

* Hausman-IV regression
ivregress 2sls y x1 x2 (price=z_hausman), vce(robust)

* Calculate own-price elasticities
gen eta_haus = _b[price]*price*(1-share)

* Display median elasticity for each good across its markets
by product, sort: summarize eta_haus, detail



****************
* Part (b.iii) *
****************

* Construct product dummies
tab product, gen(prod)

* Regression with product dummies
reg y price x1 x2 prod1 prod2 prod3 prod4, robust

* Save predicted values for later welfare calculation
predict pred

* Calculate own-price elasticities
gen eta_dum = _b[price]*price*(1-share)

* Display median elasticity for each good across its markets
by product, sort: summarize eta_dum, detail



************
* Part (c) *
************

* Calculate consumer surplus implied by T1EV structure 
gen expn = exp(pred)
by mkt, sort: egen sumn = total(expn)
gen CS_each = - (log(sumn+1)+0.5772)/_b[price] 
summarize CS_each if product==1

