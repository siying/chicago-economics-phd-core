\documentclass[twocolumn]{article}
\title{Economics 310 (Quantitative Methods)  Cheat Sheet}
\author{Jonathan D. Hall}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm, multicol}
\usepackage[margin=.25in,nohead,nofoot]{geometry} 
\usepackage{graphicx}
\pagestyle{empty}



\theoremstyle{plain}       \newtheorem*{prob}{}
\theoremstyle{plain}       \newtheorem*{theorem}{}
\theoremstyle{remark}        \newtheorem*{ans}{Answer}
\theoremstyle{plain} \newtheorem{lemma}{Lemma}
\theoremstyle{definition} \newtheorem{defn}{Definition}[section]

\setlength{\abovedisplayskip}{-10pt}
\setlength{\belowdisplayskip}{0pt}

\setlength{\parindent}{0pt}
\def\vs{\vspace{7pt}}
\def\pf{\textbf{Proof: }}
\def\so{\textbf{Solution: }}
\def\bx{\flushright $\Box$\\ \flushleft }

\setlength{\parskip}{0pt plus 0.5ex minus 0.2ex}
\renewcommand{\baselinestretch}{1}
\newenvironment{packed_enum}{
\begin{enumerate}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{-3pt}
  \setlength{\parsep}{0pt}
  }{\end{enumerate}}
\newenvironment{packed_item}{
\begin{itemize}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{-3pt}
  \setlength{\parsep}{0pt}
}{\end{itemize}}


\def\R{\mathbb{R}}
\def\C{\mathbb{C}}
\def\e{\epsilon}
\def\d{\delta}
\def\N{\mathbb{N} }
\def\l{\lambda }
\def\e{\epsilon }
\def\a{\alpha }
\def\x{\chi }
\def\b{\beta }
\def\p{\rho }
\def\g{\gamma }
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\plim}{plim}
\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\cov}{cov}
\DeclareMathOperator{\var}{var}
\DeclareMathOperator{\avar}{avar}
\def\usim{\stackrel{u}{\sim}}
\def\conp{\stackrel{\text{p}}{\to}}
\def\cond{\stackrel{\text{d}}{\to}}
\def\bh{\hat{\beta }}
\def\yh{\hat{y }}
\def\eh{\hat{\epsilon }}
\def\Yh{\hat{Y }}
\def\xh{\hat{x }}
\def\Xh{\hat{X }}
\def\ss{\sigma ^2 }
\def\ssh{\hat{\sigma }^2 }

\def\bt{\tilde{\beta }}
\def\yt{\tilde{y }}
\def\et{\tilde{\epsilon }}
\def\Yt{\tilde{Y }}
\def\xt{\tilde{x }}
\def\Xt{\tilde{X }}

\def\bb{\bar{\beta }}
\def\yb{\bar{y }}
\def\eb{\bar{\epsilon }}
\def\Yb{\bar{Y }}
\def\xb{\bar{x }}
\def\Xb{\bar{X }}
\newcommand{\pd}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\condown}[3]{\underset{\substack{\downarrow #2\\ #3}}{ #1}}
\newcommand{\conover}[3]{\underset{\stackrel{\text{#2}}{\to} #3}{ #1}}
\def\thetah{\hat{\theta }}
\def\thetat{\tilde{\theta }}
\def\der{\textrm{ d}}

\renewcommand{\labelenumi}{\alph{enumi}.}
\begin{document}
\begin{tiny}
%\begin{multicols}{3}
\textbf{Inequalities:}\\
\begin{tabular}{|ll|} 
 \hline 
 Chebyshev Inequality: & $ P\left( \left| Y-\mu \right| \geq k\sigma\right) \leq \frac{1}{k^2}\ (\forall \ k>0)$
 \\ Markov's Inequality: & $P(X\geq c)\leq \frac{E(X)}{c}\ (\forall \ c>0$ \& $X$-nonneg rv)
 \\Jensen's Inequality: & $E\left( g\left(X\right) \right) \geq g\left(E\left(X\right) \right) \qquad \text{if $g(\cdot )$ is convex}$ \\ & $E\left( g\left(X\right) \right) \leq g\left(E\left(X\right) \right) \qquad \text{if $g(\cdot )$ is concave}$ \\
Cauchy-Schwartz Inequality:& $\left(E(XY)\right)^2\leq E(X^2)E(Y^2)$ \\
  \hline
\end{tabular} 

\textbf{Normal Distribution:}
$f_x(x)=\frac{1}{\sqrt{2\pi \sigma ^2}} e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma }\right) ^2}$
\[ f(x)=\frac{1}{(2\pi )^{n/2} det(\Sigma)^{1/2}}\exp \left(\frac{-(X-\mu)'\Sigma ^{-1}(X-\mu)}{2}\right) \]
\[ X\sim N(\mu, \Sigma)\]

\textbf{Convergence:}
$X_n\stackrel{\text{r-mean}}{\to} X \Leftrightarrow E\left(||X_n-X||^r\right)\to 0, r>0$ \\
$X_n \stackrel{\text{p}}{\to}X\Leftrightarrow \forall \ \e >0\ P\left(||X_n-X||>\e\right)\to 0$\\
$X_n \stackrel{\text{d}}{\to}X\Leftrightarrow P(X_n\leq X_0)\to P(X\leq X_0)\ \forall \ X_0 \ s.t. \ P(X=X_0)=0 $\\
$\hat{Q}_n(\theta)$ converges uniformly in probability to $Q_0(\theta)\Rightarrow \sup _{\theta \in \Theta} |\hat{Q}_n(\theta)-Q_0(\theta)|\conp 0$
\begin{packed_enum}
	\item  $X_n\stackrel{\text{r-mean}}{\to}X \text{ for some } r>0  \Rightarrow X_n\stackrel{\text{p}}{\to} X\Rightarrow X_n\stackrel{\text{d}}{\to} X$.
	\item $X_n\stackrel{\text{p}}{\to}X_0\Leftrightarrow X_n\stackrel{\text{d}}{\to} X_0$
	\item  $X_n\conp X$ and $Y_n\conp Y\Rightarrow (X_n,Y_n)\conp (X,Y)$.
	\item  (CMT) $X_n\conp X\Rightarrow g(X_n)\conp g(X) \forall \ g\in C$.
	Let $A_n\conp A$ and $B_n\conp B$, then:
\begin{packed_enum}
	\item  $A_n+B_n\conp A+B$ 
	\item  $A_nB_n\conp AB$ 
	\item  $A_n^{-1}\conp A^{-1}$ if $A^{-1}$ exists with prob 1. 
\end{packed_enum}
\item In general $X_n\cond X$ and $Y_n\cond Y \not \Rightarrow (X_n,Y_n)\cond (X,Y)$.
\item $X_n\cond X$ and $Y_n\conp Y_0\Rightarrow (X_n,Y_n)\cond (X,Y_0)$. (So all results above hold in this case).
\end{packed_enum}
\textbf{Khintchine LLN:} $\{X_i\}_1^n$ iid, E($||X_i||)<\infty \Rightarrow \bar{X}_n\conp E(X_i)$.\\
\textbf{Lindberg-Levy CLT:} $X_i$ iid, $E(X_i)=\mu $ and $V(X_i)=\Sigma  \Rightarrow \sqrt{n}(\bar{X}_n-\mu)\cond N(0,\Sigma )$ or $\sqrt{n}\Sigma ^{-1/2}(\bar{X}_n-\mu)\cond N(0,I )$.\\
\textbf{Delta-Method:} $\sqrt{n} (Z_n-Z_0)\cond N(0,\Sigma )\Rightarrow \sqrt{n}(g(z_n)-g(z_0))\cond N(0,\frac{\partial g(z_0)}{\partial z'}\Sigma \frac{\partial g(z_0)}{\partial z'}') $.
\textbf{Univariate Results Related to the Normal Distribution:}
\begin{packed_item}
	\item [(T1)] $X\sim N(\mu, \sigma ^2)\Rightarrow \frac{x-\mu}{\sigma}\sim N(0,1)$
	\item $Z\sim N(0,1)\Rightarrow Z^2\sim \chi ^2(1)$
	\item $X_1,\ldots , X_n$ independent, $X_i\sim \chi ^2(r_i) \Rightarrow \sum _{i=1} ^n X_i\sim \chi ^2 (
	\sum _{i=1} ^n r_i)$
	\item $Z_1, \ldots, Z_n$ independent, $Z_i\sim N(0,1) \Rightarrow \sum _{i=1} ^n Z_i^2\sim \chi ^2 (n)$
	\item $X_1\sim \chi ^2(n_1)$ and $X_2\sim \chi ^2 (n_2)$ independent $\Rightarrow F=\frac{X_1/n_1}{X_2/n_2}\sim F(n_1,n_2)$
	\item $Z\sim N(0,1)$ and $X\sim \chi ^2(n)$ independent $\Rightarrow t=\frac{Z}{\sqrt{X/n}}\sim t(n)$
	\item $t\sim t(n)\Rightarrow t^2\sim F(1,n)$
\end{packed_item}
\textbf{Multivariate Results for the Normal Distribution:}
\[X \text{is a }n\times 1 \text{ column vector} \qquad X\sim N(\mu,\Sigma)\]
\begin{packed_item}
	\item [(T4)] Theorem: $X\sim (\mu, \Sigma). \quad Y=AX+a\sim N(A\mu +a,A\sigma A')$
	\item [(T5)] Theorem: $Z\sim N(0,I)$ and $A$ idempotent ($A^2=A$), symmetric ($A'=A$) with rank $A=j\Rightarrow Z'AZ\sim \chi ^2(j)$.
	\item [(T6)] Theorem: $X\sim N(\mu, \Sigma) \Rightarrow Z=\Sigma ^{-1/2}(X-\mu)\sim N(0,1)$
	\item [(T7)] Theorem: $X\sim N(\mu, \Sigma)\Rightarrow (X-\mu)'\Sigma ^{-1}(X-\mu)\sim \chi ^2(n)$. ($(X-\mu)'\Sigma ^{-1}(X-\mu)=Z'Z\sim\chi ^2(n)$.)
\end{packed_item}


\\ \hline
\begin{multicols}{2}
Small Sample Assumptions:
\begin{packed_item}
	\item [(SS0)] $Y=X\b+\e$.
	\item [(SS1)] rank$(X)=k$.
	\item [(SS2)] $E(\e|X)=0$.
	\item [(SS3)] $E(\e\e'|X)=\Omega $ ($=\ss I$ for homoskedasticy and no auto).
	\item [(SS2')] $\e\sim N(0,\Omega )|X$.
\end{packed_item}
\columnbreak
Large Sample Assumptions:
\begin{packed_item}
	\item [(LS0)] $Y=X\b+\e$.
	\item [(LS1)] $X'X/n\conp M \ \ PD$ and rank$(X)=k$.
	\item [(LS2)] $X'\e/n\conp 0$.
	\item [(LS3)] $X'\e/\sqrt{n}\cond N(0,V), \ V=\plim (X'\Omega X/n)$ and $X'\Omega ^{-1}\e/\sqrt{n}\cond N(0,W)$ with \hbox{$W=\plim(X'\Omega ^{-1}X/n)$}.
\end{packed_item}
\end{multicols}
\begin{theorem}[T3] Theorem: Under assumptions (SS0) to (SS3) (w/ no homo or serial) we have:
\begin{packed_enum}
	\item $E(\bh |X)=\b$ and $V(\bh |X)=\sigma ^2 (X'X)^{-1}$
	\item $\bh$ is the unique BLUE (Best Linear Unbiased Estimator) of $\b \Leftarrow $ Gauss-Markov Theorem (GMT)
	\item $s^2\equiv \frac {\eh'\eh}{n-k}$ is an unbiased estimator of $\sigma ^2$
\end{packed_enum}
\end{theorem}
\begin{theorem}[T4] Under assumptions (SS0) to (SS2') (w/ no homo or serial) we can add to (T3) the following:
\begin{packed_enum}
	\item $\bh\sim N(\b , \sigma ^2 (X'X)^{-1})$
	\item $(n-k)s^2/\sigma ^2\sim \chi ^2(n-k)$
	\item $\bh$ and $s^2$ independent.
	\item $(\bh _j-\b _j)/SE_j\sim t(n-k) \forall \ j=1,\ldots ,k$ where $SE_j=\sqrt{s^2[(X'X)^{-1}]_{jj}}$
	\item $\bh _{ols}=\bh _{mle}$.
\end{packed_enum}
\end{theorem}
\begin{theorem}[T1 Theorem:] Under (LS0)-(LS2), $\bh\conp \b $ ($\bh$ is a consistent estimator of $\b $).
\end{theorem}
\begin{theorem}[T2 Theorem:] Under (LS0)-(LS3) (w/ no homo or serial):
\begin{packed_enum}
	\item $\bh\conp \b$
	\item $\sqrt{n}(\bh-\b)\cond N(0,M^{-1}VM^{-1})$
	\item $\frac{c'(\bh-\b)}{SE(c'\bh)}\cond N(0,1)$ where $SE(c'\bh)=[c'(X'X)^{-1} X'\hat{\Omega }X(X'X)^{-1}c]^{1/2}$ for any $\hat{\Omega }$ with $\frac {X'\hat{\Omega }X}{n}\conp V$. (c is $k\times 1$).
	\item $[h(\bh)-h(\b )]'[H(\bh)(X'X)^{-1} X'\hat{\Omega }X(X'X)^{-1}H(\bh)']^{-1}[h(\bh)-h(\b )]\cond \chi ^2(J)$ for any $J\times 1$ function $h(\cdot )$ which is continuously differentiable at $\b $ and which has derivative matrix $H(\b )\equiv \pd{h(\b )}{\b '}\ (J\times k)$ with full row rank $J\leq k$ in a neighborhood around $\b $. (The full row rank in necessary since for nonlinear restrictions, 2 restrictions can look very different far from the truth $\b $, but be very similar (become tangent) around $\b $)
\end{packed_enum} \end{theorem}
\begin{theorem}[T3 Theorem:] Under (LS0)-(LS4) (no homo or serial): 
\begin{packed_enum}
	\item $\bh\conp \b$
	\item $\sqrt{n}(\bh-\b)\cond N(0,\sigma ^2M^{-1})$
	\item $s ^2\conp \sigma ^2$
	\item $\frac{c'(\bh-\b)}{SE(c'\bh)}\cond N(0,1)$ where $SE(c'\bh)=[s^2c'(X'X)^{-1}c]^{1/2}$.
	
	\item $[h(\bh)-h(\b )]'[s^2H(\bh)(X'X)^{-1}H(\bh)']^{-1}[h(\bh)-h(\b )]\cond \chi ^2(J)$ for any $J\times 1$ function $h(\cdot )$ which is continuously differentiable at $\b $ and which has derivative matrix $H(\b )\equiv \pd{h(\b )}{\b '}\ (J\times k)$ with full row rank $J\leq k$ in a neighborhood around $\b $.
\end{packed_enum}
\end{theorem}
\textbf{White Estimator for Hetro:} $\hat{V}=\frac{1}{n}\sum \eh_i^2X_iX_i'$.\\
\textbf{Robinson for Serial:} $\hat{V_r}=\frac{1}{n}\sum_{i=1}^n\hat{\gamma }_0X_iX_i'+\frac{1}{n}\sum_{i=1}^{n-1}\sum _{j=i+1}^{n}\hat{\gamma } _i[X_jX_{j-i}'+X_{j-i}X_j']$ where $\hat{\gamma }_i=\frac{1}{n}\sum _{j=i+1}^n\eh_j\eh_{j-i} \ \forall i=0,1,\ldots, n-1$. Note: In calculating $\hat{\gamma }_i$, we divide by n regardless of the number of terms in sum. This has the desirable effect of downweighting (otherwise noisier) lags. \\
\textbf{Newey-West Est. for Hetro and Serial:} $\hat{V}_{NW}=\frac{1}{n}\sum _{i=1}^n\eh_i^2X_iX_i'+\frac{1}{n}\sum_{i=1}^n\sum_{j=i+1}^nw_i\eh_j\eh_{j-i}[X_jX_{j-i}'+X_{j-i}X_j']  $ where $w_i=1-\frac{i}{m+1}$ and $m\leq n-1$ is a truncation lag chosen by the econometrician. $w_i$ decreases in i, place less weight on covariances at higher lags. Guidance in choosing m optimally: balance consideration of bias (from m too small) and loss of precision (from m too large).



\\ \hline
\textbf{Testing:}
\begin{packed_item}
	\item Type 1 Error: $H_0$, although true, is rejected.
	\item Type 2 Error: $H_0$, although false, is accepted (fail to reject).
\end{packed_item}
\[RSS=\eh'\eh \qquad TSS=\sum (y_i-\yb)^2\qquad ESS=\sum (\yh_i-\yb)^2\]
\textbf{F-Tests:} \\
\begin{tabular}{|ll|} 
\hline &\\
Wald Statistic: & $F=\frac{W}{j}\equiv \frac{(R\bh - c)'[ s ^2R(X'X)^{-1}R')]^{-1}(R\bh - c) }{j}\sim F(j,n-k)$ \\
F-Statistic: & $ F=\frac{(\eh_r'\eh_r-\eh_u'\eh_u)/j}{\eh_u'\eh_u/(n-k)}=\frac{W}{j}\sim F(j,n-k)  $ \\
$R^2$ Version: & $F=\frac{(R^2_u-R^2_r)/j}{(1-R^2_u)/(n-k)}$ \\
\hline 
\end{tabular}
Reject $H_0$ if $F>F_\a(j,n-k)$ 
\hline 

\textbf{Const. EE} If there is a function $Q_0(\theta)$ s.t.
\begin{packed_item}
	\item [EE.i] $Q_0(\theta)$ is uniquely maximized at $\theta_0$.
	\item [ii] $\Theta $ is compact.
	\item [iii] $Q_0(\theta)$ is continuous.
	\item [iv] $\hat{Q}_n(\theta)$ converges uniformly in probability to $Q_0(\theta)$.
\end{packed_item}
Then $\hat{\theta}_n\conp \theta _0$.\\
\textbf{Const. MLE} Suppose that $x_i,\ i=1,2,\ldots , N$ are iid with pdf $f(x_i;\theta_0)$ and:
\begin{packed_item}
	\item [MLE.i] if $\theta\neq \theta_0$ then $f(x_i;\theta)\neq f(x_i;\theta_0)$.
	\item [ii] $\theta _0\in \Theta$ which is compact.
	\item [iii] $\ln f(x_i;\theta)$ is continuous at each $\theta \in \Theta$ with probability one.
	\item [iv] $E\left[ \sup _{\theta\in \Theta} |\ln f(x_i;\theta)|\right]<\infty$.
\end{packed_item}
Then $\thetah_n\conp \theta$.\\
\textbf{Asy. Norm. EE} If $\thetah_n\conp \theta_0$ and:
\begin{packed_item}
	\item [EE.i] $\theta _0\in \textrm{interior }\Theta$.
	\item [ii] $\hat{Q}_n(\theta)$ is twice continuously differentiable in a neighborhood $N$ of $\theta_0$ (ok to make this assumption since $\thetah_n\conp \theta_0$).
	\item [iii] $n^{-1/2}\nabla _\theta\hat{Q}_n(\theta_0)\cond N(0,\Sigma)$.
	\item [iv] There exists $H(\theta)$ that is continuous at $\theta_0$ and satisfies $\sup_{\theta\in N} ||\nabla_{\theta \theta}\hat{Q}_n(\theta)-H(\theta)||\conp 0$ (Euclidean norm for matrices: $||A||=\sqrt{\tr A'A})$.
	\item [v] $H=H(\theta_0)$ is nonsingular.
\end{packed_item}
Then $\sqrt{n}(\hat{\theta}_n-\theta_0)\cond N(0,H^{-1}\Sigma H^{-1})$.\\
\textbf{Asy. Norm. MLE} Suppose that $x_i,\ i=1,2,\ldots, N$ are iid, that the assumptions of the consistency theorem for MLE are satisfied and that:
\begin{packed_item}
	\item [MLE.i] $\theta_0 \in \textrm{ interior } \Theta$.
	\item [ii] $f(x,\theta)$ is twice continuously differentiable and $f(x;\theta)>0$ in a neighborhood $N$ of $\theta_0$.
\item [iii] $\int \sup_{\theta\in N} || \nabla _{\theta}f(x;\theta)||\textrm{d} x<\infty$ and $\int \sup_{\theta\in N} || \nabla _{\theta\theta}f(x;\theta)||\textrm{d} x<\infty$.
	\item [iv] $J=E\left[(\nabla_\theta\ln f(x,\theta_0))(\nabla_\theta\ln f(x,\theta_0))'\right]$ exists and is nonsingular.
	\item [v] $E[\sup ||\nabla_{\theta\theta} \ln f(x,\theta)||]<\infty$.
\end{packed_item}
Then $\sqrt{n}(\thetah_n-\theta_0)\cond N(0,J^{-1})$.\\
\textbf{Const. Asy. Var. Est.}
Since:$J^{-1}=\left[E\left[(\nabla_\theta\ln f(x,\theta_0))(\nabla_\theta\ln f(x,\theta_0))'\right]\right]^{-1}=\left[-E[\nabla _{\theta\theta}\ln f(x,\theta)]\right]^{-1}=H^{-1}$ 
We can suggest either: \[ \begin{aligned} \hat{J}_1^{-1}&=\left[\frac{1}{n}\sum_{i=1}^n\left[(\nabla_\theta\ln f(x_i,\theta_0))(\nabla_\theta\ln f(x_i,\theta_0))'\right]\right]^{-1}   \\
\hat{J}_2^{-1}&=\left[-\frac{1}{n}\sum_{i=1}^n[\nabla _{\theta\theta}\ln f(x_i,\theta)]\right]^{-1} \end{aligned} \]
We can show that $\hat{J}_1^{-1}\conp J^{-1}\qquad\text{and}\qquad \hat{J}_2^{-1}\conp J^{-1}$.\\
\textbf{Wald Statistic}:$W=n\cdot a(\thetah)\left[\left(\pd{a(\thetah_p)}{\theta '}\right)\hat{V}\left(\pd{a(\thetah_p)}{\theta '}\right)'\right]^{-1}a(\thetah)\cond \chi ^2(r)$.\\
\textbf{LM/Rao/Score Statistic}: $LM=n\pd{\hat{Q}_n(\thetat)}{ \theta'}\tilde{\Sigma}^{-1}\pd{\hat{Q}_n(\thetat)}{\theta}\cond \chi ^2(r)$.\\
\textbf{LR Statistic}: $2n(\ln L(\thetah)-\ln L(\thetat))\cond \chi ^2(r)$.\\
\hline
$\pd{Bx}{x'}=B\Rightarrow \partial {(Bx)'}{x}=B'$. $\pd{x'A'X}{x}=(A+A')x\Rightarrow \pd{x'A'X}{x'}=x'(A+A')$.
Let $A$ be an invertible matrix partitioned into 4 submatrices with $A_{11}$ and $A_{22}$ invertible. Then
$$A^{-1}=\left[
\begin{array}
[c]{cc}%
A_{11} & A_{12}\\
A_{21} & A_{22}%
\end{array}
\right]  ^{-1}=\left[
\begin{array}
[c]{cc}%
F_{1} & -F_{1}A_{12}A_{22}^{-1}\\
-F_{2}A_{22}A_{11}^{-1} & F_{2}%
\end{array}
\right]$$
where $
F_{1}=\left[  A_{11}-A_{12}A_{22}^{-1}A_{21}\right]  ^{-1}\quad\text{and}\quad
F_{2}=\left[  A_{22}-A_{21}A_{11}^{-1}A_{12}\right]  ^{-1}%
$ 

$Y=G(x)\Rightarrow f_y(y)=f_x(G(y)^{-1})|G'(y)^{-1}|$.



%\end{multicols}
\end{tiny}
\end{document}