%% Problem 1 
clear all;
close all;
clc;
% 2
y=zeros(51,3);
rho=0.95;
psi=[0.2,0.5,1];
y(1,:)=1;
y(2,:)=2*rho.*cos(psi);
for t = 3:51
    y(t,:)=2*rho.*cos(psi).*y(t-1,:)-rho^2*y(t-2,:);
end
for i = 1:3
    subplot(1,3,i); plot(y(:,i));
    title(['IRF, \psi=',num2str(psi(i))]);
    xlabel('k');
    ylabel('IRF');
end

%3
syms rho sigma2 psi A vecGamma
A=[2*rho*cos(psi) -rho^2;1 0];
vecGamma=inv(eye(4)-kron(A,A))*[sigma2 0 0 0]'

rho=0.95;
psi=[0.2 0.5 1];
Gamma=zeros(6,3);
Gamma(1,:)=(1+rho^2)./((1-rho^2)*(rho^4-2*rho^2.*cos(2*psi)+1));
Gamma(2,:)=2*rho*cos(psi)./((1-rho^2).*(rho^4-2*rho^2.*cos(2*psi)+1));
for i = 3:6
    Gamma(i,:)=2*rho.*cos(psi).*Gamma(i-1,:)-rho^2*Gamma(i-2,:);
end

% 5
omega = 0 : 0.001: pi;
sy = zeros(length(omega),3);
sigma2 = zeros(3,1);
sigma2 =(1-rho^2)*(1+rho^4-2*rho^2.*cos(2*psi))/(1+rho^2);
for i = 1:length(omega)
    sy(i,:) = sigma2./(2*pi*((1+rho^2-2*rho.*cos(psi+omega(i))).*...
            (1+rho^2-2*rho.*cos(psi-omega(i)))));
end
plot(omega,sy)
title('The spectrum of AR(2), Var(y)=1');
xlabel('\omega');
ylabel('s_y(\omega)');

% 6
int=zeros(6,3);
om = -pi:0.001:pi;
for j = 0:5;
    for i = 1:length(om);
        int(j+1,:)=int(j+1,:)+1./(2*pi*((1+rho^2-2*rho.*cos(psi+om(i))).*...
                (1+rho^2-2*rho.*cos(psi-om(i)))))*0.001*cos(j*om(i));
    end            
end

% 9
psi=[0.2 0.5 1];
sigma2=zeros(3,1);
ysim=zeros(200,3);
sigma2=(1-rho^2).*(1+rho^4-2*rho^2.*cos(2*psi))/(1+rho^2);
ysim(1,:)=normrnd(0,sigma2);
ysim(2,:)=normrnd(0,sigma2);
for t = 3:200
    ysim(t,:)=2*rho.*cos(psi).*ysim(t-1,:)-rho^2.*ysim(t-2,:)+normrnd(0,sigma2);
end

%% Problem 2
% 2
irf=zeros(6,2);
t=0:5;
rho=0.95;
psi=[0.2,1]';
irf(1,:)=1;
irf(2,:)=-2*rho*cos(psi);
irf(3,:)=rho^2;
plot(t,irf);
title('IRF');
xlabel('Lag');
ylabel('IRF');

% 5
omega=0:0.001:pi;
rho=0.95;
psi=[0.2,1];
spec=zeros(length(omega),2);
sigma2=zeros(2,1);
sigma2=(1+4*rho^2.*((cos(psi)).^2)+rho^4).^(-1);
for j = 1:length(omega)
    spec(j,:)=sigma2/(2*pi).*(1+rho^2-2*rho.*cos(psi+omega(j))).*...
          (1+rho^2-2*rho.*cos(psi-omega(j)));
end
plot(omega,spec)
title('The spectrum of MA(2), Var(y)=1');
xlabel('\omega');
ylabel('s_y(\omega)');

% 7
psi=[0.2 1];
sigma2=zeros(2,1);
ysim=zeros(200,2);
esim=zeros(200,2);
sigma2=1./(1+rho^4+4*rho^2.*(cos(psi)).^2);
esim(1,:)=normrnd(0,sigma2);
esim(2,:)=normrnd(0,sigma2);
for t = 3:200
    esim(t,:)=normrnd(0,sigma2);
    ysim(t,:)=2*rho.*cos(psi).*esim(t-1,:)-rho^2.*esim(t-2,:)+esim(t,:);
end
plot(ysim)

    
