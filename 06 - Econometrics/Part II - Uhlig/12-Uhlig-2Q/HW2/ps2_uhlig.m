%%% Chattrin Laksanabunsong %%%
%%% TA PS2 Uhlig %%%

clear all;
close all;
clc;

seed = 30;
randn('seed',seed);
rand('seed',seed);

%cd 'C:\Users\ChattrinL\Documents\Economics\TA_econ311'
cd 'C:\Users\ChattrinL\Documents\Economics\TA_Econometrics 2'

n = 100000;
w = linspace(0,.8,n);
lam = 1600;
temp = (2*cos(2*w) - 8*cos(w)+6)*lam;
h = 1/2/pi*temp./(1+temp);
figure;
plot(w,h)
title('h_{HP,1600}(\omega)');
xlabel('\omega');
ylabel('Frequency');
print -djpeg ps2_out1

y = randn(100,1);
[yc] = hpfilter(y,lam);
figure
plot(0:1:99,y,0:1:99,y-yc);
legend('y','yc')
xlabel('Time');
ylabel('Value');
title('y vs. y^c')
print -djpeg ps2_out2


%%% Part 6a %%%
w = linspace(0,pi/5,n);
lam1 = 1600;
lam2 = 6.25;
lam3 = 100;
lam4 = 1000;
temp1 = (2*cos(2*w) - 8*cos(w)+6)*lam1;
temp2 = (2*cos(8*w) - 8*cos(4*w)+6)*lam2;
temp3 = (2*cos(8*w) - 8*cos(4*w)+6)*lam3;
temp4 = (2*cos(8*w) - 8*cos(4*w)+6)*lam4;
h1 = 1/2/pi*temp1./(1+temp1);
h2 = 1/2/pi*temp2./(1+temp2);
h3 = 1/2/pi*temp3./(1+temp3);
h4 = 1/2/pi*temp4./(1+temp4);
figure;
plot(w,h1,w,h2,w,h3,w,h4)
title('h_{HP,1600}(\omega)');
xlabel('\omega');
ylabel('Frequency');
legend('Original','\lambda = 6.25','\lambda = 100','\lambda = 1000',4);
print -djpeg ps2_out3

%%% Uhlig's Code %%%
%%% Parameters %%%
FN='hw2';
DOPAUSE =1;
DOEPS   =1;

DO_PROB1 = 1;
DO_PROB2 = 1;
DO_PROB5 = 1;


n=200;
HOR = 50;
HOR2 = 5;
eps=randn(n,1);
imp=zeros(n,1);
imp(3)=1;
om = 0 : .001 : pi;
%%%%%%%%%%%%%%%%%%%%

jmax = 400;
hvec=zeros(2*jmax+1,1);
cutoff=0.1;
for j = 0 : jmax,
    hvec(jmax+1+j)=sum((om>cutoff).*cos(j*om))/(sum(0*om+1));
    hvec(jmax+1-j)=hvec(jmax+1+j);
end;
jplot=5;
hndl=plot(-jplot:jplot,hvec(jmax+1-jplot:jmax+1+jplot),-jplot:jplot,0*(-jplot:jplot),'k-');
title('Band pass filter coefficients');
FNp='hw2_bandpass1';
hndl=plot(1:jmax,hvec(jmax+2:jmax+1+jmax),1:jmax,0*(1:jmax),'k-');
title('Band pass filter coefficients');
FNp='hw2_bandpass2';
jset = [10,20,50,100,400];
for k=1:length(jset),
    htilde=0*om;
    for j=0:jset(k),
        htilde=htilde+hvec(jmax+1+j)*cos(j*om);
        if j > 0,
            htilde=htilde+hvec(jmax+1-j)*cos(j*om);
        end;
    end;
    sub=1:round(length(om)/10);
    hndl=plot(om(sub),abs(htilde(sub)),om(sub),(om(sub)>cutoff),om(sub),0*om(sub),'k-');
    title(['Spectrum: chopped band pass filter',sprintf(',k=%3.0f',jset(k))]);
    FNp=['hw2_bandpass',sprintf('_k_%3.0f',jset(k))];
end;

