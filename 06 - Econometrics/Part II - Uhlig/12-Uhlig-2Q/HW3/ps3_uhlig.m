%%% Chattrin Laksanabunsong %%%
%%% Problem Set 3 Uhlig TA %%%

tic
clear all;
close all;
clc;

cd 'C:\Users\ChattrinL\Documents\ECONOMICS\TA_econ311'

seed = 27;
rand('seed',seed);
randn('seed',seed);

y = randn(1000,20);

for i=1:20
    x = y(50:end,i);
    for j=1:49
        x = [x y(50-j:end-j,i)];
    end
    Y = x(2:end,:);
    X = x(1:end-1,:);
    Atemp = lscov(X,Y)';
    A(:,i) = Atemp(1,:)';
    %%% Part 3 %%%
    [V D] = eig(Atemp);
    roots(:,i) = diag(D);
    %%% Part 4 %%%
    res = Y - X*Atemp';
    sig(i) = std(res(:,1));
    sel = zeros(1,50); sel(1,1) = 1;
    B = sel'; B(1,1) = sig(i);
    for t=1:101
        impulse(t,i) = sel*Atemp^(t-1)*B;
    end
    clear x;
    clear X;
    clear Y;
end
%%% Reporting part 1 %%%
row = {'1'};
for i=2:50
    row = [row num2str(i)];
end
col1 = {'S1','S2','S3','S4','S5','S6','S7','S8','S9','S10'};
col2 = {'S11','S12','S13','S14','S15','S16','S17','S18','S19','S20'};
matrix2latex(A(:,1:10), 'ps3_out1.tex', 'rowLabels', row, 'columnLabels', col1, 'alignment', 'c', 'format', '%-6.4f', 'size', 'tiny');
matrix2latex(A(:,11:end), 'ps3_out2.tex', 'rowLabels', row, 'columnLabels', col2, 'alignment', 'c', 'format', '%-6.4f', 'size', 'tiny');
matrix2latex(roots(:,1:9), 'ps3_out3.tex', 'rowLabels', row, 'columnLabels', col1(1:9), 'alignment', 'c', 'format', '%-6.3f', 'size', 'tiny');
matrix2latex(roots(:,10:18), 'ps3_out4.tex', 'rowLabels', row, 'columnLabels', [col1(end) col2(1:8)], 'alignment', 'c', 'format', '%-6.3f', 'size', 'tiny');
matrix2latex(roots(:,19:20), 'ps3_out5.tex', 'rowLabels', row, 'columnLabels', col2(9:10), 'alignment', 'c', 'format', '%-6.3f', 'size', 'tiny');

%%% plotting part 2 %%%
aroot = abs(roots);
r = real(roots);
c = imag(roots);
for j=1:22
    [r c v] = find((j-1)/20 <= aroot & aroot < j/20);
    h(j) = sum(v);
end
[r c v] = find(1.1 <= aroot);
h(23) = sum(v);
hist(aroot(:),.025:.05:1.125)
title('Frequency that fall into the interval of gap .05');
xlabel('Absolute Value of Roots');
ylabel('Frequency');
print -djpeg ps3_out8

%%% plotting part 3 %%%
figure;
scatter(r(:),c(:))
title('Scatter Plot of Roots on Complex Plane')
xlabel('Real');
ylabel('Imaginary');
print -djpeg ps3_out1

%%% plotting part 4 %%%
for i=1:5
    figure;
    for j=1:4
        subplot(2,2,j)
        plot(0:100,impulse(:,j+4*(i-1)))
        title(['Impulse Response Sample ' num2str(j+4*(i-1))]);
        xlabel('Time');
    end
    eval(['print -djpeg ps3_out' num2str(i+1)]);
end
impulse(:,21) = zeros(101,1);
impulse(1,21) = 1;
figure;
plot(0:100,impulse(:,21),'-x')
title(['True Impulse Response']);
xlabel('Time');
print -djpeg ps3_out7

toc