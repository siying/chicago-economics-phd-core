%%% Chattrin Laksanabunsong %%%

clear all;
close all;
clc;

cd 'C:\Users\ChattrinL\Documents\Economics\TA_Econometrics 2'
%%% Parameters %%%
rho1 = 1.8;
rho2 = [.79 .8 .81 .82]';

%%%%%%%%%%%%%%%%%%
seed = 30;
rand('seed',seed);
randn('seed',seed);


% Part 1
y1 = zeros(4,1003);
time = 0:1:1000';
epsi = randn(1001,1);

for t=1:1001
    y1(:,t+2) = rho1*y1(:,t+1) - rho2.*y1(:,t) + epsi(t);
end
figure;
for i=1:4
    subplot(2,2,i)
    plot(time,y1(i,3:end));
    title(['\rho_1 = 1.8 and \rho_2 = ', num2str(rho2(i))]);
    xlabel('Time');
    ylabel('y_t');
end
print -djpeg ps1_out1

% Part 2
y2 = zeros(4,103);
y2(:,3) = rho1*y2(:,2) - rho2.*y2(:,1) + 1;
for t=1:100
    y2(:,t+3) = rho1*y2(:,t+2) - rho2.*y2(:,t+1);
end
figure;
for i=1:4
    subplot(2,2,i)
    plot(0:1:100,y2(i,3:end));
    title(['\rho_1 = 1.8 and \rho_2 = ', num2str(rho2(i))]);
    xlabel('Time');
    ylabel('y_t');
end
print -djpeg ps1_out2
    