
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         Code for Uhlig Hwk 1 q5 of Fall 2011 Metrics           %
%                       Philip Barrett                           %
%                        11 November                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Compute the two log likelihoods For S=20
S              = 20;
theta          = (0:.001:1.1)';
n              = 1.1/0.001+1;    %The number of increments for theta
sigma_squared  = (ones(n,1)-theta.^(2*S))./(ones(n,1)-theta.^2);
    %The variance of y_0
loglikelihood0 = -.5*log(2*pi)*ones(n,1)-.5*log(sigma_squared);
    %Log likelihood when y_0=0
loglikelihood1 = loglikelihood0-.5./sigma_squared;
    %Log likelihood when y_0=1
loglikelihood2 = loglikelihood0-.5./sigma_squared*1.1^2;
    %Log likelihood when y_0=2
loglikelihood3 = loglikelihood0-.5./sigma_squared*1.25^2;
    %Log likelihood when y_0=3
loglikelihood4 = loglikelihood0-.5./sigma_squared*1.5^2;
    %Log likelihood when y_0=3
loglikelihood5 = loglikelihood0-.5./sigma_squared*2^2;
    %Log likelihood when y_0=3
   
%% Plot the MLE
MLE=y_0; %Just a way to get the dimensionality of the MLE vector ok
i=0;
y_0=0:.1:5;
m=max(size(y_0));
theta_bounded=(0:.001:1)';
n_bounded=max(size(theta_bounded));
sigma_squared_bounded=(ones(n,1)-theta_bounded.^(2*S))./(ones(n,1)-theta_bounded.^2);
for i=1:m
    loglikelihood = loglikelihood0-.5./sigma_squared*y_0(i)^2;
    [L, theta_hat_index]=max(loglikelihood);
    MLE(i)=theta_bounded(theta_hat_index);
end

%% Plot log likelihoods
figure(1);
plot(theta, [loglikelihood0 loglikelihood1 loglikelihood2 loglikelihood3 loglikelihood4 loglikelihood5]);
xlabel('\theta');
title('Log likelihood of the AR(1) for S=20', 'FontWeight', 'Bold');
legend('y_0=0', 'y_0=1', 'y_0=1.1', 'y_0=1.25', 'y_0=1.5', 'y_0=2');
legend boxoff;

%% Plot MLE
figure(2);
plot(y_0, MLE);
xlabel('y_0');
ylabel('\theta_n');
title('The MLE estimator \theta_n', 'FontWeight', 'Bold');
legend boxoff;

