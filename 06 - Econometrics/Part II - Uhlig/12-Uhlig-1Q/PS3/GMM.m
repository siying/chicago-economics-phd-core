%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%    Code to compute GMM for question 3 of Uhlig PS3    %%%
%%%            Philip Barrett 26 November 2011            %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all;

%% Controls
eta=1; beta=.95; alpha=0; rho=.999999999; sigma=.01;
T=1000; n=21; m=160;
eta_guess=eta+.1; beta_guess=beta;

%% Generate an impulse response
eps = zeros(1,n);
eps(2)=sigma;
C   = ones(1,n);
R=C; Rbar=C; logC=C;                         %Just setting up empty arrays
const=1/beta*exp(eta*(1-rho)-.5*(alpha-eta)^2*sigma^2);   %Useful constant
C_shk=C; R_shk=C; Rbar_shk=C; logC_shk=C;
Rbar(1)=exp(logC(1)*eta*(rho-1))*const;
Rbar_shk(1)=exp(logC_shk(1)*eta*(rho-1))*const;
for t=2:n
    logC(t)= 1-rho+rho*logC(t-1);
    Rbar(t)= exp(logC(t)*eta*(rho-1))*const;
    logC_shk(t)= 1-rho+rho*logC_shk(t-1)+eps(t);
    Rbar_shk(t)= exp(logC_shk(t)*eta*(rho-1))*const;
end
C=exp(logC); C_shk=exp(logC_shk);
R(2:n)=Rbar(1:n-1); R_shk(2:n)=Rbar_shk(1:n-1).*exp(alpha*eps(2:n));

%% Plot the IRF(s)
figure;
plot(0:n-1, [100*(C_shk./C-1); 100*(R_shk-R)]);
xlabel('time');
title('IRF for Consumption and the Real Rate', 'FontWeight', 'Bold');
legend('Consumption (% deviations from ss)', 'Real Rate (pp deviations from ss)')
legend boxoff;

%figure;
%plot(0:n-1, [C; C_shk]);
%xlabel('time');
%ylabel('Level of consumption');
%title('IRF for Consumption', 'FontWeight', 'Bold');
%title(['\fontsize{14} Main Title ', ...
%       '\newline \fontsize{10} \color{red} \it My Subtitle']);
%subtitle('Shock of size \sigma')
%legend('No shock', 'Shock of size \sigma')
%legend boxoff;


%% Asympototic variances
%Matrices for the true asympototic variance
G=exp(1+.5*(sigma^2)/(1-rho^2))*[rho*sigma^2/(1+rho)-(alpha-eta)*sigma^2 1/beta; rho^2*sigma^2/(1+rho) 1/beta];
LRvar=(exp((alpha-eta)^2*sigma^2)-1)*exp(2)*[exp(2*sigma^2/(1-rho^2)) exp(sigma^2/(1-rho)) ; ...
    exp(sigma^2/(1-rho)) exp(2*sigma^2/(1-rho^2))];
%Two asympototic variances
AVar1=(G'*G)^(-1)*(G'*LRvar*G)*(G'*G)^(-1);
AVar2=(G'*LRvar^(-1)*G)^(-1);

%% Simulation
eps = randn(1,T)*sigma;
C   = ones(1,T);
R=C; Rbar=C; logC=C;                         %Just setting up empty arrays
const=1/beta*exp(eta*(1-rho)-.5*(alpha-eta)^2*sigma^2);   %Useful constant
for t=2:T
    logC(t)= 1-rho+rho*logC(t-1)+eps(t);
    Rbar(t)= exp(logC(t)*eta*(rho-1))*const;
end
C=exp(logC);
R(2:T)=Rbar(1:T-1).*exp(alpha*eps(2:T));

%% Plot the simulation
figure;
plot(1:m-10, logC(11:m));
xlabel('time');
title('Simulation for log Consumption', 'FontWeight', 'Bold');

figure;
plot(1:m-10, C(11:m));
xlabel('time');
title('Simulation for Consumption', 'FontWeight', 'Bold');

figure;
plot(1:m-10, [Rbar(11:m); R(11:m)]);
xlabel('time');
title('Simulation for Real Rates', 'FontWeight', 'Bold');
legend('R bar', 'R');

%% Estimation
global W C_0 C_lead_1 R_lead_1 C_lag_1 C_lag_2 S;
S=T-3;  %Data is in periods -1, 0, 1, ..., S, S+1
W=[1 0; 0 1];
C_0=C(3:S+2); C_lead_1=C(4:S+3); R_lead_1=R(4:S+3); C_lag_1=C(2:S+1); C_lag_2=C(1:S);
fun=@FOC;
options = optimset('Display','off','TolFun',1e-1000);
thetahat1=fsolve(fun, [eta_guess, beta_guess]);
W=LRvar^(-1);
thetahat2=fsolve(fun, [eta_guess, beta_guess]);
etahat=thetahat1(1); betahat=thetahat1(2);

%% Estimated equivalents
%M=990;
M=10;          %Minimum number of observations for LR variance estimation
%Estimated equivalents
G_nhat=[betahat/S*((C_0./C_lead_1).^etahat.*log(C_0./C_lead_1).*R_lead_1)*C_lag_1', ...
         1/S*((C_0./C_lead_1).^etahat.*R_lead_1)*C_lag_1'; ...
        betahat/S*((C_0./C_lead_1).^etahat.*log(C_0./C_lead_1).*R_lead_1)*C_lag_2', ...
         1/S*((C_0./C_lead_1).^etahat.*R_lead_1)*C_lag_2'];
g_nhat=[1/S*(betahat*((C_0./C_lead_1)).^etahat.*R_lead_1-ones(1,S))*C_lag_1'; ...
        1/S*(betahat*((C_0./C_lead_1)).^etahat.*R_lead_1-ones(1,S))*C_lag_2'];
ghat=[(betahat*((C_0./C_lead_1)).^etahat.*R_lead_1-ones(1,S)).*C_lag_1; ...
        (betahat*((C_0./C_lead_1)).^etahat.*R_lead_1-ones(1,S)).*C_lag_2];
gamma_hat=zeros(2,2,2*(S-1)+1);
k=-S:S;  %Just indexing the lags
for j=M:S
    gamma_hat(:,:,j)=1/S*ghat(:,1:j)*ghat(:,S+1-j:S)';
    gamma_hat(:,:,2*S-j)=1/S*ghat(:,1:j)*ghat(:,S+1-j:S)';
end
summation= sum(gamma_hat,3);
LRvar_hat=summation(:,:,1);
AVar1_hat=(G_nhat'*G_nhat)^(-1)*(G_nhat'*LRvar_hat*G_nhat)*(G_nhat'*G_nhat)^(-1);
AVar2_hat=(G_nhat'*LRvar^(-1)*G_nhat)^(-1);


%% Monte-Carlo simulation of estimates
%K=1000; %Number of estimators to compute
K=100;    %Useful line to stop the code going on forever
K=0;
many_theta_hats=zeros(K,2);
W=[1 0; 0 1];
eta_guess_MC=eta*ones(K,1)-.5+rand(K,1);
for i=1:K
    eps = randn(1,T)*sigma;
    C   = ones(1,T);
    R=C; Rbar=C; logC=C;                         %Just setting up empty arrays
    const=1/beta*exp(eta*(1-rho)-.5*(alpha-eta)^2*sigma^2);   %Useful constant
    for t=2:T
        logC(t)= 1-rho+rho*logC(t-1)+eps(t);
        Rbar(t)= exp(logC(t)*eta*(rho-1))*const;
    end
    C=exp(logC);
    R(2:T)=Rbar(1:T-1).*exp(alpha*eps(2:T));
    disp(eta_guess_MC(i))
    many_theta_hats(i,:)=fsolve(fun, [eta_guess_MC(i), beta_guess]);
end
many_theta_hats_dev=(many_theta_hats-[eta*ones(K,1) beta*ones(K,1)]);
AVar1_MC=1/K*many_theta_hats_dev'*many_theta_hats_dev;