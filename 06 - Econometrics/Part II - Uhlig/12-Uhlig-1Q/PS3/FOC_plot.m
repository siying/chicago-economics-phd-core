close all

W=eye(2);

beta=.95; eta=.8:.005:1.2;
n=max(size(beta)); m=max(size(eta));
FOC_surface=zeros(m,2);
for i=1:m
    FOC_surface(i,:)=FOC([eta(i) beta]);
end
figure;
plot(eta, FOC_surface(:,1), 'b-', eta, zeros(m,1), 'k-');
%axis([.8 1.2 -1E-5 6E-5]);
ylabel('Derivative of the objective function');
xlabel('\eta');
title('The derivative of the GMM objective function', 'FontWeight', 'Bold');
legend boxoff;

beta=[.94999 .95 .95001]; eta=.8:.005:1.2;
n=max(size(beta)); m=max(size(eta));
FOC_surface=zeros(2,m,n);
for j=1:n
for i=1:m
    FOC_surface(:,i,j)=FOC([eta(i) beta(j)]);
end
end
figure;
plot(eta, FOC_surface(1,:,1), eta, FOC_surface(1,:,2), eta, FOC_surface(1,:,3), eta, zeros(m,1), 'k-');
axis([.8 1.2 -1.5E-4 2E-4]);
ylabel('Derivative of the objective function');
xlabel('\eta');
title('The derivative of the GMM objective function', 'FontWeight', 'Bold');
legend('\beta=.949999', '\beta=.95', '\beta=.950001', 'Location', 'Northwest');
legend boxoff;