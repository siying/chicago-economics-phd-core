***CODE TO COMPLETE EC310 EMPIRICAL EXERCISES***
clear all
cd "C:\Users\philip\Documents\PhD\Second Year\Teaching\Empirical Problems"
use ipums_resample.dta, replace



***1. COMPUTE HOURLY LOG WAGES***
keep if age >=20
keep if age <=45
gen real_wage = incwage*cpi99/(wkswork1*uhrswork)
gen real_log_wage = log(real_wage)
drop if empstat==0 | empstat==13
*Now compute skill classifications
gen skillclass = 0 if educ <=73  		// 12th grade and below
replace skillclass = 1 if educ >=80 	// Everyone else
*Employment and marrital status
gen inwork= empstat==10 | empstat==12
gen ILF =  empstat==10 | empstat==12 |empstat==20 | empstat==21 | empstat==22
gen married= marst==1
sort year age

***2. COMPUTE THE UNEMPLOYMENT RATES***
*In 2010
count if inwork==1 & year==2010
local num_atwork=r(N)
disp "`num_atwork'"
count if ILF==1 & year==2010
local num_ILF=r(N)
disp "`num_ILF'"
local unemployment_rate=1-`num_atwork'/`num_ILF'
local unemployment_rate_text="Unemployment rate 2010:" + "`unemployment_rate'"
disp "`unemployment_rate_text'"

*In 1990
count if inwork==1 & year==1990
local num_atwork=r(N)
disp "`num_atwork'"
count if ILF==1 & year==1990
local num_ILF=r(N)
disp "`num_ILF'"
local unemployment_rate=1-`num_atwork'/`num_ILF'
local unemployment_rate_text="Unemployment rate 1990:" + "`unemployment_rate'"
disp "`unemployment_rate_text'"

*In 1970
count if inwork==1 & year==1970
local num_atwork=r(N)
disp "`num_atwork'"
count if ILF==1 & year==1970
local num_ILF=r(N)
disp "`num_ILF'"
local unemployment_rate=1-`num_atwork'/`num_ILF'
local unemployment_rate_text="Unemployment rate 1970:" + "`unemployment_rate'"
disp "`unemployment_rate_text'"

*Generate year/age unemployment rates
by year age: egen inwork_total=total(inwork)
by year age: egen ILF_total=total(ILF)
gen unemp_rate = 100*(1-inwork_total/ILF_total)
*Plot this
label variable unemp_rate "Unemployment rate"
twoway (line unemp_rate age if year==1970, clcolor(black) clpattern(shortdash)) (line unemp_rate age if year==1990, clcolor(black) clpattern(dash)) ///
(line unemp_rate age if year==2010, clcolor(black)), legend(label(1 "1970") label(2 "1990") label(3 "2010")) name(unemployment,replace)




***3. COMPARISON OF SAMPLE MEANS***
*T-tests in 2010 and 1990
ttest real_log_wage if year==2010 & inwork==1, by(skillclass)
ttest real_log_wage if year==1990 & inwork==1, by(skillclass)

*F-test in 2010
reg real_log_wage skillclass if year==2010  & inwork==1
test skillclass==0

gen is34or35 = .
replace is34or35 = 1 if age==35
replace is34or35 = 0 if age==34

*The same for 34 vs. 35 year olds
ttest real_log_wage if year==2010 & inwork==1, by(is34or35)
reg real_log_wage is34or35 if year==2010  & inwork==1
test is34or35==0




***4. RUN A LOGIT ON EMPLOYMENT***
**Using stata
gen male=sex==1
//Including wages
logit inwork skillclass age male real_log_wage married if ILF==1 & year==2010
test skillclass=married
logit inwork skillclass age male real_log_wage married if ILF==1 & year==1990
//Excluding wages
logit inwork skillclass age male married if ILF==1 & year==2010
logit inwork skillclass age male married if ILF==1 & year==1990
logit inwork skillclass age male married if ILF==1 & year==1970

**By hand
do logit_likelihood
ml model lf logit_likelihood (inwork=skillclass age male real_log_wage) if  ILF==1 & year==2010
ml maximize




***5. REGRESSING WAGES ON EDUCATION***
**Via OLS
reg real_log_wage skillclass age male
reg real_log_wage skillclass age male if inwork==1 & year==2010
test male=.25
**Include selection
heckman real_log_wage skillclass age male, select(skillclass age male marst) twostep
