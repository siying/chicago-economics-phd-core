% Econ 310 - Fall 2010
% HW 1, Q3
% Code by Thorsten Drautzburg
% this Version: 11/12/2010

% housekeeping
clear all; close all;
try 
    mkdir('Graphs')
end;

% parameter input
s2=1;

% define function
ll=@(sigma) -0.5*log(2*pi)-0.5*log(sigma.^2)-0.5*s2./sigma.^2;

% plot
my_range=[-2*s2:0.01:2*s2];
hndl=plot(my_range, ll(my_range), '-b', 'LineWidth', 3);
hold on;
xlabel('\sigma', 'FontSize', 16)
ylabel('log L(\sigma|\alpha_{MLE})', 'FontSize', 16)
axis([xlim ll(0.25) max(ylim)]);
hndl=plot([1 1], ylim, ':k', 'LineWidth', 2);
hndl=plot(-[1 1], ylim, ':k', 'LineWidth', 2);

print('-depsc', 'Graphs/HW1_Q3.eps');
