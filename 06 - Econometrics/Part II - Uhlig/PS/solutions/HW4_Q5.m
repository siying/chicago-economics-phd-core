% Econ 310 - Fall 2010 - Prof. Harald Uhlig
% HW 4 Q5 
% Code by Thorsten Drautzburg

% This version: 12/03/2010

% housekeeping
clear all; close all; clc; 

% loading the data
load Ybar.mat
A=[1 0; 1 1];

% Looking at the data
hndl=plot(1:length(Ybar), Ybar);
try; enlarge; end;
display(['Means of variables: ' num2str(mean(Ybar), '%1.2f  ')]);
display(['Correlation of variables: ' num2str(corr(Ybar(:,1),Ybar(:,2)), '%1.2f  ')]);

% Construct X matrix, taking missing Y_0, Y_{-1} into account
X=[ Ybar(2:end-1,:), Ybar(1:end-2,:),ones(size(Ybar,1)-2,1)];
Y=Ybar(3:end,:);

% MLE
% old fashioned: B_hat=(X'*X)^(-1)*X'*Y
B_hat=(X'*X)\X'*Y; %a bit faster in theory, although not for T=98, m=2...
Sigma_hat=((Y-X*B_hat)'*(Y-X*B_hat))/size(Y,1);
Omega_NW=kron(Sigma_hat, (X'*X)^(-1));

% Display NW posterior
display(['\nu_T &= ' num2str(size(Y,1),'%1.0f') '\\']);
display([char({'\mathbf{N}_T &= \begin{bmatrix}';repmat(' ', size(B_hat,1)-1,1)}) num2str(X'*X, '%1.2f &') char({repmat('\\', size(B_hat,1)-1,1), '\end{bmatrix}\\'})]);
display([char({'\bar{B}_T &= \begin{bmatrix}';repmat(' ', size(B_hat,1)-1,1)}) num2str(B_hat, '%1.2f &') char({repmat('\\', size(B_hat,1)-1,1), '\end{bmatrix}\\'})]);
display([char({'S_T &= \begin{bmatrix}';repmat(' ', size(Y,2)-1,1)}) num2str(Sigma_hat, '%1.2f &') char({repmat('\\', size(Y,2)-1,1), '\end{bmatrix}\\'})]);
% Posterior variance of coefficients at mean precision
display([char({'\Omega_{NW} &= \begin{bmatrix}';repmat(' ', size(Omega_NW,1)-1,1)}) num2str(Omega_NW, '%1.2f &') char({repmat('\\', size(Omega_NW,1)-1,1), '\end{bmatrix}\\'})]);

% Kalman-Filter with zero constants

constant_prior_variance=0;

Omega=diag([ 1, 0, 0.25, 0, constant_prior_variance, 0, 1, 0, 0.25, constant_prior_variance]);
Sigma=A*A';
Xi=[[1; 0; 1; 0; 0]; [0; 1; 0; 1; 0]];
for ii=3:size(Y,1)
    H=blkdiag(X(ii,:),X(ii,:));
    G=Omega*H'*(H*Omega*H'+Sigma)^(-1);
    Xi=Xi+G*(Y(ii,:)'-H*Xi);
    Omega=Omega-G*(H*Omega*H'+Sigma)*G';
end;

% convert Xi to B
B_Kalman_zero=reshape(Xi, size(B_hat));
display([char({'\bar{B}_{K,0} &= \begin{bmatrix}';repmat(' ', size(B_Kalman_zero,1)-1,1)}) num2str(B_Kalman_zero, '%1.2f &') char({repmat('\\', size(B_hat,1)-1,1), '\end{bmatrix}\\'})]);
display([char({'\Omega_{K,0} &= \begin{bmatrix}';repmat(' ', size(Omega_NW,1)-1,1)}) num2str(Omega, '%1.2f &') char({repmat('\\', size(Omega_NW,1)-1,1), '\end{bmatrix}\\'})]);


constant_prior_variance=1e50;
Omega_nonzero=diag([ 1, 0, 0.25, 0, constant_prior_variance, 0, 1, 0, 0.25, constant_prior_variance]);
Xi_nonzero=[[1; 0; 1; 0; 0]; [0; 1; 0; 1; 0]];
for ii=3:size(Y,1)
    H=blkdiag(X(ii,:),X(ii,:));
    G=Omega_nonzero*H'*(H*Omega_nonzero*H'+Sigma)^(-1);
    Xi_nonzero=Xi_nonzero+G*(Y(ii,:)'-H*Xi);
    Omega_nonzero=Omega_nonzero-G*(H*Omega_nonzero*H'+Sigma)*G';
end;

% convert Xi to B
B_Kalman_nonzero=reshape(Xi, size(B_hat));
display([char({'\bar{B}_{K,\lnot 0} &= \begin{bmatrix}';repmat(' ', size(B_Kalman_zero,1)-1,1)}) num2str(B_Kalman_nonzero, '%1.2f &') char({repmat('\\', size(B_hat,1)-1,1), '\end{bmatrix}\\'})]);
display([char({'\Omega_{K, \lnot 0} &= \begin{bmatrix}';repmat(' ', size(Omega_NW,1)-1,1)}) num2str(Omega_nonzero, '%1.2f &') char({repmat('\\', size(Omega_NW,1)-1,1), '\end{bmatrix}\\'})]);

display([char({'\xi &= \begin{bmatrix}';repmat(' ', size(Omega_NW,1),1)}) char([{'NW & K, 0, & K, \lnot 0'} ; cellstr(num2str([reshape(B_hat, size(Xi,1),1), Xi, Xi_nonzero], '%1.2f &'))]) char({repmat('\\', size(Omega_NW,1),1), '\end{bmatrix}\\'})]);
