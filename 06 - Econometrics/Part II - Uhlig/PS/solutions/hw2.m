clear;
load regression.mat;
iota = ones(length(Y),1);
%Correlation between the regressors
Xcorr = corrcov(cov([X1 X2 X3]));
 

%(a)
R = [1 1 0 0];
c = [1];
[ba, t_statsa, WALDa, LMa, LRa, b_stdsa] = MLE (Y, [iota X1 X2 X3], R, c);
% Calculate critical value at 5%
alpha_a = chi2inv(0.05,1);

%(b)
R = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1];
c = [0;0;0;0];
[bb, t_statsb, WALDb, LMb, LRb, b_stdsb] = MLE (Y, [iota X1 X2 X3], R, c);
% Calculate critical value at 5%
alpha_b = chi2inv(0.05,4);

%(c)
R = [0 0 1 0; 0 0 0 1];
c = [0;0];
[bc, t_statsc, WALDc, LMc, LRc, b_stdsc] = MLE (Y, [iota X1 X2 X3], R, c);
% Calculate critical value at 5%
alpha_c = chi2inv(0.05,2);

%(d)
R = [0 1 0 0; 0 0 0 1];
c = [0;0];
[bd, t_statsd, WALDd, LMd, LRd, b_stdsd] = MLE (Y, [iota X1 X2 X3], R, c);
% Calculate critical value at 5%
alpha_d = chi2inv(0.05,2);