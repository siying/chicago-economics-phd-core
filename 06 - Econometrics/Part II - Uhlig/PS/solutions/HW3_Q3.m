% Econ 310 - Fall 2010 - Prof. Uhlig
% HW 3 Q3
% Code by Thorsten Drautzburg

% This version: 11/24/10

% housekeeping

clear all; close all; clc;

% input
KK=[1 10 100];
N=10;
MLE=5;
THETA=logspace(-8, log10(4), 1000);

% define functions to be plotted
post=@(k, n , mle, theta) repmat(((n.*mle+1).^((k+n)/2)./gamma((k+n)/2)),size(theta',1),1).* ...
    exp(repmat((k+n-2), size(theta',1),1).*log(repmat(theta', 1, size(k,2)))/2-0.5*(1+n.*mle).*...
    repmat(theta', 1, size(k,2))).*(repmat(theta', 1, size(k,2))>=0);
llhood=@(n, mle, theta) exp(-0.5*repmat(n.*mle, size(theta',1),1).*repmat(theta', 1, size(mle,2))...
    +0.5*repmat(n, size(theta',1),1).*log(2*pi*repmat(theta', 1, size(mle,2))));
post_j=@(n, mle, theta) exp(-0.5*repmat(n.*mle, size(theta',1),1).*repmat(theta', 1, size(mle,2))...
    +0.5*repmat(n-2, size(theta',1),1).*log(2*pi*repmat(theta', 1, size(mle,2))));


for ii=1:length(KK)
    myfig=figure(ii);
    kk=KK(ii);
    hndl=plot(THETA, log([post(kk, 0, 0, THETA), llhood(N, MLE, THETA),post(kk, N, MLE, THETA)]));
    legend({'Prior', 'Likelihood', 'Posterior'}, 'location', 'best')
    title(['Log-densities, k=' num2str(kk, '%1.0f')]);
    xlabel('\theta');
    axis([0 floor(max(THETA)) ylim]);
    try; enlarge; end;
    hold all;
    plot([1,1]*MLE^(-1), ylim, '-k');
    try; print('-depsc', ['Graphs/HW3_Q3b_' num2str(kk, '%1.0f') '.eps']); end;
end;

% figure(2)
% hndl=plot(THETA, (llhood(N, MLE, THETA)));
% legend([repmat('k =', length(KK), 1) num2str(KK', '%1.0f')], 'location', 'best')
% title('Prior')
% xlabel('\theta');
% try; enlarge; end;

    myfig=figure(length(KK)+1);
    hndl=plot(THETA, log([post_j(N, MLE, THETA), llhood(N, MLE, THETA), post([1 10], N, MLE, THETA)]));
    legend({'Jeffrey''s', 'Flat', 'Conjugate, k=1', 'Conjugate, k=10'}, 'location', 'northeast')
    title(['Posterior Log-densities']);
    xlabel('\theta');
    axis([0 ceil(1/MLE) -10 max(ylim)]);
    try; enlarge; end;
    hold all;
    plot([1,1]*MLE^(-1), ylim, '-k');
    try; print('-depsc', ['Graphs/HW3_Q3d.eps']); end;
