/* Econ 310 - Prof. Uhlig - Fall 2010 */
/* Selection Excercise */
/* Code by Thorsten Drautzburg */
/* This Version: 11/17/2010 */

drop _all
set more off
set memory 30m

use Econ310_Selection.dat


by empstat sex, sort: sum incwage 

/*generate dummies for educational achievement*/
replace educ=6 if educ<=6
tab educ if educ>=6, ge(edu_dum)
tab sex, ge(sex_dum)
tab disabwrk, ge(disab_dum)

ge age2=age^2

ge child5=1 if nchlt5>=1
replace child5=0 if child5==.

ge female_child5=1 if child5 & sex==2
replace female_child5=0 if female_child5==.

ge poswage=(incwage>0)

ge lnwage=log(incwage)


/* Descriptive statistics */
/* General */
summarize incwage lnwage poswage

summarize incwage lnwage poswage if sex==1
summarize incwage lnwage poswage if sex==2
summarize incwage lnwage poswage if sex==2 & child5


/* Probit */
probit poswage age age2 sex_dum2 edu_dum* female_child5 child5
predict xb1, xb
ge mills=normalden(-xb1)/normal(xb1)
/* 2nd stage OLS */
regress lnwage age age2 edu_dum* sex_dum2 mills if poswage, robust
estimates store TwoStage_manual

/* Stata's built-in 2 stage OLS */
heckman lnwage age age2 edu_dum* sex_dum2, select(poswage = age age2 sex_dum2 edu_dum* female_child5 child5) two
estimates store TwoStage_heckman

/* Normal inconsistent OLS */
regress lnwage age age2 edu_dum* sex_dum2 if poswage, robust
estimates store OLS

/* Two ways to do inference with MLE*/
heckman lnwage age age2 edu_dum* sex_dum2, select(poswage = age age2 sex_dum2 edu_dum* female_child5 child5) vce(oim)
estimates store mle_oim
heckman lnwage age age2 edu_dum* sex_dum2, select(poswage = age age2 sex_dum2 edu_dum* female_child5 child5) vce(opg)
estimates store mle_opg
estimates restore mle_opg

/* Computing the one-sided test statistics */
test [lnwage]age+2*[lnwage]age2*25=0
di "p-value at age 25=" 1-normal(sqrt(r(chi2))*sign([lnwage]age+2*25*[lnwage]age2))
test [lnwage]age+2*55*[lnwage]age2=0
di "p-value at age 55=" 1-normal(sqrt(r(chi2))*sign([lnwage]age+2*55*[lnwage]age2))

di "95% CI - Lower bound" ([lnwage]age+2*55*[lnwage]age2)-invnormal(0.05)*([lnwage]age+2*55*[lnwage]age2)/sqrt(r(chi2))
