% Gives the estimates for the linear regression model with normal
% unobservables.
% Input values:
% y: dependent variable (vector nx1)
% x: matrix with the regressors (nxk)
% Returned values
% b: vector with the unrestricted estimates(kx1)
% t_stats: vector with the t_stats for the null hypothesis beta=0 (kx1)
% WALD: WALD statistic for the restriction Rb - c = 0
% LM: WALD statistic for the restriction Rb - c = 0
% LR: WALD statistic for the restriction Rb - c = 0
% b_stds: estimate of the  standard deviations of the OLS estimators of the
%         coefficients(kx1)
function [b, t_stats, WALD, LM, LR, b_stds] = MLE (y, x, R, c)

%OLS estimation
b = (inv(x'*x)*x'*y);
n = (length(y) - length(b));
resids = y-x*b;
b_stds = [];
t_stats = [];
temp = inv(x'*x);
var_res = (resids'*resids)/n;

%t-statistcs and b_stds
for i=1:length(b')
 b_std = (temp(i,i)*var_res)^0.5;
 b_stds = [b_stds; b_std];
 t_stats = [t_stats; b(i)/b_std];
end

%WALD 
A = R*temp*R';
A = var_res*A;
WALD = ((R*b-c)')*inv(A)*(R*b-c);

%Estimation of the restricted OLS
b_r = b - temp*(R')*inv(R*temp*R')*(R*b-c);
resids_r = y-x*b_r;
var_res_r = (resids_r'*resids_r)/n;

%LR
LR = -0.5*n*(log(var_res)-log(var_res_r)) + (1/var_res)*resids'*resids - (1/var_res_r)*resids_r'*resids_r;
LR = 2*LR;

%LM
LM = var_res*(1/var_res_r^2)*(resids_r')*x*temp*(x')*resids_r;
