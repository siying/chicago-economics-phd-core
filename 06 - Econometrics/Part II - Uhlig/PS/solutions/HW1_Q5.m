% Econ 310 - Fall 2010
% HW 1, Q5
% Code by Thorsten Drautzburg
% this Version: 11/12/2010

% housekeeping
clear all; close all;
try 
    mkdir('Graphs')
end;

% parameter input
n=10000;
sigma2=1;
beta=[1;1;1];
% could specify X here

% consequently set k
k=length(beta);

% if X is not specified, make it up
randn('seed',820506)
if ~exist('X')
    X=randn(n, k);
end;
if ~exist('y')
    epsilon=randn(n, 1)*sqrt(sigma2);
    y=X*beta+epsilon;
end;

% Compute MLE
beta_hat=(X'*X)^(-1)*X'*y;
Q=eye(n)-X*(X'*X)^(-1)*X';
s2_hat=y'*Q*y/n;

% Compute inverse of information matrix
I_true=[(X'*X/n)^(-1)*sigma2, zeros(k,1); zeros(1, k), 2*sigma2^2];
I_estimated=[(X'*X/n)^(-1)*s2_hat, zeros(k,1); zeros(1, k), 2*s2_hat^2];

% Compute the standard errors
display('The standard errors based on the estimated / true information matrix are:');
display(num2str([sqrt(diag(I_estimated/n)), sqrt(diag(I_true/n))], '%1.4f // '));

% Output the estimate
display('Estimated / true slope coefficients:');
display(num2str([beta_hat, beta], '%1.4f // '));
display('Estimated / true sigma^2:');
display(num2str([s2_hat sigma2], '%1.4f // '));
